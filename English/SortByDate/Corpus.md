> Yesterday night 	❌
>
> **Last night** 			✔️

> Will you **remind** me what it was?
>

> What did he **tell** me?
>
> What did he **say to** me?

> I'm exhausted
>
> <u>I'm drained</u>
>
> I'm worn out
>
> I'm beat
>
> I'm ready to crash

> The internet cut out. It's not working. Our connection is slow. 
>
> My business is on the internet. I teach English on the internet.

> Don't **scroll on** social media too long.	
>
> scroll v.滚屏/翻动

> I'm doing the housework.
>
> I'm doing the laundry.
>
> I'm doing the dishes.
>
> I mop with my mop.
>
> I vacuum with a vacuum.
>
> I sweep with a broom.
>
> My house is ==as clean as whistle.==



I'm  **clearing off** the table. Then I will **wipe down** the table. Today is trash day. I have to take out my trash and my recycling. I **take out** my trash to the dumpster. In the recycling bin, I put plastic bottles. 

> **clear off      |**
>
> **wipe down |**
>
> **take out      |**



==There are too many cooks in the kitchen.==



Can you make an omelet? You need to crack the eggs. Then beat the eggs. Then you need to melt the butter. Add the eggs to the pan.



cut, slice, dice, chop



It's laundry day.

Laundry detergent.

Laundry basket.

washer, dryer.

Throw clothes in the dryer, or you can hang dry your clothes. Last, I fold the clothes. 



the power button



I pour some water.

The water sloshes in the kettle.

I spilled the water.

bottled water

tap water

sparkling water

flavored water

beer

wine

champagne 

martini

mojito

rum 

vodka 

I swig a lot of water.

Do you ever chug your drink?



I stroll in the snow.

I sprint

I hold my baby.

I pick up my baby.

I hug my baby.

I look at the bill. I squint at the small words. I stare at the shocking numbers.

I snip the paper.

I rip the paper.

I shred the paper.



My smartphone has a case

a screen protector

headphone

My battery is low.

I don't want my phone to die.

send me a text

give me a call on the phone



flash drive

nail-clipper

Q-tips

wash cloth 

hair ties

hair clip

curling iron

hair dryer

I blow dry my hair



shampoo 

conditioner

sink 

drain

faucet

counter

toilet handler flusher

toilet paper holder

Where is the restroom



I'm not a fan of beer.



see you later.



sorry?



tongue twister



Will you marry me?



Don't beat around the bush.== be direct.

A good rule of thumb. ==  good way

hold your horses == go step by step



paw 

<img src="/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-09-06 下午10.19.00.png" alt="截屏2022-09-06 下午10.19.00" style="zoom:50%;" />

claw

<img src="/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-09-06 下午10.19.20.png" alt="截屏2022-09-06 下午10.19.20" style="zoom:50%;" />

hoof-hooves

<img src="/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-09-06 下午10.19.39.png" alt="截屏2022-09-06 下午10.19.39" style="zoom:50%;" />



woof 狗叫



measuring tape	卷尺



go up the ladder to reach the roof



drive-through

the main course 

seansoning



I had an ==ex==(egg)hausted experience.



It's an **overgeneralization**.

> a written or spoken statement in which you say or write that something is true all of the time when it is only true some of the time, or the use of such statements:

- It is an overgeneralization to say that he never helps in the house.



**-ED发音**

- ​	**-id	（ed发id音）**

```
-ted:	wanted connected integrated
-ded:	ended decided
```

- ​	**t	（ed发t音）**

```
f:		laughed
x:		faxed
sh:	  washed
tch: 	watched
k:		liked
p:		helped
```

- ​	**d	（ed发d音）**

```
lived
experienced
```

**example**

```
He wanted to live downtown because when I lived downtown, I biked to work everyday.

wanted--id sound
lived--d	sound
biked--t	sound
```



have to(= hafta)

gotta(= have got to)



**with**

> **used to say that people or things are in a place together or are doing something together**

```
I was with my mom at the time(at a particular moment or period in the past when something happened)

He's impossible to work with.

Mix the butter with the sugar and then add the egg.

I'll be with you in a second.
(I will give you my attention in a second)

She's staying with her parents(at their house) for a few months.

He's been with the department(working in it) since 2010.
(be with表陪伴)
```

> **using something**

```
she wiped her lipstick off with a tissue.
Join the two pieces together with glue.
They set up a business with the help of a bank one.
```

> **having or including something**

```
 a tall woman with dark hair.
 He's married with three children.
 He spoke with a soft Irish accent
 He woke up with a terrible headache
```

> (prep) **relate to or in the case of a person or thing**

```
How are things with you.(你过得怎么样?)
He's very careless with his money.
```



**filler expression**:

```
uh	um	like	you know 	sort of/kind of  	well  so  or something  
```



**I got 8 hours of sleep.** 我睡了8小时

It's easy ==**to get distracted**== by social media.

I'm ==**getting** **hungry**.==

I ==**got** **bored**.==

That has **==pros and cons.==**

He ==**got** **home**== at 5 pm every day.

Where did you ==**get home**== from?

**When are you going to get home?**

I ==**got back from**==vacation yesterday. But I'm still tired.

I can eat ==**quite a few**== sushi rolls.

I ==**got back from**== her house last night.

I ==**got back from**== work.

I ==**don't get**== why this is popular

We need to **==get going==!**

When the teenager told her mom, "I hate you, but her mom didn't **==take it seriously==.**"

I wish I had **==taken==** school **==seriously==**.

Let's **get together.**

My dog ripped my school books and ==**on top of**== that, he ate my homework.

This vocabulary lesson is great. **==On top of==** that, it's free.

I invited 20 people to my party, but **==little to no==** people responded.

Last week he had ==**little to no**== time to cook. He was so busy.

Last year was such a ==**roller coaster**==! I got married, and then I ==**got fired from my job**==, and then I moved to New York.

**The relationship was an emotional roller coaster for 6 months.**

The restaurant was so fancy. **==I'm talking==**, **suit and tie**, a local weekly menu.

I haven't studied for my test **==at all==**!

He wants to be a doctor because he wants **==a six figures==** salary.

Last month I ==**went through**== a lot! My grandma was in the hospital, my car broke down, and I had a terrible cold.

You're **==going through==** a lot right now. Please take care of yourself.



I couldn't ==**tell**== if he was a boy or a girl.

It's hard to ==**tell**==.

I couldn't ==**tell**== by looking at his face.



I **==bet==** that you were sad when your dog died.

I ==**bet**== that you were annoyed when your boss didn't give you a raise.

I ==**bet**== it's going to rain.

I **==bet==** I'm going to break my leg when I go skiing.



Calling customer center

Let me help you **==troubleshoot==**.

Let's **==troubleshoot==** a few things.

Let's call and see how long I have **==to be on hold==**.  //to wait on the phone

I'm sorry, can I ==**put you on hold**== for just a minute. 

Thank you for calling…

Your call may be monitored or recorded for training purposes.

If you know your ==**party's**== extension, please dial it now. //the people you're trying to reach

How can I help you?

I'm ==**calling** **about**== a device that I bought back in November. I bought it as a Christmas present. 

I'm **==calling about==** a problem with the phone.

I already scanned a couple pictures and tried ==**to** **test** **out**== a couple things.	//to try something

==**Just to confirm**==, is your computer plugged in to a power source.

==**Just to confirm**==, our meeting at 2 o'clock, right?

==**I believe so.**== //yes, but I'm not completely sure.

**==Let's double check.==** //A polite way to say let's look at it again.

**Let's go ahead** and try this first …

==**I'd like to try**== to give you a different version.

==**If it still**== makes a strange noise, ==**then It's most likely**== a physical error.

**==If==** your computer **==still==** won't turn on, **==then it's most likely==** a hard drive error.

It's ==**covered under warranty**==.

If it's still encountering the same issue, just call me back.

We'll get started on the RMA process.

==**In case it doesn't work out**==

I'll look for that email. //我会去找那封邮件

**==No problem, happy to help.==**

Don't worry. **==We'll get it straightened out.==**	//我们会解决的。





**Time**

What time is it?

—2:00	It's two o'clock.

—2:05	It's five past two.

 —2:10   It's ten past two.

 —2:15   It's quarter past two.

 —2:30  It's half past two or It's two thirty.

 —2:35  It's twenty-five to three. 

![截屏2022-09-15 20.14.26](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-09-15 20.14.26.png)



### By 	until	 by the time

I will **be** at the office until noon.		//leave(until)

I will **be** at the office by noon.			//arrive(by)

I will **be** at the office by noon and I will stay until 5.



I will finish my homework at 5.

I will finish my homework by  5.  //(不能用until)

I'll work on my homework until 5. //until表示一种连续性的动作, by表示一次性的动作,针对一个时间点



I will have completed my tasks by 5.

By the time he arrived, she had already left.	//他到达的时候，她已经离开了

She waited in the lobby until he arrived.

Until he arrives, she can wait in the lobby .	



By the end of the lesson…

By the time we're done…



I will be done by 7.

I will be done **no later than** 7.

I will be there **no earlier than** noon.

I won't be there by noon.

**When are you working until?** —7, at the latest.



### In On at 

<img src="/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-09-15 20.51.53.png" alt="截屏2022-09-15 20.51.53" style="zoom: 25%;" />

#### In

##### In	(Periods of time)

in the future

in the past

in the present



In the future, I'd like to get married.

In the past, I studied hard at school.





##### In	(Years)

in 1999

in the 80s

in the 19th century



In 1999, I was born.

I was born in the 90s.



##### In	(seasons)

in summer

in the winter



In autumn, I love seeing the changing of the leaves.



##### In	(months&weeks)

in June

In five weeks' time

in a few weeks



Hopefully lockdown in the UK will end in a few weeks.



##### In	(parts of days)

in the morning

in the afternoon



in the evening I like to relax and play board games.



#### On

##### on	(Dates)

on the 10th of  June 1991, I was born.

on his birthday



##### On	(Holidays)

on New Year's Day

on Christmas Day



##### On	(Days of the week)

on Monday

on Friday

on Saturday morning



##### On	(Days of the Month)

on the 4th of the July

on the first day of September

 

#### At

##### at	(specific times)

at 10am

at 3 o'clock

at lunchtime

at sunset

at sunrise

at the moment



##### at	(holidays without Day)

At Christmas I like to spend time with my family.

At Easter



#### Summary

##### We use at:

**with particular points on the clock:**

> I’ll see you at five o’clock.
>

**with particular points in the day:**

> The helicopter took off at midday and headed for the island.
>

**with particular points in the week:**

> What are you doing at the weekend?
>

**with special celebrations:**

> At the New Year, millions of people travel home to be with their families (but we say on your birthday).
>

**We don’t use at with the question What time …? in informal situations:**

> What time are you leaving? 



##### We use *on*:

- **with dates:**

> *We moved into this house **on 25 October 1987**.*

- **with a singular day of the week to refer to one occasion:**

> *I’ve got to go to London **on Friday**.*

- **with a plural day of the week to refer to repeated events:**

> *The office is closed **on Fridays**.* (every Friday) In informal situations, we often leave out *on* before plural days:

> *Do you work **Saturdays**?*

- **with special dates:**

> *What do you normally do **on your birthday**?*



##### We use *in*:

- **with parts of the day:**

> *I’ll come and see you **in the morning** for a cup of coffee, okay?*

- **with months:**

> *We usually go camping **in July or August**.*

- **with years:**

> *The house was built **in 1835**.*

- **with seasons:**

> *The garden is wonderful **in the spring** when all the flowers come out.*

- **with long periods of time:**

> *The population of Europe doubled **in the nineteenth century**.*



##### *At* or *on*?

We use *at* to talk about public holidays and weekends, but when we talk about a particular special day or weekend, we use *on*.

**Compare**

| *We never go away **==at the New Year==** because the traffic is awful.* | ==***On New Year’s Day**==, the whole family gets together.* |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| *I’ll go and see my mother **at the weekend** if the weather’s okay.* | *The folk festival is always held **on the last weekend in July**.* |



##### *In* or *on*?

We use *in* with *morning, afternoon, evening* and *night*, but we use *on* when we talk about a specific morning, afternoon, etc., or when we describe the part of the day.

**Compare**

| *I always work best **==in the morning==**. I often get tired **in the afternoon**.* | *The ship left the harbour **==on the morning of the ninth of November==**.* |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ***In the evening** they used to sit outside and watch the sun going down.* | *It happened **on a beautiful summer’s evening**.*           |



##### *At the end* or *in the end*?

We use *at the end* (often with *of*) to talk about the point in time where something finishes. We use *in the end* to talk about things that happen after a long time or after a series of other events:

> ***At the end** of the film, everyone was crying.*

> Not: In the end of the film …

> *I looked everywhere for the book but couldn’t find it, so **in the end** I bought a new copy.*



##### *At the beginning* or *in the beginning*?

We use *at the beginning* (often with *of*) to talk about the point where something starts. We usually use *in the beginning* when we contrast two situations in time:

> ***At the beginning** of every lesson, the teacher told the children a little story.*

> ***In the beginning**, nobody understood what was happening, but after she explained everything very carefully, things were much clearer.*



#### Time expressions without *at, on, in*

We don’t normally use *at, on* or *in* before time expressions beginning with ***each*, *every*, *next, last, some, this, that, one, any, all***:

> *He plays football **every Saturday**.*

> *Are you free **next Monday** at two o’clock?*

> ***Last summer** we rented a villa in Portugal.*



#### *At*, *on* and *in* (time): typical errors

- We use *on* not *at* to talk about a particular day:

> *The two couples were married in two different cities **on the same day**, 25 years ago.*

> Not: … at the same day, 25 years ago.

- We don’t use *at* to refer to dates:

> *The General was killed **on 26 August**.*

> Not: … at 26 August.

- We use *at*, not *in*, with *weekend(s*):

> *What do you usually do **at the weekend**? Do you go away?*

> Not: What do you usually do in the weekend?

- We use *in* with months, not *on*:

> *They’re going to Australia **in September** for a conference.*

> Not: They’re going to Australia on September …



**You rock!**

> you are great

**Calm before the storm**

> A quiet period before chaos
>
> I like to wake up at 6 am, before my children wake up. It's the calm before the storm.

**under the weather**

> to feel sick
>
> I wish I could go apple picking with you, but I feel a bit under the weather today.

**when it rains, it pours**

> bad things happen at the same time

**rain or shine**

> to do something in any weather
>
> My family likes to go hiking every Friday, rain or shine.

**every cloud has a silver lining**

> something good in every bad situation
>
> Lockdowns were really tough this year, but I guess every cloud has a silver lining.

**go with the flow** 

> go along with whatever happens
>
> When you have kids, you need to be able to go with the flow every day.

**down to earth**

> practical, human, relatable
>
> You're methodical, practical and down to earth

**tip of the iceberg**

> small part of something bigger, usually negative
>
> when a parent yells at their child at the park, this is probably just the tip of the iceberg

**nip something in the bud**

> stop a bad situation from becoming worse by taking quick action
>
> When my 3 year old son first lied to me, I knew that I needed to nip it in the bud.

**beat around the bush**

> speak indirectly without getting to the main point
>
> Stop beating around the brush and tell me: Do you want to date me or not?

**The best of both worlds**

> an ideal situation

> This is the best of both worlds because we can be fulfilled by our job and also by taking care of our children

**get wind of something**

> hear news about something secret
>
> If the media gets wind of the political scandal, they are going to be taking about it for days

**plain as day**

> obvious to see
>
> It's plain as day that she loves you. Why don't you ask her on a date?

**up in the air**

> no definite plans
>
> I want to visit Switzerland next year, but because of Covid, our travel plans are up in the air.

**Go cold turkey**

> suddenly quit a bad habit
>
> If you want to quit smoking, you need to go cold turkey

**go on a wild goose chase**

> to do something pointless

**bring home the bacon**

> to make money to support your family

> When I was growing up, my dad brought home the bacon.

**two peas in a pod**

> two people who are a perfect pair

> My son and his friend are two peas in a pod when they play together

**put off**

> to delay something

> stop putting off learning phrasal verbs

**to ask around**

> I'm not sure if my company is hiring, but I'll ask around and find out for you.

**to ask someone out**

> to ask someone on a date

**to be in control of**

> Everyone thinks that they want to be in control of the business, but it's a lot of work!

**to break down**

> If you don't take care of your things, they are going to break down faster

**to break something down**

> The teacher broke down the lesson so that we could easily understand.

**to break into something**

> I forgot my keys so I had to break into my own house

**to break out**

> We were going to go on vacation and then Covid broke out

> when the news breaks out about the war, a lot of people are going to protest    //to spread

**to bring someone/something down**

> I hate to bring down the mood of this party but the cake just fell off the table

**to call around**

> I have some extra tickets to the concert so I'm going to call around and see if any of my friends want them.

**to cheer up**

> I brought you these flowers to cheer you up

**to cheer on**

> Let's cheer on our team

**to clean up**

> I'm trying to teach my sons to clean up the house

**to come across as** 

> If you don't want to come across as rude, you should smile when you meet people.

**to come apart** 

> I put my favorite sweater in the washer and now it's coming apart.

**to come forward**

> they're asking for donors and volunteers to come forward to help with disaster relief.
>
> help with 		She may need your help with some business matters.
>
> disaster relief 	赈灾

**to come from**

> where the inspiration for your art come from?
>
> My love of english comes from my teacher

**to cross off/on**

> to draw a line through something
>
> I have crossed off/out everything on my to-do list already, and it's only noon!

**to cut something out**

> to remove pieces or a part of something

> If you cut out these sections of there report, I think it will flow better
>
> Now I need to cut out the gum

**to do something over**

> to repeat sth

> if you make a mistake, it's ok you need to do it over

**to do away with sth**

> to stop doing sth

> we just need to do away with all papers and go digital

**to dress up**

> to wear nicer clothes than usual

> it's a good idea to dress up for a job interview

**to drop out of** 

> I dropped out of medical school

**to fall through**

> to fail, to end up not happening

> Our plans fell through when the flight was canceled

**to follow through**

> to keep doing something until it is finished

> It was a hard project, but she followed through and got it done

**to follow up**

> to pursue something further

> It's a good idea to follow up and interview with a thank you letter or a nice email.
>
> She said she had something to tell me so I need to follow up with her later.

**to get away**	(from)

> **to go on vacation or leave**

> It's been such a busy week. I can't wait to get away this weekend to the beach.

**to get away with**

> **to do something(usually wrong) without being noticed or punished**

> He stole money from the company, but he won't get away with it. 
>
> You could probably get away with work early but I don't recommend it.

**to get something across**

> to communicate

> She is such a great speaker that she has no problem getting her point across.

**to get back**

> I love vacation, but I'm always happy to get back home too.

**to get on**

> We got on the ferry
>
> I got on the wrong train

**to get over**

> I'm getting over a bad cold

> I'm still getting over the heartbreak of our break-up

**to give away**

> > **to reveal something or give something for free**
>
> I don't want to give away the ending.
>
> After I broke my leg, I decided to give away my roller skaters.

**to give back**

> I borrowed a coat from my cousin for our ski trip, and now I need to give it back

**to go after something**

> > **to try to achieve something**
>
> You should always go after your dreams.

**to go against**

> > **to oppose**
>
> if you go against the company police, you will have to face the consequences.

**to go ahead**

> Sorry for interrupting you, go ahead with your story.
>
> Do you want me to go ahead with the plan we discussed?

**to go over**

> > **to review something in order to clarify**
>
> I need to go over the contract before I sign it
>
> I have some questions about the project. Can we go over it together?

**to grow apart**

> We were best friends when we were children, but as we got older we just grew apart.

**to grow out of something**

> He grew out of all of his clothes, and now we need to buy new ones
>
> She has grown out of her role in the company, so she's leaving for a more challenging job.
>
> leave for 出发前往起程去

**To hand out something**

> > **to distribute something**
>
> I volunteered to hand out water and snacks at the marathon

**to hold someone/something back**

> when my boss told me that I got a promotion, I had to hold back my excitement until I left his office.
>
> I wanna say something to the rude man, but my husband held me back.

**to look up to someone**

> My parents are both hard workers, and I look up to them.
>
> Sometimes people that we look up to can let us down.



What do you do for a living?

How have you been?

long time no see



public-facing



**separate(adj/v)**

I try to keep meat separate from other food.        //not joined or touching

Use a separate sheet of paper for the next exercise.        //different

I try to keep my work and my private life separate        //not related

I separated the class into three groups.     //to divide into parts

My parents separated when I was four.        //to start to live in a different place from your husband



**implicit（adj）**

> suggested but not communicated directly:

He interpreted her comments as an implicit criticism of the government.

> felt by someone or influencing them without them being aware of it:

The experiment was designed to measure implicit racial bias.









