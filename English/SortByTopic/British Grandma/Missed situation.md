[TOC]



# Missed situation

I **was going to** go shopping **but** I got a phone call which lasted nearly an hour, and by the time the call ended the shop were all shut



I was **all set to** go on holiday, **but** then my car broke down, and I had to spend the money on repairs instead



I **would have** called you yesterday, **but** my phone wasn't working



I **had every intention of returning** the book to the library last week, **but** I have a friend staying, and She's been reading it



I **had it all arranged to** give my friend a surprise party, **but** then she found out about my plan, and said she didn't want one



**If only** I'd listened to your advice **then** **I wouldn't have invested** with that bank. **As it is**, I've lost a lot of money

 