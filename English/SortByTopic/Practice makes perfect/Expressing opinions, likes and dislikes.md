[TOC]



# Expressing opinions, likes and dislikes

Hi you must be Sarah. I can **tell** from your picture. I'm Lauren. Finally we meet! So we're going to be roomies this semester!

Yes, I **recognize** you from your photo, too! I'm glad to meet you **in person** – and I **see** from your T-shirt that you like baseball. I'm a big fan, too!

Well, the T-shirt was a **going-away present** from my brother, who's a baseball player. Look on the back – it has a photo of all the players on his team. They actually won the city championship this summer.

That's awesome. I tell you, I'm not very athletic, but I love to watch baseball, even if it's a **Little League** game. You could say I'm a professional spectator. What about you, do you play a sport?

Yes, I play tennis. As a matter of fact, I have a scholarship, and I'm going to play for the university. Now tell me, what else do you like to do?

Well – What I like to do best is dance. I'm studying classical ballet, but I also like to dance to popular music

Cool. We have a lot **in common**. I like to dance, too. I think you'll be **up for** checking out the local clubs this weekend

Oh, yeah. And the restaurants, too. **Speaking of which** – are you hungry? I'd love to **grab a bite** before it gets too late. I'm starving!

Are you kidding me? I'm always **up for** going out!  How about trying the place up the street? I'm kind of hungry for a good hamburger



Lauren, **what do you think of** your room?

To be honest with you, I really can't stand that dark color on the walls. It's , like, really depressing. I prefer light colors. Plus, I'd like to change the rug and the bedspreads. Do you like them?

No, I agree with you. They're horrible. With **a couple of coats** of paint and a few small changes, we'll make this room comfortable and cozy. Everybody will want to **hang out** here

I think we're really going to get along. I'm going to call my mom right now and tell her how cool my new roomie is

------

##  Like

> Do you like ice cream?  Yes, I do
>
> What kind of ice cream do you like?
>
> What do you like do on weekend?
>
> What would you like for your birthday?

I'm/she's/he's like … is often inserted into a conversation to emphasize what someone is currently feeling or thinking. This is especially common among young people

> I'm like really mad at him
>
> It's like the worst movie I've ever seen



## What do you think of…?

It's a way of asking someone's opinion of something



## Up for

To be up for something means to want to do it

> I'm not really up for doing anything tonight.

Alternative expressions are to **feel like doing** something or to **be in the mood for** (doing) something

> Do you feel like going to a museum?
>
> No I'm not in the mood for a museum today



## Going-away present

A going-away present is  a gift customarily given to someone who is leaving for an extended period, perhaps to go to college, to move to another area

> They gave me a picture of everyone in the office as a going-away present when I left for my new job



## Grab a bite

To get something to eat quickly

> We're in a hurry to get there, so we'll just grab a bite to eat at a fast-food place



