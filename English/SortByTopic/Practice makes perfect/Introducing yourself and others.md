[TOC]

# Introducing yourself and others 

Hi - You must be John's cousin Matt, <u>**right**?</u> From San Diego?

<u>**Correct**!</u> I just **got in** last night

I'm Todd. John's roommate from Tech. Glad to meet you. **I can assure you that** I'm not anything like what John has told you 

I'm happy to meet you, too - and, yes - I have heard about you! Football player and **party animal** **extraordinaire**

Football, yes - and as a matter of fact, I do like parties. But **tell me more about yourself** and what you do in San Diego

Well, I'm more (of) a surfer than a football player. You know, San Diego has a fantastic coast – and we can surf all day and then party on the beach at night 

That sounds awesome. How long are you staying?

Well, I'll be here for two weeks. John has promised me a **nonstop schedule**  kind of a **mix** of sightseeing, meeting his friends, checking out **the local scene**, and - hopefully - camping in the mountains for a couple of days

John's **a good guy** - and **you can be sure** he knows the local scene. He knows everybody in town. I'm sure he'll **show you a good time**. And his friends are here to help

Thanks so much - I really appreciate that. I'm still a bit **jet-lagged** at the moment but should be **in good-shape** by tomorrow. I'm looking forward to hearing what John **has in store for** me…

Don't worry. we'll all take good care of you. And don't be surprised if we **show up on your doorstep** in San Diego one day, ready for surfing!

------

#### Right?

- Correct!
- No, that's not correct

​	

#### To get in

​	To **get in** means to arrive and is usually used <u>in the past tense</u>

- What time did you get in?

- They got in late last night

​	Another way to say *arrive*, when it refers to the future, is **to get there**

- I hope we **get there** on time
- She will **get there** by six

​	To **get in** can also mean to be accepted by a school/college/university or other group with limited membership

- He applied to that college and really hopes to **get in**
- She didn't **get in** her first choice of sororities



#### I can assure you that…/you can be sure (that)…

These are common ways of saying that you believe something to be true, hoping to win the confidence of the person you are talking to

- I can assure you that I will work hard
- You can be sure that something interesting will happen

Here is another way to express that you believe something to be sure

- **I promise you that** we won't leave until the work is done



#### Party animal

 partial animal is an informal expression used to characterize someone who spends a lot of time with friends for entertainment



#### Extraordinaire

Extraordinaire is a word borrowed form French. It's used to exaggerate the meaning of the previous word

- I would like you to meet Marc - He's our pastry chef extraordinaire, You have to try his cheesecake!



#### Tell me about yourself

It is a polite way to let someone know that you are interested in learning more about him or her



#### Nonstop schedule

Nonstop schedule describes the activities of a very busy person, whether it be because of work, school, or even social life

- I don't have time to see you this week, with my nonstop schedule

Other ways to indicate nonstop activity are **around the clock** or **twenty-four–seven**

- I get telephone calls around the clock
- He works twenty-four-seven, so I hardly ever see him



#### A mix

A mix refers to a combination of different elements, usually indicating variety

- There will be a good mix of music at the wedding, to keep the grandparents, the parents happy
- We invited a mix of people - family, friends, coworkers



#### A good guy

Calling someone a good guy is a common way to recommend a male as being understanding of someone's situation, helpful, or generous. A female with the same kind of recommendation would be call **understanding/helpful/generous**

- If you are looking for a used car, go see Sam Smith; He is a good guy and will probably give you a good price
- If you want a teaching job, call Mary Johnson; She's very understanding and will give you good advice



#### The local scene

It refers to the culture and range of entertainment offered in a particular area



#### To show someone a good time

It means to make sure he or she is entertained

- If you come visit in December, we'll show you a good time



#### To be in good shape

It means to be fit financially or situationally

- My husband has a good job, so they're in good shape financially
- She has a good education and a lot of experience, so she's in good shape for the job market

A similar expression, to be in shape, means to be physically fit

- She exercises every day to stay in shape
- How do you stay in shape?



#### To have in store for

It indicates an unknown situation that someone presents to someone else; It can be good or bad

- Well, I'm going home, but I have no idea what my family will have in store for me
- We're going shopping tomorrow to see what the designers have in store for us this season



#### To show up on someone's doorstep

It means to visit someone without notice. It doesn't necessarily mean that you plan to stay overnight but it's possible

- I was just getting ready to go out when my cousin showed up on my doorstep

Related expressions are **drop in** and **drop by**, but these are used only for shorty visits - never an overnight stay

- We were in town, so we decided to drop in to see you
- Please drop by for a while. I miss seeing you

To **show up**, on the other hand, is used negatively to indicate that someone often doesn't appear when expected

- Pia said she was coming, but you never know if she'll show up or not