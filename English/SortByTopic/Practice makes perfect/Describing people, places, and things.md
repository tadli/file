# Describing people, places, and things



So, Michael, what's your new roommate like?

Well, if you have all day, I'll describe him for you. He's **quite the character.**

I don't have all day, dude — but basically — do you get along with him?

Actually, yeah — but that's only because we **hardly ever** see each other. The guy sleeps all day. Sometimes he gets up just to go to his classes, and then he comes back to the room and goes back to bed. Then he'll get up at midnight and study all night

Really? You don't eat together, **then**?

The truth is, I don't even know when he eats, or where.

**Then at least** he doesn't leave a mess in the kitchen.

No! The guy is incredibly neat. He **actually** leaves the bathroom clean every day — and he doesn't seem to have dirty clothes. He's like

a ghost.

Man, I think you have the perfect roommate!

What about yours? what's he like?

Well, he's the exact opposite of yours. **We're a lot alike**, and **we're together a lot**. I mean, we have two classes together and we're in the same fraternity, so we're really good friends.

Sounds to me like you have the ideal roommate!

Well, yes – and no. Mine is a disaster in the house. **In the first place**, he always leaves a mess in the kitchen; he doesn't wash the dishes and take out the trash. **Plus**, he throws his clothes **all over the place**. **Not to mention** how he leaves the bathroom.

Come on, Eric — **he sounds a lot like you**. **No wonder** you get along so well!

------

**In the first place**

**In the second place**

**Plus**

**Not to mention**



#### **Quiet the character**

 To be unusual in some way

> She never stops talking but can always make you laugh.
>
> — Yeah I hear she's quite the character
>
> — She must be quite the character



