# Which

#### *Which* as a question word

> **Which** car are we going in?
>
> **Which** do you prefer? Lemon cake or carrot cake?
>
> Find out **which** way they're going and we'll follow

#### Which or what?

> **Which** is the capital of Liberia? Monrovia or Greenvile?

> **What** is the capital of Liberia?

#### Which in relative clauses

**We use which in relative clauses to refer to animals and to things**

> We have seen a lot of changes which are good for business
>
> The cruise ship, which will depart from Liverpool.

**We also use which to introduce a relative clause when it refers to a whole clause or sentence**

> She seemed more talkative than usual, which was because she was nervous
>
> People think I sit around drinking coffee all day. Which, of course, I do

**We often use which with prepositions. Some formal styles prefer to use a preposition before which rather than to leave the preposition "hanging" at the end of the sentence**

> There are several small ponds in which a variety of fish live.

#### Which of

We use *of* with *which* before other determiners (*the, those, your*) and pronouns (*yours, them*):

> Which of the following features do you feel are important when choosing a house?
>
> Which of your sisters lives in Boston?