[TOC]

# Before

> ### **Warning:**
>
> In writing, when we refer back to something that we have already written, we use *above* not *before*:



## Before as a prep

We use *before* most commonly with noun phrases to refer to timed events:

> *I like to go for a run **before** breakfast.*

We use *before* to refer to place, especially when it is seen as part of a journey or as part of a sequence of events in time:

> *Get off the bus just **before** Euston Station.*



## Before, by, till, until

If you have to do something *before* a certain point in time, then when that point arrives, the action must already be completed:

> *I need to have the letter **before Friday.*** (Friday is too late. I need it in advance of Friday.)

If you have to do something *by* a certain point in time, then that time is the last moment at which the action can be completed:

> *Can we finish this meeting **by 5 pm**.*

If something is done or happens *till* or *until* a point in time, it happens over a duration of time, starting before that time and continuing up to that point:

> *I’ll be out of the office **until 17th** May. I will reply to your email after that date.* 



## Before as an adjunct

We use *before* to connect earlier events to the moment of speaking or to a point of time in the past:

> *I’m so looking forward to the trip. I haven’t been to Latin America **before**.* (up to the moment of speaking)

> *I introduced Tom to Olivia last night. They hadn’t met **before.*** (up to that point in the past)



## Just before, immediately before

We can use adverbs such as *just, immediately, shortly* and *long*, and expressions involving words such as *days, weeks, months, years* in front of *before*:

> *We got home **just before** it rained.*

> *The deadline for the essay was 5 pm. I got mine in **shortly before** five o’clock but Lily had hers in **days before** the deadline.*