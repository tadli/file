[TOC]



# Prove

## prove verb (show)

**to show a particular result after a period of time**

> The operation proved a complete success
>
> The dispute over the song rights proved impossible to resolve
>
> **The new treatment has proved to be a disaster**

## prove yourself

**to show that you are good at something**

> I wish he would stop trying to prove himself all the time

## prove verb(showing truth)

**to show that something is true**

> They suspected that she had killed him but they could never actually prove that it was her
>
> They proved him innocent
>
> That theory was proved false

