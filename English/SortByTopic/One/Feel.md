[TOC]



# Feel

## to experience something physical or emotional

> "How are you feeling?" "Not too bad, but I've still got a slight headache"
>
> How would you feel about moving to a different city?
>
> I felt like (= though that I was) a complete idiot 
>

## to have a particular option about or attitude towards something:

> I feel that I should be doing more to help her
>
> He had always felt himself (to be) inferior to his brothers
>
> I feel certain I'm right



# feel like something/doing something

## to have a wish for something, or to want to do something, at a particular moment

> I feel like a swim

## to want to do something that you do not do

> He was so rude I felt like slapping his face



