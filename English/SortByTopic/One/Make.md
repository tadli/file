# MAKE



#### **Create**

------

- **To create or prepare something by combing materials or putting parts together**

> **make something** 
>
> - She makes her own clothes
> - How do you make that dish with the peppers and olives in it?
> - made in China
>
> **be made of something**
>
> - What's your shirt made of?
>
> **be made out of something**
>
> - **What's your shirt made out of?**
>
> **be made from something**
>
> - Wine is made from grapes
>
> **make something into something**
>
> - The grapes are made into wine
>
> **make something for somebody**
>
> - She made coffee for us all
>
> **make somebody something**
>
> - She made us all coffee

- **To write, create or prepare something**

> These regulations were made to protect children
>
> My lawyer has been urging me to make a will
>
> It's possible to make a hypothesis on the basis of  this graph
>
> The Environment Secretary is to make a statement on Tuesday



#### cause to appear/happen/become/do

------

- **To cause something to appear as a result of breaking, tearing, hitting or removing material**

> **make something + adv/prep**
>
> - The stone made a dent in the roof of the car
>
> **make something**
>
> - The holes in the cloth were made by moths

- **make something To cause something to exist, happen or be done**

> to make a noise/mess/fuss
>
> She tried to **make a good impression on** the interviewer
>
> I keep **making** the same **mistakes**

- **make somebody/something/yourself + adj. To cause somebody/something to feel, show or have a particular quality; to cause somebody/something to be or become something**

> The news made him very happy
>
> She **made** her objections **clear**
>
> He **made it clear that** he objected
>
> The full story was never **made public**
>
> Can you **make yourself understood** in Russian?
>
> She couldn't **make herself heard** above the noise of the traffic
>
> The terrorists **made it known** that tourists would be targeted
>
> The company may **make** targets **difficult** or impossible to achieve
>
> Technology has **made** it **possible** to move many jobs away from high cost locations

- **make somebody/something do something To cause somebody/something to do something**

> She always makes me laugh
>
> This dress makes me look fat
>
> What makes you say that?(= Why do you think so?)
>
> Noting will make me change my mind
>
> We were made to feel extremely welcome

- **To cause somebody/something to be or become a particular kind of thing or person**

> **make something of somebody/something**  
>
> - I don't want to make an issue of it
> - Don't make a habit of it
> - You have made a terrible mess of the job
> - It's important to try and make something of your life
> - We will make a tennis player of you
>
> **make something + noun**
>
> - I made painting the house my project for the summer
> - She made it her business to find out who was responsible
> - These improvements will make the city a better place to live
> - She has mad her firm a success

-  **make a decision, guess, comment, etc. To decide, guess, etc.**

> Come on! It's time we made a start
>
> The plans was forced to make an emergency landing because of bad weather



#### Force

------

> **make somebody do something**  They made me repeat the whole story
>
> **be made to do something**  She must be made to comply with the rules
>
> **make somebody**  He never cleans his room and his mother never tries to make him



#### Appoint

------

> She made him her assistant



#### Money

------

**make something to earn or gain money**

> She makes $10000 a year
>
> to make a profit/loss
>
> We need to think of ways to make money
>
> He made a fortune **on** the stock market
>
> He makes a living as a standup comic



#### Equal

------

- linking verb + noun  to add up to or equal something

> 5 and 7 make 12
>
> A hundred cents make one euro

- linking verb + noun  to be a total of something

> That makes the third time he has failed his driving test!



**Make as if to do thing**

- *He made as if to speak.*



**Make do(With something)**

- We were in a hurry so we had to make do with a quick snack



**Make the most of something/somebody/yourself**

- It's my first trip abroad so I'm going to make the most of it
- She doesn't know how to make the most of herself



**Make or break**

- This movie will make or break him as a director
- It's make-or-break time for the company



We just managed to **make the deadline** (= to finish something in time).



Make + Object, to talk about things that we produce or create

>The scandal **made a mess of** his political career
>
>make a speech
>
>make a complaint //提出投诉
>
>make a mistake
>
>He arrived home and **made a start** on fixing dinner.	//着手处理
>
>make a note
>
>As a matter of procedure, you'll be asked to come down to the station and **make a statement** about the robbery.
>
>We **made a date** to go over our father's estate.
>
>Close your eyes and **make a wish.**
>
>Your help **made a big difference.**
>
>She **makes a point of** treating her employees fairly.	//to give one's attention to make sure that it happens		
>
>make an appointment	
>
>make a fuss 
>
>I'll **make an effort**, but I can't guarantee that I'll be home before the party starts.	



Make + object (o) + noun complement (nc) 

>**I promise you we’re going to make it a priority.**

