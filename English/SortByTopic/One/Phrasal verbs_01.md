#### pull off

**achieve something difficult**

> After a lot of practice that gymnast pulled off a triple backflip.

**To forcibly remove something (from or off something else).** 

> Don't pull off the bandage or the wound might get infected.



#### **in the long run**

**a long period of time after the beginning of something**

> Your solution may cause more problems over the long run.
>
> It may be our best option in the long run.



#### **knock** your next <u>speaking engagement</u> **out of the park**

​	*The expression “knock it out of the park” acts as a motivating statement. You can use the phrase before someone undertakes a task to inspire them to perform.*

> We’ll knock it out of the park, I promise. You can rely on us to give it everything we have
>
> Good luck with your exam today, Kim. I hope you knock it out of the park.



#### **powered by**

> Walmart does not have a **one-size-fits-all** approach to operating its international markets, but a consistent focus on strong local businesses powered by Walmart.



#### come down to

To amount to something (usually the most important or crucial aspect of something).

> It's a great offer, but my decision comes down to salary, to be honest with you.
>
> People talk about various reasons for the company's failure, but it all comes down to one thing: a lack of leadership.

To visit some place, often a location that is lower or farther south than one's starting point. 

> **Can you come down to the basement to help me for a second?**



#### The very idea

An exclamation of shocked disapproval regarding something someone said or did.

> I do not approve! That is outrageous! Resignation? The very idea! Absolutely not!



#### Round up 

drive or collect a number of people or animals together for a particular purpose

> In the afternoon the cows are rounded up for milking
>



#### To the point of something/of doing something

To such a degree or extent that something is true or could happen.

> He was focused on perfecting the project to the point of obsession.
>
> He is always so earnest and good-natured, to the point of seeming naïve at times.



#### Come down

**(of rain or snow) fall, in particular fall heavily**

> I watched the rain come down hard on the windows

**(of a building or other structure) collapse or be demolished**

> we were lucky the bridge didn't come down

**(of an aircraft) crash or crash-land**

> the aircraft came down during an attempt to land in bad weather be handed down by tradition or inheritance
>
> the name has come down from the last century

**reach a decision or recommendation in favor of one side or another**

> **advisers and inspectors came down on our side**