# Matter

Matter as a verb

>It doesn't matter
>
>It wouldn't matter



Matter in questions

>Does it matter if I leave my computer on all night?
>
>Would it matter which flight we got?
>
>Does it matter?



Matter in affirmative statements

>It matters to me.



Matter as a noun
 We can use what’s the matter (with …)? to ask someone about a problem or to ask for an explanation of a situation that looks problematic:

>What’s the matter, darling? Why are you crying?
>
>What’s the matter with Derek these days? He’s acting so strangely.

 When we reply to the question What’s the matter?, we don’t use matter, we use problem:

>The problem is he’s just broken up with his girlfriend. They’ve been together for a long time.



Matter as a countable noun
 We can use matter as a countable noun to mean ‘question, problem or issue’:

>This is a matter for the police. We cannot deal with it ourselves.
>
>It’s only a matter of time before everyone will get bored with reality TV shows.
>
>Are there any other matters to discuss today, or shall we finish?



As a matter of fact
 We can use the expression as a matter of fact to emphasize that something is different from what has been said before, or from what people think or expect:

>A: I don’t think you like Hilary, do you?
>
>B: No, that’s just not true. As a matter of fact, I’m very fond of her. It’s just that she irritates me sometimes.



In a matter of + time expression
 We can use the expression in a matter of with a time expression to refer to something that happens very quickly, or which can be done very quickly:

>It used to take an hour to get to the airport, but now with the new metro line you can get there in a matter of minutes.



No matter

>No matter what I wear, I always feel dull and old-fashioned.
>
>No matter where she looked, she could not find the missing paper.
>
>He never answers emails, no matter how many you send him.



 If there is just one clause, we use it doesn’t matter before what, when, where, which, who and how:

>A:There are three phone numbers here for the tax office. Which one do I call?
>
>B:It doesn’t matter which one you use.



For that matter
 used to say that what you are saying about one thing is also true about something else

>Ming's never been to Spain, or to any European country for that matter