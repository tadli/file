#### *At*, *on* and *in* (time)

**We use *at*:**

- with particular points on the clock:

> *I’ll see you **at five o’clock**.*

- with particular points in the day:

> *The helicopter took off **at midday** and headed for the island.*

- with particular points in the week:

> *What are you doing **at the weekend**?*

- with special celebrations:

> ***At the New Year**, millions of people travel home to be with their families* (but we say ***on\*** your birthday).

We don’t use *at* with the question *What time …?* in informal situations:

> *What time are you leaving?* (preferred to *At what time are you leaving?*)



**We use *on*:**

- with dates:

> *We moved into this house **on 25 October 1987**.*

- with a singular day of the week to refer to one occasion:

> *I’ve got to go to London **on Friday**.*

- with a plural day of the week to refer to repeated events:

> *The office is closed **on Fridays**.* (every Friday) In informal situations, we often leave out *on* before plural days:

> *Do you work **Saturdays**?*

- with special dates:

> *What do you normally do **on your birthday**?*



**We use *in*:**

- with parts of the day:

> *I’ll come and see you **in the morning** for a cup of coffee, okay?*

- with months:

> *We usually go camping **in July or August**.*

- with years:

> *The house was built **in 1835**.*

- with seasons:

> *The garden is wonderful **in the spring** when all the flowers come out.*

- with long periods of time:

> *The population of Europe doubled **in the nineteenth century**.*



#### *At* or *on*?

We use *at* to talk about public holidays and weekends, but when we talk about a particular special day or weekend, we use *on*.

| *We never go away **at the New Year** because the traffic is awful.* | ***On New Year’s Day**, the whole family gets together.*     |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| *I’ll see my mother **at the weekend** if the weather’s okay.* | *The folk festival is always held **on the last weekend in July**.* |

#### *In* or *on*?

We use *in* with *morning, afternoon, evening* and *night*, but we use *on* when we talk about a specific morning, afternoon, etc., or when we describe the part of the day.

| *I always work best **in the morning**. I often get tired **in the afternoon**.* | *The ship left the harbour **on the morning of the ninth of November**.* |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| ***In the evening** they used to sit outside and watch the sun going down.* | *It happened **on a beautiful summer’s evening**.*           |

### *At* or *in*?

*In the night* usually refers to one particular night; *at night* refers to any night in general:

| I was awake **in the night**, thinking about all the things that have happened. | *‘It’s not safe to travel **at night**,’ the officer said.* |
| :----------------------------------------------------------- | :---------------------------------------------------------- |



#### Time expressions without *at, on, in*

We don’t normally use *at, on* or *in* before time expressions beginning with ***each*, *every*, *next, last, some, this, that, one, any, all***:

> *He plays football **every Saturday**.*

> *Are you free **next Monday** at two o’clock?*

> ***Last summer** we rented a villa in Portugal*
