## Same

**Exactly like one another**

> She was wearing exactly the same dress as I was
>
> He had the same problem as I did
>
> She brought up her children in just the same way her mother did
>
> People say I look just the same as my sister

**Not another different place, time, situation, person, or thing:**

> I would do the same thing again if I have the chance
>
> Shall we meet up at the same time tomorrow
>
> Charles is just the same as always.

**Not another different thing or situation:**

> I'm terrible at physics, and it's the same with chemistry - I get it all wrong.

**(adv.) In the same way**

> We treat all our children the same
>
> I need some time to myself, the same as anybody else

**(Noun)Exactly alike, or not changed:**

> It took longer to lose weight the second time, even though my diet was exactly the same.

**The same + noun + clause**

> How was the course?  Was it the same teacher you had last time?
>
> This is the very same hotel we stay at when we were twenty years ago

**Do the same**

> She bought her ticket for the folk festival online, and we did the same.



### Similar and identical

We use similar if two or more things are not entirely the same, or identical if two or more things are exactly the same.

 We use the patterns:

-  **similar to** 
-  **identical to**
-  **a similar + noun** 
-  **a similar + one** 
-  **an identical + noun** 
-  **an identical + one**



 We don’t say a same:

This color **is similar to** that one.

Frank had a problem connecting his printer. We had **a similar problem**, so it must be the software.

~~Not: … a same problem …~~

She first showed us a beautiful 16th-century vase. Then she showed us **an identical one**, but the second one was a copy.

~~Not: … a same one …~~