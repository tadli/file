[TOC]



# Become

## become verb(be)

**to start to be**

> I was becoming increasingly suspicious of his motives
>
> It was becoming cold, so we lit the fire
>
> He has just become a father