[TOC]

# Rather

We use rather as a degree adverb (rather code, rather nice)

We also use it to express alternatives and preferences (green rather than blue)



## Rather as a degree adverb

We use *rather* to give emphasis to an adjective or adverb. It has a similar meaning to *quite*

> I'm afraid I behaved rather badly

### Rather with adjective + noun

> We had to wait rather a long time
>
> He helped her out of a rather an uncomfortable situation
>
> I had some rather bad news today

#### Rather a + noun

> It was rather a surprise to find them in the house before me

#### Rather a lot

> It costs me rather a lot of money
>
> You have give me rather  a lot
>
> They went there rather a lot (often)

#### Rather + verb

We can use rather to emphasize verbs. We use it most commonly with verbs such as enjoy, hope, like;

> I was rather hoping you had forgotten about that
>
> He rather liked the idea of a well-paid job in Japan

#### Rather like

We use rather with like to refer to similarities

> They were small animals, rather like rats
>
> I was in the middle. I felt rather like a referee at a football match trying to be fair and keep the sides apart



## Rather than: alternatives and preferences

> He wanted to be an actor rather an a comedian
>
> Can we come over on Saturday rather than Friday?





