[TOC]



# Seem

We can use it as a linking verb or with a to-infinitive.  We do not normally use seem in the continuous form:

> She seems very young to be a teacher
>
> I seem to be the only person who hasn't heard the news



## Seems as a linking verb

Seems as a linking verb is followed by an adjective or, less commonly, a noun:

>  It seems strange that no one noticed that the window was broken
>
> Buying a new car seems a complete waste of money to me

We can use the impersonal construction *it seems* or *it seemed* with a that-clause, or with *as if* or *as though* and a clause:

> It seems that the village shop will have to close down
>
> It seems as if he wants everyone to feel sorry for him, but I don't
>
> It seemed as though time was standing still

When seem is followed by a clause, we can refer to the person who experiences the situation using a prepositional phrase with to:

> It seemed to everyone that the police were over-reacting, but no one dared to criticize them
>
> It seems to me as though she needs help

*It seems like* and a clause is less formal

> It seems like he's going to sell his house and move to Canada.



## Seem + to-infinitive

We can use seem with a to-infinitive:

> Tony always seems to offend people
>
> Everybody seemed to be ready, so we set off

We can also use the impersonal construction *there seems to be* or *there seemed to be*, followed by a noun:

> There seems  to be a mistake in these calculations.
>
> There seemed to be no reason why she should not come with us, so we all travelled together