[TOC]



# Sound

## Noun

> They could hear the sound of a bell tolling in the distance
>
> She stood completely still, not making a sound
>
> Could you turn the sound down/up on the TV?



## Verb

sound good, interesting, strange, etc

> I know it sounds silly, but I'll miss his when he's gone
>
> He sounded very depressed when we spoke on the phone

sound like/as if/as though

> That sounds like a good idea
>
> It sounds as if they had a good holiday



## Adjective

good condition

> It's an old building but it's still structurally sound
>
> Was she of sound mind at the time of the incident?

good, correct

> She gave me some very sound advice
>
> I'm not sure how sound the theoretical basis for this argument is
>
> The book is very sound on the basics
>
> How sound is her knowledge of the subject?





