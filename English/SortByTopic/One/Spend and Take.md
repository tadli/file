#### Spend + time

**to use time doing something or being somewhere**

> I think we need to spend more time together.
>
> I spent a lot of time cleaning that room.
>
> I spent an hour at the station waiting for the train.
>
> How long do you spend on your homework?
>
> We **spent the weekend** in Buenos Aires.
>
> You can **spend the night** here if you like.
>
> I spent a lot of time (at the station) to see you (=reason clause)



#### Take the time to do something

>  She didn’t even take the time to say goodbye.
>
>  It took me 2 hours to cook the meal.
>
>  It took 2 hours for me to cook the meal
>
>  To cook the meal took me 2 hours.



