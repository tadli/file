[TOC]

# Look



> ## Warning
>
> We don't use look with if or whether, we use see instead:
>
> ​	=>Can you **see** if there are any biscuits in the cupboard?



## Look as a linking verb

We often use look as a linking verb like appear, be, become, seem. As a linking verb, look does not take an object and it is followed by a phrase or clause which gives more information  about the subject.

> That picture **looks** old
>
> He **looks like** someone famous
>
> It **looks like** a nice day outside
>
> She looks as if she is going to try
>
> They **looked as though** they had seen a ghost