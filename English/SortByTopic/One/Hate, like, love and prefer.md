# Hate, like, love and prefer

We can use them with an `-ing` form or with a `to`-infinitive:

> I hate to see food being thrown away
>
> I love going to the cinema
>
> I prefer listening to the news on radio than watching it on TV
>
> He prefer not to wear a tie to work



#### Would + hate, like, love, prefer

We would love to hear your sing

They would hate to cause a problem

I would prefer not to give you my name

