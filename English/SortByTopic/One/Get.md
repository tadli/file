#### **get about**

 be socially active

> Tom really gets about, doesn't he?



#### **get at**

 To physically reach someone or something.

> I'm trying to get at the truth.

To irk or annoy someone.

> That loud music has started getting at me.

To recognize or realize something.

> It's taken some time, but I think we've finally gotten at a solution that will work.

To start to work on something.

> If you don't get at this project early, you'll be up all night finishing it.



#### get ahead

 be successful

> It's very difficult to get ahead nowadays.



#### get away

escape

> The thief got away from the police.



#### get back

 recover or retrieve

> I got my books back from Tom.



#### get by

To survive financially

> Sally gets by on just $1,000 a month.



#### get into

be accepted

> He got into the university of his choice.



#### get off

exit from a train, bus etc.

> Jerry got off at 52nd Street.



#### get through

**To pierce or pass through something.**

> This blade isn't sharp enough to get through such thick material.
>
> There's a big crowd near the stage. Let's see if we can get through to the front.

**To endure some experience.**

> I don't know how she managed to get through that traumatizing experience.

**To help someone to endure some experience. In this usage, a noun or pronoun is used between "get" and "through."**

> Do we have enough water to get us through the period of quarantine?
>
> You were always there for me. You got me through the worst times.

**To complete something.**

> Once I get through my last year of school, I'll be taking a long vacation, that's for sure.

 **To communicate in a way that makes one acknowledge, accept, or understand something.**

> The best way to get through to these kids is to be honest with them.



#### get to it

begin doing something	

> Let's get to it! It's late.



#### get down to business

begin working

> Tom arrived at 12 and immediately got down to business.



#### [Get + NOUN]

arrive/receive/earn/bring or fetch/understand/affected by, or catch/catch or take

> **She got to work an hour late.**
>
> I got a book for my birthday.
>
> I get $7 an hour.
>
> Can you get that book for me?
>
> Do you get the lesson?
>
> He got a cold last week.
>
> **I got the 4:55 train to New York.**

communicate with

> I got him by phone.

have a strong effect on

> **That film really got me.**

capture or seize

> The police got him at the station.

Get can indicate a process of change. 

> **I get bored when I watch TV**
>
> You can get angry, get excited, get tired, get sick, get older, and get lost.
>
> Get the cat off the table
>
> We need to get some work done
>
> I'm going out to get some wine for the party

GET can also mean understand:

> I'm afraid I don't get the problem at all.

Keep in mind that GET can be used with 2 objects:

> He got me flowers
>
> Let me get you a drink



#### [Get + ADJ]

When we use GET before an adjective, it usually means 'become'

> The situation is getting worse
>
> Something can get better or get worse
>
> Their relationship is starting to get serious
>
> He got annoyed because she wasn’t listening to him
>
> two people can get engaged (meaning they plan to marry each other), get married, and get divorced.
>
> You can also get dressed, meaning to put on your clothes, and get ready
>
> when you get used to something it means you become accustomed to it
>
> I need to get it fixed



#### [Get + to]

When using this structure, get can mean 'arrive'

> She didn't get to Montreal till after midnight
>
> How can I get to the train station?
>
> I got to work late because of the weather

Remember that we never use to with 'home, downtown, here and there'

> How are you getting home tonight?
>
> What time will you get here?
>
> I won't get there until later



#### [Get + Infinitive]

If you get to do something, it means you have a good/special opportunity

> He’s upset because he didn’t get to take a vacation last year
>
> If my kids finish all their homework, they get to watch a movie before bed
>
> Do you get to travel much?
>
> I didn't get to see him. He was too busy
>
> When do we get to meet your new girlfriend?



#### [Get + object + Infinite]

it can mean 'to make someone do something' or 'to persuade someone to do something'

> Get him to call me
>
> I can't get her to change her mind
>
> Can you get them to stop talking?
>
> It’s hard to get my kids to help with housework