[TOC]



# remain

**to stay in the same place or in the same condition**

> The doctor ordered him to remain in bed for a few days
>
> Most commentators expect the basic rate of tax to remain at 25 percent
>
> He remained silent
>
> It remains a secret
>
> The bank will remain open while renovations are carried out

**to continue to exist when other parts or other things no longer exist**

> Only a few hundred of these animals remain today
>
> For reasons of personal safety, the informant wishes to remain anonymous
>
> The truth about the accident remains hidden beneath a shroud of secrecy

## remaining

Use half the dough and keep the remaining half covered

