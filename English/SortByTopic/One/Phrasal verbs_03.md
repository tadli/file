###### To blow over

> **I'm waiting for the storm to blow over before I go on a hike.**
>
> **Once the argument blows over, we'll be able to come to an agreement.**



###### To be blown away

> She was blown away by how amazing the artwork was.
>
> I am blown away by how much money he raised for charity.



###### To back down

> The stray cat wasn't gonna back down. He was ready to fight.
>
> Local residents have forced the local council to back down from/on its plans to build a nightclub on their street.



###### To back off

> I could tell he was getting upset, so I decided to back off and give him some space.
>
> They decided to back off their threats after someone called the cops.



###### To back out of

> They decided to back out of the cease-fire agreement.
>
> She decided to back out of the vacation plans when she realized how much it cost.



###### To back up

> You need to back up. Your parked way too close to me.
>
> Everyone back up, give me some space!



###### To bank on

> Clark is banking on his holiday bonus this year.
>
> He is banking on her saying yes to his proposal of marriage.



###### To block off

> The police had to block off a few streets while they were clearing the wreck.
>
> I need to block off a few hours in my schedule so that I can study English with Vanessa.



###### To blow up

> Don't blow up at me. I told you this would happen.
>
> Don't light a match near the gasoline. It will blow up!



###### To blow off

> I really just wanted to blow off work today and go do something fun.
>
> She didn't even say hi to me. She just blew me off!



###### To bring up

> Don't bring up her ex in conversation. It's still a really sensitive subject.
>
> He tried to bring up the possibility of a raise, and his boss just blew him off.

