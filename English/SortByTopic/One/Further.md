## Further



### **comparative: far: to a greater distance or degree, or at a more advanced level**

> I never got further than the first five pages of "Ulysses".
>
> We discussed the problem but didn't get much further in actually solving it
>
> Every day she sinks further and further into depression

#### go further/take something further

> Before we go any further with the project, I think we should make sure that there is enough money to fund it
>
> If you want to take the matter further, you can file charges against him
>
> As you go further south, you will notice a gradual change of climate
>
> Leave the main road and cross the bridge, and we live just three houses further on

------

### at a greater distance

> It was further to the town center than I remembered
>
> Fourteen miles is further than you would think once you start to run it

------

### more or extra

> Have you anything further to add?
>
> If you have any further problems, let me know
>
> It cost me 30$ a day and a further 60$ for insurance
>
> This shop will be closed until further notice
>
> We need to talk further about this
>
> For the further details, call this number
>
> Cook for a further 2 minutes = Cook for another 2 minutes
>
> Can you give me any further information?
>
> further investigation/research/study/analysis
>
> This week provided further evidence of an unfolding global energy crisis
>
> We have decided to take no further action