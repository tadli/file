# Both and All



## Both

**We use *both* to refer to two things or people together:**

> Both those chairs are occupied, I’m afraid. 
>
> Are both your parents going to Chile? 



### Both with nouns

**When we use both before a determiner (e.g. a/an, the, her, his) + noun, both and both of can be used:**

> She knew both my children. (or … both of my children.)
>
> Both her brothers are living in Canada. (or Both of her brothers …)

**We can use both before a noun:**

> This button starts both engines at the same time.



### Both with pronouns

#### Pronoun + both

> We both prefer classical music.
>
> Let’s open them both now.

#### Both of + object pronoun

> We both dislike soap operas. (subject pronoun + both)
>
> Both of us dislike soap operas. (both + of + object pronoun)



### Both as a pronoun

**We can use both on its own as a pronoun:**

> There are two youth hostels in the city. Both are described as expensive, dirty, with poor lighting and poor security.



### Both: Position

> They **both** wanted to sell the house. (between the subject and the main verb)

> They had **both** been refused entry to the nightclub. (after the first auxiliary or modal verb)

> They were **both** very nice, kind and beautiful. (after be as a main verb)



### Both of or neither of in negative clauses

**We usually use *neither of* rather than *both of … not* in negative clauses:**

> Neither of them can swim.

**When a negative verb is used, we use *either (of*).**

>  She didn’t like either dress.



## All

### All as a determiner

> All my friends are away at university.
>
> All tickets cost 25 pounds.
>
> All information about the new product is confidential.



### All with no article

**When all refers to a whole class of people or things, we don’t use the:**

> All children love stories.

**We don’t use the with time expressions such as all day, all night, week, all year, all summer:**

> I spent all day looking for my car keys.
>
> The party went on all night and some of the neighbours complained.



### All of

**We use *all of* before personal pronouns (*us, them*), demonstrative pronouns (*this, that, these, those*) and relative pronouns (*whom, which*).** 

> I need to speak to all of you for a few minutes.
>
> He brought gifts for all of us.
>
> We had to contact the insurance firm and the airline, all of which took a lot of time. 

**With demonstratives (*this, that, these, those*) we can say *all of* or *all* without *of*:**

> All (of) this has to go out into the rubbish bin.



### All: Position

> The kids all go to school on the same bus.
>
> These items could all have been bought cheaper on the Internet.
>
> The students are all here now. We can start.



## *All* meaning ‘completely’ or ‘extremely’

> He lived all alone in an old cottage in the woods.
>
> He came back all covered in mud.
>
> I lost a good friend, and all because of my stupidity.
>
> Maggie got all upset when she found out the house had been sold. (informal)



## *All: not all*

> Not all the buses go to the main bus station, so be careful which one you get.
>
> We weren’t all happy with the result.