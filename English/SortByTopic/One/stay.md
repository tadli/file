[TOC]

# Stay

## Verb

to not move away from or leave a place or situation

> Stay until the rain has stopped
>
> Because of the snow, children were told to stay at home
>
> Can you stay a bit longer?
>
> They need an assistant who is willing to stay for six months

to continue doing something, or to continue to be in a particular state

> Put a lid on the pan so the food will stay hot
>
> He decided not to stay in teaching/medicine/the army
>
> They shop stay open until 9 o'clock
>
> They stayed friends after they divorce

to not move away from or leave a place

> I have a meeting at there so I can't stay lone
>
> Can you stay for dinner?
>
> She's come back home to stay

to live or be in a place for a short time as a visitor

> I stayed in Montreal for two weeks 
>
> They said they would stay in a hotel

stay overnight( to sleep somewhere for one night)

> We have arranged to stay overnight at my sister's house



## Noun

a period of time that you spend in a place

> She planned a short stay in a hotel to celebrate their anniversary



## Business English

to not move away or leave

- stay for 2 days/ 2 weeks

> They need an assistant who is willing to stay for six months.

- stay at work/home

> We stayed at work until 10 pm.

- stay here/there

> How long do you plan to stay here in Boston?

