[TOC]



# Appear

## to start to be seen or to be present

> He suddenly appeared in the doorway
>
> His name appears in the film credits for lighting



> # Warning
>
> - appear : One or two clouds appeared in the sky
> - be/become visible : As the beach gets darker, the glow of city lights becomes more visible



## to seem

> You've got to appear( to be) calm in an interview even if you're terrified underneath
>
> Things aren't always what they appear to be
>
> **She appears to actually like the man, which I find incredible**
>
> **There appears to be some mistakes**
>
> It appears that she left the party alone
>
> It appears to me that we need to make some changes
>
> It appears as if/as though I were wrong
>
> **Everything was not as it appeared** 
>
> **"Has he left?" "It appears not/so"**