[TOC]



# Turn

## Verb

### turn verb( go round )

**to (cause to) move in a circle around a fixed point or line:**

> Turn the steering wheel as quickly as you can
>
> Slowly, I turned the door handle
>
> The earth turns on its axis once every 24 hours

### turn verb( change direction )

**to (cause to) change the direction in which you can facing or moving:**

> Turn right at the traffic lights
>
> She turned to face him
>
> He turned round and waved to us
>
> I'll just turn the  car round and go back the way we came
>
> We have to turn down/into/up the next road on the right

### turn verb( change position )

**to move, or to move an object or page, so that a different side or surface is on the top**

> Now turn the page
>
> She turned the vase over to look for the price
>
> Now turn to page 23

### turn verb( become )

**to(cause to) become, change into, or come to be something**

> The weather has suddenly turned cold
>
> She turned pale and started to shiver
>
> By the end of September, the leaves have started to turn
>
> Her attitude turned from politely interested to enthusiastic during the course of our conversation

  

## Noun

### turn noun( time to do something )

**an opportunity or a duty to do something at  a particular time or in a particular order, before or after other people**

> I waited so long for my turn to see the careers adviser 
>
> In this game if you give the wrong answer you have to miss a turn

#### 	take turns

#### 	in turn 

### turn noun

**a change in the direction in which you are moving or facing**

> When you see the school on the left, make a right turn
>
> The path was full of twists and turns

