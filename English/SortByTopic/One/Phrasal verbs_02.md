

###### To account for

> I think the cold weather accounted for the low attendance at the game
>
> I can't account for the missing money. I don't know where it went!



###### To act out

> The child acted out when her mom wouldn't give her any candy.
>
> She was so hungry that she started to act out.



###### To act up(To cause trouble)

> He always used to act up in class, but guess what? Now he's a doctor!
>
> My car battery has been acting up. I think I need to get a new one.



###### To allow for

> When you create a budget, you should allow for emergencies.
>
> I didn't allow for traffic this morning, so I think I'm gonna be late.



###### To answer for

> You will have to answer for your actions someday.
>
> He had to answer for his crimes.



###### To apply for

> My parents encouraged me to apply for every scholarship possible.
>
> Even if you don't meet the qualifications, you should apply for the job anyway.

