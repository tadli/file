# Of one's own



If you say that someone has a particular thing of their own, you mean that that thing belongs or relates to them, rather than to other people.



> Her parents want her to go to college, but she has a mind of her own and insist on trying to become an actress
>
> I can't get the camera to work right. It seems to have a mind of its own
>
> I tried to talk to him, but he was in a world of his won and didn't seem to hear what I was saying

