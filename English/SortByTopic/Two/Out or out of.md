# Out or Out of

We use *out* and *out of* to talk about position and direction

We use *out* as an adverb to mean "not in a building or an enclosed space"

> Don't come out. It's too cold

> Is Bill there?
>
> No he's out

We use *out of* as a preposition to talk about movement from within somewhere or something, usually with a verb that expresses movement (e.g. *go*, *come*). It shows where something is or was going:

> You go out of the building and turn right
>
> When I reached the corner, I jumped out of my car and ran across the road

Out is the opposite of in

Out of is the opposite of into



## Out of: all gone

We use out of to say that something is all gone

> The printer is out of ink. We need to get some soon

> I'm afraid, we're out of soup

