[TOC]



# Each/Every

1. 两者必须接可数名词单数
2. 最大的区别是: each不仅可以作为限定词，也可以作为代词。而every只能作为限定词
3. Each可以用来指定两者或两者以上的事物。而Every只能指定三个或三个以上的事物
4. Each强调个别的概念，Every强调总体，所以可以并列使用each and every
5. Each不与数词连用，every可与数词连用



> every two days
>
> every other day
>
> I want to welcome each and every balloon enthusiast in Philadelphia
>
> I want every student to succeed in the exam
>
> Each student has his own personal dream in his mind
>
> There are a lot of trees on each side of the street
