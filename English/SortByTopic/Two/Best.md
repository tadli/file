## Best

As a verb, to best means to get the better of

> Jack’s wife always bests him at bridge

Best can be a noun 

> Marilyn wanted nothing but the best for herself and her family

As an adverb *best* modifies a verb. 

> All the boys are good at drawing faces, but James does it best


