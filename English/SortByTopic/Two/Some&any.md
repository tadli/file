

[TOC]

# Some and any

1. Some和any都可以与不可数名词以及可数名词复数连用，some一般用在肯定句，any用在否定句.但希望一个正面回答，鼓励对方同意时，要用some
2. 特殊用法： any + 单数名词，它表示【无论哪个，任何一个】
3. 特殊用法：some + 单数名词，它表示【某一个】
4. 特殊用法： some + 数词，表示【大约】，常常暗指数字很大



> I don't have any friends here
>
> Would you like give me some advice?
>
> You can catch any bus
>
> Some person at the gate is asking to see you

