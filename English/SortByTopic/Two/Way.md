[TOC]



# Way

## As a noun

> I make cheese sauce a different way **from** my sister
>
> The hospital is on Sandford Road. Do you know the way?
>
> Which way shall we go?
>
> It's a long way from here on foot. You can take a bus

