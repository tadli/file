[toc]

# Sentence Patterns

## 五种基本句型

````
1. S+V
2. S+V+O
3. S+V+C
4. S+V+O+O
5. S+V+O+C
（ S ： 主 词 ， V ： 动 词 ， O ： 受 词 ， C ： 补 语 ）
````

及 物 （ transitive） 、不 及 物 （intransitive）

完全动词（complete）、 不完全动词（incomplete），不完全动词需要补语

## 主部与叙部

The dog barked `<u>`at the mailman.`</u>`

`S	V`

> The dog barked已然能构成完整句子，at the mailman是介系词片语，依附于动词当做修饰语使用

The dog bit the mailman

`S	V	O`

Don't worry. It doesn't bite

`S	V`

> 同一个动词，视用法不同，可以当及物动词使用，也可以当不及物动词

## 何谓补语？

欠缺叙述能力的动词需要补语

The dog was a bulldog

The dog was mean //那条狗很凶恶

`S	V	C`

不必翻译的动词：**be动词**

由此，引申出**连缀动词（Linking verb）**

| look	//看起来是 | That purse looks pretty. 那 个 包 包 很 好 看 。                     |
| --------------- | -------------------------------------------------------------------- |
| seem            | The question seems easy. 这 个 问 题 好 像 很 容 易 。               |
| appear	//显得是 | He appears quite a gentleman. 他 似 乎 是 个 十 足 的 绅 士 。       |
| sound           | Your suggestion sounds exciting. 你 的 提 议 听 起 来 很 刺 激 。    |
| feel            | The woman feels dizzy. 这 个 女 人 感 觉 头 晕 。                    |
| taste           | The wine tastes sweet. 酒 有 甜 味 。                                |
| stay	//仍然是   | **They stayed friends for life**. 他 们 一 生 都 是 朋 友 。   |
| remain	//继续是 | **He remains single.** 他 继 续 保 持 单 身 。                 |
| turn	//转变为   | The player turned professional. 这 位 球 员 转 入 职 业 。           |
| prove	//证实    | The drug proved effective. 药 证 实 有 效 。                         |
| become	//成为   | He became a big star. 他 成 了 大 明 星 。                           |
| make	//做为     | A school teacher makes a good wife. 娶 小 学 老 师 做 太 太 很 不 错 |

## S+V+O+C

S+V+O+C是用补语告诉读者受词【是】什么，中间暗示一个【是】的关系

| I find the purse pretty. 我 觉 得 这 个 包 包 很 漂 亮 。                             |
| ------------------------------------------------------------------------------------- |
| The tip made the question easy. 因 为 有 提 示 ， 所 以 问 题 变 得 很 容 易 。       |
| I consider him quite a gentleman. 我 认 为 他 是 个 十 足 的 绅 士 。                 |
| The boss found your suggestion exciting. 老 板 觉 得 你 的 提 议 很 刺 激 。          |
| The bus ride made the woman dizzy. 搭 巴                                              |
| The partnership made them friends for life. 合 夥 关 系 使 他 们 一 生 都 是 朋 友 。 |
| His poverty kept him single. 他 的 贫 穷 使 他 继 续 保 持 单 身 。                   |
| The victory made the player professional. 这 场 胜 利 使 这 位 球 员 转 入 职 业 。   |
| **The doctor proclaimed the drug effective. 医 生 宣 称 这 种 药 有 效 。**     |
| The hit movie made him a big star. 这 部 轰 动 的 电 影 使 他 成 了 大 明 星 。       |

补语可以是形容词，名词，也可以是**地方副词**

The key is there

`S	V	C`

He left the key there

`S	V	O	C`

**🌟补语和受词是平等的**

His wife gave him a jar of honey.

`S	V	O	O`

> him和honey都是give的受词

His wife called him honey.

`S	V	O	C`

> honey是him的补语，两者是平等的

# 名词片语

英语是一种拼音文字，用字尾变化表示单复数，在名词片语的开头位置还有限定词, 与名词字尾的单复数符合相呼应，共同determine名词的范围。冠词就是一种限定词

**名词片语：**限定词+形容词+名词，每一部分都可以省略，比如说: Of all the kids in my class, Jack is the smartest

**🌟省略名词**

~~一个错误例子：the + 形容词 = 复数名词~~

The rich are not always happier than the poor.

> The rich 省略了people，所以用are

The new is not always better than the old.

The unknown is often feared.

> The new thing, the unknown thing

**限定词：**分为前位、中位和后位

前位限定词与中位限定词共同使用时，前位限定词放在前面

| all       | all the girls      |
| --------- | ------------------ |
| both,     | both these cars    |
| half      | half an hour       |
| double    | double the sum     |
| twice     | twice my salary    |
| one-third | one-third the time |
| such      | such a surprise    |
| what      | what a day         |

中位限定词名词

a book，an egg，the money，this question，that man，these kids，those places，
 some time，any trouble，either bank，no problem，neither boy，every student，
 each worker，my home，your car，John's daughter

後位限定词包括所有的数目字（three,200）、序数（first,second,300th,last），以及many,much,little,few等等

## 专有名词

专名名词有唯一性，所以无需特指，不需要定冠词

> There are five Sundays this month.
>
> I have an appointment on Sunday.

❓为什么山川湖泊除外？

the Pacific(太平洋), 是因为the Pacific 是the pacific ocean，日久天长演变成 => the Pacific

the Alps => the Alp mountains

the Dead Sea

the Hilton (Hotel)希尔顿酒店

美国：the United states of America，America是专有名词，但states不是专有名词

联合国: the United Nations，nation不是专有名词

> I don't like living in America
>
> I do not like living in the U.S.

the可视为that或者those的弱化形式，有模糊的指示功能

> I need a book to read on on my trip
>
> I have finished the book you lent me

> Modern history is my favorite
>
> The history of recent China is a sorry record

> He should be home, I saw a light in his house
>
> Turn off the portal light

# 形容词

形容词是修饰名词专用的，副词是修饰名词以外的词类，包括动词，形容词和副词。也有几种特殊的副词可以修饰名词

形容词的位置有两种可能，名词片语和补语位置。还有一种特别情况，形容词放在名词后面当作后位修饰语

> extreme danger	//名词片语位置
>
> That building is 18 stories tall	//补语位置

#### **一些a- 开头的形容词**

有一批a-开头的形容词，不能放在名词片语中，只能放在补语位置。因为古英语里a-这个字首有暂时性的意味，所以不适合放在表示不变属性的位置，只能放在补语中

The patient was asleep

She was not aware of the latest development

The new students stayed aloof

The coffee kept him awake

I consider the two options alike

### 后位修饰的形容词

有一些特别的形容词，放在名词后面：

#### 1.复合名词后面

I'm meeting someone important tonight

> someone => 限定词+名词，所以形容词只能放在后面

#### 2.a- 开头的形容词

#### 3.外来语与惯用语

court martial

Secretary General

### 形容词的比较级

❓最高级要加定冠词？

Yellowstone is crowded.

`S	V	C`

Yellowstone is most crowded in summer. //夏天黄石公园的人潮最多

`S	V	C`

形容词crowded在两句中都是补语位置，因为没出现在名词片语中，自然没有冠词，再看下例：

Yellowstone is a crowded scenic spot

Yellowstone is the most crowd of the state's scenic spot

> the most crowd是the most crowd scenic spot的省略

另有一个例句：

John is the shorter of the twins

还有一个常见的误用是：~~My car is bigger than you~~, 正确使用是My car is bigger than yours

# 副词

### 方法、状态的副词

这一类副词专门修饰动词，通常在动词后，但副词是修饰语，不重要，如果句子中有受词、补语等主要元素，方法、状态副词就要向后移动。如果后移导致副词与动词距离过远，也可以将副词挪到动词前面

#### **S+V**

The dog barked furiously at the mailman

The dog furiously barked at the mailman

#### S+V+C

He stayed single reluctantly.

He reluctantly stayed single.

#### S+V+O

He openly harassed the girl sitting at the counter

### 具有强调功能的副词

有很大的弹性，名词、动词、形容词和副词都可以用它修饰，加强语气

#### 强调范围的副词

only, merely, also, especially, even, particularly

大部分放在所修饰对象的前面，少部分后面，但不能和它修饰的对象有任何距离

#### 加强语气的副词

通常放在修饰对象的前面

You are absolutely a genius!

The test is extremely difficult.

Your hair badly needs cutting.

Time passed amazingly fast.

#### 程度副词

程度副词做“有几成”的表示，把加强语气的副词去掉，只是语气变弱。把程度副词去掉，句子意思发生改变

The plan almost failed.

Your son is **quite** a man now.

> quite表示【相当高】的程度

I barely know John. //我算不上认识John

He plays the piano rather well.

> rather表示【还算高】的程度，修饰副词well

That is a fairly long story.  //那是一个蛮长的故事

### 修饰句子的副词

分为**连接副词**和**分离副词**

#### 连接副词

Sales has increased 20%. **Besides**, production cost has come down. **Consequently**, we should have no problem reaching our goals this year. **However**, projections for the next fiscal year are not very optimistic.

#### 分离副词

**Theoretically**, your proposal might work.

Your demand is **practically** impossible to meet. //程度副词，几乎差不多

Let's discuss this problem **practically**. 	//我们实际地讨论这个问题

**Practically**, you don't stand a chance in court.	//实际一点说，你上法庭根本没有机会赢

# 介系词

## 介系词片语

The company is in trouble.

名词介系词片语

I'm leaving for Hong Kong tomorrow,

动词介系词片语

The country is rich in mineral wealth.

形容词介系词片语

## 空间介系词

表示空间的介系词可以分为点、线、面、体四个角度探讨：

### 一、点：at

The bus will stop at the dock to pick up passengers

We have arrived at our destination.

The sniper is aiming at the kidnapper.

#### 二、线：on，along

The student memorized 10 new words on his way to school.

There are many beautiful villas along the beach

I see there are three shops on the street

#### 三、面：on

I strained my eyes but couldn't see any ship on the sea.

There is a picture hanging on the wall.

The speaker is standing on the platform.

#### 四、体：in

I like to stay in my office because it's quiet there.

We'll go our separate ways, and meet at my office at there.

I think I'll walk; There are too many cars in the street.

另外，Except for只能用于句首

# 时态

## 未来时态

- John will leave tomorrow		//语气最弱
- John is going to leave tomorrow
- John is to leave tomorrow
- John leaves tomorrow   //语气最强

🌟**When解释为当...时候，If解释成如果，这两种连接词引导的子句要用现在式代替未来式**

- When he gets there, the police will be waiting
- If he gets to the hospital in time, he will be able to see his father for the last time

### 未来完成时

- Next April, I will have worked here for 20 years.
- Come back at 5, your car will have been fixed by then.

# 语气助动词

must	will	would	should	can	could	may	might	ought to	have to	used to	need	had better	would rather

**语气助动词的两种用法：**

1. We must be very careful
2. John must be very careless

第一句，表达义务、意愿、责任、能力，是语气助动词的一般用法

第二句，用来做猜测，是猜测用法

## 一般用法

We must all keep together.

I will have my own way.

Shall we go now?

You may go now. //你们现在可以走了(表准许，许可)

## 一般用法的过去拼法+完成式

一、相对过去的时间

二、非事实语气

I would have left the country if I had known what was going to happen.

You should have attended the meeting last week, why didn't you?

With a little help, I could have finished a lot sooner.

Under a more reasonable boss, I might have done a much better job.

## 猜测用法

will, can, may表示相对有把握的猜测

would, could, might表示相对没有把握的猜测

Listen! It must be be raining hard outside.

It must have rained last night. The ground is still wet.

That will be George at the door, I expect.

You will all have heard the news last night.

This seat would be mine, right?

We should be on the right track.

He should have left. His car is gone.

Where can he be at this hour?

Where can he have gone?

Of course, I could be wrong.

The police is here; Something could have gong wrong.

Don't go near that snake; It may be dangerous.

Bring a compass; You might get lost in the woods.

## 其他助动词

Do you have to leave now?

I don't have to leave now.

He used to smoke a pipe.

Did he use(used) to smoke a pipe?

He used not to smoke a pipe.

He didn't use(used) to smoke a pipe.

Would you rather stay here?

I would rather not talk about it.

# 祈使语气

🌟**表示【希望成真，但尚未成真】**

间接命令句：

`The court demands that the witness leave the courtroom.`

如果法官直接对证人说，那句话是 `"(You must)Leave the courtroom."`

这是直接命令句，可经过第三者转述，语句不是You了，但仍然用动词原形

There is a strong expectation among the public that someone take responsibility for the disaster.

这是一个期望，还不是事实。所以是祈使语句，用动词原形

其他句式，诸如：

- It's necessary that...
- I insist that...

都是间接命令句: It's necessary that the letter be sent out before noon.

# 过去时态

**🌟过去分词有【完成】暗示性**

- I can't find my wallet. It's **gone**.
- The leaves are all **fallen**, now that winter is here.
- I'm **done**. It's all yours.

🌟**表示【感觉】的分词**

disappoint, satisfy, surprise, amaze, astonish, scare, terrify please, tire, exhaust

- He is disappointed **by** his scores.
- He is disappointed **with** his scores.
- He is unhappy **with** his scores.
- I am satisfied **with** the progress.
- He was surprised **at** the result.
- We were all amazed **at** his improvements.
- He is scared of losing his girlfriend. 他 很 怕 失 去 他 的 女 友 。
- The coach is pleased with our performance. 教 练 对 我 们 的 表 现 感 到 很 高 兴 。
- The boss is tired of his excuses. 老 板 对 他 的 藉 口 感 到 很 烦 。

# 现在分词

🌟**表示【感觉】的分词**

许多表示「 令 人 感 觉 如 何 」的形容词都是用现在分词来表示:

- The progress is satisfying to me.
- He finds the result surprising.
- His improvements were amazing to all of us.
- The idea of losing his girlfriend is scaring to him.
- Our performance is pleasing to the coach.
- The boss finds his excuses rather tiring.

## 合句

We are sitting here in the sun, and we can see snow-covered hills.

=> As we sit here in the sun, we can see snow-covered hills.

=> Sitting here in the sun, we can see snow-covered hills.

Everyone knew it, but no one admitted it.

=>Everyone knew, but no one admitted it.

# 名词子句

**名词子句的种类：**

- 由直述句改造的名词子句

  I believe that he didn't take the money
- 由疑问句改造的名词子句

  - Information Question

    I never asked what happened at his party
  - Yes/No Question

    By looking at the sky, I can tell whether it is going to rain(or not)

## 一、主词位置

### That子句

That he didn't show up on time is strange

It's strange that he didn't show up on time

### Wh- 子句

It's a big mystery where he is hiding now.

> It当作主词位置，避免头重脚轻

## 二、动词的受词位置

### That子句

The defendant said that he didn't do it.

I find it strange that he didn't show up on time.

### Wh- 子句

He explained why he had bought do much of that stock.

## 三、补语位置

### That子句

The important thing is that we are all right.

### Wh-子句

We all know what happed. The question is this. Can we do anything about it?

=>The question is whether we can do anything about it or not.

## 四、同位语位置

### That子句

The story that he once killed a man might just be true.

### Wh-子句

The question why the dinosaurs died out may never be answered.

## 五、介系词的受词位置

My response depends on what he really meant by that?

# 副词子句

### 一、时间、地方

He became more frugal **after** he got married.

I will be waiting for you **until** you are married.

It's all over **when** I got there.

A small town grow **where** three roads met. //一个小镇在三叉路口发展出来

> 【在...地方】的连接词where构成副词子句，修饰主要子句动词grew的地方

### 二、条件

**If** he calls, I'll give him the message.

He won't have it his way, **as long as** I'm here.

**Suppose** you were ill, where would you go?

### 三、因果

**As** there isn't much time left, we might as well call it a day.

There is nothing to worry about, **now that** Father is back.

The mother locked the door from the outside, **so that** the kids couldn't get out when they saw fire.

### 四、目的

I have typed out the main points in bold face, **in order that** you won't miss them.

I have underlined the key points, **lest** you miss them.

### 五、让步

**Although** you may object, I must give it a try.

**While** the disease is not fatal, it can be very dangerous.

**Whether (No matter)** you agree or not, I want to give it a try.

**Whoever(No matter who)** calls, I won't answer.

**Whichever(No matter which)** way you go, I will follow.

**Wherever(No matter where)** he is, I will get him!

# 关系子句

一、两个子句要有交集

二、把交集点改成关系词(Wh-），让它产生连接词的功能

For a boyfriend, I'm looking for a man who is tall and rich.

I know something.

Nobody else knows it.

=> I know something which nobody else know

> 将it改成关系代名词Which

**关系代名词：**who, which, that

**关系副词：**when, where, how, why

关系子句如果具有指出「哪一个」的功能，我们称为具有指示性。如果关系子句具有指示性，关系代名词who或which才能够改成that。反之，如果关系子句并不具有指出「哪一个」的功能，而只是个可有可无的补充说明（一般文法书所谓的非限制用法nonrestrictiveusage），那麽这个关系子句通常要在前後加上一对逗点（也有可能是前面加逗点、後面刚好碰上句尾的句点之类）。这一对逗点可以视为一对括弧看待。关系子句放在括弧里面，表示这个关系子句不重要、只是个可有可无的补充说明，同时也表示这个关系子句不具有指示功能。这种关系子句，关系代名词只能用who或which，不能改为that

I met a friend who(that) went to the same high school with me.

I met Jack, who went to the same high school with me.

Jack is a man.

I trust his judgment.

=> Jack is a man whose judgment I trust

I saw a mountain.

Its top was completely covered in snow.

=>I saw a mountain of which the top was completely covered in snow.

**🌟何时使用that？**

Man is an animal that is capable of reason.

先行词 animal 本来可以代表任何一种动物，范围极大。後面加上一个条件：The animal is capable of reason（有理性能力的那种），明确指出是「哪种动物」才能算人，具有指示的功能。所以关系代名词可以选择不用which而借用that。

另外有些情况，因为指示的要求更强烈，一般都应该选择用that，如果用who或which就不大恰当。这些情况包括：

一、先行词是all，anything，everything之类标示明确范围的字

二、先行词有first、next、last、only等限定词

Money is the only thing that interests him.

He is the best man that I can recommend.

Armstrong was the first man that set foot on the moon .

**🌟何时不使用that？**

I like The Da Vinci Code, which many people like too.

先行词是书名，子句属于补充说明的性质

### **复合关系代名词**

what, whatever, whichever, whoever

#### 一、what

因为先行词是thing，这种无意义的词汇

I have the thing you need.

=>I have what you need.

#### 二、whatever

如果先行词是anything，就可以用whatever省略先行词

Anything that you say now may be used against you in court.

=>Whatever you say now...

#### 三、Whichever

如果是任性哪一个，就可以用whichever

Of these cars, you can take any one that you like.

=>Of these cars, you can take whichever you like.

#### 四、Whoever

I will shoot any person!

He moves.

=>I will shoot whoever moves

I will give 100 dollars to any person that returns first.

=>I'll give 100 dollars to whoever returns first.

### 关系副词

**when(at that time)**

The rain came at a time the farmers needed it most.

=>The rain came when the farmers needed it most.

I need some time.

I can be with my daughter.

=>I need some time I can be with my daughter.

I know the time.

He will arrive then.

=>I know the time when he will arrive.

**where(in there)**

The car stopped at a place.

Three roads met there.

=>The car stopped at a place where three roads met.

=>The car stopped at a place three road met.

**How**

I never found out the way (how) he escaped.

**why**

I have forgotten the reason (why) I called.

The best museum in Paris is the Louvre, where you can see many priceless treasures.

**罗 浮 宫 是 专 有 名 词 ， 已 无 法 进 一 步 指 认 ， 所 以 关 系 子 句 要 用 括 弧 性 的 逗 点 隔 开 ， 当 作 补 充 说 明**
