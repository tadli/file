# 前端工程化和webpack

> 什么是前端工程化？

> webpack是什么？

前端工程化的解决方案。提供前端模块化支持、代码压缩混淆、浏览器兼容性、性能优化等功能。

## webpack

### 创建项目

- 生成package.json：`sudo npm init -y`	 

- 下载jQuery： `sudo npm install jquery -S`   	*-S代表下载后，将jquery记录在package.json的dependency中，无论是开发还是部署都依赖的dependency*

- 在index.js导包：`import $ from 'jquery'`	*ES6导包语法*

### 导入并配置webpack

- 导入webpack：`sudo npm i webpack@5.42.1 webpack-cli@4.7.2 -D`	*将依赖写入devDependencies，开发阶段的依赖*

- 创建webpack配置文件：`webpack.development.config.js`		*webpack的配置文件，手动创建*

```js
module.exports = {
    mode: 'development',
};

module.exports = {
    mode: 'production',
};
```

- 修改npm运行脚本，调用webpack打包：`package.json`	*更改script，用来运行webpack*

```json
  "scripts": {
    "dev": "webpack --config webpack.development.config.js",
    "prod": "webpack --config webpack.prodcution.config.js"
  },
```

`sudo npm run dev`	*run webpack*

```json
asset main.js 88.5 KiB [emitted] [minimized] (name: main) 1 related asset
runtime modules 663 bytes 3 modules
cacheable modules 287 KiB
  ./src/index.js 135 bytes [built] [code generated]
  ./node_modules/jquery/dist/jquery.js 287 KiB [built] [code generated]
```

- 在项目中引入打包后的js：`<script src="../dist/main.js"></script>`	*在HTML中这么引入webpack转化的JS*

- webpack配置打包：`src -> index.js`		*默认的打包入口文件*

​										`dist -> main.js` 	"默认的输出文件路径"

```json
const path = require('path')

module.exports = {
    mode: 'development',
    entry: './src/index1.js',
    output: {
        //必须使用absolute path
      	//path属性，path模块
        //__dirname指根目录
        path: path.join(__dirname,"dist"),
        filename: "webpack.js"
    }
};
```

### 插件webpack-dev-server

> 作用

自动打包，将js存储在内存中，存储位置为`localhost:8080/src`



- 下载webpack-dev- server：`npm install webpack-dev-server@3.11.2 -D`
- 修改packjson的script脚本

```json
"scripts": {
    "build": "webpack serve --config webpack.development.config.js"
  },
```

- 配置

```json
module.exports = {
    mode: 'development',
    entry: './src/index.js',
    output: {
        path: path.join(__dirname,"dist"),
        filename: "webpack.js"
    }
};
```



- 运行：`npm run build`
- 访问：http://localhost:8080/
- 更改html的script引用



### 插件html-webpack-plugin

> 作用

复制`index.html`到内存中，节省时间



- 下载：`npm install html-webpack-plugin@5.3.2 -D`
- 配置

```json
const path = require('path')
const HtmlPlugin = require('html-webpack-plugin')

const htmlPlugin = new HtmlPlugin({
    template: './src/index.html',
    filename: './index.html'
})

module.exports = {
    mode: 'development',  	//打包入口
    entry: './src/index.js', 	//打包后的JS文件名称
    output: {
        path: path.join(__dirname,"dist"),//必须使用absolute path，__dirname指根目录
        filename: "webpack.js"
    }
    plugins: [htmlPlugin]
};
```

### 升级配置dev-server

> 作用

build后自动打开网页，并且更换host和port

- 配置

```json
module.exports = {
    devServer: {
        open:true, //首次打包成功后，自动打开浏览器
        port:80,//http协议中，如果端口号是80，则可以省略
        host:'127.0.0.1'//将host从localhost更改为127.0.0.1
    }
};
```



### loader

> 作用

webpack只能打包JS文件，而非JS的模块需要调用loader加载器才能正常打包

`css-loader` 打包CSS

`less-loader` 打包Less

`babel-loader`打包webpack无法处理的高级JS语法



**webpack打包流程：**

- ​	JS模块->包含高级语法->配置了Babel->调用loader处理
- ​	JS模块->不包含高级语法->webpack处理
- 非JS–>配置了对应loader，调用loader处理
- 非JS->没配置->报错



#### 配置css-loader

- 下载：`npm install style-loader@3.0.0 css-loader@5.2.6 -D `
- 配置

```json
module: {//所有第三方文件模块的匹配规则
    rules: [//文件后缀名的匹配规则
        {test: /\.css$/, use: ['style-loader', 'css-loader']}
    ]
}
```

- webpack默认打包JS文件
- JS文件中`import './css/index.css'`
- webpack调用相应的loader，先调用css-loader，将处理结果转交给style-loader
- webpack吧style-loader处理的结果，合并到webpack.js中



#### 配置less-loader

- 下载 `npm install less-loader@10.0.1 less@4.1.1 -D`
- 配置modules->module->rules`{test: /\.css$/, use: ['style-loader', 'css-loader','less-loader']}`



#### 配置url-loader

> **作用**：将图片转化成base64，节省网络资源

- `sudo npm install url-loader@4.1.1 file-loader@6.2.0 -D`
- 配置modules->module->rules`{test: /\.jpg|png|gif$/, use:'url-loader?limit=22229'}`，limit指定图片大小，单位是字节

## webpack面试题

**webpack与gulp，grunt的区别？**

三者都是前端构建工具，grunt和gulp在早期比较流行，现在webpack相对来说比较主流，不过一些轻量化的任务还是会用gulp来处理，比如单独打包CSS文件等。grunt和gulp是基于任务和流（Task、Stream）的。类似jQuery，找到一个（或一类）文件，对其做一系列链式操作，更新流上的数据，整条链式操作构成了一个任务，多个任务就构成了整个web的构建流程。webpack是基于入口的。webpack会自动地递归解析入口所需要加载的所有资源文件，然后用不同的Loader来处理不同的文件，用Plugin来扩展webpack功能。

所以，从构建思路来说，gulp和grunt需要开发者将整个前端构建过程拆分成多个`Task`，并合理控制所有`Task`的调用关系；webpack需要开发者找到入口，并需要清楚对于不同的资源应该使用什么Loader做何种解析和加工对于知识背景来说，gulp更像后端开发者的思路，需要对于整个流程了如指掌webpack更倾向于前端开发者的思路