# AMQP和JMS

现在实现MQ的有两种主流方式：AMQP、JMS

- JMS是定义了统一的接口，来对消息操作进行统一；AMQP是通过规定协议来统一数据交互的格式

- JMS限定了必须使用Java语言；AMQP只是协议，不规定实现方式，因此是跨语言的。

  JMS产品：ActiveMQ，RocketMQ

  AMQP产品：RabbitMQ



生产者发送消息流程：

1、生产者和Broker建立TCP连接。

2、生产者和Broker建立通道。

3、生产者通过通道消息发送给Broker，由Exchange将消息进行转发。

4、Exchange将消息转发到指定的Queue（队列）

（**Exchange（交换机）只负责转发消息，不具备存储消息的能力**，因此如果没有任何队列与Exchange绑定，或者没有符合路由规则的队列，那么消息会丢失！）

消费者接收消息流程：

1、消费者和Broker建立TCP连接

2、消费者和Broker建立通道

3、消费者监听指定的Queue（队列）

4、当有消息到达Queue时Broker默认将消息推送给消费者。

5、消费者接收到消息。

6、ack回复

(当消费者获取消息后，会向RabbitMQ发送回执ACK，告知消息已经被接收。不过这种回执ACK分两种情况：

- 自动ACK：消息一旦被接收，消费者自动发送ACK
- 手动ACK：消息接收后，不会发送ACK，需要手动调用)



# Work消息模式

又叫做**竞争消费者模式**

它提供两种模式：

- 轮询模式，每一个消费者消费相同数量的消息
- 公平模式（fair dispatch），通过channel的basicQos设置prefetchCount为1.这样，在接收到该Consumer的ack前，他它不会将新的Message分发给它。



**如何避免消息堆积？**

1） 采用workqueue，多个消费者监听同一队列。

2）接收到消息以后，而是通过线程池，异步消费。

**如何避免消息丢失？**

1） 消费者的ACK机制。可以防止消费者丢失消息。

但是，如果在消费者消费之前，MQ就宕机了，消息就没了？

2）可以将消息进行持久化。要将消息持久化，前提是：队列、Exchange都持久化



```java
@RabbitListener(
    bindings = @QeueueBinding(
    	value = @Queue(value = "queue1", durable = true),
        exchange = @Exchange(
        	value = "amq.topic",
            type = ExchangeTypes.Topic)),
	key = {"com.#"}
)
```



**为什么要使用消息队列？**

**解耦：**引入消息队列之前，下单完成之后，需要订单服务去调库存服务减库存，调用营销服务增加营销数据，引入消息队列之后，下游服务自己监听队列就行了

**异步**




# 消息分发策略

发布订阅

轮询分发

公平分发（能者多劳）

重发

消息拉取



# 命令行

```shell
#start a server
rabbitmq-server

#enable web_management plugin
rabbitmq-plugins enable rabbitmq_management

#add a user
rabbitmqctl add_user <username> <password>

#grant 
rabbitmqctl set_user_tags <username> administrator

#change password
rabbitmqctl change_password <username> <newpassword>

#user list
rabbitmqctl list_users
```



# 组成部分

![image-20220731223024507](/Users/xiaobing/Library/Application Support/typora-user-images/image-20220731223024507.png)

`producer`

`connection(channel\channel\channel)`

`broker{`

​	`virtual host{`

​		`Exchange–>(RouteKey/Bindings)->Queue`

​	`}`

`}`

`connection(channel\channel\channel)`

`consumer`

**核心概念：**

**`broker：`**提供服务

**`connection:`**连接，应用程序与broker的网络连接，TCP三次握手/四次握手

**`channel:`**TCP连接是断连接，而channel是长连接

**`virtual host：`**虚拟地址，用于逻辑隔离

**`exchange:`**接收消息，发送消息

**`bindings:`**exchange与queue之间的虚拟连接，binding中可以保存多个routing key

**`routing key:`**是路由规则



# 简单模式

![Screen Shot 2022-07-31 at 7.51.23 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-31 at 7.51.23 PM.png)



![Screen Shot 2022-07-31 at 7.56.12 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-31 at 7.56.12 PM.png)



![Screen Shot 2022-07-31 at 7.57.13 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-31 at 7.57.13 PM.png)





# 发布/订阅模式（fanout）

![Screen Shot 2022-07-31 at 7.58.23 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-31 at 7.58.23 PM.png)



![Screen Shot 2022-07-31 at 8.00.29 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-31 at 8.00.29 PM.png)

![Screen Shot 2022-07-31 at 8.01.20 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-31 at 8.01.20 PM.png)

![Screen Shot 2022-07-31 at 8.01.44 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-31 at 8.01.44 PM.png)



# 路由模式（direct）

![Screen Shot 2022-07-31 at 8.02.51 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-31 at 8.02.51 PM.png)



![Screen Shot 2022-07-31 at 8.06.55 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-31 at 8.06.55 PM.png)

![Screen Shot 2022-07-31 at 8.07.19 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-31 at 8.07.19 PM.png)



![Screen Shot 2022-07-31 at 8.07.44 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-31 at 8.07.44 PM.png)



# Topics模式

![Screen Shot 2022-07-31 at 8.11.01 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-31 at 8.11.01 PM.png)

![Screen Shot 2022-07-31 at 8.11.16 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-31 at 8.11.16 PM.png)



#代表0，1，多个

结果：只有queue1收到了消息

![Screen Shot 2022-07-31 at 8.13.48 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-31 at 8.13.48 PM.png)





# Header模式



![Screen Shot 2022-07-31 at 8.16.15 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-31 at 8.16.15 PM.png)





# 发布/订阅模式实现

## **生产者**

```java
@Configuration
public class RabbitConfig {
    //声明注册fanout模式的交换机
    //声明队列
    //声明绑定
    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange("amq.fanout", true, false);
    }

    @Bean
    public Queue smsQueue() {
        return new Queue("queue1", true);
    }

    @Bean
    public Queue emailQueue() {
        return new Queue("queue2", true);
    }

    @Bean
    public Queue duanxinQueue() {
        return new Queue("queue3", true);
    }

    @Bean
    public Binding binding1() {
        return BindingBuilder.bind(smsQueue()).to(fanoutExchange());
    }

    @Bean
    public Binding binding2() {
        return BindingBuilder.bind(emailQueue()).to(fanoutExchange());
    }

    @Bean
    public Binding binding3() {
        return BindingBuilder.bind(duanxinQueue()).to(fanoutExchange());
    }
}
```



```java
@Service
public class OrderService {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void makeOrder(){
        String orderId = UUID.randomUUID().toString();
     
        //交换机、路由/队列名称、消息
        String exchangeName = "amq.fanout";
        String routingKey = "";
        rabbitTemplate.convertAndSend(exchangeName, routingKey, orderId);
    }
}
```

## 消费者

```java
@Service
@RabbitListener(queues = {"queue3"})
public class ConsumerService {
		
  	//message是自动注入
    @RabbitHandler
    public void receiveMessage(String msg){
        System.out.println(">>>>>>>>>>>>>"+msg);
    }
}
```

# Direct实现

```java
package com.test.mq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {
    //声明注册fanout模式的交换机
    //声明队列
    //声明绑定
    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("amq.direct", true, false);
    }

    @Bean
    public Queue smsQueue() {
        return new Queue("queue1", true,false,false);
    }

    @Bean
    public Queue emailQueue() {
        return new Queue("queue2", true,false,false);
    }

    @Bean
    public Queue duanxinQueue() {
        return new Queue("queue3", true,false,false);
    }

    @Bean
    public Binding binding1() {
        return BindingBuilder.bind(smsQueue()).to(directExchange()).with("student");
    }

    @Bean
    public Binding binding2() {
        return BindingBuilder.bind(emailQueue()).to(directExchange()).with("teacher");
    }

    @Bean
    public Binding binding3() {
        return BindingBuilder.bind(duanxinQueue()).to(directExchange()).with("class");
    }
}
```



# TTL

**对整个队列的消息设置TTL**

```java
@Bean
public Queue ttlQueue() {
    Map<String, Object> args = new HashMap<>();
    args.put("x-message-ttl", 6000);
    return new Queue("queue4", true, false, false,args);
}
```

**对单独一条消息设置TTL**

```java
public void makeOrder() {
  String orderId = UUID.randomUUID().toString();

  //交换机、路由/队列名称、消息
  String exchangeName = "amq.direct";
  String routingKey = "teacher";
  MessagePostProcessor messagePostProcessor = new MessagePostProcessor() {
      @Override
      public Message postProcessMessage(Message message) throws AmqpException {
          message.getMessageProperties().setExpiration("5000");
          message.getMessageProperties().setContentEncoding("UTF-8");
          return message;
      }
  };
  rabbitTemplate.convertAndSend(exchangeName, routingKey, orderId, messagePostProcessor);
  }
```

如果设置了队列的ttl，也设置了消息的ttl，那么以最短的那个为准

消息过期后，直接消息

过期队列里的消息过期后，消息被移入死信队列

# 死信队列

转入死信队列的消息：

- 消息被拒绝
- 消息过期
- 队列达到最大长度

建立正常交换机，正常队列

正常队列绑定**死信交换机**和**死信队列的路由规则**

```java
/**
死信交换机
死信队列
绑定，设置routing key
*/
@Bean
public DirectExchange deadDirectExchange() {
    return new DirectExchange("dead.direct", true, false);
}

@Bean
public Queue deadQueue() {
    return new Queue("queueDead", true, false, false);
}

@Bean
public Binding deadBinding() {
    return BindingBuilder.bind(deadQueue()).to(deadDirectExchange()).with("dead");

}

/**
正常交换机
正常队列，设置过期时间，设置死信交换机，设置路由规则
绑定，设置routing key
*/

@Bean
public DirectExchange normalDirectExchange() {
    return new DirectExchange("amq.direct", true, false);
}

@Bean
public Queue normalQueue() {
    Map<String, Object> map = new HashMap<>();
    map.put("x-message-ttl", 5000);
    map.put("x-dead-letter-exchange", "dead.direct");
    map.put("x-dead-letter-routing-key", "dead");
    return new Queue("queueNormal", true, false, false, map);
}

@Bean
public Binding normalBinding() {
    return BindingBuilder.bind(normalQueue()).to(normalDirectExchange()).with("normal");

}
```



# 集群搭建

**普通集群：**

普通集群模式，就是将 RabbitMQ 部署到多台服务器上，每个服务器启动一个 RabbitMQ 实例，多个实例之间进行消息通信。

创建的队列 Queue，它的元数据（主要就是 Queue 的一些配置信息）会在所有的RabbitMQ 实例中进行同步，但是队列中的消息只会存在于一个 RabbitMQ 实例上，而不会同步到其他队列。

消费消息的时候，如果连接到了另外一个实例，那么那个实例会通过元数据定位到 Queue 所在的位置，然后访问 Queue 所在的实例，拉取数据过来发送给消费者。

这种集群可以提高 RabbitMQ 的消息吞吐能力，但是无法保证高可用，因为一旦一个 RabbitMQ 实例挂了，消息就没法访问了，如果消息队列做了持久化，那么等 RabbitMQ 实例恢复后，就可以继续访问了；如果消息队列没做持久化，那么消息就丢了。

**镜像集群：**

它和普通集群最大的区别在于 Queue 数据和原数据不再是单独存储在一台机器上，而是同时存储在多台机器上。也就是说每个 RabbitMQ 实例都有一份镜像数据（副本数据）。每次写入消息的时候都会自动把数据同步到多台实例上去，这样一旦其中一台机器发生故障，其他机器还有一份副本数据可以继续提供服务，也就实现了高可用



# MQ解决分布式事务（**采用最终一致性原理**）

**1、确认生产者一定要将数据投递到MQ服务器中（采用MQ消息确认机制）**

**2、MQ消费者消息能够正确消费消息，采用手动ACK模式（注意重试幂等性问题）**

```yaml
spring:
  # 项目名称
  application:
    name: rabbitmq-consumer
  # RabbitMQ服务配置
  rabbitmq:
    host: 127.0.0.1
    port: 5672
    username: guest
    password: guest
    listener:
      simple:
        # 重试机制
        retry:
          enabled: true #是否开启消费者重试
          max-attempts: 5 #最大重试次数
          initial-interval: 5000ms #重试间隔时间（单位毫秒）
          max-interval: 1200000ms #重试最大时间间隔（单位毫秒）
          multiplier: 2 #间隔时间乘子，间隔时间*乘子=下一次的间隔时间，最大不能超过设置的最大间隔时间
```



- 可靠的消息发送
  - 订单系统，为确保数据一定发送到MQ，在同一事务中，增加一个记录表的操作，记录`每一条发往MQ的消息的ID，内容，以及发送状态`
  - 利用RabbitMQ的确认机制，开启后，MQ受理消息会返回回执，在配置中`publisher-confirms: true`
  - 得到回执
- 可靠的消息消费
  - 要防止消息重复处理，接口要幂等性，一次用户操作，只对应一次数据处理
  - 手动ACK模式
  - 消费者处理失败，需要MQ重发

![Screen Shot 2022-08-01 at 9.26.46 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-08-01 at 9.26.46 PM.png)



## **为什么要使用RabbitMQ？**

- 实现生产者都消费者的解耦
- 实现异步，后台处理请求
- 拥有持久化的机制，消息可以被保存下来



## **如何避免重复消费？**

在消息生产时，MQ内部针对每条生产者发送的消息生成一个inner-msg-id，作为去重的依据

在消息消费时，要求消息体中必须要有一个 bizId（对于同一业务全局唯一，如支付ID、订单ID、帖子ID 等）作为去重的依据，避免同一条消息被重复消费

**如何确保消息不丢失？**

==对于消息的可靠性传输，每种MQ都要从三个角度来分析：生产者丢数据、消息队列丢数据、消费者丢数据==



**生产者丢数据**

- 事务机制

```java
/*如果发送过程有什么问题，可以回滚*/
channel.txSelect();
channel.txRollback();
channel.txCommit();
```

- 确认机制

```java
/*
- 将信道设置成`confirm`模式，则所有在信道上发布的消息都会被指派一个唯一的ID
- 一旦消息被投递到目的队列后，或者消息被写入磁盘后（可持久化的消息），信道会发送一个`confirm`给生产者（包含消息唯一 ID）
- 如果 RabbitMQ发生内部错误从而导致消息丢失，会发送一条`nack`消息
*/

channel.addConfirmListener(new ConfirmListener()){
    public void handleAck(long deliveryTag, boolean multiple){...}
    public void handleNack(long deliveryTag, boolean multiple){...}
}
```

**消息队列丢数据**

- 消息持久化，前提是队列必须持久化

- RabbitMQ确保持久性消息能从服务器重启中恢复的方式是，将它们写入磁盘上的一个持久化日志文件，当发布一条持久性消息到持久交换器上时，Rabbit会在消息提交到日志文件后才发送响应



**消费者丢数据**

消费者丢数据一般是因为采用了<u>*自动确认消息模式*</u>。该模式下，虽然消息还在处理中，但是消费中者会自动发送一个确认，通知 RabbitMQ 已经收到消息了，这时 RabbitMQ 就会立即将消息删除。这种情况下，如果消费者出现异常而未能处理消息，那就会丢失该消息。

- 默认是自动ACK

- 手动ACK

  ```java
  channel.basicAck(message.getMessageProperties().getDeliveryTag(),false,false);
  ```



## **RabbitMQ的集群**

==镜像集群模式==

创建的queue，无论元数据还是queue里的消息都会存在于多个实例上，然后每次你写消息到queue的时候，都会自动把消息到多个实例的queue里进行消息同步。







## **AMQP模型的几大组件？**

\- 交换器 (Exchange)：消息代理服务器中用于把消息路由到队列的组件。

\- 队列 (Queue)：用来存储消息的数据结构，位于硬盘或内存中。

\- 绑定 (Binding)：一套规则，告知交换器消息应该将消息投递给哪个队列。







## **导致死信的几种原因？**

\- 消息被拒（`Basic.Reject /Basic.Nack`) 且 `requeue = false`。

\- 消息TTL过期。

\- 队列满了，无法再添加。







## **延迟队列？**

存储对应的延迟消息，指当消息被发送以后，并不想让消费者立刻拿到消息，而是等待特定时间后，消费者才能拿到这个消息进行消费。





## **MQ的死信队列应用场景**

1. 消息过了过期时间TTL（time to live）

2. 消息队列达到了最大长度

3. 消息被消费者拒绝（basic.reject或者basic.nack）且requeue=false
