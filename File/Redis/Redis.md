查看进程`ps -ef | grep redis`

# String

**Redis的String类型最大长度**:512M

```shell
127.0.0.1:6379> KEYS * #获得所有key
1) "age"
2) "joke"
3) "name"

127.0.0.1:6379> KEYS a* #获得以a开头的key
1) "age"

127.0.0.1:6379> DEL joke	#删除一个key
(integer) 1

127.0.0.1:6379> FLUSHALL  #清空所有库的内容
OK

127.0.0.1:6379> SET name lby	#设置key为name，value为lby
OK

127.0.0.1:6379> EXISTS name #判断key是否存在
(integer) 1

127.0.0.1:6379> EXPIRE name 10	#设置key的有效时间
(integer) 1

127.0.0.1:6379> TTL name  		#查看过期时间
(integer) 6

127.0.0.1:6379> MSET name lby age 23 	#批量添加
OK

127.0.0.1:6379> MGET name name2 	#批量获取
1) "FR"
2) "lby"

127.0.0.1:6379> SET number 1
OK

127.0.0.1:6379> INCR number		#自增
(integer) 2

127.0.0.1:6379> INCRBY number 10 #可以更改自增的步数
(integer) 12

127.0.0.1:6379> DECR number 	#自减
(integer) 11

127.0.0.1:6379> DECRBY number 10 #可以更改自增的步数
(integer) 1

127.0.0.1:6379> SET score 0.1	#浮点数
OK

127.0.0.1:6379> INCRBYFLOAT score 0.3	#浮点数必须设置增长步长
"0.4"

127.0.0.1:6379> SETNX name lby	#如果不存在，才增加key
(integer) 0

127.0.0.1:6379> SETEX joke 10 fuck		#添加数据时设置有效期
OK
127.0.0.1:6379> TTL joke
(integer) 4

127.0.0.1:6379> STRLEN name		#获得key的value的长度
(integer) 2

127.0.0.1:6379> APPEND name FR		#尾部拼接字符串
(integer) 4

127.0.0.1:6379> GET name
"FRFR"

127.0.0.1:6379> SET student:1 {"id":1,"name":"lby","age":21}
OK
```



# Hash

Hash结构：key field value

**HSCAN**，有游标的迭代，防止HGETALL导致线程阻塞的替代语法

` **HSCAN** key cursor [MATCH pattern] [COUNT count]`

```java
HSET key field value [field value ...]

127.0.0.1:6379> HSET student name lby age 23
(integer) 2
127.0.0.1:6379> HMGET student name age
1) "lby"
2) "23"
127.0.0.1:6379> HEXISTS student name
(integer) 1
127.0.0.1:6379> HLEN student
(integer) 2
127.0.0.1:6379> HKEYS student
1) "name"
2) "age"
127.0.0.1:6379> HVALS student
1) "lby"
2) "23"
127.0.0.1:6379> HSTRLEN student name
(integer) 3  
127.0.0.1:6379> HSCAN student 0 COUNT 10
  
127.0.0.1:6379> HDEL student name
(integer) 1
```

# List

双向链表结构，支持正向检索，也支持反向检索

有序

元素可以重复

插入和删除快

查询慢

常用来存储有序数据：朋友圈点赞列表，评论列表

**LREM** key count element

```shell
127.0.0.1:6379> RPUSH list 1
(integer) 1
127.0.0.1:6379> LPUSH list 1
(integer) 2
127.0.0.1:6379> LSET list 0 2
OK
127.0.0.1:6379> LRANGE list 0 -1
1) "2"
2) "1"
127.0.0.1:6379> LINDEX list 0
"2"
127.0.0.1:6379> LREM list 1 1
(integer) 1
127.0.0.1:6379> LLEN list 
(integer) 1
```

**如何用list结构模拟一个栈？**

栈：后进先出

入口出口在同一侧

`LPUSH` 和`LPOP`

**如何用list结构模拟一个队列？**

队列：先进先出

入口出门不在同一侧

`LPUSH` 和 `RPOP`

**如何用list结构模拟一个阻塞队列？**

队列：先进先出

入口出门不在同一侧

取元素：`BLPOP`或者`BRPOP`

`BLPOP`:有阻塞的取值，List里有值时，和`LPOP`一致；List没值时，等待指定时间，而不是直接返回nil

```java
127.0.0.1:6379> BLPOP people 100	#单位为秒，阻塞100秒 
```

# Set

无序

元素不可以重复

查找快

支持交集、并集、差集等功能

> sdiff key [key ...]
>
> `sdiff` 求的是在第一个集合中但不在第二个集合中的元素集

```shell
127.0.0.1:6379> SADD myset lby love fr
(integer) 3
127.0.0.1:6379> SCARD myset
(integer) 3
127.0.0.1:6379> SMEMBERS myset
1) "fr"
2) "love"
3) "lby"
127.0.0.1:6379> SSCAN myset 0 
1) "0"
2) 1) "fr"
   2) "lby"
   3) "love"
127.0.0.1:6379> SISMEMBER myset fr
(integer) 1
127.0.0.1:6379> SPOP myset 1
1) "love"
127.0.0.1:6379> SREM myset lby
(integer) 1


127.0.0.1:6379> SADD myset 1 2 3 4 5 6 7
(integer) 7
127.0.0.1:6379> SADD myset2 2 3 4 5 6 7 8 9 10
(integer) 9
127.0.0.1:6379> SDIFF myset myset2
1) "fr"
2) "1"

127.0.0.1:6379> SUNION myset myset2
 1) "8"
 2) "10"
 3) "4"
 4) "fr"
 5) "3"
 6) "6"
 7) "7"
 8) "5"
 9) "2"
10) "1"
11) "9"

127.0.0.1:6379> SINTER myset myset2
1) "4"
2) "6"
3) "3"
4) "7"
5) "5"
6) "2"
```



# SortedSet

可排序的set集合

元素不重复

查询速度快

因为有排序的特性，经常用来实现排行榜这样的功能

所有的排名默认都是升序，如果要降序，则在命令的Z后面添加REV

>  **ZSCAN** key cursor [MATCH pattern] [COUNT count]
>
> **ZCARD** key
>
>  **ZREM** key member [member ...]
>
> **ZRANGE** key start stop
>
>  **ZRANK** key member
>
> 
>
> **ZSCORE** key member
>
> **ZRANGEBYSCORE** key min max
>
> **ZCOUNT** key min max

```shell
help @sorted-set

#将班级的下列学生得分存入sorted-set里
85 Jack 89 Luck 82 Rose 95 Tom 78 Jerry 92 Amy 76 Miles
并实现下列功能：

127.0.0.1:6379> ZADD student 85 Jack 89 Luck 82 Rose 95 Tom 78 Jerry 92 Amy 76 Miles
(integer) 7

删除Tom同学
127.0.0.1:6379> ZREM student Tom
(integer) 1

获取Rose同学的排名
127.0.0.1:6379> ZREVRANK student Rose
(integer) 3

查出成绩前三名的学生
127.0.0.1:6379> ZRANGE student 0 2
1) "Miles"
2) "Jerry"
3) "Rose"
127.0.0.1:6379> ZREVRANGE student 0 2
1) "Amy"
2) "Luck"
3) "Jack"

获取Amy同学的分数
127.0.0.1:6379> ZSCORE student Amy
"94"

给Amy同学➕2分
127.0.0.1:6379> ZINCRBY student 2 Amy
"94"

查询80分以下有多少人
127.0.0.1:6379> ZCOUNT student 0 80
(integer) 2

查询80分以下的所有人
127.0.0.1:6379> ZRANGEBYSCORE student 0 80
1) "Miles"
2) "Jerry"
```



# SpringDataRedis

提供了对lettuce和jedis客户端的整合

提供了RedisTemplate统一API来操作Redis

支持Redis的发布订阅模型，支持哨兵和集群

| API                           | 返回值类型        | 说明       |
| ----------------------------- | ----------------- | ---------- |
| `redisTemplate.opsForValue()` | `ValueOperations` | 操作String |
| `redisTemplate.opsForHash()`  | `HashOperations`  | 操作Hash   |
| `redisTemplate.opsForList()`  | `ListOperations`  | 操作List   |
| `redisTemplate.opsForSet()`   | `SetOperations`   | 操作Set    |
| `redisTemplate.opsForZSet()`  | `ZSetOperations`  | 操作ZSet   |
| `redisTemplate`               |                   | 通用的命令 |

## Demo

```
<dependency>
    <groupId>org.apache.commons</groupId>
    <artifactId>commons-pool2</artifactId>
    <version>2.11.1</version>
</dependency>
```

```yaml
#配置redis连接池
spring:
  redis:
    host: 1.15.114.38
    password: P@ssword01
    port: 6379
    lettuce:
      pool:
        max-active: 8
        max-idle: 5
        min-idle: 3
        max-wait: 30
```

```java
  @Autowired
  private RedisTemplate<String, String> redisTemplate;

  @Test
  void contextLoads() {
      redisTemplate.opsForValue().set("FR", "GOOD!");
      String s = redisTemplate.opsForValue().get("FR");
      System.out.println(s);
  }
```

## 序列化

为了节省内存，不使用JSON序列化器来处理value，而是统一使用String序列化器，手动完成对象的序列化和反序列化——`StringRedisTemplate`——Spring默认提供的类

```java
private ObjectMapper mapper = new ObjectMapper();

@Test
void testUser() throws JsonProcessingException {
    User lby = new User("lby", 23);
    String lbyJson = mapper.writeValueAsString(lby);
    redis.opsForValue().set("user:1", lbyJson);
    String lbyJson2 = redis.opsForValue().get("user:1");
    User user = mapper.readValue(lbyJson2, User.class);
}

@Test
void testHash(){
    redis.opsForHash().put("User:100", "name", "fr");
    redis.opsForHash().put("User:100", "age", "24");
    Map<Object, Object> entries = redis.opsForHash().entries("User:100");
    System.out.println(entries);//{name=fr, age=24}
}
```

# 短信登录

**发送短信验证码：**

- 提交手机号
- 校验手机号格式
- 生成验证码
- 保存验证码在Redis中
- 发送验证码给用户（不需要实现）

  

**Key的选择**：

- Key的选择要有唯一性
- Key的选择要可以很容易被取出来
- 【Key：手机号】【Value：验证码 】

```java
@PostMapping("/code")
public Result sendCode(@RequestParam("phone") String phone, HttpSession session) {
    // 发送短信验证码并保存验证码
    return userService.sendCode(phone, session);
}

//ServiceImpl
@Override
public Result sendCode(String phone, HttpSession session) {
    // 1.校验手机号
    if (RegexUtils.isPhoneInvalid(phone)) {
        //2.如果不符合，返回错误信息
        return Result.fail("手机号格式错误");
    }
    //3.符合，生成验证码
    String code = RandomUtil.randomNumbers(6);
    //4.保存验证码到redis,设置有效时间2分钟
    stringRedisTemplate.opsForValue().set(LOGIN_CODE_KEY + phone, code, LOGIN_CODE_TTL, TimeUnit.MINUTES);
    //5.发送验证码
    log.debug("发送验证码成功：{}", code);
    //6.返回ok
    return Result.ok();
}
```



**短信验证码登录、注册：**

- 用户提交手机号和验证码
- 校验验证码，以【Key：手机号】查询Redis
- 根据手机号查询用户，如果用户存在，则将用户保存进Redis（方便接下来的用户校验，当作登录凭证）



**Redis的使用：**

- 保存一个用户对象
- 采用Hash结构（Value值为Map结构，每个字段独立存储，可以针对单个字段做CRUD）
  - Key  field value
- 【Key：随机生成的Token】【Value：User的信息】（前端请求时，请求头携带Token，拦截器验证时从Redis里取用户信息）

```java
/**
 * 登录功能
 *
 * @param loginForm 登录参数，包含手机号、验证码；或者手机号、密码
 */
@PostMapping("/login")
public Result login(@RequestBody LoginFormDTO loginForm, HttpSession session) {
    //  实现登录功能
    return userService.login(loginForm, session);
}

@Override
public Result login(LoginFormDTO loginForm, HttpSession session) {
    //1.校验手机号
    String phone = loginForm.getPhone();
    if (RegexUtils.isPhoneInvalid(phone)) {
        //2.如果不符合，返回错误信息
        return Result.fail("手机号格式错误");
    }
  
    //3.从redis中获取验证码，校验验证码
    String cacheCode = stringRedisTemplate.opsForValue().get(LOGIN_CODE_KEY + phone);
    String code = loginForm.getCode();
    if (!Objects.equals(cacheCode, code)) {
        //4.验证码不一致，报错
        return Result.fail("验证码错误");
    }
  
    //5.一致，根据手机号查用户 select * from tb_user where phone = ?
    User user = query().eq("phone", phone).one();
  
    //6.判断用户是否存在
    if (user == null) {
        //7.不存在，创建新用户并且保存
        user = createUserWithPhone(phone);
    }
  
    //8.保存用户信息到redis中
    //8.1 生产随机token,作为登入令牌
    String token = UUID.randomUUID().toString();
    //8.2将User对象转为hash储存
    UserDTO userDTO = BeanUtil.copyProperties(user, UserDTO.class);
    //8.3将userDTO转为Map一次存储到redis
    Map<String, Object> userMap = BeanUtil.beanToMap(userDTO, new HashMap<>(), CopyOptions.create()
            .setIgnoreNullValue(true)
            .setFieldValueEditor((fieldName, fieldValue) -> fieldValue.toString()));
    //8.4存储
    stringRedisTemplate.opsForHash().putAll(LOGIN_USER_KEY + token, userMap);
    //8.5设置有效时间
    stringRedisTemplate.expire(LOGIN_USER_KEY, LOGIN_USER_TTL, TimeUnit.MINUTES);
    //返回token
    return Result.ok(token);
}
```

有效时间：

- 用户有操作，则更新有效时间
- 所以需要拦截器A，拦截一切请求，更新Token有效期
- 因为拦截器A会放行所有请求，所以需要拦截器B，拦截需要登录才能实现的请求，确保Token里有用户信息



**校验登录状态：**

- 请求并携带Token
- 从Redis里取到用户
- 保存用户到**`ThreadLocal`**
- 放行

```java
public class RefreshTokenInterceptor implements HandlerInterceptor {

private StringRedisTemplate stringRedisTemplate;

public RefreshTokenInterceptor(StringRedisTemplate stringRedisTemplate) {
    this.stringRedisTemplate = stringRedisTemplate;
}

@Override
public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    //1.获取请求头的token
    String token = request.getHeader("authorization");
    if (StrUtil.isBlank(token)) {
        return true;
    }
    //2.基于token获取redis用户信息
    Map<Object, Object> userMap = stringRedisTemplate.opsForHash().entries(LOGIN_USER_KEY + token);

    //3.判断用户是否存在
    if (userMap.isEmpty()) {
        return true;
    }
    //5.将查询到的hash数据转为UserDTO对象
    UserDTO userDTO = BeanUtil.fillBeanWithMap(userMap, new UserDTO(), false);
    //6.用户存在，保存用户信息到ThreadLocal
    UserHolder.saveUser(userDTO);
    //7.刷新token有效期
    stringRedisTemplate.expire(LOGIN_USER_KEY + token, LOGIN_USER_TTL, TimeUnit.MINUTES);
    //8.放行
    return true;
}
```

```java
public class UserHolder {
    private static final ThreadLocal<UserDTO> tl = new ThreadLocal<>();

    public static void saveUser(UserDTO user){
        tl.set(user);
    }

    public static UserDTO getUser(){
        return tl.get();
    }

    public static void removeUser(){
        tl.remove();
    }
}
```

```java
public class LoginInterceptor implements HandlerInterceptor {

private StringRedisTemplate stringRedisTemplate;

@Override
public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    //1.判断是否需要拦截，即ThreadLocal中是否有用户
    if (UserHolder.getUser() == null) {
        //没有用户，拦截，设置状态码
        response.setStatus(401);
        return false;
    }
    //2.有用户，放行
    return true;
}
}
```

**配置拦截器：**

```java
@Configuration
public class MyConfig implements WebMvcConfigurer {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor()).excludePathPatterns(
                "/user/code",
                "/user/login",
                "/blog/log",
                "shop/**",
                "shop-type/**",
                "/upload/**",
                "voucher/**"
        ).order(1);
        registry.addInterceptor(new RefreshTokenInterceptor(stringRedisTemplate)).addPathPatterns("/**").order(0);
    }
  //先执行0，后执行1
}
```



# 商户查询缓存

## 什么是缓存？

缓存是数据交换的缓冲区，是存储数据的临时空间，读写性能较高

**缺点：**

​	影响数据一致性（缓存和主存的数据不一致）

​	

## 添加Redis缓存

客户端请求Redis，如果数据命中了，则直接返回结果，如果没有命中，Redis查询数据库



业务流程：

- 提交商户ID
- 从Redis查询商户缓存
- 判断缓存是否命中
- 如果没命中，则根据ID查询数据库，判断商户是否存在，如果不存在，返回404



### 缓存更新策略

缓存如果是旧数据，数据库是新数据，数据就会不一致

#### 内存淘汰：（自动开启）

​	当内存不足时，自动淘汰部分数据，下次查询时更新缓存

#### 超时剔除：

给缓存数据添加TTL时间，到期后自动删除缓存

（适合低一致性的场景）

#### 主动更新：

​	编写业务逻辑，修改数据库，同时删除缓存。

​	要确保对缓存和数据库的操作具有原子性，同时成功，或者同时失败

​	单体系统：将缓存和数据库放在一个事务

​	分布式系统：利用TCC分布式事务方案



- 根据id查询店铺时，如果缓存未命中，则查询数据库，将数据库结果写入缓存，并设置超时时间
- 根据id修改店铺时，先修改数据库，再修改缓存



### 缓存穿透

客户端请求的数据，在Redis和数据库中都不存在，这样的请求会直接到达数据库，给数据库添加压力

#### 解决方案：

- 缓存空对象

  - 在Redis缓存一个空对象（常用）

    - 缺点：
      - 存在数据不一致的可能性
      - 额外的内存消耗（可以给空对象添加TTL）

  - 布隆过滤（在Redis和客户端添加一个过滤器，里面存放了服务器数据的Hash值，如果不存在，则直接拒绝；如果存在，则执行Redis流程）

    - 缺点：
      - 实现复杂
      - 存在误判的可能性

  - 增加ID的复杂度，用格式验证，防止恶意访问

  - 加强用户权限校验

  - 做好热点参数的限流

    

- 根据id查询店铺时，如果缓存未命中，则查询数据库，判断数据是否存在，如果存在，将数据库结果写入缓存，并设置超时时间。如果不存在，则将空值写入Redis
- 根据id修改店铺时，先修改数据库，再修改缓存



### 缓存雪崩

同一时段大量的缓存Key同时失效，或者Redis宕机，导致大量请求到达数据库，带来巨大压力

#### 解决方案

- 给Key的TTL添加随机值
- 利用Redis集群提高服务的可用性
- 给业务添加多级缓存



### 缓存击穿

逻辑击穿问题也叫热点key问题，就是一些被**高并发访问**并且缓存重建业务比较复杂的key突然失效了，短时间内无数访问会击穿数据库

#### 解决方案：

- 互斥锁

  - 查询缓存，未命中
  - 获取互斥锁，重建缓存
  - 重建结束，释放锁

  缺点：重建缓存期间，其他线程会阻塞，影响性能

- 逻辑过期

  - 【key shop:user:1】【value {name="lby" expire="1234567"}】
  - 添加字段expire（逻辑时间），并不设置过期时间
  - 查询缓存（因为没有过期时间，一定会命中），发现逻辑时间已经过期
  - 获取锁，开启新线程，重建缓存，新线程释放锁
  - 返回过期数据
  - 这时候如果线程2，查询缓存，发现逻辑时间已经过期，它获取锁失败，会直接返回过期数据

  缺点：不能保证一致性，有额外的内存消耗



### 互斥锁解决缓存击穿问题

提交商铺id

从Redis查询缓存

判断是否命中

未命中

尝试获取锁

成功获取锁：缓存重建，释放锁

没有获取锁：休眠一段时间，再次从Redis查询缓存

```java
@Slf4j
@Component
//封装的将Java对象存进redis 的工具类
public class CacheClient {
  private final StringRedisTemplate stringRedisTemplate;

  public CacheClient(StringRedisTemplate stringRedisTemplate) {
      this.stringRedisTemplate = stringRedisTemplate;
  }

  //创建重建独立线程池
  private static final ExecutorService CACHE_REBUILD_EXECUTOR = Executors.newFixedThreadPool(10);

  //存储数据到redis
  public void set(String key, Object value, Long time, TimeUnit unit) {
      stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(value), time, unit);
  }

  //设置redis逻辑过期时间
  public void setWithLogicalExpire(String key, Object value, Long time, TimeUnit unit) {
      RedisData redisData = new RedisData();
      redisData.setData(value);
      redisData.setExpireTime(LocalDateTime.now().plusSeconds(unit.toSeconds(time)));
      stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(redisData), time, unit);
  }

  //缓存穿透解决办法(redis存空值)
  public <R, ID> R queryWithPassThrough(String keyPrefix, ID id, Class<R> type, Function<ID, R> dbFallback, Long time, TimeUnit unit) {
      //1.从redis中查询商铺缓存
      String Json = stringRedisTemplate.opsForValue().get(keyPrefix + id);
      //2.判断是否存在
      if (StrUtil.isNotBlank(Json)) {
          //3.存在，直接返回
          return JSONUtil.toBean(Json, type);
      }
      //redis命中的是空数据时
      if (Json != null) {
          //返回一个错误信息
          return null;
      }
      //4.redis不存在，根据id查询数据库
      R r = dbFallback.apply(id);
      //5.数据库不存在，返回错误
      if (r == null) {
          //将空值写入redis，防止缓存穿透
          stringRedisTemplate.opsForValue().set(CACHE_SHOP_KEY + id, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
          //返回错误信息
          return null;
      }
      //6.存在，将信息写入redis
      this.set(keyPrefix + id, r, time, unit);
      //7.返回
      return r;
  }

  //缓存击穿解决办法（设置逻辑过期时间）
  public <R, ID> R queryWithLogicalExpire(String keyPrefix, ID id, Class<R> type, Function<ID, R> dbFall, Long time, TimeUnit unit) {
      //1.从redis中查询商铺缓存
      String Json = stringRedisTemplate.opsForValue().get(keyPrefix + id);
      //2.判断是否存在
      if (StrUtil.isBlank(Json)) {
          //3.不存在，直接返回空
          return null;
      }
      //4.存在，json反序列化为对象
      RedisData redisData = JSONUtil.toBean(Json, RedisData.class);
      R r = JSONUtil.toBean((JSONObject) redisData.getData(), type);
      //4.1获取逻辑过期时间
      LocalDateTime expireTime = redisData.getExpireTime();
      //5.判断是否过期
      if (expireTime.isAfter(LocalDateTime.now())) {
          //5.1未过期，直接返回店铺信息
          return r;
      }
      //5.2已经过期，需要缓存重建
      //6.1缓存重建，获取互斥锁
      boolean isLock = tryLock(LOCK_SHOP_KEY + id);
      //6.2判断是否获取锁成功
      if (isLock) {
          //成功，开启独立线程，实现缓存重建
          CACHE_REBUILD_EXECUTOR.submit(() -> {
              try {
                  //重建重建缓存
                  R r1 = dbFall.apply(id);
                  this.setWithLogicalExpire(keyPrefix + id, r1, time, unit);
              } catch (Exception e) {
                  throw new RuntimeException(e);
              } finally {
                  //释放锁
                  unLock(LOCK_SHOP_KEY + id);
              }
          });
      }
      //7.返回店铺信息
      return r;
  }

  //缓存击穿解决办法（互斥锁）
  public <R, ID> R queryWithMutex(String keyPrefix, ID id, Class<R> type, Function<ID, R> dbFall, Long time, TimeUnit unit) {
      //1.从redis中查询商铺缓存
      String Json = stringRedisTemplate.opsForValue().get(CACHE_SHOP_KEY + id);
      //2.判断是否存在
      if (StrUtil.isNotBlank(Json)) {
          //3.存在，直接返回
          return JSONUtil.toBean(Json, type);
      }
      //redis命中的是空数据时
      if (Json != null) {
          //返回一个错误信息
          return null;
      }
      //4.redis不存在,进行缓存重建
      //4.1获取互斥锁
      R r;
      try {
          boolean isLock = tryLock(LOCK_SHOP_KEY + id);

          //4.2判断是否获取成功
          if (!isLock) {
              //4.3休眠并且重试
              Thread.sleep(50);
              return queryWithMutex(keyPrefix, id, type, dbFall, time, unit);
          }
          //4.4成功，根据id去数据库查询
          r = dbFall.apply(id);
          //5.数据库不存在，返回错误
          if (r == null) {
              //将空值写入redis，防止缓存穿透
              this.set(CACHE_SHOP_KEY + id, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
              //返回错误信息
              return null;
          }
          //6.存在，将信息写入redis
          this.set(CACHE_SHOP_KEY + id, JSONUtil.toJsonStr(r), CACHE_SHOP_TTL, TimeUnit.MINUTES);
      } catch (Exception e) {
          throw new RuntimeException(e);
      } finally {
          //7.释放互斥锁
          unLock(LOCK_SHOP_KEY + id);
      }
      //8.返回
      return r;
  }

  //获取锁
  private boolean tryLock(String key) {
      Boolean flag = stringRedisTemplate.opsForValue().setIfAbsent(key, "1", LOCK_SHOP_TTL, TimeUnit.SECONDS);
      return BooleanUtil.isTrue(flag);
  }

  //删除锁
  private void unLock(String key) {
      stringRedisTemplate.delete(key);
  }
}
```

# 优惠劵秒杀

### 全局唯一ID

订单表不使用自增ID，自增ID的缺点：

​	规律明显，容易暴露订单量

​	受到单表数据量的限制，一年下来，如果有几亿条订单，就得分表，其他表也是自增，就会导致ID重复



**全局ID生成器：**

为了增加ID的安全性，不可以直接使用Redis自增的数值，而是拼接一些其他信息：

​				**符号位+时间戳+序列号**

```java
@Component
public class RedisIdWorker {
    //开始时间戳
    private static final long BEGIN_TIMESTAMP = 1640995200L;

    //序列号的位数
    private static final int COUNT_BITS = 32;

    private StringRedisTemplate stringRedisTemplate;

    public RedisIdWorker(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    public long nextId(String keyPrefix) {
        //1.生成时间戳
        LocalDateTime now = LocalDateTime.now();
        long nowSecond = now.toEpochSecond(ZoneOffset.UTC);
        long timestamp = nowSecond - BEGIN_TIMESTAMP;

        //2.生成序列号
        //2.1获取当前日期，精确到天
        String date = now.format(DateTimeFormatter.ofPattern("yyy:MM:dd"));
        //2.2自增长
        Long count = stringRedisTemplate.opsForValue().increment("icr:" + keyPrefix + ":" + date);

        //3.拼接并返回
        return timestamp << COUNT_BITS | count;
    }
}

```



### 实现秒杀业务

- 提交优惠价ID
- 查询优惠卷信息
- 判断秒杀是否开始
- 如果开始，判断库存是否充足
- 如果充足，则扣去库存
- 创建订单
- 返回订单ID

### 超卖问题

多线程情况下，可能导致超卖问题，即卖多了

用CAS解决问题（更新数据用CAS）：先判断库存是否充足，如果充足，则执行购买操作，需要库存-1，而执行条件是再次判断库存的数量是否大于0。如果小于0，则取消减库存操作，返回库存不足



### 一人一单问题

![IMG_0015](/Users/Typora_Pictures/:Users:xiaobing:Downloads:IMG_0015.png)

【判断订单】是否存在这一步需要用悲观锁，因为乐观锁对应的是修改操作，悲观锁对应的是读写操作。

`synchronized`锁住的对象是从`ThreadLocal`里取出的对象的id，使用`userId.toString().intern()`，因为`toString()`返回的对象是实例化出来的新对象，而`intern()`返回的是字符串常量池里的地址。为了确保锁住的是同一个对象。

通过加锁可以解决单机情况下的一人一单问题，但是在集群模式下就不行了。**不同JVM的锁监视器是不同的。**所以需要一个多个JVM可以读取到的锁。



### 分布式锁

因为JVM里存储了对象实例，对象头关联着Monitor，如果是不同的JVM组成一个集群，串行模式的锁就不能使用了，需要使用分布式锁（不同JVM线程使用同一个Monitor）

**常见的三种：**

MySQL本身的互斥锁机制

Redis的`Setnx`这样的互斥命令

Zookeeper：利用节点的唯一性和有序性实现互斥

### 基于Redis的分布式锁

**获取锁：**

​		**原子性：**

​		`SET lock thread1 NX EX 10`超时释放（防止服务器宕机，造成死锁）

**释放锁：**

​		`DEL lock`(手动释放)

```java
public interface ILock {
    /**
    * 尝试获取锁
    * @param timeoutSec 锁持有的超时时间，过期后自动释放
    * @return true 代表获取锁成功；false代表获取锁失败
    * */
    boolean tryLock(long timeoutSec);

    /*
    * 释放锁
    * */
    void unlock();
}


public class SimpleRedisLock implements ILock {

    private String name;
    private StringRedisTemplate stringRedisTemplate;

    private static final String KEY_PREFIX = "lock:";
    private static final String ID_PREFIX = UUID.randomUUID() + "-";
    private static final DefaultRedisScript<Long> UNLOCK_SCRIPT;

    static {
        UNLOCK_SCRIPT = new DefaultRedisScript<>();
        UNLOCK_SCRIPT.setLocation(new ClassPathResource("unlock.lua"));
        UNLOCK_SCRIPT.setResultType(Long.class);
    }

    public SimpleRedisLock(String name, StringRedisTemplate stringRedisTemplate) {
        this.name = name;
        this.stringRedisTemplate = stringRedisTemplate;
    }

    @Override
    public boolean tryLock(long timeoutSec) {
        //获取线程标示
        String threadId = ID_PREFIX + Thread.currentThread().getId();
        //获取锁
        Boolean success = stringRedisTemplate.opsForValue()
                .setIfAbsent(KEY_PREFIX + name, threadId, timeoutSec, TimeUnit.SECONDS);
        return Boolean.TRUE.equals(success);
    }

    @Override
    public void unlock() {
        //调用lua脚本
        stringRedisTemplate.execute(UNLOCK_SCRIPT,
                Collections.singletonList(KEY_PREFIX + name),
                ID_PREFIX + Thread.currentThread().getId()
        );
    }

    /*
    *
    @Override
    public void unLock() {
        //获取线程标识
        String threadId = ID_PREFIX + Thread.currentThread().getId();
        //获取锁中的标识
        String id = stringRedisTemplate.opsForValue().get(KEY_PREFIX + name);
        //判断标识是否一致
        if (threadId.equals(id)) {
            //释放锁
            stringRedisTemplate.delete(KEY_PREFIX + name);
        }
    }
    * */
}
    @Override
    public Result seckillVoucher(Long voucherId) {
        //1.查询优惠卷
        SeckillVoucher voucher = seckillVoucherService.getById(voucherId);
        //2.判断秒杀是否开始
        if (voucher.getBeginTime().isAfter(LocalDateTime.now())) {
            //尚未开始
            return Result.fail("秒杀尚未开始");
        }
        //3.判断秒杀是否已经结束
        if (voucher.getEndTime().isBefore(LocalDateTime.now())) {
            //已经结束
            return Result.fail("秒杀已经结束");
        }
        //4.判断库存是否充足
        if (voucher.getStock() < 1) {
            return Result.fail("库存不足");
        }
        //5.查询用户id
        Long userId = UserHolder.getUser().getId();
        //6.对相同用户id的操作加锁，防止并行执行，一人多单
        //6.1创建锁对象
        //使用的redisson获取的锁类
        RLock lock = redissonClient.getLock("lock:order:" + userId);
        //6.2获取锁
        boolean isLock = lock.tryLock();
        //6.3判断获取锁是否成功
        if (!isLock) {
            //获取锁失败，返回错误
            return Result.fail("一个用户只能下一单");
        }
        try {
            //7获取代理对象（确保事务Transactional生效，保证createVoucherOrder 提交完事务之后再释放锁）
            IVoucherOrderService proxy = (IVoucherOrderService) AopContext.currentProxy();
            return proxy.createVoucherOrder(voucherId);
        } finally {
            //8.释放锁
            lock.unlock();
        }
    }
```





**问题：**

- 如果获得锁之后，业务处理花费很长时间，TTL结束，就会自动释放锁。如果这时候再释放锁，就会误删别的线程的锁。**所以释放锁之前，需要进行判断，判断是否为自己的锁**。锁的value都要有一致性
- 判断是否为自己的锁，但刚要释放的时候，因为GC，线程阻塞，导致锁自动释放了，再释放的时候，就会直接释放别的线程的锁。所以**锁的释放要有原子性**
- Redis的Lua脚本，在一个脚本中编写多条Redis命令，确保多条命令的原子性



**释放锁的流程：**

- 获取线程的标识
- 获取当前线程的标识
- 如果标识相等，则释放锁

````java
public class SimpleRedisLock implements ILock {

    private String name;
    private StringRedisTemplate stringRedisTemplate;

    private static final String KEY_PREFIX = "lock:";
    private static final String ID_PREFIX = UUID.randomUUID() + "-";
    private static final DefaultRedisScript<Long> UNLOCK_SCRIPT;

    static {
        UNLOCK_SCRIPT = new DefaultRedisScript<>();
        UNLOCK_SCRIPT.setLocation(new ClassPathResource("unlock.lua"));
        UNLOCK_SCRIPT.setResultType(Long.class);
    }

    public SimpleRedisLock(String name, StringRedisTemplate stringRedisTemplate) {
        this.name = name;
        this.stringRedisTemplate = stringRedisTemplate;
    }

    @Override
    public boolean tryLock(long timeoutSec) {
        //获取线程标示
        String threadId = ID_PREFIX + Thread.currentThread().getId();
        //获取锁
        Boolean success = stringRedisTemplate.opsForValue()
                .setIfAbsent(KEY_PREFIX + name, threadId, timeoutSec, TimeUnit.SECONDS);
        return Boolean.TRUE.equals(success);
    }

    @Override
    public void unlock() {
        //调用lua脚本
        stringRedisTemplate.execute(UNLOCK_SCRIPT,
                Collections.singletonList(KEY_PREFIX + name),
                ID_PREFIX + Thread.currentThread().getId()
        );
    }
  
/**
lua脚本
  --比较线程标识与锁中标识是否一致
if (redis.call('get', KEYS[1]) == ARGV[1]) then
    --释放锁，del key
    return redis.call('del', KEYS[1])
end
return 0
*/

    /*
    *
    @Override
    public void unLock() {
        //获取线程标识
        String threadId = ID_PREFIX + Thread.currentThread().getId();
        //获取锁中的标识
        String id = stringRedisTemplate.opsForValue().get(KEY_PREFIX + name);
        //判断标识是否一致
        if (threadId.equals(id)) {
            //释放锁
            stringRedisTemplate.delete(KEY_PREFIX + name);
        }
    }
    * */
}
````

**思路：**

- set nx ex获取锁，并设置过期时间，value为线程标识
- 释放锁时先判断线程标识是否一致，如果一致则删除锁。（用Lua脚本脚本保证一致性）

**特性：**

利用set nx保证互斥下

利用set ex保证故障时依旧能是否锁，避免死锁，提高安全性

利用Lua脚本保证释放锁的原子性

 

### Redission分布式锁

Redis分布式锁的问题：

- 不可重入（同一个线程不能多次获取同一把锁）
- 不可重试，只能尝试一次
- 超时释放有隐患（业务没执行完，锁就释放了）
- Redis主从一致性有延迟（在主节点获得锁，主节点宕机，从节点没有完成同步，锁就消失了）



Redission，一种分布式的工具包，提高分布式锁

```java
@Configuration
public class RedissonConfig {
@Bean
public RedissonClient redissonClient(){
    //配置
    Config config = new Config();		   			    config.useSingleServer().setAddress("redis://192.168.10.104:6379").setPassword("200 294");
    //创建redissonClient 对象
    return Redisson.create(config);
}
}


private void handleVoucherOrder(VoucherOrder voucherOrder) {
    //1.1获取用户id
    Long userId = voucherOrder.getUserId();
    //使用的redisson获取的锁类
    //1.2创建锁对象
    RLock lock = redissonClient.getLock("lock:order:" + userId);
    //1.3获取锁
    boolean isLock = lock.tryLock();
    //1.4判断获取锁是否成功
    if (!isLock) {
        //获取锁失败，返回错误
        log.error("不允许重复下单");
        return;
    }
    try {
        proxy.createVoucherOrder(voucherOrder);
    } finally {
        //3.释放锁
        lock.unlock();
    }
}
```



### Redis消息队列实现异步秒杀

![IMG_0017](/Users/Typora_Pictures/:Users:xiaobing:Downloads:IMG_0017.png)



消息队列有三个角色：	消息队列、生产者（发送消息到消息队列）、消费者（从消息队列获取消息）

Redis提供三种不同方式的消息队列：

- list结构：双向链表，队列是入口和出口不在一遍，`LPUSH`和`BRPOP`结合

  - 缺点：无法避免消息丢失、只支持单消费者（消息有一个人拿走了，就消失了）

    

- PubSub：发布订阅，消费者可以订阅一个/多个channel，生产者向channel发送消息后，所有订阅者收到消息

```shell
#subscribe channel
SUBSRIBE order.q1
#publish channel msg
PUBLISH order.q1 joke
```

​	优点：支持多生产，多消费

​	缺点：不支持数据持久化、无法避免消息丢失、消费者的消息堆积有上限，超出时数据丢失



# Redis持久化

## RDB

（Redis Database BackUp File）

把当前进程数据⽣成快照保存到硬盘的过程，触发RDB持久化过程分为**⼿动触发**和**⾃动触发**

RDB⽂件是⼀个压缩的⼆进制⽂件，通过它可以还原某个时刻数据库的状态。由于RDB⽂件是保存在硬盘上的，所以即使Redis崩溃或者退出，只要RDB⽂件存在，就可以⽤它来恢复还原数据库的状态。



### **手动触发**

```shell
#由主进程执行RDB会阻塞所有命令
save

#开启子进程执行RDB，避免主进程受到影响
bgsave
```

bgsave的流程是：fork主进程得到一个子进程，共享内存空间（主进程操作虚拟内存，虚拟内存映射实际内存，复制映射关系，即可共享内存空间）。子进程读取内存数据并写入新的RDB文件，新的文件会覆盖旧的RDB文件



### **自动触发**

- 使⽤save相关配置，如“save m n”。表⽰m秒内数据集存在n次修改时，⾃动触发bgsave

- 默认是服务停止时

  

-  如果从节点执⾏全量复制操作，主节点⾃动执⾏bgsave⽣成RDB⽂件并发送给从节点 

- 执⾏debug reload命令重新加载Redis时，也会⾃动触发save操作 

**配置文件：redis.conf**

```shell
#在900秒内，如果有一个key被修改，则执行bgsave
#自动触发
save 900 1
save 300 10

#rdb文件名
dbfilename dump.rdb

#文件保存的路径目录,默认保存在当前运行目录
dir ./
```



## AOF

（Append Only File）追加文件

将Redis处理的每一个写命令都记录在AOF文件，恢复数据，只需要读取AOF文件，将指令重新执行一遍

AOF的主要作⽤是解决了数据持久化的实时性（因为RDB是每隔x秒执行一次RDB，如果期间有宕机，这些数据将会丢失）

AOF默认是关闭的，需要配置

**配置文件：**

```shell
#开启AOF
appendonly yes
#文件名
appendfilename "appendonly.aof"
#默认方案：写命令执行完先放入缓冲区，每隔一秒将缓冲区数据写入AOF文件(最多丢失一秒数据)
appendfsync eversec
```

因为是记录命令，AOF文件大小比RDB大，而且**AOF会记录对同一个key的多次写操作**，但实际上，只有最后一次是有意义的。通过执行`bgrewriteaof`命令，可以让AOF文件执行重写功能，用最少的命令达到相同的效果，压缩文件大小

可以**自动触发AOF文件重写**

```shell
#AOF文件比上一次增长超过多少百分比，则触发重写
auto-aof-rewrite-percentage 100
#AOF文件体积大小达到多少，则触发重写
auto-aof-rewrite-min-size 64mb
```

## RDB和AOF对比

**RDB | 优点**

1. 只有⼀个紧凑的⼆进制⽂件 dump.rdb ，⾮常适合备份、全量复制的场景。

2. 容灾性好 ，可以把RDB⽂件拷贝道远程机器或者⽂件系统，⽤于容灾恢复。

3. 恢复速度快 ，RDB恢复数据的速度远远快于AOF的⽅式

**RDB | 缺点**

1. 实时性低 ，RDB 是间隔⼀段时间进⾏持久化，没法做到实时持久化/秒级持久化。如果在这⼀间隔事件发⽣故障，数据会丢失
1. 存在兼容问题 ，Redis演进过程存在多个格式的RDB版本，存在⽼版本Redis⽆法兼容新版 本RDB的问题。



**AOF | 优点**

1. 实时性好 ，aof 持久化可以配置 appendfsync 属性，有 always ，每进⾏⼀次命令操作就记录到 aof ⽂件中⼀次。

2. 通过 append 模式写⽂件，即使中途服务器宕机，可以通过 redis-check-aof ⼯具解决数据 ⼀致性问题。

**AOF | 缺点** 

1. AOF ⽂件⽐ RDB ⽂件⼤ ，且恢复速度慢 。

2. 数据集⼤ 的时候，⽐ RDB 启动效率低



**Redis 启动时加载数据的流程：**

1. AOF持久化开启且存在AOF⽂件时，优先加载AOF⽂件。
2. AOF关闭或者AOF⽂件不存在时，加载RDB⽂件。
3. 加载AOF/RDB⽂件成功后，Redis启动成功。
4. AOF/RDB⽂件存在错误时，Redis启动失败并打印错误信息



Redis4.0之后，使用混合持久化的方案。在 Redis 重启的时候，可以先加载 rdb 的内容，然后再重放增量 AOF ⽇志就可以完全 替代之前的 AOF 全量⽂件重放，重启效率因此⼤幅得到提升



# Redis主从

实现读写分离：Master写、Slave读

**开启主从关系：**临时/永久

```shell
#从机配置
replicaof 127.0.0.1 6379
replica-read-only yes
```

​		永久：修改配置文件`slaveof <masterip> <masterport>`

​		临时：在客户端执行slave命令（重启失效）`slaveof  <masterip> <masterport>`

​		查看链接状态：`info replication`



### 主从复制的原理

主从第一次同步是**全量同步**：

- `slave`第一次执行`replica`命令，携带`replication id`和`offset`偏移量，`master`会返回信息，`slave`保存这些信息
- `master`执行`bgsave`，生成RDB文件，`slave`清空本地数据，加载RDB文件
- `master`将RDB期间的所有命令记录在`repl_baklog`（slave和master之间数据差距的缓冲区），然后发送给`slave`，slave执行命令



### Master如何判断slave是不是第一次访问？

- **`Replication id:`**是数据集的标记，id一致说明是同一数据集，每一个master都有唯一的id，slave则会继承master节点的id
- **`offset：`**偏移量，随着记录在`repl_baklog`中的数据增多而逐渐增大。`slave`完成同步时也会记录当前同步的`offset`。如果`slave`的`offset`小于`master`的`offset`，说明`slave`数据落后于`master`，需要更新

​	`slave`做数据同步，必须向`master`发送自己的`replication id`和`offset`，如果id不一致就是第一次来，做**全量同步**，如果一致就做**增量同步**



### 增量同步

主从第一次同步是**全量同步**，但如果`slave`重启之后，则执行**增量同步**,`slave`提交自己的`offset`到`master`，`master`获取`rep_baklog`中`offset`之后的命令，发送给`slave`

`repl_baklog`可以理解成一个圆形的数据结构，如果`slave`宕机了，`master`仍然在持续记录，记完一圈，重新再来一圈，旧的数据已经被覆盖了，这时候`slave`的`offset`也失效了。`slave`必须再次全量同步



### 主从复制的主要作用

**数据冗余**：主从复制实现了数据的热备份，是持久化之外的⼀种数据冗余⽅式

**故障恢复**：当主节点出现问题时，可以由从节点提供服务，实现快速的故障恢复 (实际 上是⼀种服务的冗余) 

**负载均衡**： 在主从复制的基础上，配合读写分离，可以由主节点提供写服务，由从节点 提供读服务 （即写 Redis 数据时应⽤连接主节点，读 Redis 数据时应⽤连接从节点） ， 分担服务器负载。尤其是在写少读多的场景下，通过多个从节点分担读负载，可以⼤⼤提 ⾼ Redis 服务器的并发量

**⾼可⽤基⽯**： 除了上述作⽤以外，主从复制还是哨兵和集群能够实施的基础 ，因此说 主从复制是 Redis ⾼可⽤的基础



# 哨兵

## 工作原理

主从复制存在⼀个问题，没法完成⾃动故障转移（master宕机了），于是有了Redis Sentinel（哨兵）



Redis Sentinel，由两部分组成，**哨兵节点**和**数据节点**

​		哨兵节点：哨兵系统由⼀个或多个哨兵节点组成，哨兵节点是特殊的 Redis 节点，不存储数据，对数据节点进⾏监控

​		数据节点：主节点和从节点都是数据节点



**Sentinel作用：**

- 监控：Sentinel不断检测master和slave是否正常工作

- 自动故障恢复：如果master故障，Sentinel将一个slave提升为master，当故障恢复后，也以新的master为主

- 通知：哨兵可以将故障转移的结果发送给客户端。



**Sentinel的机制：**

- 每隔1秒，sentinel向每个节点和其余sentinel发送ping命令，做一次心跳检测
- 每隔10秒，每个sentinel会向主节点和从节点发送info命令获取最新的拓扑结构

​		主观下线：如果发现某实例未在规定时间响应，则认为该实例**主观下线**

​		客观下线：如果超过指定数量(quorum)的sentinel节点都认为该实例主观下线，则该实例**客观下线**



**选举新的master：**

​	一旦发生master故障，sentinel会在slave中选择一个作为新的master，依据是：

​			如果slave节点与master节点断开时间超过指定值，则排除该slave节点

​			判断slave节点的offset值，越大说明数据越新*

​			最后判断slave节点的运行id大小，越小优先级越高



**故障转移：**

- sentinel向slave发送请求，`slaveof no one`，这节点就成了master
- sentinel向其他slave发送`slaveof ip port`，让这些节点成为新master的slave
- 最好sentinel将故障节点标记为slave，故障恢复后，该节点会自动成为slave



## Redis Template的哨兵模式

sentinel的配置文件

`sentinel.conf`

```shell
port xxxx
sentinel announce-ip <ip>
sentinel monitor <masterName> <ip> <port> <quorum>
```



```yaml
spring:
	redis:
		sentinel:	#RedisTemplate只需要监控`sentinel`，就能获得整个Redis集群的信息
			master: <masterName>
			nodes:		#sentinel集群地址
				- <ip>:<port>
				- <ip>:<port>
				- <ip>:<port>
```

**配置读写分离：**

```java
@Bean
public LettuceClientConfigurationBuilderCustomized config(){
  return configBuilder->configBuilder.readFrom(ReadFrom.REPLICA_PREFERRED)
}
```

ReadFrom配置的是Redis的读取策略，是一个枚举

- Master:优先master
- Matser_PREFERRED:
- REPLICA:
- REPLICA_PREFERRED:优先slave，slave不可用就用master



# Redis分片集群

主从和哨兵解决了高可用和高并发问题，但还有缺点：

- 海量数据存储问题
- 高并发写的问题



## **分片集群：**

- 集群中有多个master，每个master保存不同的数据
- 每个master都有多个slave节点
- master之间通过ping监测彼此健康状态
- 客户端请求可以访问任意节点，最终都会转发到正确节点

```shell
cluster-enabled yes
cluster-node-timeout 5000
damonize yes
```

`redis-cli --cluster create --cluster-replicas 1 <ip>:<port>...`



## 散列插槽

Redis将所有的master和一个插槽绑定，每一个master分配到一部分的插槽。然后对数据的key的有效部分进行计算，得到插槽值，写入对应插槽值的master。如果master宕机，把这一部分的插槽分配给新的master，不会丢失数据

**key的有效部分：**

​		key包含{},且{}中存在一个字符，则{}中是有效部分

​		key不包含{},整个key都是有效部分

如何将同一类数据固定地保存在同一个redis实例？

​		这一类数据使用相同的有效部分：{typeid}name



## 集群伸缩

集群动态地增加节点/移除节点。集群扩容和缩容的关键点，就在于槽和节点的对应关系，扩容和缩容就是将⼀部分 槽 和 数据 迁移给新节点。

`redis-cli -cluster add-node <ip>:<port>`

`redis-cli -p <port> cluster nodes`

`redis-cli --cluster reshard <ip>:<port>`



## 故障转移

Redis集群⼀般由多个节点组成，节点数量⾄少为6个才能保证组成完整⾼可⽤的集群。每个节 点需要开启配置cluster-enabled yes，让Redis运⾏在集群模式下

Redis集群内节点通过ping/pong消息实现节点通信，集群中每个节点都会定期向其他节点发送 ping消息，接收节点回复pong 消息作为响应。如果在cluster-node-timeout时间内通信⼀直失败，则发送节点会认为接收节点存在故障，把接收节点标记为主观下线（fail）状态。当某个节点判断另⼀个节点主观下线后，相应的节点状态会跟随消息在集群内传播。集群内节点不断收集到故障节点的下线报告。当半数以上持有槽的主节点 都标记某个节点是主观下线时。触发客观下线流程

**手动故障转移：**

在新的slave节点执行命令`cluster failover`，手动让集群中的某个master宕机，实现无感知的数据迁移



## RedisTemplate访问分片集群

```yaml
spring:
	redis:
		cluster:
			nodes:	#每个节点的地址
				- <ip>:<port>
```

配置读写分离

```java
@Bean
public LettuceClientConfigurationBuilderCustomized config(){
  return configBuilder->configBuilder.readFrom(ReadFrom.REPLICA_PREFERRED)
}
```



# Redis缓存预热

将热点数据提前查询并保存到Redis中

```java
@Compoent
public class RedisHandler implements InitializingBean{
  @Autowired
  private RedisTemplate redisTemplate;
  
  @Autowired
  private ItemService service;
  @Override
  public void afterPropertiesSet(){
    //初始化缓存
    //查询商品信息
     List<Item> list = service.list();
    //放入缓存
    redisTemplate.opsForValue().set("Item:id"+item.getId(),list.toString);
  }
}
```



# Redis最佳实践

**Key的设计**

​	基本格式：【业务名称】：【数据名】：【id】

**存储一个对象**

- ​	json字符串
-    字段打散 `user:1:name Jack  user:1:age 21`(占用空间太多)
-    hash`user:1 name jack age 21`



**PipeLine管道：**

存放命令，批量执行

```java
Pipeline pipeline = jedis.pipelined()
pipeline.set("test:key"+i,i);
pipeline.sync();
```

 

**先更数据，后删缓存？**

更新数据，耗时可能在删除缓存的百倍以上。在缓存中不存在对应的key，数据库又没有完成 更新的时候，如果有线程进来读取数据，并写⼊到缓存，那么在更新成功之后，这个key就是 ⼀个脏数据。毫⽆疑问，先删缓存，再更数据库，缓存中key不存在的时间的时间更长，有更⼤的概率会产⽣脏数据。



**Redis 如何实现延时队列?**

可以使⽤ zset这个结构，⽤设置好的时间戳作为score进⾏排序，使⽤ zadd score1 value1 ....命 令就可以⼀直往内存中⽣产消息。再利⽤ zrangebysocre 查询符合条件的所有待处理的任务， 通过循环执⾏队列任务即可。



**Redis ⽀持事务吗？**

multi命令代表事务开始

exec命令代表事务结束

Redis事务的原理，是所有的指令在 exec 之前不执⾏，⽽是缓存在 服务器的⼀个事务队列中，服务器⼀旦收到 exec 指令，才开执⾏整个事务队列，执⾏完毕后 ⼀次性返回所有指令的运⾏结果。

Redis 事务是不⽀持回滚的，不像 MySQL 的事务⼀样



# 多级缓存

## nginx本地缓存

> nginx本地内存有限，只能用于抗部分热数据，其他相对不那么热的数据会走redis
>
> 抗的是**热数据**的**高并发访问**，经常**会被访问的那些数据**，就**会被保留到nginx本地缓存**中。对于**那些热数据的大量访问**，直接**走nginx**就可以了

## Redis分布式大规模缓存

> redis cluster的多master写入，横向扩容，1T+以上的海量数据支撑，几十万的数据读写QPS，99.99%的高可用性，可以有效的抗住大量的离散请求
>
> 抗的是**很高的离散访问**，**支撑海量的数据，高并发的访问，高可用的服务**



# Jedis

## 基本使用

创建Jedis对象

创建连接

使用Jedis

关闭连接

```java
public class TestJedis {
    private Jedis jedis;

    @BeforeEach
    void setUp() {
        jedis = new Jedis("1.15.114.38",6379);
        jedis.auth("P@ssword01");
        jedis.select(0);

    }

    @Test
    void testString() {
        String result1 = jedis.set("name", "LBY");
        String name = jedis.get("name");
        System.out.println("set result:"+result1+"\n"+"value:"+name);
    }

    @Test
    void testHash(){
        jedis.hset("user1", "name","lby");
        jedis.hset("user1", "age","23");
        Map<String, String> map = jedis.hgetAll("user1");
        String hget = jedis.hget("user1", "name");
        System.out.println(map);
        System.out.println(hget);
    }

    @AfterEach
    void  shutdown(){
        Optional<Jedis> optional = Optional.of(this.jedis);
        optional.get().close();
    }
}

```

## Jedis连接池

Jedis是线程不安全的，所以使用Jedis连接池

```java
//单例模式

public class JedisUtils {
    private static final JedisPool pool;

    static {
        JedisPoolConfig config = new JedisPoolConfig();
        //最大连接数
        config.setMaxTotal(8);
        //最大空闲连接
        config.setMaxIdle(3);
        //最小空闲连接
        config.setMaxIdle(1);
        //最长等待时间（连接销毁的时间）
        config.setMaxWait(Duration.ofSeconds(20));
        pool = new JedisPool(config, "1.15.114.38", 6379);
    }

    public static Jedis getInstance() {
        return pool.getResource();
    }
}


   private Jedis jedis;

    @BeforeEach
    void setUp() {
        jedis = JedisUtils.getInstance();
        jedis.auth("P@ssword01");
        jedis.select(0);
    }
```

