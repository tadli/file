[TOC]



# Docker命令

## docker命令

```shell
systemctl start docker #启动Docker
systemctl stop docker	#停止Docker
systemctl restart docker #重启Docker
systemctl status docker #查看Docker服务状态
systemctl enable docker #开机启动Docker
docker info #查看info
docker <command> --help
```

## image命令

```shell
docker images #查看镜像
#仓库、版本号、镜像ID、创建时间、镜像大小
#REPOSITORY	TAG	IMAGE	ID	CREATED	SIZE
#搜素镜像
docker search redis --limit 5
#拉取镜像
#docker pull <imageName>:<tag>
docker pull redis:5.0
#删除镜像
docker rmi -f <image_ID>
docker rmi -f $(docker images -aq)
#查看占用空间<image><contaner><volumn>
docker system df 
```

## 容器

```shell
docker ps	#查看容器  
docker ps -a 	#-n 指定数量

#创建并启动容器 
#-i保持运行 
#-t分配一个终端 
#-d后台运行容器,并返回容器ID 
#--name分配名字 image /bin/bash
#-P:随机端口映射
#-p:指定端口映射
#-restart=always
docker run redis -it --name=testRedis /bin/bash
docker run -id --name=lby2 redis

#退出容器
exit #run进去容器，exit退出，容器停止运行
ctrl+p+q #run进去容器，ctrl+p+q退出，容器不会停止

#进入容器
docker exec -it lby2 /bin/bash
docker exec -it lby bash


docker start <imageID>
docker restart <imageID>
docker stop <imageID>
docker kill <imageID>
docker rm <imageID>

#查看容器信息
docker logs <imageID>
docker inspect <imageID>
docker top <imageID> #查看image运行的进程

#复制容器文件到host machine
docker cp <imageID>:<path> <hostPath>
docker cp testRedis:/data/dump.rbd /Users/xiaobing/Desktop

#导出容器的内容
#导入容器的内容
docker export <imageID> > <fileName>.tar
cat <fileName>.tar | docker import - <imageID>/<imageName>:<Tag>
```

# 镜像

UnionFS，是一种分层的文件系统，支持对文件系统的修改作为一次提交来一层层的叠加，同时将不同目录挂载到同一个虚拟机系统下，UnionFS是Docker镜像到基础。镜像可以通过分层来进行继承，基于基础镜像，可以制作各种具体的应用镜像

```shell
docker commit -m="message" -a="author" <contianerID> <nameImageName:Tag>
```



# 数据卷

数据卷是宿主机中的一个目录/文件，把目录挂载在容器中，实现数据同步。一个数据卷可以挂载多个容器，一个容器可以挂载多个数据卷.

所以容器删除后，容器产生的数据不会消失，实现持久化

所以容器可以和外部机器通过数据卷交换文件

所以多个容器可以通过数据卷进行数据交互

```shell
#目录必须是绝对路径
#如果目录不存在，会自动创建
#可以挂载多个数据卷
docker run ... -v <宿主机目录/文件>：<容器内目录/文件>
docker run -it --name=lby -v /Users:/root/data_container redis /bin/bash
cd ~
```

## **数据卷容器**

多容器进行数据交换：

- 多容器挂载同一个数据卷
- 数据卷容器

![image-20220803223839397](/Users/xiaobing/Library/Application Support/typora-user-images/image-20220803223839397.png)

```shell
#创建数据卷容器
docker run -it --name=data_container -v /volume centos:7 /bin/bash
#创建A和B容器，使用--volumes-from <数据卷名字> <images> /bin/bash
docker run -id --name=AA --volumes-from data_container centos
docker exec -it AA bash
```

# 应用部署

## MySQL

搜索镜像

拉取镜像

创建容器

操作容器里的MySQL

```shell
docker pull mysql

#在root目录下创建mysql目录，用于存储mysql数据信息
mkdir ~/mysql
cd ~/mysql

#端口映射：-p 3307:3306
#将主机当前目录下的conf/my.cnf挂载到容器的。etc/mysql/my.cnf
docker run -id\
 -p 3308:3306 --name=mysql\
 -v $PWD/conf:/etc/mysql/conf.d\
 -v $PWD/logs:/logs\
 -v $PWD/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 mysql 
```

容器内的网络服务和外部机器不能直接通信

外部机器和宿主机可以直接通信

宿主机和容器可以直接通信

**端口映射：**将容器中提供的服务的端口映射到宿主机内的端口，外部机器访问宿主机的端口，从而间接访问容器内的服务

## Tomcat

```shell
docker search tomcat
docker pull tomcat
mkdir ~/tomcat
cd ~/tomcat
docker run -id --name=c_tomcat \
-p 8080:8080 \
-v $PWD:/usr/local/tomcat/webapps \
tomcat 
```



## Redis

```shell
docker run -id --name=myredis -p 6379:6379 redis
#外部机器连接redis
redis-cli -h <ip> -p 6379
```



# Dockerfile

## Docker镜像原理

**Docker镜像的本质是什么？**

分层文件系统

Docker镜像是由特殊的文件系统叠加而成的

最底层是bootfs，并且使用宿主机的bootfs

第二层是root文件系统rootfs，成为base image

再往上可以叠加其他的镜像文件

![IMG_8A75D825E881-1](/Users/Typora_Pictures/:Users:xiaobing:Downloads:IMG_8A75D825E881-1.png)

一个镜像可以放在另一个镜像上面，下面的镜像叫父镜像，最底层的镜像叫基础镜像



**Docker中的一个镜像文件为什么只有几百MB？**

Docker复用了宿主机的bootfs



## Docker镜像制作

1.**容器转化为镜像**

```shell
#commit提交镜像，不会记录挂载文件
docker commit <容器id> <镜像名称>:<版本号>
#将镜像转化成压缩文件
docker save -o 压缩文件名称 镜像名称：版本号
#将压缩文件转化成镜像
docker load -i 压缩文件名称
```

**2.dockerfile**

一个文本文件，包含一条条指令

每一条指令构建一层，基于基础镜像，最终构建出一个新的镜像

可以为开发人员提供完全一致的开发环境

在dockerhub上找到dockerfile



| 关键字  | 作用                     | 备注                                                         |
| ------- | ------------------------ | ------------------------------------------------------------ |
| FROM    | 指定父镜像               | 指定dockerfile基于哪个image创建                              |
| RUN     | 执行一段命令             |                                                              |
| CMD     | 容器启动时执行的命令     |                                                              |
| COPY    | 复制文件                 | build的时候复制文件到image中                                 |
| ADD     | 添加文件                 | build的时候添加文件到image中，不仅仅局限于build上下文，可以来源于远程服务 |
| ENV     | 环境变量                 |                                                              |
| VOLUME  | 定义外部可以挂载的数据卷 |                                                              |
| EXPOSE  | 暴露端口                 |                                                              |
| WORKDIR | 工作目录                 |                                                              |
| SHELL   | 执行shell脚本            |                                                              |

```shell
#定义dockerfile，发布一个springboot项目
1.用Maven给项目打包
2.定义父镜像 Java8
3.定义作者信息
4.添加jar包，取名app.jar
5.定义启动命令
FROM java:8
MAINTAINER lby<15942011070>
ADD springboot.jar app.jar
CMD java -jar app.jar

docker build -f ./springboot_dockerfile -t app .
```



# Dockerfile案例

自定义centos7镜像，要求：

- 默认登录路径为/usr
- 可以使用vim

```shell
1.定义父镜像：				     FROM centos:7
2.定义作者信息：	 			    MAINTAINER lby<15942011070>
3.执行安装vim的命令：	     RUN yum install -y vim
4.定义默认的工作目录：  	  WORKDIR /usr
5.定义容器启动执行的命令：   CMD /bin/bash
6.构建镜像								docker build -f ./centos_dockerfile -t my_centos:1 .
```



# Docker服务编排

微服务架构中包含过许多服务，每个微服务都会部署多个实例，每个服务都要手动启停，工作量很大

**服务编排**：按照一定的业务规则批量管理容器

**DockerCompose**：编排多容器分布式部署的工具，包括服务构建、启动和停止

- 利用`Dockerfile`定义运行环境镜像
- 使用`docker-compose.yml`定义组成应用的各服务
- 运行`docker-compose up`启动应用



**使用docker-compose编排nginx+springboot项目**



```shell
mkdir ~/docker-compose
cd docker-compose
vim docker-compose.yml

version: '1.29.2'
services: 
 nginx:
  image: nginx
  ports:
 	 - 80:80
  links:
 	 - app
 	volumes:
 	 - ./nginx/conf.d:/etc/nginx/conf.d
 	apps:
 		image: app
 		expose:
 		 - "8080"
 		 
docker-compose up 		 
```

# 私有仓库

官方仓库：`dockerhub`

私有仓库：管理和上传镜像



搭建私有仓库

```shell
docker pull registry
docker run -id --name=registry -p 5000:5000 registry
#在浏览器输入http://<服务器ip>：5000/v2/_catalog
vim /etc/docker/daemon.json
{"insecure-registries": ["ip:5000"]}

restart docker
docker start registry
```

**上传镜像**

```shell
#将本地镜像标记为私有仓库的镜像
docker tag centos:7 ip:5000/centos:7
#上传标记的镜像
docker push ip:5000/centos:7
```

**下载镜像**

```shell
docker pull ip:5000/centos:7
```



容器镜像包含代码、运行时环境、系统工具



# 面试题

**什么是Docker？**

把代码和运行时的环境都打包生成一个镜像，然后上传到私有仓库，其他同事只需要dokcer pull下来，就可以通过镜像启动容器了



**镜像和容器的区别？**

Docker镜像是Docker容器的源代码，创建容器需要通过镜像。Docker容器包括应用程序及所有的依赖



 **DockerFile中的命令COPY和ADD命令有什么区别？**

COPY添加的文件只能是本地文件，ADD可以从远程仓库里拉取文件



**如何在生产中监控docker容器？**

Docker统计数据：当我们使用容器ID调用`docker stats`时，我们获得容器的CPU，内存使用情况等。它类似于Linux中的top命令



**如何临时退出正在运行的容器，并且不关闭它**

按Ctrl+p，后按Ctrl+q



**查看日志**

`docker logs <容器名称>` 



**如何启动和停止容器？**

`docker start <container>`

`docker stop <container>`



**如何访问正在运行的容器？**

`docker exec -it <container> bash`



**使用Docker Compose时如何保证容器1先于容器2运行？**

为了控制容器执行顺序，可以使用属性`depends_on`

```yml
version: "2.4"
services:
 backend:
   build: .
   depends_on:
     - db
 db:
   image: postgres
```


