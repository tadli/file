# 简介

关系型数据库

建立在关系模型基础上，由多张连接的**二维表**组成数据库

注释：`/*  */`



# SQL分类

**DDL：**(Data definition language)定义数据库、表、字段

**DML：**(Data manipulate language)增删改

**DQL：**(Data query language)查询

**DCL：**(Data control language)创建数据库用户，赋予访问权限



# DDL

## 数据库

```sql
#显示全部数据库
SHOW DATABASES;
#查看当前使用的数据库
SELECT DATABASE();
#创建数据库
CREATE DATABASE IF NOT EXISTS <数据库名>；
#删除数据库
DROP DATABASE IF EXISTS <数据库名>；
#使用数据库
USE <数据库名>；
```



## 数据类型

| 类型     | 大小          | 有符号范围               | 无符号范围（unsigned） | 描述                              |
| -------- | ------------- | ------------------------ | ---------------------- | --------------------------------- |
| INT      | 4 bytes       | (-2147483648,2147483647) | 2147483648*2           |                                   |
| TINYINT  | 1 bytes       | (-128-127)               | (0-255)                | 小整数值                          |
| FLOAT    | 4 bytes       |                          |                        |                                   |
| DOUBLE   | 8 bytes       |                          |                        |                                   |
|          |               |                          |                        |                                   |
| CHAR     | 0-255 bytes   |                          |                        | 固定大小，缺少位置由0补位，性能高 |
| VARCHAR  | 0-65535 bytes |                          |                        | 可以伸缩，性能低                  |
| BLOB     | 0-65535 bytes |                          |                        | 保存二进制数据                    |
| TEXT     | 0-65535 bytes |                          |                        | 保存文本数据                      |
|          |               |                          |                        |                                   |
| DATE     | 3 bytes       | 1000-01-01至9999-12-31   |                        |                                   |
| TIME     | 3 bytes       | 12:00:00                 |                        |                                   |
| YEAR     | 1 bytes       | 1901-2155                |                        |                                   |
| DATETIME | 8 bytes       | 1000-01-0 12:00:00       |                        |                                   |



## 表结构

```mysql
#查看全部表
SHOW TABLES;
#查看表结构
DESC 表名;
#展示建表语句
SHOW CREATE TABLE 表名;
#建表
CREATE TABLE user_01(
	id int primary key comment 'id',
  name varchar(10) comment 'name',
  gender char(1) comment 'girl/boy',
  age tinyint unsigned comment 'age is unsigned and is lower than 150'
);

#修改表名
ALTER TABLE 表名 RENAME TO 新表名

#增加字段
ALTER TABLE 表名 ADD joke varchar(10) comment 'joke';

#修改数据类型
/*如果已有数据不兼容新的数据类型，会修改失败*/
ALTER TABLE 表名 MODIFY 字段名 新数据类型；

#修改字段名和字段类型
ALTER TABLE 表名 CHANGE 旧字段名 新字段名 类型（长度）;

#删除字段
ALTER TABLE 表名 DROP 字段名;

#删除表
DROP TABLE IF EXISTS 表名;
#删除表，并重建表
TRUNCATE TABLE 表名;
```





### 数据库三大范式 

一：每个属性都是不可以再分割的原子项

二：满足第一范式，并且非主属性完全依赖于主键，如果是复合主键，非主属性不能只依赖一部分

三：满足第二范式，非主属性与非主属性之间不能有依赖关系，非主属性必须直接依赖于主属性，不能间接依赖主属性

# DML

```sql
#添加数据
#字符串和日期类型要有引号
INSERT INTO 表名(id,anme...) VALUES(1,2,3);
INSERT INTO 表名(id,anme...) VALUES(1,2,3),VALUES(1,2,3);

#修改数据
#如果不写where条件，所有数据都会被修改
#UPDATE是对索引加行锁，如果WHERE后字段没有索引，行锁就会变成表锁
UPDATE 表名 SET name = 'lby' WHERE
UPDATE students SET name='lby' WHERE id>5 or id<7;
UPDATE students SET score=score+10 WHERE score<80;

#删除数据
#如果不写where条件，所有数据都会被删除
DELETE FROM 表名 WHERE

**mysql怎么删除前10000条数据**

在一个连接中循环执行 20 次delete from T limit 500;

串行化执行，将相对长的事务分成多次相对短的事务，则每次事务占用锁的时间相对较短，其他客户端在等待相应资源的时间也较短。这样的操作，同时也意味着将资源分片使用（每次执行使用不同片段的资源），可以提高并发性。
```



# DQL

```sql
SELECT

DISTINCT

#给字段取别名
AS 

FROM

WHERE
	<
	<=
	>
	>=
	!=
	IN
	LIKE '%x'
	#三个下划线代表三个字符
	LIKE '_ _ _X'
	IS NULL
	ADD
	OR
	NOT
	IS NOT NULL
	
	
GROUP BY

#WHERE是分组之前进行过滤，不满足where条件，不参与分组，而having是分组之后进行过滤
#分组之后，查询的字段一般为聚合函数和分组字段，查询其他字段没有意义
HAVING
	#不同工作地点的员工数量和平均年龄,且只有员工数量大于3才参与统计
	SELECT workAddress,COUNT(*),AVG(age) FROM user GROUP BY workAddress HAVING COUNT(*)>3;

ORDER BY
	SELECT * FROM user ORDER BY id ASC,age DESC；

#起始索引从0开始，起始索引=（查询页码-1）*每页显示记录数
LIMIT 起始索引，查询记录数

COUNT()
MAX()
MIN()
AVG()
SUM()

#执行顺序：
FROM WHERE GROUP BY HAVING SELECT ORDER BY LIMIT
```

# DCL

```mysql
#系统数据库
USE mysql

#查询全部用户
SELECT * FROM user/G;

#创建用户
CREATE USER '用户名'@'主机名' IDENTIFIED BY 'password';
#修改密码
ALTER USER '用户名'@'主机名' IDENTIFIED WITH mysql_native_password BY 'newPassword';
#删除用户
DROP USER '用户名'@'主机名'
```



| 权限                | 说明               |
| ------------------- | ------------------ |
| ALL, ALL PRIVILEGES | 所有权限           |
| SELECT              | 查询数据           |
| INSERT              | 插入数据           |
| UPDATE              | 更新数据           |
| DELETE              | 删除数据           |
| ALTER               | 修改表结构         |
| DROP                | 删除数据库/表/视图 |
| CREATE              | 创建数据库/表      |

```mysql
#主机名可以用%通配符
SHOW GRANTS FOR '用户名'@'主机名';
#表名可以用*通配符
GRANT 权限列表 ON 数据库名.表名 TO '用户名'@'主机名';
REVOKE 权限列表 ON 数据库名.表名 TO '用户名'@'主机名';

create user 'FR'@'localhost' IDENTIFIED BY 'ILOVEYOU';
GRANT ALL ON test.* TO 'FR'@'localhost';
```



# 函数

## 字符串函数

| 函数                        | 功能                                        |
| --------------------------- | ------------------------------------------- |
| `CONCAT(s1,s2…sn)`          | 字符串拼接                                  |
| `LOWER(str)`                | 转化成小写                                  |
| `UPPER(str)`                | 转化成大写                                  |
| `TRIM(str)`                 | 去掉头尾空格                                |
| `SUBSTRING(str, start,end)` | 返回字符串str从start到end到字符串           |
| `LPAD（str,n,"pad"）`       | 左填充字符串str，达到n位数，用'pad'进行填充 |
| `RPAD（str,n,"pad"）`       | 右填充字符串str，达到n位数，用'pad'进行填充 |

业务需要。统一员工工号为5位，不足5位，左侧补0:

`update user set workId = Lpad(workId,5,'0');`

## 数值函数

| 函数           | 功能                  |
| -------------- | --------------------- |
| `celi(num)`    | 向上取整              |
| `floor(num)`   | 向下取整              |
| `rand()`       | 生成0-1随机数         |
| `round(num,n)` | 四舍五入，保留n位小数 |

## 日期函数

| 函数                    | 功能                                   |
| ----------------------- | -------------------------------------- |
| `curdate()`             | 返回当前日期                           |
| `curtime()`             | 返回当前时间                           |
| `now()`                 | 获取当前的日期和时间                   |
| `year(date)`            | 获取指定date的年份                     |
| `month(date)`           | 获取指定date的月份                     |
| `day(date)`             | 获取指定date的日期                     |
| `datediff(date1,date2)` | 返回起始时间date1和date2的之间的时间差 |

```sql
mysql> select curdate();
+------------+
| curdate()  |
+------------+
| 2022-08-11 |
+------------+

mysql> select curtime();
+-----------+
| curtime() |
+-----------+
| 23:36:00  |
+-----------+

mysql> select now();
+---------------------+
| now()               |
+---------------------+
| 2022-08-11 23:36:12 |
+---------------------+

mysql> select year(now());
+-------------+
| year(now()) |
+-------------+
|        2022 |
+-------------+
```

## 流程控制函数

| 函数                    | 功能                                             |
| ----------------------- | ------------------------------------------------ |
| `IF(value,t,f)`         | 如果value为true，则返回t，否则返回f              |
| `IFNULL(value1,value2)` | 如果value1不为null，则返回value1，否则返回value2 |



# 约束

约束是作用于表中字段上的规则，限制表中的数据

| 约束     | 描述                                   | 关键字         |
| -------- | -------------------------------------- | -------------- |
| 非空约束 | 字段数据不能为null                     | NOT NULL       |
| 唯一约束 | 保证字段数据唯一                       | UNIQUE         |
| 主键约束 | 主键是一行数据的唯一标识、不能重复     | PRIMARY KEY    |
| 默认约束 | 为指定字段数据，则采用默认值           | DEFAULT        |
| 外键约束 | 让两张表建立连接，保证数据的一致和完整 | FOREIGN KEY    |
| 自增     |                                        | AUTO_INCREMENT |

## **添加外键：**

```mysql
ALTER TABLE user 
ADD CONSTRAINT fk_user_tid 	/*约束名：fk_表名_字段名*/
FOREIGN KEY (person_tid)
REFERENCES person(id);
```

## **删除外键：**

```mysql
ALTER TABLE user 
DROP FOREIGN KEY fk_user_tid;
```

## **删除/更新行为：**

```mysql
#默认情况：No Action
当在父表中删除/更新数据时，首先检查该记录是否有外键，如果有，则不允许删除/更新

#级联操作：cascade
当在父表中删除/更新数据时，首先检查该记录是否有外键，如果有，则删除/更新外键在子表中的数据
alter table 表名 ADD CONSTRAINT FOREIGN KEY (外键字段) REFERENCES 主表名（主表字段） on update CASCADE on delete CASCADE;

#SET NULL
当在父表中删除数据时，首先检查该记录是否有外键，如果有，则set外键在子表中的数据为null
```





# 多表查询

## 关系

> 一对多关系

关系：一个部门有多个员工

实现：在**多的一方**，也就是员工一方建立外键，指向**一的一方**的主键，即员工的外键指向部门的主键



> 多对多关系

关系：一个学生可以选多门课程，一个课程可以有多个学生选择

实现：建立第三张**中间表**，中间表包含两个**外键**，分别指向两方的**主键**

| id   | student_id | class_id |
| ---- | ---------- | -------- |
| 1    | 1          | 1        |



> 一对一关系

关系：用于单表拆分，将一张表的部分基础字段放在另一张表里，以提升查询效率

实现：在任意一方加入外键，关联另一方的主键





## 普通多表查询

`select * from students,classes;`

查询结果是students和classes表的“乘积”，又称作笛卡尔集

<img src="/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-06-23 at 9.40.35 PM.png" alt="Screen Shot 2022-06-23 at 9.40.35 PM" style="zoom:25%;" />



## 内连接

**隐式内连接：**

```mysql
SELECT * FROM student,class WHERE student.id = class.id;
```

**显式内连接：**

```mysql
SELECT * FROM student INNER JOIN class ON student.id = class.id;
```



## 外连接

```sql
SELECT * FROM student LEFT OUTER JOIN class ON student.id = class.id;
SELECT * FROM student RIGHT OUTER JOIN class ON student.id = class.id;
SELECT * FROM student FULL OUTER JOIN class ON student.id = class.id;
```



## 自连接

![IMG_20F7D4738A08-1](/Users/Typora_Pictures/:Users:xiaobing:Downloads:IMG_20F7D4738A08-1.png)



```mysql
自查询必须取别名
SELECT * FROM 表A 别名A,表A 别名B WHERE 条件

查询员工及其所属领导的名字
SELECT a.name,b.name FROM emp A,emp B where a.managerId = b.id;

查询员工及其所属领导的名字,如果员工没有领导，也要查询处理
SELECT a.name,b.name FROM emp A left outer join emp B on a.managerId = b.id;
```

## 联合查询

把多次查询的结果合并起来，形成一个新的查询结果集

```mysql
SELECT student.id FROM student
UNION/UNION ALL
SELECT class.id FROM class;
```

**UNION和UNION ALL的区别：**UNION ALL直接将查询结果合并，UNION直接将查询结果去重

查询成功的条件：两张表的**字段列数和字段类型**必须保持一致，否则查询失败





## 子查询（嵌套查询）

根据子查询出现的位置，分为：`WHERE`之后、`FROM`之后、`SELECT`之后



### 标量子查询

```mysql
查询在“李炳煜”入职之后的员工信息
	a.查询李炳煜的入职日期
	b.查询在指定入职日期后的员工信息
select * from emp wehre entrydate > (select entrydate from emp where name ='lby');
```



### 列子查询

**常见操作符：**`IN`、`NOT IN`、`ANY`、`ALL`

```mysql
查询销售部和市场部的所有员工信息
 a.查询销售部和市场部的id
 b.根据部门id查询信息
 select id from dept where name = '销售部' or name = '市场部';
 select * from emp where dept_id IN (id);

select * from emp where dept_id IN (select id from dept where name = '销售部' or name = '市场部');

查询比财务部所有人工资都高的员工信息
	a.查询财务部的id
	b.查询财务部与员工的工资列表
	select id from dept where name = '财务部';
	select salary from emp where dept_id = id;
	select salary from emp where dept_id = (select id from dept where name = '财务部');
	select * from emp where salary > all (select salary from emp where dept_id = (select id from dept where name = '财务部'));

查询比研发部其中任意一人工资高的员工信息
	select id from dept where name = '研发部';
	select salary from emp where dept_id = (select id from dept where name = '研发部');
	select * from emp where salary > any(select salary from emp where dept_id = (select id from dept where name = '研发部'));
```



### 行子查询

返回结果为一行数据，包含多个列

**`=`、`<>`、`IN`、`NOT IN`**

```mysql
查询与李炳煜工资及其直属领导相同的员工信息
	a.李炳煜的工资，及其领导
	select salary,manager_id from emp where name = 'lby';
	b.符号a条件的数据
	select * from emp where (salary,manager_id) = (select salary,manager_id from emp where name = 'lby');
```



### 表子查询

返回结果是多行多列

`IN`

```mysql
查询与李炳煜、付娆的职位和薪资相同的员工信息
	a.查询李炳煜、付娆的职位和薪资
	select position,salary from emp where name = 'lby' or name = 'fr';
	select * from emp where (position,salary) in (select position,salary from emp where name = 'lby' or name = 'fr');
	
查询入职日期是2006-01-01	之后的员工信息，及其部门信息
	a.入职日期是2006-01-01	之后的员工信息
	select * from emp where entrydate > '2006-01-01';
	因为数据在两张表内就用连接查询
	select e.*,d.* from (select * from emp where entrydate > '2006-01-01') e left outer join dept on e.dept_id = dept.id;
```

```mysql
SELECT * FROM user 
where user.id IN(
	SELECT order.user_id FROM order
);

SELECT * FROM user
WHERE EXISTS(
	SELECT order.user_id FROM order
  WHERE user.id = order.user_id
);
```

**IN和EXISTS的区别：**

IN先查内表，再查外表，适用于外表数据量大的情况

EXISTS先查外表，再查内表，适用于内表数据量大的情况




## 事务

> 简介

定义：一组操作的集合，事务把所有的操作作为一个整体一起向数据库提交或者回滚

实现：A向B转账100。查询A账户是否有100块钱，A账户-100，B账户+100

MySQL的事务是自动提交的

开启事务：`BEGIN`

提交事务：`COMMIT`

回滚：`ROLLBACK`



> 四大特性

**ACID**

A	atomic：原子性，事务是不可分割的最小操作单元，要么全部执行成功，要么全部执行失败。（例如转账）

C consistency：一致性，数据状态保持一致，A-100，B+100

I isolation：隔离性，若干个并发事务作出的修改不会互相影响

D	duration：持久性，事务完成后对数据的改变是永久的



> 并发事务引发的问题

脏读：一个事务读到其它事务没有提交的数据

不可重复读：一个事务先后读取同一条数据，但两次数据不同

幻读：：一个事务查询数据时，数据不存在，插入数据时，又发现它已经存在了



> 事务的隔离级别

| 级别                    | 脏读 | 不可重复读 | 幻读 |
| ----------------------- | ---- | ---------- | ---- |
| Read Uncommitted        | 可   | 可         | 可   |
| Read committed          |      | 可         | 可   |
| Repeatable Read（默认） |      |            | 可   |
| Serializable            |      |            |      |

隔离级别越高，数据越安全，性能越低



**mysql怎么防止幻读？**

```sql
SELECT * FROM USER WHERE id = 1 FOR UPDATE;
```

通过对select操作手动加行X锁(SELECT。原因是InnoDB中行锁锁定的是索引，纵然当前记录不存在，当前事务也会获得一把记录锁（记录存在就加行X锁，不存在就加next-key lock间隙X锁），这样其他事务则无法插入此索引的记录，杜绝幻读



## 存储引擎

### MySQL体系结构

**连接层：**连接处理、授权认证

**服务层：**缓存的查询，SQL分析和优化，函数的执行

**引擎层：**负责数据的存储和提取

**存储层：**将数据存储在文件系统上

### 存储引擎

存储引擎是基于表的。存储数据，建立索引，更新/查询数据

**InnoDB特性：**事务	外键	行级锁

​	xxx.ibd，xxx是表名，每张表都对应这样一个表空间文件，保存表结构，数据和索引

## Linux安装MySQL

1.查看Linux的操作系统版本和系统内核版本

```shell
[root@nfs_client ~]# cat /etc/redhat-release     查看操作系统版本
CentOS Linux release 7.5.1804 (Core) 
[root@nfs_client ~]# uname -r               查看系统内核版本
3.10.0-862.el7.x86_64
```

2.在官网选择与系统版本和内核版本相对应的MySQL版本

https://dev.mysql.com/downloads/mysql/

**不同的系统和内核要安装不同版本的MySQL**

3.找到版本后，在清华大学的镜像官网，找到相应的MySQL下载链接

https://mirrors.tuna.tsinghua.edu.cn/mysql/downloads/MySQL-8.0/

**选择 RPM Bundle**

4.在Linux系统下载文件，并解压文件

```shell
[root@nfs_client ~]	wget https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-5.7.22-1.el7.x86_64.rpm-bundle.tar

tar -xvf	xxx.tar	#解压语句
```

5.**使用 rpm 命令安装MySql组件**

按照依赖关系依次安装rpm包 依赖关系依次为common→libs→client→server

**期间会报错，根据提示安装不同的依赖**

```shell
rpm -ivh mysql-community-common-5.7.22-1.el7.x86_64.rpm
rpm -ivh mysql-community-libs-5.7.22-1.el7.x86_64.rpm
rpm -ivh mysql-community-client-5.7.22-1.el7.x86_64.rpm
rpm -ivh mysql-community-server-5.7.22-1.el7.x86_64.rpm
```

6.**启动MySql**

```shell
systemctl start mysqld.service    启动mysql
systemctl status mysqld.service  查看mysql状态
systemctl stop mysqld.service   关闭mysql
```

7.更改密码

在安装过程中，会在安装日志中生成一个临时密码

先关闭MySQL（存疑）

使用`grep 'temporary password' /var/log/mysqld.log`找到临时密码

```shell
[root@nfs_client tools]# grep 'temporary password' /var/log/mysqld.log    # 在/var/log/mysqld.log文件中搜索字段‘temporary password’
2018-07-18T06:02:23.579753Z 1 [Note] A temporary password is generated for root@localhost: n(jPp4l-C33#
```

 **n(jPp4l-C33#**即为登录密码。使用这个随机密码登录进去，然后修改密码，使用命令：

  **mysql -uroot -p**

更改密码语句：

**低强度密码会报错**

```mysql
mysql> alter user root@localhost identified by 'sdbrk';
ERROR 1819 (HY000): Your password does not satisfy the current policy requirements
             或
mysql> set password for root@localhost=password('sdbrk');
ERROR 1819 (HY000): Your password does not satisfy the current policy requirements
```



## 索引

索引是用来帮助MySQL高效获取数据的数据结构（有序）

索引提高了查询效率，但降低了增删改的效率



### 索引结构

**不同的存储引擎有不同的结构：**

- **B+Tree索引**，最常见的类型
- Hash索引，不支持范围查询，只能精确匹配查询



**为什么选择B+Tree：**

- 二叉树缺点：顺序插入时，节点全在左侧，会形成一个链表，查询性能大大降低
- 红黑树：大数据量情况下，层级较深
- B+ Tree（多路平衡查找树），对B Tree的优化

#### B Tree

**查看演变过程的网站：**

http://www.rmboot.com/BTree.html

如果最大度数（Max. Degree） = 5，那么一个节点最多存储4个key值，5个指针<img src="/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-06-27 at 10.31.01 PM.png" alt="Screen Shot 2022-06-27 at 10.31.01 PM" style="zoom:50%;" />

插入一个数13，它在0011，0088之间，所以它被分配到第二个指针指向的空间，然后插入在0012和0078之间，与此同时，这个节点存储了5个key，超过范围，会从中间开始裂变：

中间数0012进位到上一层节点，然后上一层节点存储5个key，超过范围，也开始裂变，中间数0088进位，从中间开始分裂

<img src="/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-06-27 at 10.33.00 PM.png" alt="Screen Shot 2022-06-27 at 10.33.00 PM" style="zoom:50%;" />

#### B+Tree

如果最大度数（Max. Degree） = 4，那么一个节点最多存储3个key值，4个指针<img src="/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-06-27 at 10.37.37 PM.png" alt="Screen Shot 2022-06-27 at 10.37.37 PM" style="zoom:50%;" />

B+Tree最大的区别，所有的节点都会出现在叶子节点，也就是最后一个层级，叶子节点形成一个单向链表，上面的层级只是起到索引作用。

**MySQL对B+Tree进行优化**：在原有基础上，增加了一个相邻节点的链表指针，也就是说，**叶子节点是双向链表**



#### Hash索引

哈希索引采用一定的hash算法，将键值换算成hash值，映射到对应的槽位值，存储在Hash表中

如果两个键值映射到同一个相同的槽位上，就会产生哈希冲突/碰撞，可以通过再增加一层链表解决问题

​	005槽位->金庸->韦小宝->李炳煜

​	006槽位->付娆



缺点：

- Hash索引只能用于精确查找(=,in)，不支持范围查询(< > )

- 查询效率高

- 不能完成排序操作



#### **MySQL为什么采用B+Tsree？**

相比于二叉树，层级少，效率高。

B Tree无论是叶子节点还是非叶子节点都会存储数据，保存大量数据，只能增加树的高度，这样导致效率低。而B+Tree非叶子节点充当索引，加快查询速度，叶子节点是双向链表，支持范围索引和排序操作



### 索引分类

- 主键索引、唯一索引、常规索引、全文索引
- 聚集索引、二级索引
- 单列索引、联合索引



主键索引	PRIMARY

唯一索引	UNIQUE  	为某一个字段添加唯一约束，就自动创建唯一索引

常规索引

全文索引	FULLTEXT	//很少用



**<u>根据索引的存储形式，又分成两种：</u>**

1. **聚集索引：**将数据存储与索引放在一起，<u>索引结构的每一个叶子节点存储一行数据</u>		必须有，而且只有一个
2. **二级索引：**将数据与索引分开存储，索引结构的叶子节点存储的是对应的主键。（如果对字段name建立唯一索引index_name，那么index_name就是二级索引，它每一个叶子结点的值是name，然后指向主键



**聚集索引的选取规则：**

- 如果存在主键，主键索引就是聚集索引
- 如果不存在主键，将第一个唯一索引作为聚集索引



**单列索引与联合索引：**

- 单列索引：一个索引只包含单个列
- 联合索引：一个索引包含了多个列

如果存在多个查询条件，建立索引时，建议使用联合索引



**聚集索引与二级索引的区别：**

**一条数据：**`id=1 name=lby age=23 gender=male`

`id`是主键，也是聚集索引的叶子节点的key，这个叶子节点存储了这一行的数据

如果对`name`添加唯一索引，就生成一个二级索引，叶子节点的key是`name`的值，也就是`lby`，存储的数据是主键`id`

**查询语句：**`select * from user where name="lby";`

因为`name`有二级索引，所以在二级索引找到`key=lby`的叶子节点，得到主键`id`，因为`select *`,所以用`id`去聚集索引查询，找到`key=1`的叶子节点，从而得到一整行数据。这叫回表查询。回表查询效率低



**以下哪个SQL语句效率高？**

`select * from user where id = 1;`

`select * from user where name='lby'`

第一条效率高



### 索引语法

**创建索引：**

`CREATE INDEX index_tableName_columnName ON table_name(name);  `

```mysql
CREATE INDEX idx_user_name ON user(name);	#给user表的name字段创建索引
CREATE UNIQUE INDEX idx_user_phone ON user(phone); #user表的phone字段创建唯一索引
CREATE INDEX idx_user_profession_age_status ON user(profession,age,status);
#user表的profession,age,status字段创建联合索引
```

**查看索引：**

`SHOW INDEX FROM table_name\G`

**删除索引：**

`DROP INDEX index_name ON table_name`



### SQL性能分析

#### SQL执行频率

根据查询频率，对频率高的SQL进行优化

通过`show [session|global] status`命令可以提供服务器状态信息，然后查看当前数据库的SQL访问频率

```mysql
show global status like 'Com_______';	#七个下划线代表七个字符，查看全局SQL频率
show session status like 'Com_______';	#查看一次会话的SQL频率
```



#### **慢查询日志**

慢查询日志记录了所有执行时间超过指定参数（默认10秒）的SQL语句的日志。慢查询日志默认不开启，需要在MySQL配置文件（/etc/my.cnf）中配置如下信息：

```mysql
[root@VM-4-8-centos lighthouse]# vi /etc/my.cnf

#开启 慢查询日志  
slow_query_log=1
#时长超过两秒的查询为慢查询
long_query_time=2
```

开启慢查询后，在/var/lib/mysql中存放在xxxslow.log，即慢查询日志

```shell
#重启MySQL
[root@VM-4-8-centos lighthouse]# systemctl restart mysqld 

cd /var/lib/mysql
ll
VM-4-8-centos-slow.log
cat VM-4-8-centos-slow.log
```



#### profile详情

show profiles能够在SQL优化时帮我们了解时间都耗费在哪里了。通过have_profiling参数，能够看出MySQL是否支持profile操作

```mysql
#是否支持profile操作
mysql> SELECT @@have_profiling;

#profile操作是否打开，0关闭，1开启
mysql> SELECT @@profiling;

#开启profiling
mysql> set profiling = 1;

#查看每一条SQL的耗时时间
mysql> show profiles;
+----------+------------+--------------------+
| Query_ID | Duration   | Query              |
+----------+------------+--------------------+
|        1 | 0.00015675 | SELECT @@profiling |
|        2 | 0.00449875 | show databases     |
|        3 | 0.00015125 | SELECT DATABASE()  |

#查看一条SQL在各个阶段的耗时时间
mysql> show profile for query 2;
+--------------------------------+----------+
| Status                         | Duration |
+--------------------------------+----------+
| starting                       | 0.000196 |
| Executing hook on transaction  | 0.000009 |
| starting                       | 0.000010 |
| checking permissions           | 0.000008 |
| Opening tables                 | 0.002227 |
| init                           | 0.000015 |
| System lock                    | 0.000019 |
| optimizing                     | 0.000040 |
| statistics                     | 0.000089 |
| preparing                      | 0.000457 |
| Creating tmp table             | 0.001121 |
| executing                      | 0.000198 |
| end                            | 0.000007 |
| query end                      | 0.000005 |
| waiting for handler commit     | 0.000015 |
| closing tables                 | 0.000032 |
| freeing items                  | 0.000038 |
| cleaning up                    | 0.000015 |
+--------------------------------+----------+
```



#### explain执行分析

EXPLAIN或者DESC，后接任意一条SQL语句

`EXPLAIN SELECT * FROM xxxx `

```mysql
ysql> desc select * from user\G;
*************************** 1. row ***************************
           id: 1
  select_type: SIMPLE
        table: user
   partitions: NULL
         type: ALL
possible_keys: NULL
          key: NULL
      key_len: NULL
          ref: NULL
         rows: 2
     filtered: 100.00
        Extra: NULL
1 row in set, 1 warning (0.00 sec)
```

**id**:表示查询中执行select子句的执行顺序，id越大，越先执行

**type：**表示连接类型，性能由好到差分别是NULL、system、const（id、唯一索引）、eq_ref、ref（非唯一索引）、range、index、all

**possible_keys：**可能用到的索引

**key：**使用的索引

**key_len**：索引长度，长度越短越好

**Extra**：值为NULL，代表执行了回表查询

**filtered:**返回结果的行数占需读取行数的百分比，值越大越好，代表读取的少，命中率高



### 索引使用

#### SQL提示

在SQL语句中建议MySQL使用哪个索引

**`use index`**

建议使用某个索引

```mysql
SELECT * FROM user USE INDEX(idx_user_pro) WHERE profession='软件工程';
```

**`ignore index`**

建议不使用某个索引

```mysql
SELECT * FROM user IGNORE INDEX(idx_user_pro) WHERE profession='软件工程';
```

**`force index`**

强制使用某个索引

```mysql
SELECT * FROM user FORCE INDEX(idx_user_pro) WHERE profession='软件工程';
```



#### 覆盖索引

查询的字段都有索引，`select id,name from user`，id和name都有索引，这样可以避免回表查询

![IMG_0013](/Library/Screen Savers/IMG_0013.jpg)

第一条语句，直接查询聚集索引，效率高

第二条语句，直接查询辅助索引，根据name找到id，效率高

第三条语句，先查询辅助索引，得到id，然后用id在聚集索引里查到gender数据。这叫回表查询，效率低



**Question：**

一张表有四个字段（id、name、password、status），由于数据量大，需要对以下SQL进行优化，如果优化才是最优方案：

`select id,name,password from user where name='lby';`

答案：根据name和password建立索引，从而避免回表查询



#### 最左前缀法则（联合索引）：

针对联合索引，如果查询的时候，最左侧的字段没有使用，则索引失效



对profession、age、status建立联合索引

`SELECT * FROM user where profession="软件工程" and age=23 and status=0`;	//走索引

`SELECT * FROM user where age=23 and status=0`;	//不走索引，如果最左侧的列消失，右侧的列不会走索引

`SELECT * FROM user where profession="软件工程" and status=0`;//走索引，最左侧的列没有消失

`SELECT * FROM user where status=0 and profession="软件工程" `;//走索引



#### 范围查询（复合索引）：

联合索引中，出现范围查询（>,<），范围查询右侧的列索引失效

`SELECT * FROM user where profession="软件工程" and age>23 and status=0`;//status不会走索引

**规避方案：**

`SELECT * FROM user where profession="软件工程" and age>=23 and status=0`;//三个字段都走索引



#### 前缀索引：

当字段类型为字符串时，有时候需要索引很长的字符串，这会让索引变得很大，查询时效率很低。因此，对字符串的一部分前缀建立索引，就可以节约索引空间，提高查询效率

建立前缀索引:

`create index idx_xxx on table_name(column(n));`

**column**为字段，**n**为字符数

难点是：n应该为多少，也就是说，前缀长度应该为多少

n的长度是根据索引的选择性来决定，选择性是指【不重复的索引值】和【记录总数】的比值。选择性越高，查询效率越高。最好的索引选择性是1

```mysql
SELECT count(distinct email)/count(*) from user;#不重复的email值/记录总数
SELECT count(distinct substring(email,1,5))/count(*) from user;
#substring对email截取前五个字符，所有email值的前五个字符里不重复的数量/记录总数，如果为1，则n为5

CREATE index idx_email_5 ON user(email(5));
```



#### 索引失效的情况	

##### 索引列运算：

在索引列上进行运算操作，索引将失效

##### 字符串不加引号：

字符串不加单引号，索引将失效

##### 模糊查询：

头部模糊匹配，索引失效

`SELECT * FROM user where profession like "%工程" `;//索引失效，type变成ALL，全表扫描

##### or连接的条件：

用or分割开的条件，如果or前的条件中的列有索引，而后面的列没有索引，那么涉及的索引都不会用到

`SELECT * FROM user where id=10 or age=23 `;//id有索引，age无索引，所以不用索引

##### 数据分布影响

查询结果的数据量特别多，走索引比走全表扫描更慢，则不使用索引



#### 索引设计原则

- 针对数据量大，且查询频率高的表建立索引

- 针对常作为查询条件Where、排序Order by、分组group by操作的字段建立索引

- 如果是字符串类型的字段，字段较长，可以建立前缀索引

- 尽量使用联合索引，避免回表查询

- 控制索引数量，索引会影响增删改的效率

  

## SQL优化

### 插入数据优化

#### 批量插入

频繁访问数据库要建立链接，浪费资源

insert into user values(…),(…),(…)

#### 手动提交事务

MySQL自动开启事务，开始/关闭事务都耗费资源，所以可以手动提交事务

start transaction

insert….

commit

#### 主键顺序插入

主键顺序插入1、2、3、4、…

#### 大批量插入数据

插入大批量数据，insert性能低，用MySQL提供的load指令进行插入:

```mysql
#客户端连接服务端时，加上参数 -local-infile
mysql --local-infile -u root -p

#查看是否打开
select @@local_infile

#设置全局参数local_infile为1，开启从本地加载文件导入数据的开关
set global local_infile = 1;

#执行load指令将准备好的数据加载到表结构中
mysql> load data local infile '/home/lighthouse/test.log' into table user fields terminated by ',' lines terminated by '\n';
```



### 主键优化

#### 数据组织方式

在InnoDB里，表数据都是根据主键顺序存放的

##### **页分裂：**

主键乱序插入会导致一页内的主键数据并不连贯，再插入中间的数值就没有空间存放，然后InnoDB会分割不连贯的数据，将其分割成好几页，然后再插入中间的数据，以使主键连贯。



##### **页合并：**

当删除一行的数据，实际上记录没有被物理删除，而是被标记为删除，并且允许它的空间被其他记录使用。当删除的记录达到50%（默认值）时，InnoDB会在前后页看看是否可以将页面合并以优化空间

#### 主键设计原则

- 主键长度要低，二级索引叶子节点保存的是主键
- 插入数据要顺序插入，使用auto_increment自增主键
- 不要用UUID/其他主键作为主键
- 业务操作，尽量避免对主键修改



### order by优化

通过索引顺序扫描直接返回有序数据，不需要额外排序，效率高

**简单说，order by的字段要有索引**

```mysql
select id,age,phone from user order by age,phone;
create index idx_user_age_phone on user(age,phone);
#如果全部使用升序/降序，依旧会走索引。然后一个升，一个降，则不会走索引
select id,age,phone from user order by age desc,phone desc;
#再创建索引
create index idx_user_age_phone_ad by age asc,phone desc;
```



### group by优化

`group by`和`order by`都需要满足最左前缀法则



### limit优化

大数据量时，起始数越大，效率越低。`limit 10000,10`效率就很低。仅仅返回10条数据，然后丢掉其他数据，代价很大。

优化思路：避开回表查询，再加上子查询进行优化。

```mysql
#效率低
SELECT * FROM user limit 10000,10;

#效率高
SELECT id FROM user limit 10000,10;
#避开了回表查询，但是数据不全
#将SELECT id FROM user limit 10000,10当作一个表
#IN查询会报错，语法不支持
SELECT u.* FROM user u,(SELECT id FROM user limit 10000,10) s where u.id=s.id;
```



### count优化

count用法:`count(*),count(主键)、count（字段）、count（1）`

`count(主键):`将每一行的id都取出来，直接进行累加

`count（字段）:`字段没有not null约束，将每一行的值都取出来，判断是否为null，不为null计数累计。如果有not null约束，则直接累加

`count(1):`遍历整张表，但不取值，服务层对于每一行放一个数字1，直接按行累加

`count(*):`专门做了优化，不取值，服务层直接按行累加

效率：*>1>n>主键>字段

### update优化

InnoDB的行锁是对索引加锁，而不是针对记录加锁，并且索引不能失效，否则从行锁升级成表锁。

`update user set phone='1234' where name='lby'；`

如果name没有索引，就会对整张表都进行锁。如果name有索引，就仅仅对name=lby这一行语句加锁



## 存储过程

事先经过编译并存储在数据库中的一段SQL语句的集合，调用存储过程可以简化开发，减少数据在数据库和服务器之间的传输，可以提高数据处理效率。简单说，就是对SQL代码的封装和复用。

- 封装、复用
- 可以接收参数，也可以返回数据
- 减少网络交互，提高效率

### 语法

#### 创建存储过程

```mysql
#创建
CREATE PROCEDURE 存储过程名称【参数列表】
BEGIN
	--SQL
END;

#调用
CALL 名称【参数】

#example
create procedure p1()
begin
	select * from user;
end;

call p1();
```

#### 查看存储过程

```mysql
#查看特定数据库的存储过程
SELECT * FROM information_schema.ROUTINES WHERE ROUTINE_SCHEMA = 'test';
SHOW CREATE PROCEDURE p1;

#删除
DROP PROCEDURE name;
```

**用命令行写存储过程会遇到一个问题：**

select语句写完后有；MySQL默认语句结束了，所以用`delimiter $$`,将结束符号改成`$$`



### 系统变量

（存储过程的语法结构）

系统变量：Mysql提供的变量，有全局变量（GLOBAL）和会话变量（SESSION）

#### 查看系统变量

```mysql
SHOW [SESSION|GLOBAL] VARIABLES;
SHOW [SESSION|GLOBAL] VARIABLES like '。。。';
SELECT @@[SESSION|GLOBAL] 系统变量名；
```

#### 设置系统变量

```mysql
SET [SESSION|GLOBAL] 系统变量名=值;
SET @@[SESSION|GLOBAL] 系统变量名=值;
```

**例子：**

```mysql
#查找自动提交
SHOW GLOBAL VARIABLES like 'auto%';
--------------------------+-------+
| Variable_name            | Value |
+--------------------------+-------+
| auto_generate_certs      | ON    |
| auto_increment_increment | 1     |
| auto_increment_offset    | 1     |
| autocommit               | ON    |
| automatic_sp_privileges  | ON    |
+--------------------------+-------+
#关闭自动提交
#SESSION是会话级别，重启后就消失
SET SESSION autocommit = '0';
```



### 用户自定义变量

用户变量不能提前声明，用`@变量名`使用就可以，作用域为当前连接

#### 赋值

```mysql
SET @var_名字 = 值;
SET @var_名字 = 值 ,@var_名字2 = 值2; 
SET @var_名字 := 值;
#把select 字段 from 表名 的 查询结果赋值给@var_name
SELECT 字段 into @var_name from 表名;
```



#### 使用

```mysql
SELECT @var_name;
```



### 局部变量

访问之前需要declare声明，局部变量的范围是在BEGIN…END块

#### 声明

```mysql
DECLARE 变量名 变量类型 [default];
SET 变量名 = 值;
SET 变量名 := 值;
SELECT 字段 into 变量名 from 表名;

#example
delimiter $$

create procedure p2()
begin
	declare user_count int;
	select count(*) into user_count from user;
	select user_count;
end$$

+------------+
| user_count |
+------------+
|          7 |
+------------+
```

### 条件判断

```mysql
if 条件1 then
	...
elseif 条件2 then	--可选
	...
else		--可选
	...
end if$$

#example 
#传入score参数，判断等级
create procedure p3()
begin
	declare myscore int default 85;
	declare result varchar(10);
	
	if myscore >=85 then
		set result := 'good!';
	elseif myscore >=60 then
  	set result := 'ok!';
  else 
  	set result := 'bad!';
	end if;
	select result;
end$$

call p3()$$

+--------+
| result |
+--------+
| good!  |
+--------+
```

### 参数

- **IN**：该参数作为输入，也就是调用时传入的参数。为默认类型
- **OUT**：该参数作为输出，也就是可以作为返回值
- **INOUT**：既可以作为输入参数，也可以作为输出参数

```MySQL
CREATE PROCEDURE name(IN/OUT/INOUT 参数名 参数类型)
BEGIN
	--SQL
END；	
```

**例子：**

```mysql
#example 
#传入score参数，判断等级，并返回
create procedure p4(IN score int,OUT result varchar(10))
begin
	if score >=85 then
		set result := 'good!';
	elseif score >=60 then
  	set result := 'ok!';
  else 
  	set result := 'bad!';
	end if;
end$$

#一个参数为IN，一个参数为out，out参数为用户自定义变量
mysql> call p4(40,@result)$$
SELECT @result$$

+---------+
| @result |
+---------+
| bad!    |
+---------+
```



### case

```mysql
#语法一
#case_value = when_value1,执行statement1
case case_value
	when when_value1 then statement1
	when when_value1 then statement2
	else statement
end case;	

#语法二
#condition1成立,执行statement1
case 
	when condition1 then statement1
	when condition2 then statement2
	else statement
end case;	
```

**例子：**

```mysql
#根据传入的月份，判断月份为1、2、3、4季度
create procedure p5(IN month int, OUT result2 varchar(10))
begin
	case 
		when (month>=1 and month <=3) then 
			set result2 := '1';
		when (month>=4 and month <=6) then 
			set result2 := '2';
    when (month>=7 and month <=9) then 
			set result2 := '3';
    when (month>=10 and month <=12) then 
			set result2 := '4';  
		else
    	set result2 := 'wrong';
  end case;
  select concat('月份为：',month,'季度为:',result2);
end$$

call p5(4,@result2)$$
+---------------------------------------------------+
| concat('月份为：',month,'季度为:',result2)        |
+---------------------------------------------------+
| 月份为：4季度为:2                                 |
+---------------------------------------------------+
```

### 循环

#### while

```mysql
WHILE 条件 DO
	SQL;
END WHILE;

#example
#计算从1到n的累加值，n为传入的参数
set @n := 10$$

create procedure p6(IN n int)
BEGIN
	DECLARE total int default 0;
	WHILE(n>0) DO
		set total := total+n;
		set n := n-1;
	END WHILE;
	SELECT total;
END$$

mysql> call p6(@n)$$
+-------+
| total |
+-------+
|    55 |
+-------+
```

#### repeat

有条件的循环语句，满足条件后退出循环

```mysql
#先执行一次逻辑，如果满足，则推出，不满足则进行下一次循环
REPEAT
	SQL
	UNTIL 条件
END REPEAT；	
```

**例子：**

```mysql
#从1累加到N，返回N的值
set @n := 10;

CREATE PROCEDURE p7(IN n int)
BEGIN
	DECLARE result int default 0;
	REPEAT
		set result := result+n;
		set n := n-1;
	until n <= 0
	END REPEAT;	
	select result;
END$$

call p7(@n)$$
+--------+
| result |
+--------+
|     55 |
+--------+
```

#### loop

**LEAVE:**退出当前循环

**ITERATE：**跳过这一次循环，进入下一次

```mysql
[begin_label:]LOOP
	SQL
END LOOP[begin_label];

LEAVE label;
ITERATE label;
```

**例子：**

```mysql
#从1累加到N，返回N的值
CREATE PROCEDURE p8(IN n int)
BEGIN
	DECLARE total int default 0;
	sum:LOOP
		if(n<0) then
			leave sum;
		end if;
		set total := total+n;
		set n := n-1;
	END LOOP sum;
	select total;
END$$
```

### 游标：

游标是存储查询结果集的数据类型，在存储过程和函数中可以使用游标对结果集进行循环处理。

游标的使用包括：游标的声明、OPEN、FETCH和CLOSE：

```mysql
DECLARE 游标名字 CURSOR FOR 查询语句：
OPEN 游标名字;
FETCH 游标名字 INTO 变量;
CLOSE 游标名字;
```

条件处理程序：handler，当流程控制结果出现问题时的处理步骤

```mysql
DECLARE handler_action HANDLER FOR condition_value statement

handler_action:
	continue
	exit
condition_value：
	SQLSTATE ‘code’:状态码，如02000
	SQLWARNING：以01开头的状态码的简写
	NOT FOUND:以02开头的状态码的简写
```



**例子：**

```mysql
#将user表的数据都复制到user2中
#变量声明要在游标之前
#查询user表的name和age
#handler作用是当出现02000错误时，退出并关闭游标
#创建user2
#打开游标
#开启循环，将游标存储的数据分别赋值给声明的变量
#将变量插入到user2中，主键自增，所以为null
#这是一个while死循环，所以一定会报错
CREATE PROCEDURE p1(in user_id int)
BEGIN
	DECLARE u_name varchar(10);
	DECLARE u_age int;
	DECLARE u_cursor cursor for select name,age from user where id <=user_id;
	DECLARE EXIT HANDLER FOR SQLSTATE'02000' close u_cursor;
	
	CREATE TABLE user2(
  id int primary key auto_increment,
  name varchar(10),
  age int
  );
  
  OPEN u_cursor;
  while true do
	  FETCH u_cursor into u_name,u_age;
	  insert into user2 values(null,u_name,u_age);
  end while;
  CLOSE u_cursor;
END$$
```

# 锁

![截屏2022-08-09 下午10.48.01](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-08-09 下午10.48.01.png)

### 概述

协调多个进程/线程并发访问某个资源的机制。保证数据并发访问的一致性、有效性。

MySQL锁的分类：

- 全局锁：锁所有表

- 表级锁：锁一张表

- 行级锁：锁一行数据

  

## 全局锁

对整个数据库加锁，整个数据库变成只读状态，更新操作的语句会被阻塞

典型场景：做全库的逻辑备份，使用msqldump

```mysql
#对数据库加入全局锁
flush tables with read lock;

#退出mysql，在系统目录下执行
[root@VM-4-8-centos lighthouse]# mysqldump -u root -p test>test.sql

unlock tables;
```

**特点：**

- 在主库上备份，业务停摆
- 在从库上备份，备份期间从库不能执行从主库同步过来的binlog，导致主从延迟

**解决：在备份时加上参数`--single-transaction`，完成不加锁的一致性数据备份**

```mysql
[root@VM-4-8-centos lighthouse]# mysqldump --single-transaction -u root -p test>test.sql
```



## 表级锁

锁住一张表，锁的粒度大，发生锁冲突的概率高，并发度低

表级锁分成三类：表锁、元数据锁、意向锁



### 表锁

- 表共享读锁（read lock）
- 表独占写锁（write lock）

```mysql
#加锁
lock tables 表名 read/write

#释放锁
unlock tables 或者 关闭客户端连接
```



### 元数据锁

- 自动加锁

- 表上有活动事务的时候，不可以对元数据（表结构）进行写入操作

  

### 意向锁

线程A对user表的一行数据加了行锁。线程B对user表加表锁之前，会检查每一条数据是否有行锁。为了减少行锁的检查，引入了意向锁。线程B会直接检查意向锁。

意向共享锁（select）：与表读锁兼容，与表写锁互斥

意向排他锁（update，insert）：与表读锁和表写锁互斥

```mysql
#为这行数据加入行锁，并且为表加入意向共享锁
select * from user where id = 3 lock in share mode;

#update会自动加行锁，并且加入意向排他锁
```



## 行级锁（InnoDB）

锁的粒度最小，发生冲突概率最低，并发度最高

行锁是对索引上的索引项加锁，而不是对数据加锁，如果查询字段没有索引，行级锁会自动升级成表锁

行锁分为三类：行锁（Record Lock）、间隙锁（Gap Lock）、临键锁（Next-key Lock）



**mysql默认⾏锁类型就是 临键锁(Next-Key Lock)**

临键锁就是记录锁(Record Locks)和间隙锁(Gap Locks)的结合，即除了锁住记录本⾝，还要再锁住索引之间的间隙。当我们使⽤范围查询，并且命中了部分 record 记录，此时锁住的就是临键区间。注意，临键锁锁住的区间会包含最后⼀个record的右边的临键区间。例如 `select * from t where id > 5 and id <= 7 for update`; 会锁住(4,7]、(7,+∞)。



#### 行锁

共享锁：允许一个事务读数据，阻止其他事务获取相同数据的排他锁

排他锁：允许获得排他锁的事务更新数据，防止其他事务获得相同数据的共享锁和排他锁

| 锁类型                | S共享锁（请求锁类型） | X排他锁（请求锁类型） |
| --------------------- | --------------------- | --------------------- |
| S共享锁（当前锁类型） | 兼容                  | 冲突                  |
| X排他锁（当前锁类型） | 冲突                  | 冲突                  |

| SQL                       | 行锁类型 | 说明                                   |
| ------------------------- | -------- | -------------------------------------- |
| INSERT                    | 排他锁   | 自动加锁                               |
| UPDATE                    | 排他锁   | 自动加锁                               |
| DELETE                    | 排他锁   | 自动加锁                               |
| SELECT                    | 不加锁   |                                        |
| SELECT…LOCK IN SHARE MODE | 共享锁   | 手动在SELECT之后加`lock in share mode` |
| SELECT…FOR UPDATE         | 排他锁   | 手动在SELECT之后加`for update`         |



### 间隙锁/临键锁

主要是为了防止幻读

1.索引上的等值查询（唯一索引），给不存在的记录加锁的时候，优化为间隙锁

```mysql
begin
update set name = 'lby' where id = 8; 
#如果id为8的数据不存在，就会对8数据之前的间隙加锁，防止其他事务插入数据，导致幻读
```

2.索引上的等值查询（普通索引(非唯一)），向右遍历时最后一个值不满足查询需求时,next-key lock退化成间隙锁

```mysql
select * from user where id =3 lock in share mode;
正常select不会添加任何锁
它会在数据3加行锁
在3和3之前以及之后的数据加入间隙锁，防止幻读
```

3.索引上的范围查询（唯一索引），会访问到不满足条件的第一个值为止

```mysql
select * from user where id >= 19 lock in share mode;
对19记录加行锁
对19和下一条数据之间加间隙锁
```

提交事务，自动释放锁



索引上的等值查询（唯一索引），给不存在的记录加锁时，会优化为间隙锁

```mysql
#并不存在id为100的数据，此时update会没有结果的。
#幻读的核心是插入数据，第一次使用这条数据时，它不存在。第二次使用，它就存在了。
#此时，给id为100的记录会加间隙锁，对这一条记录和上记录之间加间隙锁，防止插入，防止幻读
update user set age=100 where id=100;
```



# InnoDB

## InnoDB存储结构

![IMG_9634B3879293-1](/Users/Typora_Pictures/:Users:xiaobing:Downloads:IMG_9634B3879293-1.png)



![IMG_E1DB84742DE0-1](/Users/Typora_Pictures/:Users:xiaobing:Downloads:IMG_E1DB84742DE0-1.png)

![IMG_E56D406111F3-1](/Users/Typora_Pictures/:Users:xiaobing:Downloads:IMG_E56D406111F3-1.png)



![IMG_8C8BC3AF058A-1](/Users/Typora_Pictures/:Users:xiaobing:Downloads:IMG_8C8BC3AF058A-1.png)



![IMG_78FB2D7EB377-1](/Users/xiaobing/Downloads/IMG_78FB2D7EB377-1.jpeg)



**ACD：**redo.log undo.log

​		**I：**锁、MVCC

## redo log：保持持久性

重做日志：记录事务提交时数据页的物理修改，实现事务的持久性

`redo log buffer`和`redo log file`，前者在内存，后者在磁盘

作用：刷新脏页到磁盘，发生错误时，通过redo.log进行数据恢复

![IMG_EB7324062736-1](/Users/xiaobing/Downloads/IMG_EB7324062736-1.jpeg)

提交事务后，InnoDB首先更改Buffer Pool里的数据，这页数据又叫脏页，隔一段时间后，再将脏页刷新到磁盘。（数据的存储页不连续，写入时效率很慢）。如果刷新脏页的时候，出现了错误，数据就消失了。所以引入了redo.log

事务更改缓冲区的数据后，首先更新redo log buffer，然后将log更新到磁盘里的redo log。（log是追加记录，效率很高）。然后隔一段时间，再刷新脏页到磁盘。

## undo.log：原子性

回滚日志：用于记录数据被修改前到信息

作用：提供回滚和MVCC（多版本并发控制）

undo log是逻辑日志，记录的是操作，而不是数据

当`insert`的时候，产生的undo log只在回滚时候需要，提交事务就立刻被删除

当`update、delete`的时候，产生的undo log不仅在回滚时需要，在快照读也需要，不会被立刻删除

## MVCC

### 当前读：

**定义：**确保读取的是记录的最新版本，读取时还要保证其他并发事务不能修改当前记录，会对读取的记录进行加锁。

**产生条件：**`select... lock in share mode`、`select... for update`、`insert`、`update`、`delete`都是一种当前读

**实现方式：**当前读是通过 next-key 锁(行记录锁+间隙锁)来是实现的

> 行锁（Record Lock）：锁直接加在索引记录上面。
>
> 间隙锁（Gap Lock）：是 Innodb 为了解决幻读问题时引入的锁机制，所以只有在 Read Repeatable 、Serializable 隔离级别才有。
>
> Next-Key Lock ：Record Lock + Gap Lock，锁定一个范围并且锁定记录本身 。



### 快照读

读取的记录数据的可见版本

第一个select语句才是快照读的地方



### MVCC

- 在快照读读情况下，MySQL通过MVCC来避免幻读。

- 在当前读读情况下，MySQL通过next-key来避免幻读

  

多版本并发控制，指维护一个数据的多个版本，使读写操作没有冲突，提供了非阻塞读功能

具体实现依赖于：数据库记录中的三个隐藏字段、undo log、readView

#### 隐藏字段

`DB_TRX_ID`:最近修改事务的ID，记录插入这条记录或最后一次修改该记录的事务ID

`DB_ROLL_PTR`:回滚指针，指向这条记录的上一个版本，用于配合undo log，指向上一个版本

`DB_ROW_ID`:隐藏主键

#### Undo log

当`insert`的时候，产生的undo log只在回滚时候需要，提交事务就立刻被删除

当`update、delete`的时候，产生的undo log不仅在回滚时需要，在快照读也需要，不会被立刻删除

![IMG_D49AC543AAAD-1](/Users/xiaobing/Downloads/IMG_D49AC543AAAD-1.jpeg)

在MVCC多版本控制中，通过读取undo log的历史版本数据可以实现不同事务版本号都拥有自己独立的快照数据版本

#### readView

是快照读SQL执行MVCC提取数据的依据，记录并维护系统当前活跃的事务（未提交的事务）

Read View是一个数据库的内部快照，该快照被用于InnoDB存储引擎中的MVCC机制。简单点说，Read View就是一个快照，保存着数据库某个时刻的数据信息。Read View会根据事务的隔离级别决定在某个事务开始时，该事务能看到什么信息。就是说通过Read View，事务可以知道此时此刻能看到哪个版本的数据记录（有可能不是最新版本的，也有可能是最新版本的）

![IMG_E6BA735CFCBE-1](/Users/xiaobing/Downloads/IMG_E6BA735CFCBE-1.jpeg)



## 运维

### 日志

#### 错误日志

该日志默认开启，默认存放记录/var/log，默认文件名mysqld.log

查看日志位置:`show variables like '%log_error%'`

```mysql
#查看后50行log信息
[root@VM-4-8-centos lighthouse]# tail -50 /var/log/mysqld.log
```



#### 二进制日志

binlog记录了所有SQL语句（增删改、创建表），但不包括SELECT、SHOW语句

**作用：**

- 灾难时的数据恢复
- MySQL主从复制

查看日志位置:`show variables like '%log_bin%'`

二进制文件不能直接查看，需要:

```shell
mysqlbinlog [参数] log_file_name;

参数：
	-d	指定数据库名称，只查看某个数据库的相关操作
	-o	忽略日志前o行数据
	-v	将行事件重构为SQL
	-w	将行事件重构为SQL，并输出注释
	
```

**删除log:**

| 指令                                             | 含义                           |
| ------------------------------------------------ | ------------------------------ |
| reset master                                     | 删除所有binlog                 |
| purge master logs to 'binlog.xxxxxx'             | 删除xxxx编号之前的binlog       |
| purge master logs before 'yyyy-mm-dd hh24:mi:ss' | 删除日志在''yyyy…'之前的binlog |

也可以设置二进制过期时间:

```mysql
mysql> show variables like '%binlog%';

+----------------------------+---------+
| Variable_name              | Value   |
+----------------------------+---------+
| binlog_expire_logs_seconds | 2592000 |
+----------------------------+---------+
```



#### 查询日志

默认不开启

`show variables like '%general%';`

开启配置：

```mysql
#0关闭，1开启
mysql> set global general_log =1;
```



#### 慢查询日志

默认不开启。记录所有执行时间超过参数`long_query_time`设置值的SQL语句，默认为10

```mysql
set global slow_query_log = 1;
set global long_query_time = 5;
```



### 主从复制

将主库（master）的二进制日志（binlog)传到从库服务器（slave），然后对这些日志重新执行，保证数据一致

**好处：**

- 主库出现问题，可以切换到从库
- 实现读写分离
- 可以从从库备份，避免备份影响主库服务

**步骤：**

1. Master提交事务时，将数据变更记录保存在binlog
2. slave读取master的binlog，写入中继日志relay log
3. slave执行relay log



**搭建：**

```shell
#开放3306端口
firewall-cmd --zone=public --add-port=3306/tcp -permanent
firewall-cmd -reload

#或者关闭防火墙
[root@VM-4-8-centos lighthouse]# systemctl stop firewalld
[root@VM-4-8-centos lighthouse]# systemctl disable firewalld
#查看防火墙状态
firewall-cmd --state


#修改MySQL的server_id,master和slave不能一致
mysql> show variables like '%server%';
+---------------------------------+--------------------------------------+
| Variable_name                   | Value                                |
+---------------------------------+--------------------------------------+
| character_set_server            | utf8mb4                              |
| collation_server                | utf8mb4_0900_ai_ci                   |
| immediate_server_version        | 999999                               |
| innodb_dedicated_server         | OFF                                  |
| innodb_ft_server_stopword_table |                                      |
| original_server_version         | 999999                               |
| server_id                       | 1                                    |
| server_id_bits                  | 32                                   |
| server_uuid                     | ddf00dd4-ff3b-11ea-a997-b31c71e320f6 |
+---------------------------------+--------------------------------------+



```

#### 主库配置

```mysql
#主库配置：

#修改MySQL的读写
mysql> show variables like '%read_only%';
+-----------------------+-------+
| Variable_name         | Value |
+-----------------------+-------+
| innodb_read_only      | OFF   |
| read_only             | OFF   |
| super_read_only       | OFF   |
| transaction_read_only | OFF   |
+-----------------------+-------+

#修改参数需要重启mysql
set global read_only=0;


#创建远程连接的账号
CREATE USER 'lby3'@'%'IDENTIFIED WITH mysql_native_password BY 'P@ssword01';
#赋予主从复制的权限
GRANT REPLICATION SLAVE ON *.* TO 'lby3';

show grants for 你的用户;
SELECT * FROM mysql.user;

#通过指令，查看binlog坐标
mysql> show master status;
+---------------+----------+--------------+------------------+-------------------+
| File          | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+---------------+----------+--------------+------------------+-------------------+
| binlog.000012 |      658 |              |                  |                   |
+---------------+----------+--------------+------------------+-------------------+
```

#### 从库配置

```mysql
#修改MySQL的server_id,master和slave不能一致
mysql> select @@server_id;
+-------------+
| @@server_id |
+-------------+
|           2 |
+-------------+

#修改MySQL的读写
#1代表开启read_only,0代表关闭
mysql> set global read_only=1;

#重启mysql

#设置主库相关配置
#8.0.23的语法
CHANGE REPLICATION SOURCE TO SOURCE_HOST='',SOURCE_USER='',SOURCE_PASSWORD='',SOURCE_LOG_FILE='',SOURCE_LOG_POS='';

CHANGE REPLICATION SOURCE TO 
SOURCE_HOST='1.15.114.38',
SOURCE_USER='lby2',
SOURCE_PASSWORD='P@ssword01',
SOURCE_LOG_FILE='binlog.000012',
SOURCE_LOG_POS='4057';

#8.0.23之前的语法
CHANGE MASTER TO MASTER_HOST='',MASTER_USER='',MASTER_PASSWORD='',MASTER_LOG_FILE='',MASTER_LOG_POS='';

mysql> CHANGE MASTER TO
MASTER_HOST='1.15.114.38',
MASTER_USER='lby3',
MASTER_PASSWORD='P@ssword01',
MASTER_LOG_FILE='binlog.000013',
MASTER_LOG_POS=660;

#8.0.23的语法
start replica
stop replica

#8.0.23之前的语法
start slave;
stop slave;
```



- 准备两台服务器。确保主机服务器对3306开放端口号，或者直接关闭防火墙
- 确保两台MySQL的server_id不一致，同时master的read_only是off，slave的权限是on。主机可读可写，从机只能读，确保数据一致性。做完这一步要重启MySQL
- 在主机MySQL创建用户，@后代表可以从哪个IP地址访问，和密码，然后赋予权限，on后代表可以访问哪个数据库
- 检查权限是否赋予成功
- 然后show master status，找到主机正在写入的binlog和position
- 配置从机MySQL，访问主机，填写host，user，password，binlog的name和position
- 检查show slave status，确保slave io有两个yes
- start slave
- 在主机开始创建数据库，表，数据，看从机是否能同步更新
- stop slave

### 分库分表

#### 概述

**单数据库的局限：**

- IO瓶颈：请求数据太多，产生大量磁盘IO，带宽不够
- CPU瓶颈：排序、分组、连接查询等SQL会占用CPU资源



**拆分策略：**

- 垂直拆分
  - 垂直分库
    - 以表为依据，根据业务将不同表拆分到不同库中
  - 垂直分表
    - 以字段为依据，将不同字段拆分到不同表中，不同表也可以分到不同数据库里
- 水平拆分
  - 水平分库
    - 以字段为依据，将一个库中的数据拆分到多个库中。每个库的表结构一致、数据不一致
  - 水平分表
    - 以字段为依据，将一张表的数据拆分到多个库中。每个库的表结构一致、数据不一致

**实现技术：**

shardingJDBC：基于AOP，对本地执行的SQL进行拦截、解析、改写、路由处理，只支持Java，性能高

Mycat：分库分表中间件，不用调整代码就可以实现分库分表，支持多种语言，性能较慢



# 面试题

**`char:`**

- char表⽰定长字符串，长度是固定的
- 如果插⼊数据的长度⼩于char的固定长度时，则⽤空格填充；
- 对于char来说，最多能存放的字符个数为255，和编码⽆关
- 因为长度固定，所以存取速度要⽐varchar快很多，甚⾄能快50%，但正因为其长度固定， 所以会占据多余的空间，是空间换时间的做法；

**`varchar:`**

- varchar表⽰可变长字符串，长度是可变的；
- 对于varchar来说，最多能存放的字符个数为65532
- varchar在存取⽅⾯与char相反，它存取慢，因为长度不固定，但正因如此，不占据多余的 空间，是时间换空间的做法； 



**`DateTime和TimeStamp的异同？`**

- ⽇期范围 ：DATETIME 的⽇期范围是 1000-01-01 00:00:00.000000 到 9999-1231 23:59:59.999999 ；TIMESTAMP 的时间范围是 1970-01-01 00:00:01.000000 UTC 到 ``2038-01-09 03:14:07.999999 UTC
- 存储空间 ：DATETIME 的存储空间为 8 字节；TIMESTAMP 的存储空间为 4 字节
- 默认值 ：DATETIME 的默认值为 null；TIMESTAMP 的字段默认不为空(not null)，默认 值为当前时间(CURRENT_TIMESTAMP)



**`MySQL⾥记录货币⽤什么字段类型⽐较好？`**

货币在数据库中MySQL常⽤Decimal和Numric类型表⽰，这两种类型被MySQL实现为同样的类型

例如salary DECIMAL(9,2)，9(precision)代表将被⽤于存储值的总的⼩数位数，⽽2(scale)代表 将被⽤于存储⼩数点后的位数。



**`drop、delete、truncate的区别？`**

- 回滚：只有`delete`可以回滚
- 删除内容：`delete & truncate`是删除数据，保留表结构。而`drop`连表结构也会删除掉，所以速度最快
- 删除表用`drop`
- 保留表结构清除数据，用`truncate`



**`count(*)`和`count(列名)`的区别？**

- count(*)包括了所有的列，相当于⾏数，在统计结果的时候，包含列值为NULL的列
- count(列名)只包括列名那⼀列，在统计结果的时候，不包含列值为NULL的列



# **MySQL的日志**

- 错误日志`error log`

- 慢查询日志`slow query log`

- 一般查询日志`general log`

- 二进制日志`binlog`

- 重做日志`redo log`

- 回滚日志`undo log`

  

**两个InnoDB存储引擎特有的⽇志⽂件：**

重做⽇志 （redo log）：记录了对于InnoDB存储引擎的事务⽇志。 

回滚⽇志 （undo log）：回滚⽇志的作⽤就是对数据进⾏回滚。当事务对数据库进⾏修改，InnoDB引擎不仅会记录redo log，还会⽣成对应的undo log⽇志；如果事务执⾏失败或调⽤了rollback，导致事务需要回 滚，就可以利⽤undo log中的信息将数据回滚到修改之前的样⼦。



![截屏2022-08-09 下午10.41.50](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-08-09 下午10.41.50.png)

1. 从内存/磁盘，查询出这一行数据
2. 修改值后，将数据存入内存
3. 写入redo log，将事务改成prepare
4. 写入binlog
5. 写入redo log，将事务改成commit，并提交事务
6. 将redo log写入磁盘，修改磁盘里的值



**如何解决幻读的问题？**

![截屏2022-08-18 下午11.47.51](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-08-18 下午11.47.51.png)

![截屏2022-08-18 下午11.48.18](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-08-18 下午11.48.18.png)

![截屏2022-08-18 下午11.48.37](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-08-18 下午11.48.37.png)