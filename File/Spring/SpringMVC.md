# POM

```xml
    <dependencies>  <!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.28</version>
        </dependency>

        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>1.2.17</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.projectlombok/lombok -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.22</version>
            <scope>provided</scope>
        </dependency>
        <!-- https://mvnrepository.com/artifact/javax.servlet.jsp/jsp-api -->
        <dependency>
            <groupId>javax.servlet.jsp</groupId>
            <artifactId>jsp-api</artifactId>
            <version>2.2</version>
            <scope>provided</scope>
        </dependency>
        <!-- https://mvnrepository.com/artifact/javax.servlet/javax.servlet-api -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>4.0.1</version>
            <scope>provided</scope>
        </dependency>
        <!-- https://mvnrepository.com/artifact/javax.servlet/jstl -->
        <!-- https://mvnrepository.com/artifact/javax.servlet/jstl -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
            <version>1.2</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/taglibs/standard -->
        <!-- https://mvnrepository.com/artifact/org.apache.taglibs/taglibs-standard-impl -->
        <dependency>
            <groupId>org.apache.taglibs</groupId>
            <artifactId>taglibs-standard-impl</artifactId>
            <version>1.2.5</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>5.1.9.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>2.5</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.13.3</version>
        </dependency>

        <!-- 数据库连接池 -->
        <dependency>
            <groupId>com.mchange</groupId>
            <artifactId>c3p0</artifactId>
            <version>0.9.5.2</version>
        </dependency>

        <!--Mybatis-->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.5.2</version>
        </dependency>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis-spring</artifactId>
            <version>2.0.2</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>5.1.9.RELEASE</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.springframework/spring-aop -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aop</artifactId>
            <version>5.3.19</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.aspectj/aspectjweaver -->
        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjweaver</artifactId>
            <version>1.9.1</version>
        </dependency>
    </dependencies>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>false</filtering>
            </resource>
            <resource>
                <directory>src/main/java</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>false</filtering>
            </resource>
        </resources>
    </build>
```

# 核心组件

![Screen Shot 2022-07-17 at 7.27.52 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-17 at 7.27.52 PM.png)

1. 客户端向服务端发送⼀次请求，这个请求会先到前端控制器DispatcherServlet(也叫中央控制器)。

2. DispatcherServlet接收到请求后会调⽤HandlerMapping处理器映射器。由此得知，该请求该 由哪个Controller来处理（并未调⽤Controller，只是得知）

3. DispatcherServlet调⽤HandlerAdapter处理器适配器，告诉处理器适配器应该要去执⾏哪个 Controller

4. HandlerAdapter处理器适配器去执⾏Controller并得到ModelAndView(数据和视图)，并层层返回给DispatcherServlet

5. DispatcherServlet将ModelAndView交给ViewReslover视图解析器解析，然后返回真正的视图

6. DispatcherServlet将模型数据填充到视图中

7. DispatcherServlet将结果响应给客户端

   

# SpringMVC Restful风格

Restful的响应格式是json，⽤到⼀个常⽤注解：@ResponseBody

```java
@GetMapping("/user") 
@ResponseBody 
public User user(){ 
  return new User(1,"张三"); 
}
```



# 注解实现SpringMVC

**1.	注册DispatcherServlet，链接Spring配置文件，设置启动级别**

```xml
<servlet>
    <servlet-name>springMVC</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
    <init-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>classpath:springMVCConfig.xml</param-value>
    </init-param>
    <load-on-startup>1</load-on-startup>
</servlet>
<servlet-mapping>
    <servlet-name>springMVC</servlet-name>
    <url-pattern>/</url-pattern>
</servlet-mapping>
```

2. **编写Spring配置文件，开启包扫描、mvc不处理静态资源文件、mvc开启注解支持、配置视图解析器**

```xml
<context:component-scan base-package="com"/>
<mvc:default-servlet-handler/>
<mvc:annotation-driven/>

<!--    <bean id="/hello" class="com.controller.HelloController"/>-->
<bean class="org.springframework.web.servlet.view.InternalResourceViewResolver" id="resolver">
    <property name="prefix" value="/WEB-INF/jsp/"/>
    <property name="suffix" value=".jsp"/>
</bean>
```

3. 编写Controller

```java
@Controller		//让Spring IOC容器初始化时自动扫描到
public class HelloController2 {
		
  	//可以在@RequestMapping注解里面加上method=RequestMethod.GET	
    @RequestMapping("/h1")	//映射请求路径
    public String hello(Model model) {
        model.addAttribute("msg", "HELLO SPRING");
        return "test";		//方法返回的结果是视图的名称
    }
    
}
```

注意：==/和/*的区别是，前者不包括jsp文件，后者包括jsp文件，后者会导致文件路径拼接的时候嵌套==

# 页面跳转

**转发和重定向**：

```java
@Controller
public class ResultSpringMVC2 {
   @RequestMapping("/rsm2/t1")
   public String test1(){
       //转发
       return "test";
  }
}

Controller
public class RESTFulController {
    @RequestMapping("/add")
    public String testREST(Model model) {
      	//重定向
        return "redirect:/index.jsp";
    }
}
```

# 参数名一致问题

### 1.**提交的域名称和处理方法的参数名一致**

提交数据 : http://localhost:8080/hello?name=FR

处理方法 :

```java
@RequestMapping("/hello")
public String hello(String name){
   System.out.println(name);	//FR
   return "hello";
}
```

### 2.**提交的域名称和处理方法的参数名不一致**

提交数据 : http://localhost:8080/hello?username=FR

处理方法 :

```java
@RequestMapping("/hello")
public String hello(@RequestParam("username") String name){
   System.out.println(name);	//FR
   return "hello";
}
```

### 3.**提交的是一个对象**

要求提交的表单域和对象的属性名一致  , 参数使用对象即可

1、实体类

```java
public class User {
   private int id;
   private String name;
   private int age;
}
```

2、提交数据 : http://localhost:8080/mvc04/user?name=FR&id=1&age=15

3、处理方法 :

```java
@RequestMapping("/user")
public String user(User user){
   System.out.println(user);	//User { id=1, name='FR', age=15 }
   return "hello";
}
```

说明：如果使用对象的话，前端传递的参数名和对象名必须一致，否则就是null

# 乱码问题

### 方式一：默认的过滤器

在XML中配置

```xml
<filter>
   <filter-name>encoding</filter-name>
   <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
   <init-param>
       <param-name>encoding</param-name>
       <param-value>utf-8</param-value>
   </init-param>
</filter>
<filter-mapping>
   <filter-name>encoding</filter-name>
   <url-pattern>/*</url-pattern>
</filter-mapping>
```

### 方式二：自定义过滤器

```java
package com.kuang.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
* 解决get和post请求 全部乱码的过滤器
*/
public class GenericEncodingFilter implements Filter {

   @Override
   public void destroy() {
  }

   @Override
   public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
       //处理response的字符编码
       HttpServletResponse myResponse=(HttpServletResponse) response;
       myResponse.setContentType("text/html;charset=UTF-8");

       // 转型为与协议相关对象
       HttpServletRequest httpServletRequest = (HttpServletRequest) request;
       // 对request包装增强
       HttpServletRequest myrequest = new MyRequest(httpServletRequest);
       chain.doFilter(myrequest, response);
  }

   @Override
   public void init(FilterConfig filterConfig) throws ServletException {
  }

}

//自定义request对象，HttpServletRequest的包装类
class MyRequest extends HttpServletRequestWrapper {

   private HttpServletRequest request;
   //是否编码的标记
   private boolean hasEncode;
   //定义一个可以传入HttpServletRequest对象的构造函数，以便对其进行装饰
   public MyRequest(HttpServletRequest request) {
       super(request);// super必须写
       this.request = request;
  }

   // 对需要增强方法 进行覆盖
   @Override
   public Map getParameterMap() {
       // 先获得请求方式
       String method = request.getMethod();
       if (method.equalsIgnoreCase("post")) {
           // post请求
           try {
               // 处理post乱码
               request.setCharacterEncoding("utf-8");
               return request.getParameterMap();
          } catch (UnsupportedEncodingException e) {
               e.printStackTrace();
          }
      } else if (method.equalsIgnoreCase("get")) {
           // get请求
           Map<String, String[]> parameterMap = request.getParameterMap();
           if (!hasEncode) { // 确保get手动编码逻辑只运行一次
               for (String parameterName : parameterMap.keySet()) {
                   String[] values = parameterMap.get(parameterName);
                   if (values != null) {
                       for (int i = 0; i < values.length; i++) {
                           try {
                               // 处理get乱码
                               values[i] = new String(values[i]
                                      .getBytes("ISO-8859-1"), "utf-8");
                          } catch (UnsupportedEncodingException e) {
                               e.printStackTrace();
                          }
                      }
                  }
              }
               hasEncode = true;
          }
           return parameterMap;
      }
       return super.getParameterMap();
  }

   //取一个值
   @Override
   public String getParameter(String name) {
       Map<String, String[]> parameterMap = getParameterMap();
       String[] values = parameterMap.get(name);
       if (values == null) {
           return null;
      }
       return values[0]; // 取回参数的第一个值
  }

   //取所有值
   @Override
   public String[] getParameterValues(String name) {
       Map<String, String[]> parameterMap = getParameterMap();
       String[] values = parameterMap.get(name);
       return values;
  }
}
```

然后在XML中配置

# Json

- JSON(JavaScript Object Notation, JS 对象标记) 是一种轻量级的数据交换格式，目前使用特别广泛。
- 采用完全独立于编程语言的**文本格式**来存储和表示数据。

- 对象表示为键值对，数据由逗号分隔
- 花括号保存对象
- 方括号保存数组

```json
var obj = {a: 'Hello', b: 'World'}; //这是一个对象，注意键名也是可以使用引号包裹的
var json = '{
						"a": "Hello", 
						"b": "World"
						}'; 
//这是一个 JSON 字符串，本质是一个字符串
```

## Jackson

` @ResponseBody`设置返回体是json，不走视图解析器

`@RestController`在类上直接使用 **@RestController** ，里面所有的方法都只会返回 json 字符串了，不用再每一个都添加@ResponseBody ！我们在前后端分离开发中，一般都使用 @RestController！

`ObjectMapper`对象的`writeValueAsString（）`将对象转化成JSON字符串



### Java对象转Json

```java
Person p = new Person();
p.setName("张三");
p.setAge(23);
p.setGender("男");
p.setBirthday(new Date());

//2.创建Jackson对象 ObjectMapper
ObjectMapper mapper = new ObjectMapper();
//3.转换为JSOn
String json = mapper.writeValueAsString(p);
```

### Json转Java对象

```java
//json字符串
String str="{\"gender\":\"男\",\"name\":\"zhangsan\",\"age\":23}";

//Jackson核心对象
ObjectMapper mapper = new ObjectMapper();

//使用readValue方法进行转换
Person person = mapper.readValue(str, Person.class);
```



## Jackson注解

`@JsonIgnore` 此注解用于属性上，作用是进行JSON操作时忽略该属性

`@JsonFormat` 此注解用于属性上，作用是把Date类型直接转化为想要的格式

​						如`@JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")`

`@JsonProperty` 此注解用于属性上，作用是把该属性的名称序列化为另外一个名称，如把`trueName`属性序列化为`name`

`@JsonPropertyOrder` *用于类,指定属性在序列化时 json 中的顺序*

​						`@JsonPropertyOrder({"date", "user_name"})`

## SpringBoot配置Jackson

```yaml
spring:
  jackson:
    # 设置属性命名策略,对应jackson下PropertyNamingStrategy中的常量值，SNAKE_CASE-返回的json驼峰式转下划线，json body下划线传到后端自动转驼峰式
    property-naming-strategy: SNAKE_CASE
    # 全局设置@JsonFormat的格式pattern
    date-format: yyyy-MM-dd HH:mm:ss
    # 当地时区
    locale: zh_CN
    # 设置全局时区
    time-zone: GMT+8
    # 常用，全局设置pojo或被@JsonInclude注解的属性的序列化方式
    default-property-inclusion: NON_NULL #不为空的属性才会序列化,具体属性可看JsonInclude.Include
    # 常规默认,枚举类SerializationFeature中的枚举属性为key，值为boolean设置jackson序列化特性,具体key请看SerializationFeature源码
```



## 乱码解决：

在SpringMVC配置文件里添加消息转化配置：

```xml
<mvc:annotation-driven>
   <mvc:message-converters register-defaults="true">
       <bean class="org.springframework.http.converter.StringHttpMessageConverter">
           <constructor-arg value="UTF-8"/>
       </bean>
       <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
           <property name="objectMapper">
               <bean class="org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean">
                   <property name="failOnEmptyBeans" value="false"/>
               </bean>
           </property>
       </bean>
   </mvc:message-converters>
</mvc:annotation-driven>
```

# 拦截器

## 拦截和过滤器的区别

| 区别     | 过滤器                                                | 拦截器                                                       |
| -------- | ----------------------------------------------------- | ------------------------------------------------------------ |
| 使用范围 | 是Servlet规范中的一部分                               | 是SpringMVC框架提供的                                        |
| 拦截范围 | 在url-pattern中配置了/*之后可以对所有要访问的资源拦截 | 只会拦截访问的控制器方法，如果访问的是jsp, html, css, image或者js是不会进行拦截的 |

## 自定义拦截器

①创建拦截器类实现 HandlerInterceptor接口
		②配置拦截器

**①创建拦截器类实现 HandlerInterceptor接口**

```java
public class MyInterceptor1 implements HandlerInterceptor {
    //在目标方法执行之前执行 也就是在show方法执行之前
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("perHandle......");
      //默认返回false表示拦截不放行 后面的都无法执行
        return false;
    }

    //在目标方法执行之后 视图对象返回之前执行
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("postHandle......");
    }

    //整个流程都执行完毕之后在执行
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("afterCompletion......");
    }
}
```

​	**②配置拦截器**

```java
<!--配置拦截器-->
<mvc:interceptors>
    <mvc:interceptor>
        <!--对哪些资源进行拦截操作-->
        <mvc:mapping path="/**"/>
        <bean class="com.itheima.interceptor.MyInterceptor1"/>
    </mvc:interceptor>
</mvc:interceptors>
```

# SpringMVC异常处理机制

- `方式一`：使用SpringMVC提供的简单异常处理器`SimpleMappingExceptionResolver`
- `方式二`：实现Spring的异常处理接口`HandlerExceptionResolver`自定义自己的异常处理器



## 方式一：

SpringMVC已经定义好了该类型转换器，在使用时可以根据项目情况进行相应异常与视图的映射配置

```java
<!--配置异常处理器-->
<bean class="org.springframework.web.servlet.handler.SimpleMappingExceptionResolver">
     <property name="defaultErrorView" value="error"></property>
    <property name="exceptionMappings">
        <map>
            <entry key="java.lang.ClassCastException" value="error1"></entry>
            <entry key="com.itheima.exception.MyException" value="error2"></entry>
        </map>
    </property>
</bean>

```

## 方式二：

```java
public class MyExceptionResolver1 implements HandlerExceptionResolver {
    /*
       参数Exception：异常对象
       返回值ModelAndView：跳转到错误视图信息
    */
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        ModelAndView modelAndView = new ModelAndView();
        if(e instanceof MyException){
            modelAndView.addObject("info", "自定义异常");
        }else if(e instanceof ClassCastException){
            modelAndView.addObject("info", "类型转换异常");
        }else {
            modelAndView.addObject("info", "未知异常");
        }
        modelAndView.setViewName("error");
        return modelAndView;
    }
}
```

配置：

```java
<!--配置异常处理器-->
<bean class="com.itheima.resolver.MyExceptionResolver1"/>
```



# 整合SSM

### 第一步：Dao层接口

```java
public interface BookMapper {
     int addNewBook(Books book);
     int deleteBookById(int bookID);
     int updateBook(Books book);
     List<Books> queryAllBooks();
     Books queryBookById(int bookID);
}
```

### 第二步：`Mapper.xml`

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.dao.BookMapper">

    <!--增加一个Book-->
    <insert id="addNewBook" parameterType="Books">
        insert into ssmbuild.books(bookName,bookCounts,detail)
        values (#{bookName}, #{bookCounts}, #{detail})
    </insert>

    <!--根据id删除一个Book-->
    <delete id="deleteBookById" parameterType="int">
        delete from ssmbuild.books where bookID=#{bookID}
    </delete>

    <!--更新Book-->
    <update id="updateBook" parameterType="Books">
        update ssmbuild.books
        set bookName = #{bookName},bookCounts = #{bookCounts},detail = #{detail}
        where bookID = #{bookID}
    </update>

    <!--根据id查询,返回一个Book-->
    <select id="queryBookById" resultType="Books">
        select * from ssmbuild.books
        where bookID = #{bookID}
    </select>

    <!--查询全部Book-->
    <select id="queryAllBooks" resultType="Books">
        SELECT * from ssmbuild.books
    </select>
</mapper>
```

### 第三步：Service层接口

```java
public interface BookService {
    int addNewBook(Books book);

    int deleteBookById(int bookID);

    int updateBook(Books book);

    List<Books> queryAllBooks();

    Books queryBookById(int bookID);
}
```

### 第四步：Service层实现类

```java
public class BookServiceImpl implements BookService {

    private BookMapper bookMapper;

    public void setBookMapper(BookMapper bookMapper) {
        this.bookMapper = bookMapper;
    }

    public int addNewBook(Books book) {
        return bookMapper.addNewBook(book);
    }

    public int deleteBookById(int bookID) {
        return bookMapper.deleteBookById(bookID);
    }

    public int updateBook(Books book) {
        return bookMapper.updateBook(book);
    }

    public List<Books> queryAllBooks() {
        return bookMapper.queryAllBooks();
    }

    public Books queryBookById(int bookID) {
        return bookMapper.queryBookById(bookID);
    }
}
```

### 第五步：编写数据库文件`database.properties`

```java
jdbc.driver=com.mysql.cj.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/ssmbuild
jdbc.username=root
jdbc.password=123456
```

### 第六步：编写`MybatisConfig.xml`

```java
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>

    <typeAliases>
        <package name="com.pojo"/>
    </typeAliases>

    <mappers>
        <mapper resource="com/dao/BookMapper.xml"/>
    </mappers>
  
</configuration>
```

### 第六步：编写`applicationConfig.xml`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd">

    <import resource="Spring_dao.xml"/>
    <import resource="Spring_service.xml"/>
    <import resource="Spring_mvc.xml"/>

</beans>
```

### 第七步：在`web.xml`中注册DispatcherServlet，并配置过滤器

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    <welcome-file-list>
        <welcome-file>index.jsp</welcome-file>
    </welcome-file-list>
    <servlet>
        <servlet-name>springMVC</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:applicationConfig.xml</param-value>
        </init-param>
    </servlet>
    <servlet-mapping>
        <servlet-name>springMVC</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>

    <filter>
        <filter-name>encoding</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>utf-8</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>encoding</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
</web-app>
```

### 第八步：编写`Spring_dao.xml`

导入数据库文件

连接池

`sqlSessionFactory`

`MapperScannerConfigurer`用来扫描dao层接口

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       https://www.springframework.org/schema/context/spring-context.xsd">

    <context:property-placeholder location="classpath:database.properties"/>

    <bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource">
        <!-- 配置连接池属性 -->
        <property name="driverClass" value="${jdbc.driver}"/>
        <property name="jdbcUrl" value="${jdbc.url}"/>
        <property name="user" value="${jdbc.username}"/>
        <property name="password" value="${jdbc.password}"/>

        <!-- c3p0连接池的私有属性 -->
        <property name="maxPoolSize" value="30"/>
        <property name="minPoolSize" value="10"/>
        <!-- 关闭连接后不自动commit -->
        <property name="autoCommitOnClose" value="false"/>
        <!-- 获取连接超时时间 -->
        <property name="checkoutTimeout" value="10000"/>
        <!-- 当获取连接失败重试次数 -->
        <property name="acquireRetryAttempts" value="2"/>
    </bean>

    <bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <property name="dataSource" ref="dataSource"/>
        <property name="configLocation" value="classpath:MybatisConfig.xml"/>
    </bean>

    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
        <property name="basePackage" value="com.dao"/>
    </bean>

</beans>
```

### 第九步：编写`Spring_service.xml`

扫描service包

注册service实现类

配置`transactionManager`,引用`dataSource`

配置事务

配置AOP

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
   http://www.springframework.org/schema/beans/spring-beans.xsd
   http://www.springframework.org/schema/context
   http://www.springframework.org/schema/context/spring-context.xsd
   http://www.springframework.org/schema/aop
   https://www.springframework.org/schema/aop/spring-aop.xsd
   http://www.springframework.org/schema/tx
   https://www.springframework.org/schema/tx/spring-tx.xsd">

    <context:component-scan base-package="com.service"/>

    <bean id="BookServiceImpl" class="com.service.BookServiceImpl">
        <property name="bookMapper" ref="bookMapper"/>
    </bean>

    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <property name="dataSource" ref="dataSource"/>
    </bean>

    <tx:advice id="txAdvice" transaction-manager="transactionManager">
        <tx:attributes>
            <tx:method name="addNewBook" propagation="REQUIRED"/>
            <tx:method name="deleteBookById" propagation="REQUIRED"/>
            <tx:method name="updateBook" propagation="REQUIRED"/>
        </tx:attributes>
    </tx:advice>

    <aop:config>
        <aop:pointcut id="pointCut" expression="execution(* com.dao.BookMapper.*.*(..))"/>
        <aop:advisor advice-ref="txAdvice" pointcut-ref="pointCut"/>
    </aop:config>

</beans>
```

第十步：配置`Spirng_MVC.xml`

扫描Controller包

让mvc不处理静态资源

开启注解支持

配置视图解析器，编辑前缀和后缀

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
   http://www.springframework.org/schema/beans/spring-beans.xsd
   http://www.springframework.org/schema/context
   http://www.springframework.org/schema/context/spring-context.xsd
   http://www.springframework.org/schema/mvc
   https://www.springframework.org/schema/mvc/spring-mvc.xsd">
    
    <!--    包扫描-->
    <!--    让mvc不处理静态资源，.css .js .html-->
    <!--    开启注解支持-->

    <context:component-scan base-package="com.controller"/>
    
    <mvc:default-servlet-handler/>
    
    <mvc:annotation-driven>
        <mvc:message-converters register-defaults="true">
            <bean class="org.springframework.http.converter.StringHttpMessageConverter">
                <constructor-arg value="UTF-8"/>
            </bean>
            <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
                <property name="objectMapper">
                    <bean class="org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean">
                        <property name="failOnEmptyBeans" value="false"/>
                    </bean>
                </property>
            </bean>
        </mvc:message-converters>
    </mvc:annotation-driven>

    <!--    <bean id="/hello" class="com.controller.HelloController"/>-->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="viewClass" value="org.springframework.web.servlet.view.JstlView" />
        <property name="prefix" value="/WEB-INF/jsp/" />
        <property name="suffix" value=".jsp" />
    </bean>

</beans>
```

### 第十一步：编写Controller

```java
@Controller
@RequestMapping("/book")
public class BookController {
    @Autowired
    @Qualifier("BookServiceImpl")
    private BookService bookService;

    @RequestMapping("/allBook")
    public String allBook(Model model){
        List<Books> books = bookService.queryAllBooks();
        model.addAttribute("list",books);
        return"allBook";
    }
  
  @RequestMapping("/toUpdateBook")
public String toUpdateBook(Model model, int id) {
   Books books = bookService.queryBookById(id);
   System.out.println(books);
   model.addAttribute("book",books );
   return "updateBook";
}

@RequestMapping("/updateBook")
public String updateBook(Model model, Books book) {
   System.out.println(book);
   bookService.updateBook(book);
   Books books = bookService.queryBookById(book.getBookID());
   model.addAttribute("books", books);
   return "redirect:/book/allBook";
}
}
```
