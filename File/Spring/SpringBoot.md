

## 运行原理

### 配置

> starter

定义了项目使用的所有依赖的坐标，目的是减少依赖配置

> parent

所以`springboot`项目要继承的项目，定义了坐标版本号，目的是减少依赖冲突

### 引导类

SpringBoot的引导类是工程的执行入口，扫描引导类所在包及其子包



## 配置文件

**默认配置文件：**application.properties

​	语法结构 ：key=value

**yaml：**application.yaml

- 语法结构 ：key: value



**配置文件加载的优先级：**properties>yml>yaml



**yaml**:

1、空格不能省略

2、以缩进来控制层级关系，只要是左边对齐的一列数据都是同一个层级的。

3、属性和值的大小写都是十分敏感的。

```yaml
person:
  name: qinjiang
  age: 3
  happy: false
  birth: 2000/01/01
  maps: {k1: v1,k2: v2}
  lists:
   - code
   - girl
   - music
  dog:
    name: 旺财
    age: 1
```

### **yaml读取单一属性：**

使用@Value读取单个数据，属性名引用方式：${一级属性名.二级属性名}

```java
/**
user:
	name: lby
	
likes:
	-1
	-2
*/

@Value("${user.name}")
private String name;

@Value("${likes[1]")
private String likes;
```

### yaml文件中的变量引用：

${}引用数据

```yaml
baseUrl: C:\windows

temp: ${baseUrl}\joke
```

属性值如果出现特殊字符，要双引号包裹起来：

```yaml
lesson: "Spring\n"
```

### 读取全部属性

yaml数据封装到Environment对象里

```java
@Autowired
private Environment env;

Sysout(env.getProperty("server.port"))	//8080
```

### 用对象封装想要的数据

```java
/*
@ConfigurationProperties作用：
将配置文件中配置的每一个属性的值，映射到这个组件中；
告诉SpringBoot将本类中的所有属性和配置文件中相关的配置进行绑定
参数 prefix = “person” : 将配置文件中的person下面的所有属性一一对应
*/
@Component //注册bean，由Spring容器管理
@ConfigurationProperties(prefix = "person")
public class Person {
    private String name;
    private Integer age;
    private Boolean happy;
    private Date birth;
    private Map<String,Object> maps;
    private List<Object> lists;
    private Dog dog;
}
```

测试类：

```java
@SpringBootTest
class DemoApplicationTests {

    @Autowired
    Person person; //将person自动注入进来

    @Test
    public void contextLoads() {
        System.out.println(person); //打印person信息
    }

}
```

**配置文件占位符：**

```yaml
person:
    name: qinjiang${random.uuid} # 随机uuid
    age: ${random.int}  # 随机int
    happy: false
    birth: 2000/01/01
    maps: {k1: v1,k2: v2}
    lists:
      - code
      - girl
      - music
    dog:
      name: ${person.hello:other}_旺财
      age: 1
```

## 3.JSR303数据校验 

在字段是增加一层过滤器验证，可以保证数据的合法性

注解：`@Validated`

```xml
空检查
@Null       验证对象是否为null
@NotNull    验证对象是否不为null, 无法查检长度为0的字符串
@NotBlank 检查约束字符串是不是Null还有被Trim的长度是否大于0,只对字符串,且会去掉前后空格.
@NotEmpty 检查约束元素是否为NULL或者是EMPTY.
 
Booelan检查
@AssertTrue     验证 Boolean 对象是否为 true  
@AssertFalse    验证 Boolean 对象是否为 false  
 
长度检查
@Size(min=, max=) 验证对象（Array,Collection,Map,String）长度是否在给定的范围之内  
@Length(min=, max=) Validates that the annotated string is between min and max included.
 
日期检查
@Past           验证 Date 和 Calendar 对象是否在当前时间之前  
@Future     	验证 Date 和 Calendar 对象是否在当前时间之后  
@Pattern    	验证 String 对象是否符合正则表达式的规则
 
数值检查，建议使用在Stirng,Integer类型，不建议使用在int类型上，因为表单值为“”时无法转换为int，但可以转换为Stirng为"",Integer为null
@Min            验证 Number 和 String 对象是否大等于指定的值  
@Max            验证 Number 和 String 对象是否小等于指定的值  
@DecimalMax 被标注的值必须不大于约束中指定的最大值. 这个约束的参数是一个通过BigDecimal定义的最大值的字符串表示.小数存在精度
@DecimalMin 被标注的值必须不小于约束中指定的最小值. 这个约束的参数是一个通过BigDecimal定义的最小值的字符串表示.小数存在精度
@Digits     验证 Number 和 String 的构成是否合法  
@Digits(integer=,fraction=) 验证字符串是否是符合指定格式的数字，interger指定整数精度，fraction指定小数精度。
 
@Range(min=, max=) 检查数字是否介于min和max之间.
@Range(min=10000,max=50000,message="range.bean.wage")
private BigDecimal wage;
 
@Valid  递归的对关联对象进行校验, 如果关联对象是个集合或者数组,那么对其中的元素进行递归校验,如果是一个map,则对其中的值部分进行校验.(是否进行递归验证)
    
@CreditCardNumber 信用卡验证
@Email  验证是否是邮件地址，如果为null,不进行验证，算通过验证。
@ScriptAssert(lang= ,script=, alias=)
    
@URL(protocol=,host=, port=,regexp=, flags=)
```

## 4.配置文件

### **复制模块:**

1.在工作空间制作母版，复制母版，修改工程名

2.**修改pom文件的artifactId文件名**，删除多余文件，仅保存src和pom

3.删掉<name><description>

### **properties**

```shell
#关闭默认banner
spring.main.banner-mode=off
#显示log的级别
logging.level.root=error
```

参考文件：https://docs.spring.io/spring-boot/docs/current/reference/html/

### 优先级

application-test.properties 代表测试环境配置

application-dev.properties 代表开发环境配置

```
优先级1：项目路径下的config文件夹配置文件
优先级2：项目路径下配置文件
优先级3：资源路径下的config文件夹配置文件
优先级4：资源路径下配置文件
```

不同配置文件中的相同配置按照加载优先级互相覆盖，不同配置则全部保留

根据配置选择激活的环境：

```properties
#比如在配置文件中指定使用dev环境，我们可以通过设置不同的端口号进行测试；
#我们启动SpringBoot，就可以看到已经切换到dev下的配置了；
spring.profiles.active=dev
```

### **yaml**

不需要创建多个配置文件

```yaml
server:
  port: 8081
#选择要激活那个环境块
spring:
  profiles:
    active: prod

---
server:
  port: 8083
spring:
  profiles: dev #配置环境的名称


---

server:
  port: 8084
spring:
  profiles: prod  #配置环境的名称
  
spring:
  thymeleaf:
    cache: false
      #国际化
  messages:
    basename: internationlization.login
server:
  servlet:
    context-path: /joke  
```

### Configuation

```java
@Configuration
public class Myconfig implements WebMvcConfigurer {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
    }
}
```

## 5.REST开发

REST（representational state transfer）

优点：隐藏资源的访问行为，无法通过地址得知对资源进行何种操作

- http://localhost/users	查询全部用户信息   GET
- http://localhost/users    添加用户信息   POST
- http://localhost/users    修改用户信息  PUT
- http://localhost/users/1  查询指定用户信息   GET
- http://localhost/users/1  删除用户信息   DELETE



### 入门案例

@RestController对整个类的方法都起作用

@ResponseBody只对一个方法起作用



REST开发：

1.在@RequestMapper里规定请求的方法 `method = RequestMethod.XXXX`

2.在@RequestMapper里规定请求的参数`value = "/books/{id}"`

3.在参数里关联请求参数和形参`@PathVariable Integer id`

4.用postman测试

```java
@Controller
public class BookController {

    @RequestMapping(value = "/books", method = RequestMethod.POST)
    @ResponseBody
    public String getById() {
        System.out.println("running");
        return "running";
    }

    @RequestMapping(value = "/books/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String getById2(@PathVariable Integer id) {
        System.out.println(id);
        return id.toString();
    }
}
```

### 扩展

@RequestBody，用于接收json。前端讲请求数据封装成对象，以json传递给后端

@RequestParam，用于接收url地址传参，或者表单传参

```java
@RestController						//全部方法都不走视图解析器
@RequestMapping("/books")
public class BookController {

/**postman：
		{
    	"name":"Add_Book_test",
    	"price":800
    }
*/
    @PostMapping
    public String add(@RequestBody Book book) {
        System.out.println(book);
        return "SUCCESS!";
    }

    @GetMapping
    public String getAll() {
        return "SUCCESS";
    }

    @DeleteMapping(value = "/{id}")		//postamn：http://localhost:8080/books/100
    public String delete(@PathVariable Integer id) {
        return id+"SUCCESS";
    }
}
```



## 整合第三方技术

### Junit

```java
@SpringBootTest(classes = Demo2ApplicationTests.class)
//如果文件位置不对，需要配置classes属性，引导类就可以找到测试类
class Demo2ApplicationTests {

    @Autowired
    BaseDao dao;

    @Test
    void contextLoads() {
        dao.add();
    }

}
```



### Mybatis

```xml
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.1.1</version>
</dependency>
```

配置：

```properties
#下面这些内容是为了让MyBatis映射
#指定Mybatis的Mapper文件
mybatis.mapper-locations=classpath:mybaits/*xml
#指定Mybatis的实体目录
mybatis.type-aliases-package=com.example.demo.pojo
```

**Dao接口：**

```java
@Mapper
public interface StudentMapper {
  
    List<Student> getStudents();
}
```

**XML:**

/src/main/resources/mybaits/StudentMapper.xml

```xml
<mapper namespace="com.example.demo.dao.StudentMapper">
    <select id="getStudents" resultType="Student">
        select * from student;
    </select>
</mapper>
```

**Controller:**

```java
@RestController
public class StudentController {
    @Autowired
    StudentMapper studentMapper;

    @RequestMapping("/hello")
    public String test() {
        List<Student> students = studentMapper.getStudents();
        return students.toString();
    }
}
```

### 整合Druid

整合第三方技术：

1.导入starter

2.根据配置格式，填写配置

```properties
spring.datasource.druid.password=
spring.datasource.druid.username=
spring.datasource.druid.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.druid.url=jdbc:mysql://localhost:3306/Mybatis_test?serverTimezone=UTC
```

## SpringBoot Web开发

### 静态资源导入

这些目录的文件会被导入

```java
"classpath:/META-INF/resources/"
"classpath:/resources/"
"classpath:/static/"
"classpath:/public/"
```

**欢迎页定制：**

`index.html`放在静态资源文件夹



**图标定制：**

1.关闭默认图标

```yaml
#关闭默认图标
spring.mvc.favicon.enabled= false
```

2.放在静态资源目录下，图片名为：favicon.ico



### 扩展MVC

```java
//应为类型要求为WebMvcConfigurer，所以我们实现其接口
//可以使用自定义类扩展MVC的功能
@Configuration
public class MyMvcConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        // 浏览器发送/test ， 就会跳转到test页面；
        registry.addViewController("/test").setViewName("test");
    }
}
```

### 拦截器

编写拦截器：实现HandlerInterceptor接口

```java
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Object loginer = request.getSession().getAttribute("loginer");
        if (loginer == null) {
            request.getRequestDispatcher("/index.html").forward(request, response);
            return false;
        }
        return false;
    }
}
```

在配置文件中注册：

```java
   @Override
    public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/**").excludePathPatterns("index.html","/","/login","/static/**");
    }
```

# 异常处理

```java
@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler
    public void doException(Exception e){
        e.printStackTrace();
    }
}
```



# 碎片

SpringBoot重定向：`return "redirect:/test";`

配置`error`页面:在`templates`创建文件夹`error`，将`404.html`/`500.html`放入其中

`static`放置css/js

`templates`放置html



# Data protocol

```java
@Data
public class R {

    private Boolean flag;/*标志*/
    private Object data;/*数据*/
    private String msg;/*消息*/

    /*返回状态*/
    public R(Boolean flag) {
        this.flag = flag;
    }

    /*返回数据，带状态*/
    public R(Boolean flag, Object data) {
        this.flag = flag;
        this.data = data;
    }

    public R(String msg) {
        this.flag = false;
        this.msg = msg;
    }

    public R(Boolean flag,String msg){
        this.flag=flag;
        this.msg=msg;
    }
}
```

# 异常处理器

```java
/*作为springmvc的异常处理器*/
@RestControllerAdvice
public class ProjectExceptionAdvice {

    /*拦截所有的异常信息*/
    @ExceptionHandler
    public R dpException(Exception ex){
        /*记录日志*/
        /*通知运维*/
        /*通知开发*/
        ex.printStackTrace();
        return new R("服务器故障，请稍后再试！");
    }
}
```

# Spring、SpringMVC、SpringBoot

1、Spring



Spring是一个开源容器框架，可以接管web层，业务层，dao层，持久层的组件，并且可以配置各种bean,和维护bean与bean之间的关系。其核心就是控制反转(IOC),和面向切面(AOP),简单的说就是一个分层的轻量级开源框架。



2、SpringMVC



Spring MVC属于SpringFrameWork的后续产品，已经融合在Spring Web Flow里面。SpringMVC是一种web层mvc框架，用于替代servlet（处理|响应请求，获取表单参数，表单校验等。SpringMVC是一个MVC的开源框架，SpringMVC=struts2+spring，springMVC就相当于是Struts2加上Spring的整合



3、SpringBoot



Springboot是一个微服务框架，延续了spring框架的核心思想IOC和AOP，简化了应用的开发和部署。Spring Boot是为了简化Spring应用的创建、运行、调试、部署等而出现的，使用它可以做到专注于Spring应用的开发，而无需过多关注XML的配置。提供了一堆依赖打包，并已经按照使用习惯解决了依赖问题—>习惯大于约定。



# `@SpringBootApplication`

```java
@SpringBootConfiguration
/**
启动类标注了 @Configuration 之后，本身其实也是一个 IoC 容器的配置类！
*/
@EnableAutoConfiguration
/**
@EnableAutoConfiguration 也是借助 @Import 的帮助，将所有符合自动配置条件的 bean 定义加载到 IoC 容器
*/
@ComponentScan
```



