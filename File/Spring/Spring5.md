# Spring5

## 	简介

Spring 是⼀个轻量级、⾮⼊侵式的控制反转 (IoC) 和⾯向切⾯ (AOP) 的框架。

特性：IOC实现了高内聚低耦合，AOP面向切面编程，声明式事务，可以继承各种第三方框架Mybatis

## 常用注解：

### Web

`@Controller`:组合注解（组合了@Component注解）,应用在控制层

`@RestController`：相当于@Controller和@ResponseBody的组合，意味着，该Controller的所有方法都默认加上了@@ResponseBody

`@RequestMapping:@GetMapping、@PostMapping、@PutMapping、@DeleteMapping`：用于映射Web请求，包含路径和参数，如果是Restful风格接口，就可以用后四个注解：查询、提交、更新、删除

`@ResponseBody`：该注解对应的方法return后不会走视图解析器，而是返回字符串，常用于返回json数据

`@RequestBody`：@RequestBody主要用来接收前端传递给后端的json字符串中的数据(**请求体中的数据**)而最常用的使用请求体传参的无疑是POST请求了，所以使用@RequestBody接收数据时，一般都用POST方式进行提交。RequestBody 接收的是请求体里面的数据；而RequestParam接收的是key-value

`@RequestParam`:语法：@RequestParam(value=”参数名”,required=”true/false”,defaultValue=””)

​										value：参数名

​										required：是否包含该参数，默认为true，表示该请求路径中必须包含该参数，如果不包含就报错

​										@RequestParam将请求参数绑定到控制器的方法参数上（是springmvc中接收普通参数的注解）	



`@PathVariable`：⽤于接收路径参数，⽐如@RequestMapping(“/hello/{name}”)申明的路径， 将注解放在参数中前，即可获取该值，通常作为Restful的接⼜实现⽅法

### 容器

`@Component`：表⽰⼀个被注释的类是⼀个“组件”，成为Spring管理的Bean

`@Service`：组合注解（组合了@Component注解），应⽤在service层（业务逻辑层）

`@Repository`：组合注解（组合了@Component注解），应⽤在dao层（数据访问层）

`@Autowired`：自动装配

`@Qualifier`：通常跟 @Autowired ⼀起使⽤，当想对注⼊的过程做更多的控制， @Qualifier 可帮助配置，⽐如两个以上相同类型的 Bean 时 Spring ⽆法抉择，⽤到此注解

`@Configuration`：：声明当前类是⼀个配置类

`@Value`：可⽤在字段，构造器参数跟⽅法参数，指定⼀个默认值，⽀持 #{} 跟 ${} 两个 ⽅式。⼀般将 SpringbBoot 中的 application.properties 配置的属性值赋值给变量。

`@Bean`：注解在⽅法上，声明当前⽅法的返回值为⼀个Bean

`@Scope`：定义我们采⽤什么模式去创建Bean（⽅法上，得有@Bean） 其设置类型包括： Singleton 、Prototype

### AOP

`@Aspect`：声明⼀个切⾯（类上）

`@After`：在⽅法执⾏之后执⾏（⽅法上）

`@Before`：在⽅法执⾏之前执⾏（⽅法上）

`@Around`：在⽅法执⾏之前与之后执⾏（⽅法上）

### 事务

`@Transactional`:：在要开启事务的⽅法上使⽤@Transactional注解，即可声明式开启事务。



### @Component 和 @Bean 的区别是什么？

作用对象不同: @Component 注解作用于类，而@Bean注解作用于方法。

@Component通常是通过类路径扫描，自动装配到Spring容器中（我使用 @ComponentScan 定义要扫描的路径）。这种方法对需要进行逻辑处理的控制非常有限，因为它纯粹是声明性的。

@Bean 注解通常在标有该注解的方法中产生这个 bean,@Bean告诉了Spring这是某个类的实例。@Bean则常和@Configuration注解搭配使用。当引用第三方库中的类需要装配到 Spring容器时，则只能通过 @Bean来实现。

```java
@Configuration
public class WebSocketConfig {
    @Bean
    public Student student(){
        return new Student();
    }
}
```



## 什么是IOC？

 在没有引入IOC容器之前对象A依赖于对象B，那么对象A在初始化或者运行到某一点的时候，必须主动去创建对象B或者使用已经创建的对象B。无论是创建还是使用对象B，控制权都在自己手上。

在引入IOC容器之后，对象A与对象B之间失去了直接联系，所以，当对象A运行到需要对象B的时候，IOC容器会主动创建一个对象B注入到对象A需要的地方

本质：将对象的创建交给用户



## Spring容器启动阶段会⼲什么？

Spring的IOC容器⼯作的过程，可以划分为两个阶段：**容器启动阶段**和**Bean实例化阶段**



## Spring Bean⽣命周期

Spring IOC 中Bean的⽣命周期⼤致分为四个阶段：实例化（Instantiation）、属性赋值

（Populate）、初始化（Initialization）、销毁（Destruction）。



## Hello Spring

**XML**:

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:util="http://www.springframework.org/schema/util"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="
	    http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
	    http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.0.xsd
	    http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-4.0.xsd
	    http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-4.0.xsd
	    http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-4.0.xsd">


<!--    使用Spring创建对象-->
  	<bean id="object_test" class="com.pojo.test"></bean>
    <bean id="hello" class="com.pojo.Hello">
        <property name="test" value="Hello_Spring"></property>
      	<property name="object" ref="object_test"></property>
    </bean>
</beans>
```

**test:**

```java
public class MyTest
{
    public static void main(String[] args)
    {
//        获取上下文对象
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");		//加载XML
        Hello hello = (Hello)context.getBean("hello");	//获取bean
        String result = hello.toString();	
        System.out.println(result);
    }
}
```

IOC默认使用无参构造

**有参构造方法：**

```java
public Hello(String name){ 
	this.name=name;
}

<bean id="hello" class="">
	<constructor-arg index="0" value="test">	//index refers to argument
<bean/>
```

xml是一个容器，其中的对象在程序加载容器时，自动创建

## Spring配置

```xml
<alias name="user" alias="alias"></alias>
```

合并XML：

在总XML里汇合子XML

```xml
<import resource="bean1.xml"></import>
<import resource="bean2.xml"></import>
```

## 依赖注入

**Set注入**：

	依赖：bean对象的创建依赖于容器
	
	注入：bean对象的所有属性，由容器注入，实际上是通过set方法注入

```java
public class Student
{
    private String name;
    private Address address;
    private String[] books;
    private List<String> hobbys;
    private Map<String,String> card;
    private Set<String> games;
	...
}
```

ApplicationContext.xml

```xml
    <bean id="address" class="com.pojo.Address">
    </bean>

    <bean id="student" class="com.pojo.Student">
        <property name="name" value="FR"></property>	
        <property name="address" ref="address"></property>
        <property name="books">
            <array>
                <value>book1</value>
                <value>book2</value>
                <value>book3</value>
            </array>
        </property>
        <property name="hobbys">
            <list>
                <value>reading</value>
                <value>jogging</value>
            </list>
        </property>
        <property name="card">
            <map>
                <entry key="01" value="01"></entry>
                <entry key="02" value="02"></entry>
                <entry key="03" value="03"></entry>
            </map>
        </property>
        <property name="games">
            <set>
                <value>set01</value>
                <value>set02</value>
            </set>
        </property>
    </bean>
```

## Bean作用域

1.singleton,默认机制，从容器里取的对象是相同的

```xml
<bean id="user" class="" scope="singleton"/>
```

2.prototype,从容器里取的对象是不同的

```xml
<bean id="user" class="" scope="prototype"/>
```

## 自动装配

**装配方式：**

	1.XML
	
	2.Java中显示配置
	
	3.隐式的自动装配Bean*

```xml
    <bean id="cat" class="com.pojo.Cat">
        <property name="name" value="kitten"></property>
    </bean>

    <bean id="dog" class="com.pojo.Dog">
        <property name="name" value="puppy"></property>
    </bean>

    <bean id="people" class="com.pojo.People" autowire="byName">	
<!-- byName/byType根据对象的set方法，找到符合条件的属性-->
<!--        <property name="cat" ref="cat"></property>-->
<!--        <property name="dog" ref="dog"></property>-->
        <property name="name" value="FR"></property>
    </bean>
</beans>
```

## 使用注解实现自动装配

XML：

```xml
<!--    开启注解支持-->
    <context:annotation-config></context:annotation-config>

    <bean id="cat" class="com.pojo.Cat">
        <property name="name" value="kitten"></property>
    </bean>

    <bean id="dog" class="com.pojo.Dog">
        <property name="name" value="puppy"></property>
    </bean>

    <bean id="people" class="com.pojo.People">
        <property name="name" value="FR"></property>
    </bean>
```

People:

```java
public class People
{
    @Autowired
    private Cat cat;
    @Autowired
    private Dog dog;
    private String name;
}
```

根据名字，自动装配Cat和Dog对象

## Spring 中的 Bean 的作⽤域有哪些?

singleton : 在Spring容器仅存在⼀个Bean实例，Bean以单实例的⽅式存在，是Bean默认的 作⽤域。 		prototype : 每次从容器重调⽤Bean时，都会返回⼀个新的实例

request：每一次HTTP请求都会产生一个新的bean，该bean仅在当前HTTP request内有效。

session：每一次HTTP请求都会产生一个新的bean，该bean仅在当前HTTP session内有效。



## 循环依赖

A依赖B的同时B也依赖了A

```java
@componet
public class A{
    @Autowired
    private B b;
}

@componet
public class B{
    @Autowired
    private A a;
}
```

**Spring解决循环依赖的前置条件:**

- 出现循环依赖的Bean必须要是单例
- 依赖注入的方式不能是构造器注入的方式

```java
@componet
public class A{
    public A(B b){
        
    }
}
//这种循环依赖是无法解决的
```



## 使用注解开发

使用注解开发，需要导入AOP包，需要导入context约束

1.bean

```xml
    <context:annotation-config></context:annotation-config>
    <context:component-scan base-package="com.pojo"></context:component-scan>
```

2.属性注入

```java
@Component
public class User
{
    @Value("lby")
    public String name;
}
```

3.衍生注解

`dao[@Repository]`

`service[@Service]`

`controller[@Controller]`

`**@scope("singleton")**`

4.**最佳实践**

xml管理bean

注解完成属性注入



## 用Java配置Spring

JavaConfig可以不用XML配置bean，而是用java类

```java
@Configuration
@ComponentScan("com.pojo")
@Import(Configuation2.class)
public class Configuation
{
    @Bean
    public User getUser(){
        return new User();
    }
}
```

test:

```java
    public static void main(String[] args)
    {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Configuation.class);
        User user = context.getBean("getUser", User.class);
        System.out.println(user.getName());
    }
```



## AOP——代理模式

代理模式：

### 		**静态代理：**

- 抽象角色：使用接口/抽象类来解决

- 代理角色：代理真实角色，并添加附属操作

- 真实角色：被代理

- 客户：

  ```java
  //接口，只关注租房行为
  public interface Rent
  {
      public void rent();
  }
  //房东，出租房子
  public class HomeOwner implements Rent
  {
      public void rent()
      {
          System.out.println("房东出租房子");
      }
  }
  //代理，实现出租接口、操作真实对象，添加新的公共业务
  public class Proxy implements Rent
  {
      private HomeOwner homeOwner;
      public Proxy(){
  
      }
  
      public Proxy(HomeOwner homeOwner){
          this.homeOwner=homeOwner;
      }
  
      public void rent()
      {
          checkOutHouse();
          System.out.println("代理帮房东租房子！");
      }
  
      public void checkOutHouse(){
          System.out.println("带你看房子！");
      }
  }
  
  //租客，操作代理实现租房
  public class Tenant
  {
      public static void main(String[] args)
      {
          HomeOwner owner = new HomeOwner();
          Proxy proxy = new Proxy(owner);
          proxy.rent();
      }
  }
  ```

  

  代理模式的好处：

  1.真实角色的操作更加简单，不用关注一些公共的业务

  2.公共业务可以交给代理对象，实现了业务分工

  3.公共业务扩展时，只需要修改代理

  缺点：

  1.一个真实角色就会产生一个代理角色，代码量翻倍

  

  ### **动态代理：**

动态代理和静态代理角色一样

动态代理的代理类是动态生成的

动态代理分为两类：

1、基于接口的动态代理——JDK动态代理

2、基于类的动态代理——cglib

3、Java字节码实现：JAVAssist



需要了解两个类：Proxy、InvocationHandler

**用`InvocationHandler`代理接口，在内部用`Proxy`生成代理类**

```java
public class ProxyInvocationHandler implements InvocationHandler
{
    private Object target;	//被代理的接口
  
    public void setTarget(Object target)
    {
    this.target = target;
}

  //用来生成代理类
    public Object getProxy(){
        return Proxy.newProxyInstance(this.getClass().getClassLoader(),target.getClass().getInterfaces(),this);
    }

  //方法的调用和扩展
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
    {
        Object result = method.invoke(target,args);
        rent();
        return result;
    }

    public void rent()
    {
        System.out.println("代理帮房东租房子！");
    }
}
```

test：

```java
public class Tenant
{
    public static void main(String[] args)
    {
        HomeOwner owner = new HomeOwner();		//真实对象
        ProxyInvocationHandler proxy = new ProxyInvocationHandler();
        proxy.setTarget(owner);		//设置被代理对象
        Rent proxyRent = (Rent)proxy.getProxy();	//生成代理类
        proxyRent.rent();	
    }
}
```



## AOP

AOP：aspect oriented programming(**面向切面编程**)

Spring AOP是基于动态代理实现的，分为两种代理：基于接口的jdk动态代理，基于类的cglib代理

==提供声明式事物，允许用户自定义切面==

引入AOP包：

```xml
<!-- https://mvnrepository.com/artifact/org.springframework/spring-aop -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-aop</artifactId>
    <version>5.3.19</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.aspectj/aspectjweaver -->
<dependency>
    <groupId>org.aspectj</groupId>
    <artifactId>aspectjweaver</artifactId>
    <version>1.9.1</version>
</dependency>

```

**方式一：XML**

```java
//切面
public class PointCut
{
    public void beforeMethod(){
        System.out.println("====before====");
    }

    public void afterMethod(){
        System.out.println("====after====");
    }
}

//被扩展的类
public class UserServiceImpl implements UserService
{
    public void add()
    {
        System.out.println("add");
    }
}
```

XML：

```java
//XML
  <bean id="userService" class="com.service.UserServiceImpl"></bean>
  <bean id="cut" class="com.PointCut"></bean>
    <aop:config>
        <aop:aspect ref="cut">
            <aop:pointcut id="pointOne" expression="execution(* com.service.UserServiceImpl.*(..))"/>
            <aop:before method="beforeMethod" pointcut-ref="pointOne"></aop:before>
            <aop:after method="afterMethod" pointcut-ref="pointOne"></aop:after>
        </aop:aspect>
    </aop:config>
```

```xml
execution(修饰符 返回类型 切入点类 切入点方法(参数) 异常抛出)
<!-- 【1、拦截所有public方法】 -->
expression="execution(public * *(..))"
<!-- 【2、拦截所有save开头的方法】 -->
expression="execution(* save*(..))" 
```

**方式二：注解实现AOP**

```java
@EnableAspectJAutoProxy
@Aspect
@Component
public class AnnotationPointCut
{
    @Before("execution(* com.service.UserServiceImpl.add (..))")
    public void beforeMethod(){
        System.out.println("====before====");
    }

    @After("execution(public * com.service.UserServiceImpl.add (..))")
    public void afterMethod(){
        System.out.println("====after====");
    }
}

@Configuration
@ComponentScan("com")
public class Configuation
{
    @Bean
    public UserServiceImpl getUserService(){
        return new UserServiceImpl();
    }

    @Bean
    public AnnotationPointCut getAnnotation(){
        return new AnnotationPointCut();
    }
}

public class MyTest
{
    public static void main(String[] args)
    {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Configuation.class);
        UserService userService = (UserService)context.getBean("getUserService", UserService.class);
        userService.delete();
    }
}
```



## 事务管理

**ACID原则：**

原子性

一致性

隔离性：多个业务可能操作同一个资源，防止数据损坏

持久性

**确保要么都成功，要么都失败**

（事务相关内容参考MySQL的事务）



Spring事务的本质其实就是数据库对事务的支持，使用JDBC的事务管理机制完成对事务的提交



**第一种方式：XML配置**

配置事务管理器transactionManager，关联到数据库datesource

在tx:advice关联事务管理器，然后配置对哪个方法进行拦截

通过AOP配置增强器（advisor）与切点的连接

```xml
<bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <constructor-arg ref="datesource"/>
</bean>

<tx:advice id="interceptor" transaction-manager="transactionManager">
    <tx:attributes>
      /*假设查询方法都是标准的find开头，比如findAll、findOne)*/
        <tx:method name="find*" read-only="true" propagation="REQUIRED"/>
    </tx:attributes>
</tx:advice>

<aop:config>
    <aop:pointcut id="txPointcut" expression="execution(* com.mapper.*.*(..))"/>
    <aop:advisor advice-ref="interceptor" pointcut-ref="txPointcut"/>
</aop:config>
```



### 事务的传播行为

Spring定义了七种传播行为：

​	**PROPAGATION_REQUIRED**

​	**PROPAGATION_SUPPORTS**

​	**PROPAGATION_MANDATORY**

​	…



**PROPAGATION_REQUIRED**

方法A调用方法B，两个方法都有事务

单独调用methodB方法时，因为当前上下文不存在事务，所以会**开启一个新的事务**。
		调用methodA方法时，因为当前上下文不存在事务，所以会开启一个新的事务。当执行到methodB时，methodB发现当前上下文有事务，因此就**加入到当前事务**中来

```java
@Transactional(propagation = Propagation.REQUIRED)
public void methodA() {
 methodB();
 ...
}

@Transactional(propagation = Propagation.REQUIRED)
public void methodB() {
	...
}
```



**PROPAGATION_SUPPORTS**

如果存在一个事务，支持当前事务。如果没有事务，则非事务的执行

调用methodB时，methodB方法是非事务的执行的。当调用methdA时,methodB则加入了methodA的事务中,事务的执行。

```java
@Transactional(propagation = Propagation.REQUIRED)
public void methodA() {
 methodB();
// do something
}

// 事务属性为SUPPORTS
@Transactional(propagation = Propagation.SUPPORTS)
public void methodB() {
    // do something
}
```



**PROPAGATION_MANDATORY**

如果已经存在一个事务，支持当前事务。如果没有一个活动的事务，则抛出异常。

当单独调用methodB时，因为当前没有一个活动的事务，则会抛出异常throw new IllegalTransactionStateException(“Transaction propagation ‘mandatory’ but no existing transaction found”);当调用methodA时，methodB则加入到methodA的事务中，事务地执行。

```java
@Transactional(propagation = Propagation.REQUIRED)
public void methodA() {
 methodB();
// do something
}

// 事务属性为MANDATORY
@Transactional(propagation = Propagation.MANDATORY)
public void methodB() {
    // do something
}
```



### **大事务**

一个方法有许多语句，只有两行语句和事务有关，事务的粒度太大。

解决办法：用编程式事务，减少粒度



### **`@Transactional`失效场景**

- `@Transactional`应用在非public方法上，Spring要求被代理的方法必须是public



- 方法用final修饰，也无法被代理，事务会失效。因为AOP代理当前类，重写当前方法，这样才能实现代理。可是final方法不能被重写，所以事务失效



- `@Transactional`注解属性propagation设置错误

  - PROPAGATION_NOT_SUPPORTTED：如果存在事务，就把事务挂起
  - PROPAGATION_NEVER
  - `REQUIRED` 如果当前上下文中存在事务，那么加入该事务，如果不存在事务，创建一个事务，这是默认的传播属性值。
  - `SUPPORTS` 如果当前上下文存在事务，则支持事务加入事务，如果不存在事务，则使用非事务的方式执行。
  - `MANDATORY` 如果当前上下文中不存在事务，否则抛出异常。

  

- `@Transactional`注解属性rollbackOn设置错误

  - 默认只抛出unchecked异常（空指针）和Error异常（OOM），其他异常不会回滚
  - 在属性里设置`rollbackFro="Exception.class"`

  

- 自己try catch了异常/throws，就不会回滚



- 当在Service中定义了一个B方法并且开启事务之后，从Controller里面调用该B方法可以实现切入，但是当在同一个Service中用另一个A方法调用它，则事务失效

  <u>Service中如此调用并非调用的是代理类中的方法，是不会被切进去的</u>。换言之，必须要调用代理类才会被切进去

  解决办法：`AopContext.currentProxy().callMethodB();`获得代理类，调用代理类的方法B
