## **段落：**`<p></p>`

## **换行：**`<br/>`

## **粗体：**`<strong></strong>`

## **斜体：**`<em></em>`*(emphasis)*

## **下划线：**`<ins></ins>`<u>(inserted text)</u>

## **删除线：**`<del></del>`

## **盒子：**

​		`<div></div>`	division，一个div独占一行，它是块级元素

​		`<span></span>`	行级元素

## **图像：**`<img src="">`

​		**属性：**

​				**路径：**`src`

​				**无障碍：**`alt`

​				**提示：**`title`

​				**宽度：**`width`

​				**高度：**`height`

​				**边框：**`border`

## **超链接：**

​			`<a href="外部链接" target="窗口弹出方式">百度</a>` anchor

​			`<a href="内部链接" target="_blank">百度</a>`

​			**下载链接：**如果href地址是一个压缩包，就会下载链接

​			**图像链接：**`<a href="..." target="_blank"><img src="截屏.png"></a>`

​			**锚点链接：**`<a href="#id" target="_blank">anchor_id</a>`

## **特殊字符：**

![截屏2022-08-29 上午2.54.56](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-08-29 上午2.54.56.png)

## **表格：**

```html
<!--    cellpadding：文字和表格间的空隙-->
<!--    cellspacing:表格之间的空隙-->

<table align="center" border="1" cellpadding="10" cellspacing="0" width="200">

    <thead>
    <tr>
        <th>名字</th>
        <th>年龄</th>

    </tr>
    </thead>

    <tbody>

    <tr align="center">
        <td>lby</td>
        <td>23</td>
    </tr>

    <tr align="center">
        <td>fr</td>
        <td>24</td>
    </tr>

    <tr>
        <td align="center" colspan="2">none</td>
    </tr>

    <tr>
        <td align="center" rowspan="2">none</td>
        <td>right_padding</td>
    </tr>

    <tr>
        <td>right_padding</td>
    </tr>

    </tbody>

</table>
```

![截屏2022-08-29 上午3.16.37](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-08-29 上午3.16.37.png)

## 列表

```html
<ol>
    <li>A</li>
    <li>B</li>
    <li>C</li>
</ol>
<ul>
    <li>A</li>
    <li>B</li>
    <li>C</li>
</ul>

<dl>
    <dt>关注我</dt>
    <dd>微信</dd>
    <dd>QQ</dd>
    <dd>微博</dd>
</dl>
```

![截屏2022-08-29 上午3.21.21](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-08-29 上午3.21.21.png)

## 表单

```html
<form name="login_form" method="post" action="https://...">

    <label for="name">名字：</label><input id="name" type="text" name="name" required placeholder="write your name" maxlength="10"><br/>
    <label for="password">密码：</label><input id="password" type="password" placeholder="write your password" maxlength="8"><br/>
    <label for="age">年龄：</label><input id="age" type="text" name="age" required placeholder="write your age" maxlength="3"><br/>
    <input type="hidden" value="uuid">

    性别：<select>
    <option selected>male</option>
    <option>female</option>
</select><br/>

    爱好：
    阅读<input type="checkbox" name="hobby" value="read">
    音乐<input type="checkbox" name="hobby" value="music" checked>
    电影<input type="checkbox" name="hobby" value="movie"><br/>

    婚姻状况：

    已婚<input type="radio" name="marriage" value="yes">
    未婚<input type="radio" name="marriage" value="yes" checked><br/>

    注册日期：<input type="date"><br/>
    <input type="button" value="获得验证码"><br/>
    输入验证码：<input type="text" name="code" required placeholder="write your code" maxlength="4"><br/>
    自我介绍：
    <textarea></textarea><br>
    提交：<input type="submit" value="submit your info">
    重置：<input type="reset" value="reset your input">
</form
```

![截屏2022-08-29 上午3.53.18](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-08-29 上午3.53.18.png)



# Emmet语法

- 生成标签：标签名+tab

`div		+tab`

- 生成多个相同标签：标签名*数量+tab

`div*3	+tab`

- 生成父子关系标签：父标签>子标签+tab

`ul>li*3		+tab`

- 生成有id/类名的标签：标签#id 标签.类名+tab

`div#container		+tab`

`div.left-container		+tab`

- 生成带有顺序的标签，用自增符号：	$

`div.container$*3`

```
<div class="container1"></div>
<div class="container2"></div>
<div class="container3"></div>
```

- 生成的标签里写内容，用{}

`div.container{I'm $}*3`

```
<div class="container">I'm 1</div>
<div class="container">I'm 2</div>
<div class="container">I'm 3</div>
```
