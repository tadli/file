[TOC]

# Clean code

> Principle

- Expressiveness
- Tiny abstractions
- Leave the campground cleaner than you found it



# Chapter1 – Meaningful Names

## Use intension-revealing names

:star:No comments 

 If a name requires a comment, then the name does not reveal its intent

```java
int daysSinceCreation;
int fileAgeInDays;
```



**Compare**:

:green_book: bad 

```java
public List<int[]> getThem() {
	List<int[]> list1 = new ArrayList<int[]>();
	for (int[] x : theList)
		if (x[0] == 4)
			list1.add(x);
	return list1;
}
```

:closed_book: good

```java
public List<Cell> getFlaggedCells() {
	List<Cell> flaggedCells = new ArrayList<Cell>();
	for (Cell cell : gameBoard)
		if (cell.isFlagged())
			flaggedCells.add(cell);
	return flaggedCells;
}
```



## Avoid Disinformation

:star:Avoid adding type to the name

Do not refer to a grouping of accounts as an `accountList` unless it's actually `List`. 

If the container holding the accounts is not actually a `List`, it may lead to false conclusions. So `accountGroup` or `bunchOfAccounts` or just plain `accounts` would be better



## Make Meaningful Distinctions

Noise words are another meaningless distinction.

What's the different among these?

`getProduct()`

`getProductInfo()`

`getProductData()`

-  The word `variable` should never appear in a variable name
- The word `table` should never appear in a table name



## Use Pronounceable Names



## Use Searchable Names

Single letter names and numeric constants have a particular problem in that they are not easy to locate across a body of text



## Interfaces and Implementations

`ShapeFactory` – interface, do not add `I` in the front of name

`ShareFacrotyImp` - implementation



## Class Names

Class and Objects should have noun or noun phrase names like `Customer`, `WikiPage`. Avoid words like `Manager`, `Processor`. A class name should not be a verb



## Method Names

Methods should have verb or verb phrase names like `postPayment`, `deletePage`. Accessors, mutators, and predicates should be named for  their value and prefixed with `get`, `set`, and `is` 



## Don't Use Solution Domain Names



##  Add Meaningful Context

​	Imagine that you have variables named `firstName`, `lastName`, `street`, `houseNumber`, `city`, `state`, and `zipcode`. Taken together it’s pretty clear that they form an address. But what if you just saw the state variable being used alone in a method?

​	You can add context by using prefixes: `addrFirstName`, `addrLastName`, `addrState`, and so on. At least readers will understand that these variables are part of a larger structure. Of course, a better solution is to create a class named Address. Then, even the compiler knows that the variables belong to a bigger concept.



## Chapeter1 – Summary

- Class name must use `UpperCamelCase` style, except for DO/ BO/ DTO/ VO
- Don't use only one constant class to contain all constants, `CacheConsts`, `ConfigConsts`

- Any bool variables in the class cannot start with **`is`**, such as `isDeleted`, if there is a method called `isDeleted()`, Spring structure will parse falsely
- Abstract class must start with `Abstract` / `Base`; Exception Class must end with `Exception`, test class must start with tested class and end with `Test`
- If interface, class or method is implemented with design pattern,  must reveal pattern in the name. 
	- `public class OrderFactory`
	- `public class LoginProxy` 
- VO  -> DTO -> DO
- If a service name is used to describe capability, it must end with `-able`
	- `Translatable`
- Any class and variable in the interface cannot with `public`

# Chapter2 - Function

## One Level of Abstraction per function

In order to make sure our functions are doing "one thing", we need to make sure that the statements within our function are all the same level of abstraction. 

