## 云基础概念

### **Iaas**（infrastructure as a service）

基础网络

### SaaS	（Software as a service）

软件

### PaaS  （platform as a service）

平台



## AWS Global Infrastructure

### regions

全球部署的data center，是一组AZ的集合

### availability zones

一个region包含多个可用区，包含多个数据中心，AWS服务利用AZ之间低延迟连接来复制数据，以达到高可用性和弹性

### edge locations

不用于部署主要基础架构，如EC2实例，EBS存储，VPC或者RDS资源

但AWS CloudFront和AWS Lambda使用它来缓存数据，用作CDN（Content Distribution Network），减少Users的延迟

### regional edge caches

新型的edge locations，位于CloudFront Origin服务器和edge locations之间，用来存放访问频率很低的缓存数据文件



