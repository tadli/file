# IDEA快捷键



```html
Ctrl+Alt+L													格式化代码
Alt+Shift+Up/Down										上/下移一行
Ctrl+D															复制行
Ctrl+J															自动代码（例如：serr）
Ctrl+Alt+J													用动态模板环绕
Ctrl+H															显示类结构图（类的继承层次）
Ctrl+O															重写方法
Ctrl+Shift+U												大小写转化
Ctrl+"+/-"													当前方法展开、折叠
Shift+C															重命名
com+1，2，3，4 											切换tool window
com+E																最近修改的文件
```

IDEA Editor - file types 可以隐藏文件
