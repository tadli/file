> MVVM组成：数据绑定、指令系统、组件式编程、路由和导航、状态保持、第三方组件库

# 基础知识

Angular 定义了 `NgModule`。每个 Angular 应用都有一个*根模块*，通常命名为 `AppModule`。根模块提供了用来启动应用的引导机制。一个应用通常会包含很多特性模块。

**NPM管理包：**package.json

**执行流程：**当 Angular 在 main.js 中调用 bootstrapModule 函数时，它读取 AppModule 的元数据，在启动组件中找到 AppComponent 并找到 my-app 选择器，定位到一个名字为 my-app 的元素，然后在这个标签之间的载入内容

# 组件

```tsx
import {Component} from "@angular/core";
@Component({
  selector: 'app-mycon',
  templateUrl: './app.mycomponent.html',
  styleUrls: ['./app.mycomponent.css']
})
export class mycomponent {}
```

1. 从 *angular2/core* 引入了 *Component* 包。
2. *@Component* 是 Angular 2 的*装饰器* ，它会把一份元数据关联到 AppComponent 组件类上。
3. *my-app* 是一个 CSS 选择器，可用在 HTML 标签中，作为一个组件使用。
4. *export* 指定了组件可以在文件外使用。
5. 打开 app.module.ts 文件，导入新的 AppComponent ，并把它添加到 NgModule 装饰器的 declarations
6. 快速生成语句：`ng g component <name>`



# 数据绑定

## html绑定

`{{ng表达式}}`

```tsx
export class myapp{
  username = "lby";
  age = 20;
}

<div>
<!--    取模型数据-->
  username:{{username}}<br>
  age:{{age}}<br>
<!--    三目运算符-->
  {{(age>=18 && age<60) ? "已成年" : "未成年"}}<br>
<!--    函数-->
  {{username.length}}<br>
  {{username.toUpperCase()}}<br>
  {{username[2]}}<br>
</div>
```

## 属性绑定

```tsx
<!--  在属性上用{{}}绑定属性-->
<p title="{{username}}">test attribute binding</p>
<!--  用[]绑定-->
<p [title]="username">test attribute binding</p>
```



## 指令绑定

**循环绑定：**

```tsx
emplist = ["A", "B", "C"];

<ul>
  <li *ngFor="let item of emplist;index as index">{{item}}{{index}}</li>
</ul>
```

**选择绑定：**

```tsx
<p *ngIf="age<18; else forChildren" >fuck you</p>

<ng-template #forChildren> 
  <strong>给孩子看的内容</strong>
</ng-template>
```



## 事件绑定

```tsx
<button (click)="printUsername()">r2r2</button>

export class myapp{
  username = "lby";
  age = 20;

  printUsername(){
    alert(this.username);
  };
}

<!--title绑定的是参数count，其余部分用单引号拼接-->
<strong [title]="'当前数量为：'+count">{{count}}</strong>
<button (click)="countDown()">+</button>

countUp(){
  this.count--;
}
```



## 双向数据绑定

