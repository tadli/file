# JavaScript Fundamentals



## Hello World

```javascript
<script src="url"/>
```

:star: As a rule, Complex scripts should be put into separate files. The benefit of a separate file is that the browser will download it and store it in its cache. 



#### The modern mode, "use strict"

To keep the old code working, most such **modification(ES5) are off by default**. You need to explicitly enable them with a special directive: `"use strict"`



#### Variable 

There are two limitations on variable name in JavaScript:

1. The name must contain only letters, digits, or the symbols `$` and `_`
2. The first character must **not be a digit**

:star:: **Case matters**, Variables named `apple` and `APPLE` are two different variables



#### Constants

`const COLOR_RED = '#F00'`



## Data types

There are **eight basic data types** in JavaScript. 

:point_right:`String`:point_right: `Number`:point_right: `Boolean`:point_right: `BigInt`:point_right: `Object`:point_right: `Symbol`:point_right: `Null`:point_right: `Undefined`



### Number

The number type represents both **integer** and **floating** point numbers

Besides regular numbers, there are so-called "special numeric values" which also belong to this data type: **`Infinity`, `-Infinity` and `NaN`**

```js
typeof NaN
'number'
```

- `Infinity`  represents the mathematical ∞

```javascript
(1/0); //Infinity
```

- `NaN` represents a computational error

```javascript
alert("not a number"/2); //NaN
```

If there is a `NaN` somewhere in a mathematical expression, it propagates to the whole result.

(Only one exception: `NaN ** 0` is `1`)



:star:: Doing math is "Safe", we can do anything: divide by zero, treat non-numeric strings as numbers, etc. The script will never stop with a fatal error. <u>At worst, we'll get `NaN` as the result</u>



### BigInt

A `BigInt` value is created by **appending `n` to the end of an integer**:

```javascript
const bigInt = 1234567890123456789012345678901234567890n;
```



### String

Double quotes:`"Hello"`

Single quotes:`'Hello'`

Backticks: \` \`

Backticks are "extended functionality" quotes. They allow us to embed variables and expressions into a string by wrapping them in `${...}`

```JavaScript
let name = "Tad";
alert(`Hello,${name}`);
alert(`the result is ${1+2}`);
```



### undefined

The meaning of `undefined` is “value is not assigned”



### Objects and Symbols

The `symbol` type is used to create unique identifiers for objects.



### The `typeof` operator

The `typeof` of operator returns the type of the operand. 

```javascript
typeof undefined // "undefined"
typeof 10n // "bigint"
typeof Symbol("id") // "symbol"
typeof Math // "object"  (1)
typeof null // "object"  (2)
```

1. `Math` is a **built-in object** that provides mathematical operations. 
2. The result of `typeof null` is `"object"`. That’s an officially recognized error in `typeof`



## Interaction: alert, prompt, confirm

### alert

`alert("Hello");`

### prompt

The function `prompt` accepts two arguments:

```js
let result = prompt(title, [default]);
```

If input nothing, it returns a empty string`''`

If cancel modal, it returns a `null`

### confirm

```js
let result = confirm(question);
```

Modal: OK(`true`) and Cancel(`false`)



## Type Conversions
### String Conversion

```js
value = String(true); //'true'
```

A `false` becomes `"false"`, `null` becomes `"null"`



### Numeric Conversion

Numeric conversion rules:

| Value            | Becomes…                                                     |
| :--------------- | :----------------------------------------------------------- |
| **`undefined`**  | **`NaN`**                                                    |
| **`null`**       | **`0`**                                                      |
| `true and false` | `1` and `0`                                                  |
| `string`         | **If the remaining string is empty, the result is `0`. <br />An error gives `NaN`.** |

```js
alert( "6" / "2" ); // 3
alert( Number("   123   ") ); // 123
Number("123n")    // NaN 
alert( Number(true) );        // 1
alert( Number(false) );       // 0
```



### Boolean Conversion

```js
alert( Boolean(1) ); // true
alert( Boolean(0) ); // false
alert( Boolean("hello") ); // true
alert( Boolean("") ); // false
alert(Boolean(NaN)); //false 
```

**:star:Note**: 

```js
alert( Boolean("0") ); // true
alert( Boolean(" ") ); // spaces, also true
```



## Basic operators, maths

### Exponentiation **

```js
alert( 2 ** 2 ); // 2² = 4
alert( 4 ** (1/2) ); // 2 (power of 1/2 is the same as a square root)
alert( 8 ** (1/3) ); // 2 (power of 1/3 is the same as a cubic root)
```



### String concatenation

```js
alert( 6 - '2' ); // 4, converts '2' to a number
alert( '6' / '2' ); // 3, converts both operands to numbers
```

:star: The plus operator `+` applied to a single value, doesn’t do anything to numbers. But if the operand is not a number, the unary plus converts it into a number. It actually does the same thing as `Number(...)`, but is shorter.

```js
let operator = -1;
alert(+operator); //-1

alert(+ture); //1
alert(+""); //0


let a = "2";
let b = "3";
alert( +a + +b); //5
```

:star: Increment/decrement can only be applied to variables. Trying to use it on a value like **`5++` will give an error.**



## Comparison

### strict comparison is a must

```js
alert( null > 0 );  // (1) false
alert( null == 0 ); // (2) false
alert( null >= 0 ); // (3) true
```

The reason is that an equality check `==` and comparisons `> < >= <=` work differently. 

The equality check `==` for `undefined` and `null` is defined such that, without any conversions, they equal each other and don’t equal anything else. That’s why (2) `null == 0` is false.

### An incomparable undefined

The value `undefined` shouldn’t be compared to other values:

```js
alert( undefined > 0 ); // false (1)
alert( undefined < 0 ); // false (2)
alert( undefined == 0 ); // false (3)
```

### Summary

```js
undefined == undefined; //true
null == null; //true
NaN == NaN; //false
```



## Conditional branch: ?

### Multiple ‘?’

```js
let message = (age < 3) ? 'Hi, baby!' :
  						(age < 18) ? 'Hello!' :
						  (age < 100) ? 'Greetings!' : 'What an unusual age!';
```



## Logical operators

There are four logical operators in JavaScript: `||` (OR), `&&` (AND), `!` (NOT), `??` (Nullish Coalescing).

### || OR

:star: In classical programming, the logical OR is meant to manipulate boolean values only. In JavaScript, the operator is a little bit trickier and more powerful.

- **If an operand is not a boolean, it’s converted to a boolean for the evaluation.**
-  **If the result is `true`, stops and returns the original value of that operand.**
- **If all operands have been evaluated (i.e. all were `false`), returns the last operand.**

```js
let result = "A" || ""; //A
alert( null || 0 || 1 ); // 1 
alert( undefined || null || 0 ); // 0 
```

This leads to an interesting usage:

```js
let firstName = "";
let lastName = "";
let nickName = "SuperCoder";

alert( firstName || lastName || nickName || "Anonymous");//SuperCoder
```



### && AND

The AND `&&` operator does the following:

- For each operand, converts it to a boolean. If the result is `false`, stops and returns the original value of that operand.
- If all operands have been evaluated (i.e. all were truthy), returns the last operand.

```js
alert( 1 && 2 && null && 3 ); // null
```



### ! NOT

1. Converts the operand to boolean type: `true/false`.
2. Returns the inverse value.

```js
let result  = !""; //true

A double NOT !! is sometimes used for converting a value to boolean type:

alert( !!"non-empty string" ); // true

+"A"; //NaN
!NaN; //true
!null; //trueF
```



### ??

`??` returns the first argument if it’s not `null/undefined`. Otherwise, the second one.

The common use case for `??` is to provide a default value.

```js
let user;
alert(user ?? "Anonymous"); // Anonymous (user is undefined)

let firstName = null;
let lastName = null;
let nickName = "Supercoder";
alert(firstName ?? lastName ?? nickName ?? "Anonymous"); // Supercoder
```

#### Comparison with `||`

The important difference between them is that:

- `||` returns the first *truthy* value.
- `??` returns the first *defined* value.

In other words, `||` doesn’t distinguish between `false`, `0`, an empty string `""` and `null/undefined`. They are all the same – falsy values. If any of these is the first argument of `||`, then we’ll get the second argument as the result.

```js
let height = 0;

alert(height || 100); // 100
alert(height ?? 100); // 0
```



## Loops: while and for

### Skipping parts

Any part of `for` can be skipped.

```js
let i = 0; // we have i already declared and assigned

for (; i < 3; i++) { // no need for "begin"
  alert( i ); // 0, 1, 2
}
```

### Breaking the loop

:star: The combination **"infinite loop + `break` as needed "** is great for situations when a loop's condition must be checked not in the beginning or end of the loop, but in the middle or even in several places of its body.

### Labels for break/continue

Sometimes we need to break out from multiple nested loops at once.

```js
outer: for (let i = 0; i < 3; i++) {
  for (let j = 0; j < 3; j++) {
	...
    if (!input) break outer; // (*)
  }
}
```



## The "switch" statement

- The value is checked for a **strict equality** to the value.
- If the equality is found, `switch` starts to execute the code starting from the corresponding `case`, until the nearest `break` (or until the end of `switch`).

### Grouping of "case"

Several variants of `case` which share the same code can be grouped.

The ability to “group” cases is a side effect of how `switch/case` works without `break`.

```js
let a = 3;

switch (a) {
  case 4:
    alert('Right!');
    break;
        
  case 3: // (*) grouped two cases
  case 5:
    alert('Wrong!');
    alert("Why don't you take a math class?");
    break;

  default:
    alert('The result is strange. Really.');
}
```



## Functions Basics

### Default values

If a function is called, but an argument is not provided, then the corresponding value becomes `undefined`.

```js
function showMessage(from, text = "no text given") {
  alert( from + ": " + text );
}
```

The default value also jumps in if the parameter exists, but strictly equals `undefined`, like this:

```js
showMessage("Ann", undefined); 
```

### Return values

**A function with an empty** `return` **or without it returns** `undefined`

```js
function doNothing() { /* empty */ }
function doNothing() {
  return;
}

alert( doNothing() === undefined ); // true
```

## Function expressions

### Function Expression vs Function Declaration

```js
// Function Declaration
function sum(a, b) {
  return a + b;
}

// Function Expression
let sum = function(a, b) {
  return a + b;
};
```

The more subtle difference is *when* a function is created by the JavaScript engine.

- **A Function Expression is created when the execution reaches it and is usable only from that moment.**

  **A Function Declaration can be called earlier than it is defined.**

  For example, a global Function Declaration is visible in the whole script, no matter where it is.

  That’s due to internal algorithms. When JavaScript prepares to run the script, it first looks for global Function Declarations in it and creates the functions. We can think of it as an “initialization stage”.

**In strict mode, when a Function Declaration is within a code block, it’s visible everywhere inside that block. But not outside of it.**

```js
let age = prompt("What is your age?", 18);

// conditionally declare a function
if (age < 18) {
  function welcome() {
    alert("Hello!");
  }
} else {
  function welcome() {
    alert("Greetings!");
  }
}

// ...use it later
welcome(); // Error: welcome is not defined
```

### Function is a value

```js
let sayHi = function() { // (1) create
  alert( "Hello" );
};

let func = sayHi;
func();
```

### Callback functions

Let’s look at more examples of passing functions as values and using function expressions.

The idea is that we pass a function and expect it to be “called back” later if necessary.

```js
function ask(question, yes, no) {
  if (confirm(question)) yes()
  else no();
}

ask(
  "Do you agree?",
  function() { alert("You agreed."); },
  function() { alert("You canceled the execution."); }
);
```



## Arrow functions, the basics

```js
let age = prompt("What is your age?", 18);

let welcome = (age < 18) ?
  () => alert('Hello!') :
  () => alert("Greetings!");

welcome();
```



# Objects: the basics

## Objects

```js
let user = {
    name : "John",
    age : 30, //this comma is called hanging comma, make it eaiser to add/remove properties.
}

user.isPerson = true; //addd an attrbute
delete user.isPerson; //delete an attribute
```

### Square brackets

```js
let user = {};
user["likes birds"] = true;
```

Square brackets also provide a way to obtain the property name as the result of any expression

```js
let key = "likes birds";
user[key] = true;
```

### Property name limitations

There are no limitations on property names. They can be any **strings** or **symbols**. <u>Other types are automatically converted to strings.</u>

```js
let obj = {
  0: "test" // same as "0": "test"
};

alert( obj["0"] ); // test
alert( obj[0] ); // test 
```

### Property existence test, "in" operator

The syntax is:

```js
"key" in object
```

For instance:

```js
let user = {
    name: "Tad",
    age: 30
}

console.log("age" in user)// true
```

### The "for...in" loop

```js
for (let key in object) {
  // executes the body for each key among object properties
}
```

For instance:

```js
let user = {
  name: "John",
  age: 30,
  isAdmin: true
};

for (let key in user) {
  alert( key );  // name, age, isAdmin
  alert( user[key] ); // John, 30, true
}
```



## Object references and copying

**A variable assigned to an object stores not the object itself, but its “address in memory” – in other words “a reference” to it.**

We may think of an object variable, such as `user`, like a sheet of paper with the address of the object on it.

**When an object variable is copied, the reference is copied, but the object itself is not duplicated.**



We can use either variable to access the object and modify its contents:!

```js
let user = { name: 'John' };
let admin = user;

admin.name = 'Pete'; 
alert(user.name); // 'Pete'
```

### Comparison by reference

Two objects are equal only if they are the same object.

```js
let a = {};
let b = a; // copy the reference

alert( a == b ); // true
alert( a === b ); // true
```

And here two independent objects are not equal, even though they look alike (both are empty):

```js
let a = {};
let b = {}; // two independent objects

alert( a == b ); // false
```

:star:: **Const objects can be modified**

An important side effect of storing objects as references is that an object declared as `const` *can* be modified.

```js
const user = {
  name: "John"
};
user.name = "Pete"; // (*)

alert(user.name); // Pete
```



### Cloning and merging, `Object.assign`

🚀What if we need to duplicate an object?

We can create a new object and replicate the structure of the existing one, by iterating over its properties and copying them on the primitive level.

- If the copied property name already exists, it gets overwritten

```js
let user = {
    name1:"Tad",
    age1:23,
};
let user2 = {
    name2:"Tad",
    age2:23,
};
let person = Object.assign({},user,user2); //person { name1: 'Tad', age1: 23, name2: 'Tad', age2: 23 }

Object.assign(user,user2);  //In this way, Copy user2 to user is also OK. 
```

There is an another way to clone an object.

```js
let cloneUser = {...user};
```

### Nested cloning

 There are two way:

`structuredClone()`: support nest clone but doesn't support copy function properties

`{...object}`: support nested clone and function 



## Object methods, "this"

### "this"  is not bound

The value of `this` is evaluated during the run-time, depending on the context.

```js
function sayHi() {
  alert( this.name );
}
```

### Arrow function have no "this"

Arrow functions are special: they don’t have their “own” `this`. If we reference `this` from such a function, it’s taken from the outer “normal” function.

```js
let user = {
  firstName: "Ilya",
  sayHi() {
    let arrow = () => alert(this.firstName);
    arrow();
  }
};

user.sayHi(); // Ilya
```



## Constructor, operator "new"

### Construction function

- They are named with **capital letter first**.
- They should be executed only with `new` operator 

```js
function User(name){
    // this = {};  (implicitly)
    this.name = name;
    this.isAdmin = false;
    
    this.sayHi = function(){
        console.log("Hi:"+this.name);
    }
    //return this; (implicitly)
}

let user = new User("Tad");
user.sayHi(); //Hi:Tad
```



## Optional chain '?.'

The optional chain  `?.` is a safe way to access nested object properties, even if an intermediate property doesn’t exist.

### The "non-existing property" problem

As an example, let’s say we have `user` objects that hold the information about our users. Most of our users have addresses in `user.address` property, with the street `user.address.street`, but some did not provide them.

In such case, when we attempt to get `user.address.street`, and the user happens to be without an address, we get an error:

```js
let user = {}; // a user without "address" property
alert(user.address.street); // Error!
```

As `user.address` is `undefined`, an attempt to get `user.address.street` fails with an error.

In many practical cases we’d prefer to get `undefined` instead of an error here (meaning “no street”).

But use this way to avoid error is ugly.

```js
let user = {}; // user has no address

alert( user.address && user.address.street && user.address.street.name ); // undefined (no error)
```



### Optional chaining

The optional chaining `?.` stops the evaluation if the value before `?.` is `undefined` or `null` and returns `undefined`.

`value?.prop`, if `value` exists, work as `value.prop`. Otherwise, when `value` is `null` or `undefined`, it returns `undefined`.

Here is the safe way to access `user.address.street` using `?.`

```js
let user = {}
console.log(user?.address?.street); //undefined(no error)

let html = document.querySelector('.elem')?.innerHTML; // will be undefined, if there's no element
```

### Other variants: `?.()`,`?.[]`

`?.()` is used to call a function that may not exist.

```js
user.admin?.();
```

`?.[]` If we’d like to use brackets `[]` to access properties instead of dot `.`. Similar to previous cases, it allows to safely read a property from an object that may not exist.

:star:**We can use** `?.` **for safe reading and deleting, but not writing**

```js
user?.name = "John"; // Error, doesn't work
// because it evaluates to: undefined = "John"
```

### Summary

The optional chaining `?.` syntax has three forms:

1. `obj?.prop` – returns `obj.prop` if `obj` exists, otherwise `undefined`.
2. `obj?.[prop]` – returns `obj[prop]` if `obj` exists, otherwise `undefined`.
3. `obj.method?.()` – calls `obj.method()` if `obj.method` exists, **otherwise returns `undefined`.**



## Symbol type

By specification, only two primitive types may serve as object property keys:

- string type
- symbol type

Otherwise, if one uses another type, such as number, it’s auto-converted to string. So that `obj[1]` is the same as `obj["1"]`, and `obj[true]` is the same as `obj["true"]`.

Until now we’ve been using only strings.

Now let’s explore symbols, see what they can do for us.

### Symbols

To summarize, a symbol is a “primitive unique value” with an optional description

Upon creation, we can give symbols a description (also called a symbol name), mostly useful for debugging purposes:

```js
let id = Symbol("id");
```

:star:**Symbols are guaranteed to be unique.**

 Even if we create many symbols with exactly the same description, they are different values. The description is just a label that doesn’t affect anything.

```js
let id_1 = Symbol("id");
let id_2 = Symbol("id");
console.log(id_1 === id_2);//false
```

### "Hidden" properties

Symbols allow us to create “hidden” properties of an object, that no other part of code can accidentally access or overwrite.

if we’re working with `user` objects, that belong to a third-party code. We’d like to add identifiers to them.

Let’s use a symbol key for it:

```js
let user = { // belongs to another code
  name: "John"
};

let id = Symbol("id");
user[id] = 1;
```

### Symbols in an object literal

If we wanna use a symbol in an object literal `{…}`, we need square brackets around it.

```js
let id = Symbol("id");

let user = {
  name: "John",
  [id]: 123 // not "id": 123
};
```

:fire:Symbolic properties do not participate in `for..in` loop.

[Object.keys(user)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys) also ignores them. That’s a part of the general “hiding symbolic properties” principle. 

In contrast, [Object.assign](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign) copies both string and symbol properties

### Global symbols

As we’ve seen, usually all symbols are different, even if they have the same name. But sometimes we want same-named symbols to be same entities. For instance, different parts of our application want to access symbol `"id"` meaning exactly the same property.

To achieve that, there exists a ***global symbol registry***. We can create symbols in it and access them later, and it guarantees that repeated accesses by the same name return exactly the same symbol.

#### `Symbol.for()`

In order to read (create if absent) a symbol from the registry, use `Symbol.for(key)`.

```js
// read from the global registry
let id = Symbol.for("id"); // if the symbol did not exist, it is created

// read it again (maybe from another part of the code)
let idAgain = Symbol.for("id");

// the same symbol
alert( id === idAgain ); // true
```

#### `Symbol.keyFor()`

`Symbol.for(key)` returns a symbol by name. To do the opposite – return a name by global symbol – we can use: `Symbol.keyFor(sym)`:

```js
// get symbol by name
let sym = Symbol.for("name");
let sym2 = Symbol.for("id");

// get name by symbol
alert( Symbol.keyFor(sym) ); // name
alert( Symbol.keyFor(sym2) ); // id
```



# Data types

```js
let john = {
  name: "John",
  sayHi: function() {
    alert("Hi buddy!");
  }
};
```

```js
let num = Number("123");
```



## Numbers

### More way to write a number

```js
let billion = 1000000000;
let billion = 1_000_000_000;
let billion = 1e9;
let mcs = 1e-6;
```

### Common API

**`Math.floor()`**

**`Math.ceil()`**

**`Math.round()`**

**`Math.trunc()`**

**`toFixed()`**

**`isFinite()`**

**`isNaN()`**

**`Object.is()`** is the same as `===`

**`parseInt()`**

**`parseFloat()`**

## Strings

### Quote

The advantage of using backticks is that they allow a string to span multiple lines:

```js
let guestList = `Guests:
 * John
 * Pete
 * Mary
`;

console.log(`${sum(1,2)}`);
```

### Accessing characters

```js
let str = `Hello`;

// the first character
alert( str[0] ); // H
alert( str.at(0) ); // H

// the last character
alert( str[str.length - 1] ); // o
alert( str.at(-1) );
```

As you can see, the `.at(pos)` method has a benefit of allowing negative position. If `pos` is negative, then it's counted from the end of the string.

So `.at(-1)` means the last character, and `.at(-2)` is the one before it, etc.

The square brackets always return `undefined` for negative indexes, for instance:

```js
let str = `Hello`;

alert( str[-2] ); // undefined
alert( str.at(-2) ); // l
```

we can also iterate over characters using `for...of`:

```js
for (let char of "Hello"){
  alter(char);
}
```

### `str.indexOf(substr, pos)`

It returns the index of the target, or returns `-1` if nothing can be found.The `pos` is optional that allows us to start searching from a given position.

But there is a trick. There is a slight inconvenience with `indexOf` in the `if` test. Because if the `target` at the position 0, it returns `0` as a result. But `0` means `false` in the `if` test.

Best practice: `str.indexOf("target") !== -1)`

```js
 let str = "Widget with id";

 if (str.indexOf("Widget") !== -1) {
  console.log("We found it"); // works now!
 }
```

`substring()`

`substr(start [,length])`

In contrast with previous methods, this one allows us to specify the `length` instead of the ending position

`slice()`

```js
let str = "stringify";
alert( str.slice(0, 5) ); // 'strin'
alert( str.slice(0, 5) ); // 'strin'
```

`codePointAt(pos)`

`fromCodePoint(code)`

```js
alert( "Z".codePointAt(0) ); // 90
alert( String.fromCodePoint(90) ); // Z
```

`str1.localeCompare(str2)`

```js
let result = "A".localeCompare("A"); //0
```

## Arrays

### Get last element with "at"

There are two ways to get last element in a array.

```js
array = [1,2,3];
let lastElement = array[array.length - 1];
let lastElement2 = array.at(-1);
```

### Method pop/push, shift/unshift

`queue`: `shift` + `push`

`stack`: `pop` + `push`

### Performance

Methods `push/pop` run fast, while `shift/unshift` are slow.

### Loops `for ...of`

```js
 let array = [1,2,3];
 for (let element of array){
  console.log("🚀Test:" + element);
 }
```

### Length

Another interesting thing about the `length` property is that it’s writable.

If we increase it manually, nothing interesting happens. But if we decrease it, the array is truncated. The process is irreversible, here’s the example:

```js
let arr = [1, 2, 3, 4, 5];

arr.length = 2; // truncate to 2 elements
alert( arr ); // [1, 2]

arr.length = 5; // return length back
alert( arr[3] ); // undefined: the values do not return
```

### Methods

`splice()`

```js
arr.splice(start[, deleteCount, elem1, ..., elemN])
```

```js
let arr = ["I", "study", "JavaScript", "right", "now"];

// remove 3 first elements and replace them with another
arr.splice(0, 3, "Let's", "dance");
alert( arr ) // now ["Let's", "dance", "right", "now"]
```

🚀 The splice method is also able to insert the elements without any removals. For that we need to set `deleteCount` to `0`

```js
let arr = ["I", "study", "JavaScript"];
arr.splice(2,0,"complex","language");
alert(arr); //// "I", "study", "complex", "language", "JavaScript"
```

`slice()`

```js
arr.slice([start],[end]);
```

It returns a new array copying to it all items from index `start` to `end`. (not including `end`). 

```js
let arr = ["t", "e", "s", "t"];
alert( arr.slice(1, 3) ); // e,s (copy from 1 to 3)
```

`concat()`

The method `arr.concat()` creates a new array that includes values from other arrays and additional items.

```js
arr.concat(arg1, arg2...)
```

**It accepts any number of arguments – either arrays or values.**

```js
let arr = [1, 2];

alert( arr.concat([3, 4]) ); // 1,2,3,4
alert( arr.concat([3, 4], [5, 6]) ); // 1,2,3,4,5,6
alert( arr.concat([3, 4], 5, 6) ); // 1,2,3,4,5,6
```

### Iterate: forEach

The `arr.forEach` method allows to run a function for every element of the array. 

```js
arr.forEach(function(item, index, array) {
  // ... do something with item
});
```

### Searching in  array

`indexOf()`

`includes()`

:star: **The** `includes` **method handles** `NaN` **correctly**

A minor, but noteworthy feature of `includes` is that it correctly handles `NaN`, unlike `indexOf`:

```js
const arr = [NaN];
alert( arr.indexOf(NaN) ); // -1 (wrong, should be 0)
alert( arr.includes(NaN) );// true (correct)
```

`find()`

`findIndex()`

`findLastIndex()`

```js
let result = arr.find(function(item, index, array) {
  // if true is returned, item is returned and iteration is stopped
  // for falsy scenario returns undefined
});
```

```js
let users = [
  {id: 1, name: "John"},
  {id: 2, name: "Pete"},
  {id: 3, name: "Mary"}
];

let user = users.find(item => item.id === 1);

alert(user.name); // John
```

`filter()`

The syntax is similar to `find`, but `filter` returns an array of all matching elements:

```js
let results = arr.filter(function(item, index, array) {
  // if true item is pushed to results and the iteration continues
  // returns empty array if nothing found
});
```

### Transform an array

`map`

It calls the function for each element of the array and returns the array of results.

```js
let lengths = ["Bilbo", "Gandalf", "Nazgul"].map(item => item.length);
alert(lengths); // 5,7,6
```

`sort`

It also returns the sorted array, but the returned value is usually ignored, as `arr` itself is modified.

```js
function compareNumeric(a, b) {
  if (a > b) return 1;
  if (a == b) return 0;
  if (a < b) return -1;
}

let arr = [ 1, 2, 15 ];

arr.sort(compareNumeric);
```

:star: Use `localeCompare` for strings

```js
let countries = ['Österreich', 'Andorra', 'Vietnam'];

alert( countries.sort( (a, b) => a.localeCompare(b) ) ); 
```

`reverse`

```js
let arr = [1, 2, 3, 4, 5];
arr.reverse();

alert( arr ); // 5,4,3,2,1
```

`split and join`

The `split` method has an optional second numeric argument – a limit on the array length. If it is provided, then the extra elements are ignored. In practice it is rarely used though:

```js
let arr = 'Bilbo, Gandalf, Nazgul, Saruman'.split(', ', 2);

alert(arr); // Bilbo, Gandalf
```

:star: **Split into letters**

The call to `split(s)` with an empty `s` would split the string into an array of letters:

```js
let str = "test";

alert( str.split('') ); // t,e,s,t
```

The call `arr.join(glue)` does the reverse to split. It creates a string of `arr` items joined by glue between them.

```js
let arr = ['Bilbo', 'Gandalf', 'Nazgul'];

let str = arr.join(';'); 
alert( str ); // Bilbo;Gandalf;Nazgul
```

`reduce/reduceRight`

They are used to calculate a single value based on the array.

The syntax is:

```js
let value = arr.reduce(fuction(accumaltor,item,index,array){},[initial]);
```

- `accumulator` – is the result of the previous function call, equals `initial` the first time (if `initial` is provided).



`Array.isArray()`

`arr.fill(value, start, end)` – fills the array with repeating value from index start to end.

`arr.flat(depth)` create a new flat array from a multidimensional array.

### Summary

A cheat sheet of array methods:

- To add/remove elements:
  - `push(...items)` – adds items to the end,
  - `pop()` – extracts an item from the end,
  - `shift()` – extracts an item from the beginning,
  - `unshift(...items)` – adds items to the beginning.
  - `splice(pos, deleteCount, ...items)` – at index `pos` deletes `deleteCount` elements and inserts `items`.
  - `slice(start, end)` – creates a new array, copies elements from index `start` till `end` (not inclusive) into it.
  - `concat(...items)` – returns a new array: copies all members of the current one and adds `items` to it. If any of `items` is an array, then its elements are taken.
- To search among elements:
  - `indexOf/lastIndexOf(item, pos)` – look for `item` starting from position `pos`, return the index or `-1` if not found.
  - `includes(value)` – returns `true` if the array has `value`, otherwise `false`.
  - `find/filter(func)` – filter elements through the function, return first/all values that make it return `true`.
  - `findIndex` is like `find`, but returns the index instead of a value.
- To iterate over elements:
  - `forEach(func)` – calls `func` for every element, does not return anything.
- To transform the array:
  - `map(func)` – creates a new array from results of calling `func` for every element.
  - `sort(func)` – sorts the array in-place, then returns it.
  - `reverse()` – reverses the array in-place, then returns it.
  - `split/join` – convert a string to array and back.
  - `reduce/reduceRight(func, initial)` – calculate a single value over the array by calling `func` for each element and passing an intermediate result between the calls.
- Additionally:
  - `Array.isArray(value)` checks `value` for being an array, if so returns `true`, otherwise `false`.

Please note that methods `sort`, `reverse` and `splice` modify the array itself.

## Map and Set

- `new Map() `
- map.set(key, value) 
- map.get(key) 
- map.has(key) 
- map.delete(key)
- map.clear() 
- map.size()
- map.keys()
- map.values()
- map.entries()

### Object.entries: Map from Object

```js
let obj = {
 name: "John",
 age: 30
};

let map = new Map(Object.entries(obj));

console.log("🚀Test:" + map.get("name"));//John
```

### Object.fromEntries: Object from Map

```js
let map = new Map();
map.set('banana', 1);
map.set('orange', 2);
map.set('meat', 4);

let obj = Object.fromEntries(map); // make a plain object (*)

console.log("🚀Test:" + obj.banana);
```

### Iteration over set

We can loop over a set either with `for..of` or using `forEach`



## WeakMap and WeakSet

[`WeakMap`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakMap) is `Map`-like collection that allows only objects as keys and removes them together with associated value once they become inaccessible by other means.

[`WeakSet`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakSet) is `Set`-like collection that stores only objects and removes them once they become inaccessible by other means.

Their main advantages are that they have weak reference to objects, so they can easily be removed by garbage collector.

:star: Each of them only support `set`, `get`, `has` and `delete`, but not `size`, `keys()` and no iterations.

### WeakMap

Its main usage is to be cache.

```js
// 📁 cache.js
let cache = new WeakMap();

// calculate and remember the result
function process(obj) {
  if (!cache.has(obj)) {
    let result = /* calculate the result for */ obj;

    cache.set(obj, result);
    return result;
  }

  return cache.get(obj);
}

// 📁 main.js
let obj = {/* some object */};

let result1 = process(obj);
let result2 = process(obj);

// ...later, when the object is not needed any more:
obj = null;

// Can't get cache.size, as it's a WeakMap,
// but it's 0 or soon be 0
// When obj gets garbage collected, cached data will be removed as well
```

### WeakSet

Being “weak”, it also serves as additional storage. But not for arbitrary data, rather for “yes/no” facts.

```js
let visitedSet = new WeakSet();

let john = { name: "John" };
let pete = { name: "Pete" };
let mary = { name: "Mary" };

visitedSet.add(john); // John visited us
visitedSet.add(pete); // Then Pete
visitedSet.add(john); // John again

// visitedSet has 2 users now

// check if John visited?
alert(visitedSet.has(john)); // true

// check if Mary visited?
alert(visitedSet.has(mary)); // false

john = null;

// visitedSet will be cleaned automatically
```



## Object.keys, values, entries

- Object.keys(obj) – returns an array of keys.
- Object.values(obj) – returns an array of values.
- Object.entries(obj) – returns an array of [key, value] pairs.

````js
let prices = {
  banana: 1,
  orange: 2,
  meat: 4,
};

let doublePrices = Object.fromEntries(
  // convert prices to array, map each key/value pair into another pair
  // and then fromEntries gives back the object
  Object.entries(prices).map(entry => [entry[0], entry[1] * 2])
);

alert(doublePrices.meat); // 8
````



## Destructuring assignment

*Destructuring assignment* is a special syntax that allows us to "unpack" arrays or objects into a bunch of variables, as sometimes that is more convenient

### Array destructuring

```js
let arr = ["John", "Smith"]

// sets firstName = arr[0]
// and surname = arr[1]
let [firstName, surname] = arr;
```

It looks great when combined with `split` or other array-returning methods:

```js
let [firstName, secondName] = "John Smith".split(' ');
```

#### **Ignore elements using commas**

Unwanted elements of the array can also be thrown away via an extra comma:

```js
// second element is not needed
let [firstName, , title] = ["Julius", "Caesar", "Consul", "of the Roman Republic"];

alert( title ); // Consul
```

#### **Works with any iterable on the right side.**

```js
let [a,b,c] = "abc";
let [one,two,three] = new Set([1,2,3]);
```

#### **Assign to anything at the left side**

```js
let user = {};
[user.name, user.surname] = "John Smith".split(' ');
```

#### **Looping with `.entries()`**

```js
let user = {
  name: "John",
  age: 30
};

// loop over keys-and-values
for (let [key, value] of Object.entries(user)) {
  alert(`${key}:${value}`); // name:John, then age:30
}
```

The similar code for a `Map` is simpler:

```js
let user = new Map();
user.set("name", "John");
user.set("age", "30");

// Map iterates as [key, value] pairs, very convenient for destructuring
for (let [key, value] of user) {
  alert(`${key}:${value}`); // name:John, then age:30
}
```

#### **Swap variables trick**

```js
let guest = "Jane";
let admin = "Pete";

// Let's swap the values: make guest=Pete, admin=Jane
[guest, admin] = [admin, guest];
```

#### **The rest `…`**

```js
let [name1, name2, ...rest] = ["Julius", "Caesar", "Consul", "of the Roman Republic"];

// rest is array of items, starting from the 3rd one
alert(rest[0]); // Consul
alert(rest[1]); // of the Roman Republic
alert(rest.length); // 2
```

#### **Default values**

```js
// default values
let [name = "Guest", surname = "Anonymous"] = ["Julius"];
```



### Object destructuring

The basic syntax is:

```js
let {var1, var2} = {var1:...,  var2:...}
```

**The order does not matter.**

```js
let options = {
  title: "Menu",
  width: 100,
  height: 200
};

let {title, width, height} = options;
```

If we want to assign a property to a variable with another name, for instance, make `options.width`  go into the variables named `w`, then we can set the variable name using a colon:

```js
let options = {
  title: "Menu",
  width: 100,
  height: 200
};

let {width: w = 100, height: h = 200, title} = options;
```

#### The rest pattern **`...`**

```js
let options = {
  title: "Menu",
  height: 200,
  width: 100
};

// title = property named title
// rest = object with the rest of properties
let {title, ...rest} = options;
```

:star:**Gotcha if there’s no** `let`

It's acceptable but there is a catch

```js
// error in this line
{title, width, height} = {title: "Menu", width: 200, height: 100};
```

Because JavaScript threat `{...}` as a code block. So we need to let it know, it's not a code block!

```js
//no error
({title, width, height} = {title: "Menu", width: 200, height: 100});
```



### Nested destructuring

```js
let options = {
  size: {
    width: 100,
    height: 200
  },
  items: ["Cake", "Donut"],
  extra: true
};

// destructuring assignment split in multiple lines for clarity
let {
  size: { // put size here
    width,
    height
  },
  items: [item1, item2], // assign items here
  title = "Menu" // not present in the object (default value is used)
} = options;

alert(title);  // Menu
alert(width);  // 100
alert(height); // 200
alert(item1);  // Cake
alert(item2);  // Donut
```

## JSON methods, toJSON

JSON is data-only language-independent specification, so some JavaScript-specific object properties are skipped by `JSON.stringify`.

Namely:

- Function properties (methods).
- Symbolic keys and values.
- Properties that store `undefined`.

```js
let user = {
  sayHi() { // ignored
    alert("Hello");
  },
  [Symbol("id")]: 123, // ignored
  something: undefined // ignored
};

alert( JSON.stringify(user) ); // {} (empty object)
```

### `JSON.parse`

The syntax:

```js
let value = JSON.parse(str, [reviver]);
```

**reviver**

Optional function(key,value) that will be called for each `(key, value)` pair and can transform the value.

Here are typical mistakes in hand-written JSON:

```js
let json = `{
  name: "John",                     // mistake: property name without quotes
  "surname": 'Smith',               // mistake: single quotes in value (must be double)
  'isAdmin': false                  // mistake: single quotes in key (must be double)
  "birthday": new Date(2000, 2, 3), // mistake: no "new" is allowed, only bare values
  "friends": [0,1,2,3]              // here all fine
}`;
```

#### Using reviver

Here is an example:

```js
let str = '{"title":"Conference","date":"2017-11-30T12:00:00.000Z"}';

let meetup = JSON.parse(str);

alert( meetup.date.getDate() ); // Error!
```

The value of `meetup.date` s a string, not a `Date` object!

So this is the solution:

```js
let str = '{"title":"Conference","date":"2017-11-30T12:00:00.000Z"}';

let meetup = JSON.parse(str, function(key, value) {
  if (key == 'date') return new Date(value);
  return value;
});

alert( meetup.date.getDate() ); // now works!
```



# Advanced function

## Recursion

Let's return to functions and learn them more in-depth. If you are not new to programming, then it is probably familiar

`Recursion` is a programming patten that is useful in situations when a task can be naturally split into several tasks of the same kind. Or when a task can be simplified into an easy action plus a simpler variable of the same task

### Two ways of thinking

```js
pow(2,2) = 4;
```

There are two ways to implement it

1. Iterative thinking: the `for` loop:

```js
fuction pow(x, n){
  let result = 1;
  for (let i = 0; i < n; i++){
    result = result*x
  }
  return result;
}
```

2. Recursive thinking: simplify the task and call itself:

```js
function pow(x, n){
  if (n == 1){
    return x;
  } else{
    return x * pow(x,n-1);
  }
}
```

Here we can rewrite the same using the conditional operator `?` instead of `to` to make `pow(x, n)` more terse and still very readable:

```js
function pow(x, n){
  return (n==1) ? x : (x* pow(x, n-1));
}
```



## Rest parameters and spread syntax

A function can be call with any number of arguments, no matter how it is defined

The rest of the parameters can be included in the function definition by using three dots `…` followed by the name of the array that will contain them. 

:star:Note: The rest parameters gather all remaining arguments, so the following does not make sense and causes an error. In a word, the `...rest` must always be last

### The "arguments" variables

There is also a special array-like object named `arguments` that contains all arguments by their index

The downside is that although `arguments` is both array-like and iterable, it's not an array. It does not support array methods

### Spread syntax

We've just seen how to get an array from the list of parameters

But sometimes we need to do exactly the reverse

```js
let arr1 = [1, -2, 3, 4];
let arr2 = [8, 3, -8, 1];

alert( Math.max(...arr1, ...arr2) ); // 8
```

The spread syntax internally use iterators to gather elements, the same way as `for…of` does

```js
let str = "Hello";
alert(Array.from(str));
```

### Copy an array/object

```js
let obj = { a: 1, b: 2, c: 3 };
let objCopy = { ...obj };
```

This way of copying an object is much shorter than `let objCopy = Object.assign({}, obj)` or for an array `let arrCopy = Object.assign([], arr)` so we prefer to use it whenever we can



## Variable scope, closure

JavaScript is a very function-oriented language. A function can be created at any moment, passed as an argument or another function, and then called from a totally different place of code later.

### Code blocks

If a variable is declared inside a code block `{...}`, it's only visible inside that block

We can use this to isolate a piece of code that does its own task, with variables that only belong to it/

```js
{
  let message = "Hello";
  console.log(message);
}
{
  let message = "World";
  console.log(message);
}
```

### Nested function

What's much more interesting, a nested function can be returned: either as a property of a new object or as a result by itself. It can then be used somewhere else, no matter where, it still has access to the same outer varibales

```js
function makeCounter(){
    let count = 0;
    return function(){
        return count++;
    };
}

let counter = makeCounter();
console.log(counter()); //0
console.log(counter()); //1
console.log(counter()); //2
```

### Lexical Environment

The in-depth technical explanation lies ahead!

#### variables

In JavaScript, every running function, code block, and the script as a whole have an internal associated object known as the *Lexical Environment*

The Lexical Environment object consists of parts:

- `Environment Record` - an object that stores all local variables as its properties and some other information like the value of `this`
- A reference to the outer lexical environment, the one associated with the outer code



When then script starts, the Lexical Environment is pre-populated with all declared variables

- Initially, they are in the "Uninitialized" state. That's a special internal state, it means engine knows about the variable, but it cannot be referenced until it has been declared with `let`. 
- Then `let` definition appears. There's no assignment yet, so its values is `undefined`

#### Function Declarations

A function is also a value, like a variable

The difference is that a Function Declaration immediately fully initialized

#### Inner and outer Lexical Environment

When the code wants to access a variable - the inner Lexical Environment is searched first, then the outer one, then the more outer one and so on until the global one

At the beginning of each `makeCounter()` call, a new Lexical Environment object is created, to store variables for this `makeCounter` run 

```js
let counter = makeCounter();
let counter2 = makeCounter();
console.log(counter()); //0
console.log(counter()); //1
console.log(counter2()); //0
console.log(counter2()); //1
```

#### Garbage collection

Usually , a Lexical Environment is removed from memory with all the variables after the function call finishes. That's because there are no references to it. 

However, if there's a nested function that is still reachable after the ned of a function, then it has `[[Environment]]` property that references the lexical environment. 



## The old "var"

- "var" has no block scope
- "var" tolerates redeclarations
- "var" variables can be declared below their use

### IIFE 

```js
(funcion(){
 ...
 })();
```

There is no reason to use it



## Global Object

In a browser, it is named `window`. For Node.js it is `global`



## Function object, NFE

As we already know, a function in JavaScript is a value, every value has a type. Function are objects.

### The "name" property

```js
funciton sayHi(){
  ...
}
sayHi.name; //sayHi
```

### The "length" property

There is another built-in property "length" that returns the number of function parameters

```js
function f1(a){}
function many(a, b, ...more) {}
console.log(f1.length); //1
console.log(many.length); //2
```

In the code below, the `ask` function accepts a `question` to ask and an arbitrary number of `handler` functions to call

Once a user provides their answer, the function calls the handlers. We can pass two kinds of handlers:

- A zero-argument function, which is called when the user gives a positive answer
- A function with arguments, which is called in either case and return returns an answer

```js
function ask(question, ...handlers){
  let isYes = confirm(question);
  
  for (let handler of handlers){
    if (handler.length == 0){
      if (isYes) handler();
    } else {
      handler(isYes);
    }
  }
}

ask("Question", ()=> alert("You said yes"), (result) => alert(result));
```

### Custom properties

We can also add properties of our own

```js
function sayHi(){
    console.log("Hi");
    sayHi.counter++;
}

sayHi.counter = 0;
```

:warning: A property is not a variable. 

Function properties can replace closures sometimes. 

```js
function makeCounter(){
    makeCounter.count = 0;
    return function(){
       makeCounter.count++; 
       console.log(makeCounter.count);
    }
}

let counter = makeCounter();
let counter2 = makeCounter();
counter();//1
counter();//2
counter2();//3
counter2();//4
```

The main difference is that if the value of `count` lives in an outer variable, then external code is unable to access it. Only nested functions may modify it. And if it's bound to a function, then such a thing is possible

```js
function makeCounter(){
    makeCounter.count = 0;
	...
}
makeCounter.count = 10;
```



### Named Function Expression

```js
let sayHi = function func(who) {
  if (who) {
    alert(`Hello, ${who}`);
  } else {
    func("Guest"); // Now all fine
  }
};
```



## The "new function" syntax

There's one more way to create a function, it's rarely used, but sometimes there is no alternative

```js
let func = new Function ([arg1, arg2, ...argN], functionBody);
```

Example:

```js
let sum = new Function('a', 'b', 'return a + b');
alert( sum(1, 2) ); // 3

let sayHi = new Function('alert("Hello")');
```

The major difference from other ways we’ve seen is that the function is created literally from a string, that is passed at run time.

 When a function is created using `new Function`, its `[[Environment]]` is set to reference not the current Lexical Environment, but the global one.



## Scheduling: setTimeout and setInterval

`setTimeout()`

`clearTimeout()`

`setInterval()`

`clearInterval()`

```js
function sayHi() {
  alert('Hello');
}

setTimeout(sayHi, 1000);

setTimeout(() => alert('Hello'), 1000);
```

### Nested setTimeout

This way the next call may be scheduled differently, depending on the result of the current one

```js
let delay = 5000;

let timerId = setTimeout(funcion request(){
  if (...){
  	delay *= 2;    
	}
  timerId = setTimeout(request, delay);
})
```

`setInterval(func, 1000)`

:arrow_double_down: 1s

`setInterval(func, 1000)`



`setTimeout(func, 1000)`

:arrow_double_down:execution time + 1s

`setTimeout(func, 1000)`

The nested `setTimeout` guarantees the fixed delay 



### Zero delay setTimeout

`setTimeout(func, 0)`

The scheduler will invoke it only after the currently executing script is complete



## Decorators and forwarding, call/apply

### Using "func.call" for the context

`func.call(context, arg1,arg2, …)`

```js
function sayHi(){
  console.log(this.name);
}

let user = {name: "1"};
let admin = {name: "2"};

sayHi.call(user);
sayHi.call(admin);
```

### Using "func.apply" for the context

`func.apply(context, args)`



### Borrowing a method

```js
function hash() {
  alert( [].join.call(arguments) ); // 1,2
}

hash(1, 2);
```



## Function binding

### Losing "this"

```js
let user = {
  firstName: "John",
  sayHi() {
    alert(`Hello, ${this.firstName}!`);
  }
};

setTimeout(user.sayHi, 1000); // Hello, undefined!
```

That's because `setTimeout` got the function `user.sayHi` separately from the object

And the method `setTimeout()` in browser is a little special: it sets `this = window` for the function call

### Solution 1: a wrapper

```js
let user = {
  firstName: "John",
  sayHi() {
    alert(`Hello, ${this.firstName}!`);
  }
};

setTimeout(function() {
  user.sayHi(); // Hello, John!
}, 1000);
```

It receives `user` from the outer lexical environment, and then calls the method normally

It can be shorter:`setTimeout(() => user.sayHi(), 1000); `

But a slight vulnerability appears in our code structure:

What if before `setTimeout` triggers, `user` changes value? Then, it will call the wrong object!

```js
let user = {
  firstName: "John",
  sayHi() {
    alert(`Hello, ${this.firstName}!`);
  }
};

setTimeout(() => user.sayHi(), 1000);

// ...the value of user changes within 1 second
user = {
  sayHi() { alert("Another user in setTimeout!"); }
};
```



### Solution 2: bind

```js
let user = {
  firstName: "John",
  sayHi() {
    alert(`Hello, ${this.firstName}!`);
  }
};

let sayHi = user.sayHi.bind(user); 

sayHi(); // Hello, John!
setTimeout(sayHi, 1000); // Hello, John!

// even if the value of user changes within 1 second
// sayHi uses the pre-bound value which is reference to the old user object
user = {
  sayHi() { alert("Another user in setTimeout!"); }
};
```

## Arrow functions revisited

### Arrow functions have no "this"

```js
let group = {
  title: "Our Group",
  students: ["John", "Pete", "Alice"],

  showList() {
    this.students.forEach(function(student) {
      // Error: Cannot read property 'title' of undefined
      alert(this.title + ': ' + student);
    });
  }
};

group.showList();
```

The error occurs because `forEach` runs functions with `this = undefined` by default

### Arrows have no "arguments"

That's great for decorators, when we need to forward a call with the current `this` and `arguments`

```js
function defer(f, ms) {
  return function() {
    setTimeout(() => f.apply(this, arguments), ms);
  };
}

function sayHi(who) {
  alert('Hello, ' + who);
}

let sayHiDeferred = defer(sayHi, 2000);
sayHiDeferred("John"); // Hello, John after 2 seconds
```

# Object properties configuration

## Property flags

Object properties, besides a `value`, have three special attributes

- `writable` — if the value can be changed
- `enumrable` — if can be listed in loops
- `configurable`  — if can be deleted and these three attributes can be modified



`Object.getOwnPropertyDescriptor(obj, propertyName)`

`Object.defineProperty(obj, propertyName, descriptor)`

`Object.getOwnPropertyDescriptors(obj)`

`Object.defineProperties(obj, descriptors)`

`let clone = Object.defineProperties({}, Object.getOwnPropertyDescriptors(obj));`

```js
let user ={
    name: "Tad",
    age: 24
}

console.log(Object.getOwnPropertyDescriptors(user));

/*
{
  name: {
    value: 'Tad',
    writable: true,
    enumerable: true,
    configurable: true
  },
  age: { 
    value: 24, 
	writable: true, 
    enumerable: true,
    configurable: true 
    }
}
*/
```

## Property getters and setters

There are two kinds of object properties

- data properties
- accessor properties

```js
let user = {
  name: "John",
  surname: "Smith",

  get fullName() {
    return `${this.name} ${this.surname}`;
  }
};

alert(user.fullName); // John Smith
```

### Accessor descriptors

Descriptors for accessor properties are different from those for data properties

There is **no** `value` and `writable`, but instead there are `get` and `set` functions

That is, an accessor descriptor may have:

- `get`
- `set`
- `configurable`
- `enumerable`

For instance

```js
let user = {
    name: "",
    surname: ""
};

Object.defineProperty(user, "fullName", {
    get() {
        return `${this.name} ${this.surname}`
    },
    set(value) {
        [this.name, this.surname] = value.split(" ");
    }
});

user.fullName = "Tad BingYuLi";
console.log("🚀Test:" + user.fullName);//Tad BingYuLi
```

### Smarter getters/setters

```js
let user = {
  get name() {
    return this._name;
  },

  set name(value) {
    if (value.length < 4) {
      alert("Name is too short, need at least 4 characters");
      return;
    }
    this._name = value;
  }
};
```



# Prototypes, inheritance

## Prototypal inheritance

### [[Prototype]]

In JavaScript, objects have a special hidden property `[[Prototype]]`, this is either `null` or references another object. That object is called "a prototype"

The property `[[Prototype]]` is internal and hidden, but there are many ways to set it

One of them is use the special name `_proto_`

:notebook_with_decorative_cover:`_proto_` is a histrorical getter/setter for `[[Prototype]]`

The modern way is `Object.getPrototypeOf()`/`Object.setPrototypeOf()`

There is a built-in method:`obj.hasOwnProperty(key)`: It returns `true` if `obj` has its own property named `key`



:notebook:Almost all other key/value-getting methods ignores inherited properties, such as `Object.keys()`. `Object.values()`, and so on ignore inherited properties

## F.prototype

Remember, new objects can be created with a constructor function, like `new F()`

If `F.prototype` is an object, then the `new` operator uses it to set `[[Prototype]]` for the new object

Here is the example:

```js
let animal = {
  eats: true
};

function Rabbit(name) {
  this.name = name;
}

Rabbit.prototype = animal;
let rabbit = new Rabbit("White Rabbit"); //  rabbit.__proto__ == animal
alert( rabbit.eats ); // true
```

:warning:`F.prototype` only used at `new F()` time

`F.prototype` property is only used when `new F` is called, is assigns `[[Prototype]]` of the new object

### Default F.prototype, constructor property

Every function has the `prototype` property even if we don't supply it.

The default "prototype"  is an object with the only property `constructor` that points back to the function itself

Like this:

```js
function Rabbit() {}
//Rabbit.prototype = {constructor: Rabbit};
```

We can check it:

```js
function Rabbit() {}
console.log(Rabbit.prototype.constructor === Rabbit);//true
```

```js
function Rabbit() {}

let rabbit = new Rabbit();
console.log(rabbit.constructor == Rabbit);//true
```

![Screenshot 2023-04-02 at 13.56.02](/Users/xiaobing/Desktop/learning/File/JS/:Users:xiaobing:Library:Application Support:typora-user-images:Screenshot 2023-04-02 at 13.56.02.png)

What you should know:

- 1. Every function has a property called "prototype"
  2. Every new object's `[[Prototype]]` inherit directly  from "F.prototype"
  3. The constructor of the "F.prototype" is the function itself

```js
let rabbit = new Rabbit("White Rabbit");
let rabbit2 = new rabbit.constructor("Black Rabbit");
```



## Native prototypes

The "prototype" is widely used by the core of JavaScript itself. All built-in constructor functions use it



### Object.prototype

```js
let obj = {};
console.log(obj); //[object object]
```

The short notation `obj = {}` is the same as `obj = new Object()`, where `Object` is a built-in object constructor function, with its own `prototype` referencing a huge object with `toString()`  and other methods

When `new Object()` is called, the `[[Prototype]]` of it is set to `Object.prototype` according to the rule.

![Screenshot 2023-04-02 at 14.15.46](/Users/xiaobing/Desktop/learning/File/JS/:Users:xiaobing:Library:Application Support:typora-user-images:Screenshot 2023-04-02 at 14.15.46.png)

So then when `obj.toString()` is called the method is taken from `Object.prototype`

We can check it:

```js
let obj = {};

console.log(obj._proto_ === Object.prototype); //true
console.log(obj.toString() === obj._proto_.toString;//
console.log(obj.toString === Object.prototype.toString);//true
Object.getPrototypeOf(obj) === Object.prototype
```

### Other built-in prototypes

Other built-in objects such as `Array`, `Date`, `Function` and others also keep methods is "prototype"

```js
let arr = [1, 2, 3];

// it inherits from Array.prototype?
alert( arr.__proto__ === Array.prototype ); // true

// then from Object.prototype?
alert( arr.__proto__.__proto__ === Object.prototype ); // true
```

### Primitives

The most intricate thing happens with strings, numbers and booleans

As we remember, they are not objects. But if we try to access their properties, temporary wrapper objects are created using built-in constructors `String`, `Number` and `Boolean`

### Borrowing from prototypes

For instance, if we're making an array-lie object, we may want to copy some `Array` methods to it:

```js
let obj = {
  0: "Hello",
  1: "world!",
  length: 2,
};

obj.join = Array.prototype.join;
```



## Prototype methods, objects without _proto_

The modern methods of get/set a prototype are:

`Object.getPrototypeOf(obj)`

`Object.setPrototypeOf(obj,proto)`

Although, there's a special method for this too:

`Object.create(proto,[descriptors])`: create an empty object with given `proto` as `[[Prototype]]` and optional property descriptors

```js
let animal = {
  eats: true
};

let rabbit = Object.create(animal);

let clone = Object.create(Object.getPrototypeof(rabblt), Object.getOwnPropertyDescriptors(rabbit));
```





# Class

The basic syntax is:

```js
class MyClass extends FaterClass{
  level = 1;
  click = () ={
    console.log(this.name)
  }
  constructor(name) {
    super(age);
    this.name = name;
  }
 get fullName() {}
 set fullName(name) {}
 method1() {}
 static method2() {}
}
```

1. Class methods are non-enumerable
2. Classes always `use strict`

The important difference of class fields is that they are set on individual objects, not `User.prototype`

**How to deal with "losing this"?**

solution 1: Pass a wrapper-function, such as `setTimeout(() => button.click(), 1000)`

solution 2: Bind the method to object

```js
click = () => {
  console.log(this.name)
}
```

The class field `click = () => {...}` is created on a per-object basis, there’s a separate function for each `Button` object, with `this` inside it referencing that object. We can pass `button.click` around anywhere, and the value of `this` will always be correct



## Class inheritance 

Internally, `extends` keyword works using the good old prototype mechanics. It sets `Rabbit.prototype.[[Prototype]]` to `Animal.prototype`. So, if a method is not found in `Rabbit.prototype`, JavaScript takes it from `Animal.prototype`.

![](C:/Users/tadli/Desktop/learning/File/JS/2.png)

:notebook_with_decorative_cover:Arrow functions have no super





# Error handling

## Error handling, "try…catch"

No matter how great we are at programming, sometimes our scripts have errors.

### Error Object

When an error occurs, JavaScript generates an object containing the detailed about it. The object is then passed as an argument to `catch`:

```js
Error Object{
    name:""
    message:""
    stack:""
}
```

### "Throw" operator 

JavaScript has many built-in constructors for standard errors:

`Error`

`SyntaxError`

`ReferenceError`

`TypeError`

…

```js
catch (err) {
  if (err instanceof SyntaxError) {
    alert( "JSON Error: " + err.message );
  } else {
    throw err; // rethrow (*)
  }
}
```

:notebook_with_decorative_cover: `finally` and `return`

The `finally` clause works for any exit from `try...catch`. That includes an explicit `return`

`finally` is executed just before the control returns to the outer code



:notebook:`try…finally`

The `try...finally` construct, without `catch` clause is also useful. We apply it when we don't want to handle error, but want to be sure that processes that we started are finalized



## Custom errors, extending Error

When we develop something, we often need our own error classes to reflect specific things that may to go wrong in our tasks

Th `Error` class is built-in, but here's its approximate code so we can understand what we're extending:

```js
class Error {
  constructor(message) {
    this.message = message;
    this.name = "Error"; // (different names for different built-in error classes)
    this.stack = <call stack>; // non-standard, but most environments support it
  }
}
```

Now let's inherit `validationError` from it and try it in action:

```js
class ValidationError extends Error{
    constructor(message){
        super(message);
        this.name = "ValidationError";
    }
}
```

We could also look at `err.name`, like this

```js\
//instead of (err instanceof SyntaxError)
} else if (err.name == "SyntaxError)
```

The `instanceof` version is much better, because in the future we are going to extend `ValidationError`, make subtypes of it, like `PropertyRequiredError`. And `instanceof` check will continue to work for new inheriting classes. So that’s future-proof

### Further inheritance 

The `ValidationError` class is very generic. Many things may go wrong. The property may be absent or it may be in a wrong format. Let’s make a more concrete class `PropertyRequiredError`, exactly for absent properties. It will carry additional information about the property that’s missing

Please note that `this.name` in `PropertyRequiredError` constructor is again assigned manually. That may become a bit tedious – to assign `this.name = <class name>` in every custom error class. We can avoid it by making our own “basic error” class that assigns `this.name = this.constructor.name`. And then inherit all our custom errors from it.

### Wrapping exceptions

The code which calls `readUser` should handle these errors. Right now it uses multiple `if`s in the `catch` block, that check the class and handle known errors and rethrow the unknown ones.

The scheme is like this:

```js
try {
  ...
  readUser()  // the potential error source
  ...
} catch (err) {
  if (err instanceof ValidationError) {
    // handle validation errors
  } else if (err instanceof SyntaxError) {
    // handle syntax errors
  } else {
    throw err; // unknown error, rethrow it
  }
}
```

The technique that we describe here is called “wrapping exceptions”:

1. We’ll make a new class `ReadError` to represent a generic “data reading” error.
2. The function `readUser` will catch data reading errors that occur inside it, such as `ValidationError` and `SyntaxError`, and generate a `ReadError` instead.
3. The `ReadError` object will keep the reference to the original error in its `cause` property.

```js
class ReadError extends Error {
  constructor(message, cause) {
    super(message);
    this.cause = cause;
    this.name = 'ReadError';
  }
}

class ValidationError extends Error { /*...*/ }
class PropertyRequiredError extends ValidationError { /* ... */ }

function validateUser(user) {
  if (!user.age) {
    throw new PropertyRequiredError("age");
  }

  if (!user.name) {
    throw new PropertyRequiredError("name");
  }
}

function readUser(json) {
  let user;

  try {
    user = JSON.parse(json);
  } catch (err) {
    if (err instanceof SyntaxError) {
      throw new ReadError("Syntax Error", err);
    } else {
      throw err;
    }
  }

  try {
    validateUser(user);
  } catch (err) {
    if (err instanceof ValidationError) {
      throw new ReadError("Validation Error", err);
    } else {
      throw err;
    }
  }

}

try {
  readUser('{bad json}');
} catch (e) {
  if (e instanceof ReadError) {
    alert(e);
    // Original error: SyntaxError: Unexpected token b in JSON at position 1
    alert("Original error: " + e.cause);
  } else {
    throw e;
  }
}
```

