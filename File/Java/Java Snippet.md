Stream `flapMap`

```java
Stream.of(RunIndyProgram.COMMON_RUN_MODE_ARGS,
            List.of(new Argument(PARA_FORCE_NEW_REQUEST, true, List.of("Yes", "No")))).flatMap(Collection::stream).collect(Collectors.toList());
```

