```mermaid
classDiagram
	Collection*--Set
	Collection*--List
	class Collection{
	}
	class List{
	}
	class Set{
	}
	
	List*--ArrayList
	List*--LinkedList
	class ArrayList{
	}
	class LinkedList{
	}
	
	Set*--SortedSet
	SortedSet*--TreeSet
	Set*--HashSet
	HashSet<--LinkedHashSet
	class SortedSet{
	}
	class HashSet{
	}
	class LinkedHashSet{
	}
	class TreeSet{
	}
	
	class Map{
	}
	class SortedMap{
	}
	class TreeMap{
	}
	class HashMap{
	}
	class LinkedHashMap{
	}
	class HashTable{
	}
	
	Map*--HashMap
	Map*--HashTable
	Map*--SortedMap
	SortedMap<--TreeMap
	HashMap<--LinkedHashMap
	
	
```

# **`Collection:`**

## **`List:`**

**特性：**

- 元素可以重复

- **内部维护元素之间的顺序，是有序集合**(遍历时按照插入顺序遍历)
- 插入元素可以为null

```java
//添加
boolean add(E e)；
boolean addAll(Collection c)；

//获取
E get(int index);
Iterator iterator();

//删除
void clear();
E remove(int index);
boolean remove(Object o);
boolean removeAll(Collection e);

//修改
E set(int index, E element);

boolean isEmpty();
boolean contains(Object o);
int size();
Object[] toArray();
```



### **`ArrayList:`**

**实现方式：**数组

**优点：**查询和遍历快

**缺点：**插入和删除慢、线程不安全

实现`Cloneable`，得到了`clone()`方法，可以实现克隆功能



### **`LinkedList`**

**实现方式：**双向链表

**优点：**插入和删除快、提供了操作表头表尾元素的API，可以当作堆栈、队列和双向链表使用

**缺点：**随机访问和遍历慢

实现`Cloneable`，得到了`clone()`方法，可以实现克隆功能



## **`Set:`**

**特性：**

- 元素不可以重复，判断依据是对象的hashCode



### **`HashSet（hash表）:`**

**实现方式 :**hash表

**特性**：哈希表边存放的是**哈希值**。 存储元素的顺序并不是按照存入时的顺序（和 List 显然不同） 而是**按照哈希值来存的**所以取数据也是按照哈希值取得。元素的哈希值是通过元素的hashcode方法来获取的, HashSet首先判断两个元素的哈希值，如果哈希值一样，接着会比较equals方法 如果 equls结果为true ，HashSet就视为同一个元素。如果equals 为false就不是同一个元素



### **`LinkedHashSet:`**

**实现方式：**hash表和链表

哈希表决定了它元素是唯一的，而链表则保证了他是有序的（存储和取出顺序一致）

```java
LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>();
linkedHashSet.add(1);
linkedHashSet.add(9);
linkedHashSet.add(5);
linkedHashSet.add(3);
linkedHashSet.add(10);
linkedHashSet.add(2);
System.out.println(linkedHashSet.toString());

[1, 9, 5, 3, 10, 2]
```



### **`TreeSet（二叉树）:`**

**实现方式：**红黑树

**特点：**按照二叉树排序，遍历时也是按照中序方式遍历。<u>当TreeSet中要存入自己实现的类为元素时，该类必须实现`Comparable`接口重写`compareTo()`或者在创建Set时传入一个该类的Comparator比较器</u>

| 返回值 | API            | 描述                                                         |
| ------ | -------------- | ------------------------------------------------------------ |
| E      | `first()`      | 返回集合中序遍历第一个                                       |
| E      | `last()`       | 返回集合中序遍历最后一个                                     |
| E      | `pollFirst()`  | 删除集合中序遍历第一个并返回                                 |
| E      | `pollLast()`   | 删除集合中序遍历最后一个并返回                               |
| E      | `floor(E e)`   | 返回比传入元素小的所有元素中最大的一个。包含相等（因为传入的元素可能不在集合中，也可能在集合中，这个方法若传入元素在集合中则返回传入元素，如不在则返回比传入元素小的所有元素中最大的一个） |
| E      | `ceiling(E e)` | 返回比传入元素大的所有元素中最小的一个，包含相等             |
| E      | `lower(E e)`   | 返回比传入元素小的所有元素中最大的一个                       |
| E      | `higher(E e)`  | 返回比传入元素大的所有元素中最小的一个                       |

**中序遍历：**先左节点、后根节点、最后右节点

```java
TreeSet<Integer> treeSet = new TreeSet<>();
treeSet.add(1);
treeSet.add(9);
treeSet.add(5);
treeSet.add(3);
treeSet.add(10);
treeSet.add(2);
System.out.println("first->"+treeSet.first());
System.out.println("last->"+treeSet.last());
System.out.println("floor->"+treeSet.floor(3));
System.out.println("ceiling->"+treeSet.ceiling(3));
System.out.println("lower->"+treeSet.lower(3));
System.out.println("higher->"+treeSet.higher(3));
System.out.println(treeSet.toString());
for (Integer integer : treeSet) {
    System.out.println(integer);
}

first	->1
last	->10
floor	->3
ceiling	->3
lower	->2
higher	->5
结果都是按照中序遍历
[1, 2, 3, 5, 9, 10]
1
2
3
5
9
10
```



# **`Map`**

## **`HashMap（hash表）:`**

**实现方式：**数组+链表+红黑树



## **`LinkedHashMap（hash表和红黑树）:`**

就如`LinkedHashSet`，`LinkedHashMap`可以按照插入的顺序，进行遍历

## **`TreeMap（红黑树）:`**

就如`TreeSet`，`TreeMap`也是按照中序遍历

**注意：**如果key为`String`类型，`String`类型的`compareTo()`方法，比较的是字典顺序，需要匿名构造比较器，转化成`Integer`类型，再进行比较

```java
TreeMap<String, Object> treeMap = new TreeMap<>(new Comparator<String>() {
    @Override
    public int compare(String o1, String o2) {
        Integer s1 = Integer.parseInt(o1);
        Integer s2= Integer.parseInt(o2);
        return s1.compareTo(s2);
    }
});
treeMap.put("1", 1);
treeMap.put("9", 9);
treeMap.put("5", 5);
treeMap.put("3", 3);
treeMap.put("10", 10);
treeMap.put("2", 2);

System.out.println("10".compareTo("2"));
System.out.println(treeMap.toString());

-1
{1=1, 2=2, 3=3, 5=5, 9=9, 10=10}
```



# foreach

## 属性

- **collection:** 需做foreach(遍历)的对象
- **item：** 集合元素迭代时的别名称，该参数为必选项；
- **index：** 在list、array中，index为元素的序号索引。但是在Map中，index为遍历元素的key值，该参数为可选项；
- **open：** 遍历集合时的开始符号，通常与close=")"搭配使用。使用场景IN(),values()时，该参数为可选项；
- **separator：** 元素之间的分隔符，类比在IN()的时候，separator=",",最终所有遍历的元素将会以设定的（,）逗号符号隔开，该参数为可选项；
- **close：** 遍历集合时的结束符号，通常与open="("搭配使用，该参数为可选项；



## collection的三种情况

- 如果传入的参数类型为List时，collection的默认属性值为list,同样可以使用@Param注解自定义keyName;

  ```xml
  List<UserList> getUserInfo(@Param("userName")List<String> userName);
  
  <select id="getUserInfo" resultType="com.test.UserList">
  	select * from user_info where
      <if test="userName != null and userName.size()>0">
          userName in
          <foreach collection="userName" item="item" open="(" seperator="," close=")">
          	#{item}
          </foreach>
      </if>
  </select>
      
  <select id="getUserInfo" resultType="com.test.UserList">
  	select * from user_info where
      <if test="userName != null and userName.size()>0">
          userName in
          <foreach collection="list" item="item" open="(" seperator="," close=")">
          	#{item}
          </foreach>
      </if>
  </select>
  ```

  

- 如果传入的参数类型为array时，collection的默认属性值为array,同样可以使用@Param注解自定义keyName;

- 如果传入的参数类型为Map时，collection的属性值可为三种情况：（1.遍历map.keys;2.遍历map.values;3.遍历map.entrySet()）,稍后会在代码中示例；

```java
List<UserList> getUserInfo(@Param("user")Map<String,String> user);
```

```xml
<select id="getUserInfo" resultType="com.test.UserList">
	select * from user_info where
    <if test="userName != null and userName.size()>0">
        userName in
        <foreach collection="user" index="key" item="value" open="(" seperator="," close=")">
        	(#{key},#{value})
        </foreach>
    </if>
</select>
```



# ResultType

**基本类型 ：**resultType=基本类型

**List类型：** resultType=List中泛型的类型

**Map类型    **

​		**单条记录：**resultType =map

​		**多条记录：**resultType =Map中value的类型

**对象类型：**对于对象类型resultType直接写对象的全类名就可以了



# JVM内存分配机制

## 对象创建过程

`A a = new A();`

类加载：检查引用的类是否已被加载，如果没有，则执行类加载过程

分配内存：类加载检查通过后，JVM为新生对象分配内存

初始化：JVM将分配到的内存空间（不包括对象头）都初始化为零值

设置对象头：请求头里包含了对象是哪个类的实例，对象的GC分代年龄等信息

调用构造方法：设置实例数据

与栈的指针建立关联

## 分配内存的方式

 **指针碰撞：**将一块内存分配为已使用和未使用，中间分配使用指针分隔，分配内存就是将指针移动到与对象大小相同的内存块处。

**空闲列表：**一般内存中可能有不相邻的内存块，JVM会维护一个列表，标记哪些是使用过的内存和未使用的内存。分配内存的时候会更新这个列表。

**堆抢占的解决办法：**

    TLAB：每一个线程有一段单独的缓冲区叫TLAB，Thread Local Allocation Buffer。
    默认使用-XX:+/-UseTLAB参数设定是否开启TLAB，默认jdk1.8开启，使用-XX:TLABSIZE指定TLAB大小
## **设置对象头**

> 对象在内存中分为对象头，实例数据，对齐填充

**对象头：**

- mark Word

- Klass Pointer指针(堆中对象指向方法区的类信息)

- 数组长度



# 新生代和老年代

**新生代(Young Gen)：**

​	年轻代主要存放新创建的对象，内存大小相对会比较小，垃圾回收会比较频繁。

​	**区域划分：**1个Eden Space和2个Suvivor Space（命名为From和To）

​	**执行流程：**当对象在堆创建时，将进入新生代的Eden Space。垃圾回收器进行垃圾回收时，扫描Eden Space和From Suvivor Space，如果对象仍然存活，则复制到To Suvivor Space，如果To Suvivor Space已经满，则复制到Old Gen。同时，在扫描Suvivor Space时，如果对象已经经过了几次的扫描仍然存活，JVM认为其为一个持久化对象，则将其移到Old Gen。扫描完毕后，JVM将Eden Space和A Suvivor Space清空，然后交换A和B的角色（即下次垃圾回收时会扫描Eden Space和B Suvivor Space。这么做主要是为了减少内存碎片的产生。

**年老代(Tenured Gen)：**年老代主要存放JVM认为生命周期比较长的对象（经过几次的Young Gen的垃圾回收后仍然存在），内存大小相对会比较大，垃圾回收也相对没有那么频繁（譬如可能几个小时一次）。年老代主要采用压缩的方式来避免内存碎片（将存活对象移动到内存片的一边，也就是内存整理）。当然，有些垃圾回收器（譬如CMS垃圾回收器）出于效率的原因，可能会不进行压缩。

# String

String不属于8种基本数据类型，String是一个对象

常量池(constant pool)指的是在编译期被确定，并被保存在已编译的.class文件中的一些数据。它包括了关于类、方法、接口等中的常量，也包括字符串常量。

String.intern()

实例str调用intern()方法时，Java查找常量池中是否有相同Unicode的字符串常量，如果有，则返回其的引用，如果没有，则在常量池中增加一个Unicode等于str的字符串并返回它的引用



# String为什么是final

 字符串是常量。它们的值在它们创建之后是无法改变的。由于字符串对象是不可变的，所以它们可以被共享。



# 联合索引本质

当创建(a,b,c)联合索引时，相当于创建了(a)单列索引，(a,b)联合索引以及(a,b,c)联合索引
想要索引生效的话,只能使用 a和a,b和a,b,c三种组合；当然，我们上面测试过，a,c组合也可以，但实际上只用到了a的索引，c并没有用到！