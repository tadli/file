# Lambda

## Example

```java
//An interface
@FunctionalInterface
public interface TestInterface{
    int add(int a,int b);
}

//Impliment Interface and override method
public class Test implements testInterface{
    @Override
    public int add(int a,int b){
        return  a+b;
    }
}

//invoke it
public static void main(String[] args) {
	TestInterface test = new Test();
    test.add(1,2);
}

/**
如果使用lambda表达式，可以免去定义实现类这一步，直接用lambda创建匿名实现类，作为接口的实现
*/
public static void main(String[] args) {
	TestInterface test = (a,b)->{a+b};
    test.add(1,2);
}
```



# Optional

目的：减少空指针判断代码

| 方法          | 描述                                                         |
| ------------- | ------------------------------------------------------------ |
| of            | 把指定的值封装为Optional对象，如果指定的值为null，则抛出NullPointerException |
| empty         | 创建一个空的Optional对象                                     |
| ofNullable    | 把指定的值封装为Optional对象，如果指定的值为null，则创建一个空的Optional对象 |
| **get**       | 如果创建的Optional中有值存在，则返回此值，否则抛出NoSuchElementException |
| **orElse**    | 如果创建的Optional中有值存在，则返回此值，否则返回一个默认值 |
| orElseGet     | 如果创建的Optional中有值存在，则返回此值，否则返回一个由Supplier接口生成的值 |
| **filter**    | 如果创建的Optional中的值满足filter中的条件，则返回包含该值的Optional对象，否则返回一个空的Optional对象 |
| map           | 如果创建的Optional中的值存在，对该值执行提供的Function函数调用 |
| **isPresent** | 如果创建的Optional中的值存在，返回true，否则返回false        |
| ifPresent     | 如果创建的Optional中的值存在，则执行该方法的调用，没有返回值 |

```java
//创建一个optional对象，如果值为null，则抛出异常
Optional<String> optional = Optional.of("lby");

//为指定的值创建Optional对象，不管所传入的值为null不为null，创建的时候都不会报错
Optional<String> optionalNull = Optional.ofNullable(null);

//创建一个空的optional对象
Optional<Object> empty = Optional.empty();

//如果有值，则获得值。如果为null，则抛出异常
String value = optionalNull.get();
//如果有值，则获得值。如果为null，则输出默认值
String orElse = optionalNull.orElse("orElse");
//如果有值，则获得值。如果为null，则输出默认值
String orElseGet = optionalNull.orElseGet(() -> "orElseGet");

//可以执行流
Optional<String> optionalFilter = optional.filter((element) -> true);

Optional<String> optionalUpper = optional.map(String::toUpperCase);

//如果创建的Optional中的值存在，则执行该方法的调用，没有返回值
optionalUpper.ifPresent(System.out::println);
```



# Stream

:star: 串行流的效率没有for循环高

:star:并行流效率高，但也会产生并发问题，占用CPU也高，适用于：**数据量比较大下，CPU负载不是很高，不要求顺序执行**



生成流->处理流（filter/sorted/map）->汇总(collect)



**注意：**

①Stream 自己不会存储元素。

②Stream 不会改变源对象。相反，他们会返回一个持有结果的新Stream。

③Stream 操作是延迟执行的。这意味着他们会等到需要结果的时候才执行。



## 生成流

**一、通过Collection集合类提供的stream()方法或者parallelStream()方法来创建Stream**

```java
ArrayList<String> list = new ArrayList<String>();
list.stream();
list.parallelStream();
```

**二、通过Arrays中的静态方法stream()获取数组流**

```java
String[] strings = new String[]{"A","B","C"};
Arrays.stream(strings);
```

**三、通过Stream类的静态方法of()获取数组流**

```java
Stream<String> stream = Stream.of("A","B","C");
stream.forEach(System.out::println);
```
**四、创建空流**

```java
Stream<String> stream = Stream.empty();
```



## 处理

分为三类：

1. **筛选与切片**
2. **映射**
3. **排序**

### 筛选与匹配

`filter(Predicate p)`: 过滤数据，将符合条件的元素提取到新流中

`distinct()`:去重，通过流所生成元素的`hashCode()`和`equals()`去重

`limit(long maxSize)`:截断流，使其元素不超过规定的数量

`skip(long n),返回一个扔掉前n个元素的流，如果流中元素不足n，则返回空流`



#### **`filter`**

```java
ArrayList<Person> list = new ArrayList<>(
        Arrays.asList(
        new Person("A",1),
        new Person("B",2))) ;

list.stream().filter((element)->element.getAge()>1).forEach(System.out::println);
}

//Person [name=B, age=2]
```

#### `distinct`、`limit`

distinct去重

limit限制元素数量

```java
ArrayList<Person> list = new ArrayList<>(
        Arrays.asList(
        new Person("A",1),
        new Person("A",1),
        new Person("B",2),
        new Person("B",2),
        new Person("C",10),
        new Person("C",10),
        new Person("D",11),
        new Person("E",12),
        new Person("F",13))) ;
list.stream().distinct().filter((element)->element.getAge()>1).limit(5).forEach(System.out::println);
```



### 映射

`map(Function f):`接收一个函数作为参数，该函数将应用到每个元素上，并将其映射成一个新的元素

```java
ArrayList<Person> list = new ArrayList<>(
        Arrays.asList(
        new Person("A",1),
        new Person("A",1),
        new Person("B",2),
        new Person("B",2),
        new Person("C",10),
        new Person("C",10),
        new Person("D",11),
        new Person("E",12),
        new Person("F",13))) ;
list.stream().
    distinct().
    map((element)->element.getName().toLowerCase()).
    forEach(System.out::println);
}

a
b
c
d
e
f
```



### 排序

`sorted()`： 将元素按规则排序，生成新的流

`sorted(Comparator comp)`：按照比较器规则排序，生成新的流

```java
list.stream()
    .sorted((element1,element2)->{
        if(element1.getAge() == element2.getAge()){
            return element1.getName().compareTo(element2.getName());
        }else if(element1.getAge() > element2.getAge()){
            return 1;
        }else{
            return -1;
        }
    }).forEach(System.out::println);

Person [name=A, age=1]
Person [name=A, age=1]
Person [name=B, age=2]
Person [name=B, age=2]
Person [name=C, age=10]
Person [name=C, age=10]
```



## 汇总

`max()`： 获取最大值

`min()`： 获取最小值

`count()`： 获取数量

```java
//Person::getAge，将list流转化成Integer流
Optional<Integer> collect = list.stream().map(Person::getAge).max(Integer::compareTo);

long collect = list.stream().map(Person::getAge).count();
```



`reduce`：聚合操作，用来做统计，将流中元素反复结合起来统计计算，得到一个值.。

```java
long collect = list.stream().map(Person::getAge).reduce(0, (element1,element2)->element1*10+element2);
```



`forEach()`： 遍历每一个元素进行操作

`collect()`： 汇总数据，转成集合

`toArray`：返回流中元素对应的数组对象

```java
// 求总数
Long count = personList.stream().collect(Collectors.counting());
// 求平均工资
Double average = personList.stream().collect(Collectors.averagingDouble(Person::getSalary));
// 求最高工资
Optional<Integer> max = personList.stream().map(Person::getSalary).collect(Collectors.maxBy(Integer::compare));
// 求工资之和
Integer sum = personList.stream().collect(Collectors.summingInt(Person::getSalary));
// 一次性统计所有信息
DoubleSummaryStatistics collect = personList.stream().collect(Collectors.summarizingDouble(Person::getSalary));

// 将员工按薪资是否高于8000分组
Map<Boolean, List<Person>> part = personList.stream()
    .collect(Collectors.partitioningBy(x -> x.getSalary() > 8000));

// 将员工按性别分组
Map<String, List<Person>> group = personList.stream()
    .collect(Collectors.groupingBy(Person::getSex));

// 将员工先按性别分组，再按地区分组
Map<String, Map<String, List<Person>>> group2 = personList.stream()
  	.collect(Collectors.groupingBy(Person::getSex, Collectors.groupingBy(Person::getArea)));

//joining
List<String> list = Arrays.asList("A", "B", "C");
String string = list.stream()
  	.collect(Collectors.joining("-"));
```



```java
Map<String, Integer> map = list.stream().map(Person::getAge).collect(Collectors.toMap(element -> element.toString(), element -> element));

Object[] array = list.stream().map(Person::getAge).toArray();
```



`findFirst`： 短路操作，获取第一个元素。

`findAny`：短路操作，获取任一元素。

```java
Optional<Person> any = list.stream().findFirst();
any.ifPresent(System.out::println);
```



`anyMatch`：短路操作，有一个符合条件返回true。

`allMatch`：所有数据都符合条件返回true。

`nonoMatch`：只要有一个元素匹配传入的条件，就返回 false；如果全部匹配，则返回 true

```java
 boolean match = list.stream().allMatch(element -> element.getAge() > 10);
 boolean match = list.stream().anyMatch(element -> element.getAge() > 10);
 boolean match = list.stream().noneMatch(element -> element.getAge() > 10);
```