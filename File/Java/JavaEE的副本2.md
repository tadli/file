## 基本数据类型

- 整数类型：byte，short，int，long
- 浮点数类型：float，double
- 字符类型：char
- 布尔类型：boolean

计算机内存的最小存储单元是字节（byte），一个字节就是一个8位二进制数，即8个bit

byte 1字节

char 2字节

int 4字节

float 4字节



## 数组遍历的三种方式

1.for循环

2.forEach循环

```java
int[] ns = { 1, 4, 9, 16, 25 };
for (int n : ns) {
    System.out.println(n);
}
```

3.`Arrays.toString();`

```java
int[] ns = { 1, 4, 9, 16, 25 };
Arrays.toString(ns);
```



## Arrays.deepToString()与Arrays.toString()的区别

Arrays.deepToString()主要用于数组中还有数组的情况，而Arrays.toString()则相反，对于Arrays.toString()而言，当数组中有数组时，不会打印出数组中的内容，只会以地址的形式打印出来。



## Arrays常用方法

### **遍历数组：**

**`Arrays.toString(arr)`**

### **排序数组：**

**`Arrays.sort(arr);`**

### **二分查找法找指定元素的索引值:**

搜索前必须排序

**`Arrays.sort(arr);`**

**`Arrays.binarySearch(arr,40);`**



### **比较数组元素是否相等:**

`Arrays.equals(arr1,arr2);`

equals比较的是两个对象的地址，但Arrays重写了equals，所以可以比较数组里的值

### **截取数组：**

`Arrays.copyOf()`

`Arrays.copyOfRange()`

```java
int[] arr = {1,2,3,4,5};
int[] newArr = Arrays.copyOf(arr,3);	//1,2,3
int[] newArr2 = Arrays.copyOfRange(arr,1,3);//2,3
```

### **把数组转化成List：**

`Arrays.asList(strArray);`

`Arrays.asList(strArray).contains("a");`

### **把List转化成数组：**

`list.toArray()`



## 数组和List之间的转换

List 转Array，必须使用集合的 `toArray(T[] array)`

```java
String[] array = list.toArray(new String[list.size()])
```

Array转List，返回值是一个内部类的ArrayList，而不是java.util.ArrayList，因此不能使用修改集合的相关方法

```java
ArrayList<String> list = new ArrayList<>(Arrays.asList(strArray));
```



## Java打印数组

`Arrays.toString(arr)`



## String常用方法

### **字符串比较：**

`s1.equals(s2)`

### **忽略大小写比较:**

`equalsIgnoreCase()`

### **是否包含：**

`contains()`

### **搜索子串：**

`indexOf()`

`startsWith()`

`endsWith()`

`lastIndexOf()`

### **去掉首尾空白字符：**

`.trim()`

### **判断字符串是否为空和空白字符串**

`isEmpty()`

`isBlank()`

### **替换子串：**

`str.replace("a","b");`	//用b替换所有的a

`str.replaceAll("\d","a");`	//用a替换所有匹配正则的字符

### **分割字符串：**

```java
String s = "A,B,C,D";
String[] ss = s.split("\\,"); // {"A", "B", "C", "D"}
```

### **拼接字符串：**

```java
String[] arr = {"A", "B", "C"};
String s = String.join("***", arr); // "A***B***C"
```

### **转换类型：**

```java
String.valueOf(123); 
int n1 = Integer.parseInt("123");
```

### **大小写转换:**

`toUpperCase()`

` toLowerCase()`



## String拼接的三种方式

**+号拼接**

**`String.concat(String str)`**

**`StringBuilder.append()`** 

**对比：**用+拼接字符串过程中会生成新的字符串对象，浪费内存。**若只是两个字符串拼接，使用`concat`**。**多字符或循环体中拼接字符串优先使用`StringBuilder`**，提高效率，还能链式编程



## String转化成Integer

```java
Integer.parseInt(String s);
Integer.valueOf(String s);
```



## equals和== 的区别

#### 对于 == 来说：

如果比较的是基本数据类型变量，比较两个变量的值是否相等。(不一定数据类型相同)
如果比较的是引用数据类型变量，比较两个对象的地址值是否相同，即两个引用是否指向同一个地址值

`int a=1; char b=1; a==b//(true)`

#### 对于 equals 来说：

如果类中重写了equals方法，比较内容是否相等。

如果类中没有重写equals方法，比较地址值是否相等（是否指向同一个地址值）。



## compareTo()方法有什么用？

Java提供了一个Comparable接口，该接口里定义了一个compareTo(Object obj)方法，该方法返回一个整数值，实现该接口的类必须实现该方法，实现了该接口的类的对象就可以比较大小。

如果该方法返回0，则表示两个对象相等，如果该方法返回一个正整数，则表明obj1大于obj2；如果该方法返回一个负整数，则表明obj1小于obj2。

**已经实现了Comparable接口的常用类：**

BigDecimal、String、Data、Time





## 为什么要重写equals方法？

`List`中有两个方法，`list.contains()`和`list.indexof()`，在`Map`的内部，对`key`做比较也是通过`equals()`实现的。所以需要重写`equals`方法

```java
public boolean contains(Object o) {
    return indexOf(o) >= 0;
}

public int indexOf(Object o) {
    if (o == null) {
        for (int i = 0; i < size; i++)
            if (elementData[i]==null)
                return i;
    } else {
        for (int i = 0; i < size; i++)
            if (o.equals(elementData[i]))		//equals在这！
                return i;
    }
    return -1;
}
```



## ArrayList常用方法

`add()`

`get()`

`size()`

`set(int i, Object element)`

将索引i位置元素替换为element，并返回被替换的元素

`clear()`

`isEmpty()`

`contains()`

`remove(int index)`

`remove(Object 0)`

移除集合中第一次出现的元素o



## ArrayList和LinkedList的区别

ArrayList是数组的数据结构，LinkedList是使用双向链表实现存储

随机访问的时候，ArrayList的效率比较高，因为LinkedList要移动指针，而ArrayList是基于索引(index)的数据结构，可以直接映射到。

插入、删除数据时，LinkedList的效率比较高，因为ArrayList要移动数据。

LinkedList比ArrayList开销更大，因为LinkedList的节点除了存储数据，还需要存储引用



## *在遍历 ArrayList 时移除一个元素

**在forEach循环里对元素进行remove/add操作，会报fail-fast错误**

### 第一种方法：推荐

```java
Iterator iterator = list.iterator();
while(iterator.hasNext()){
    if(iterator.next.equals("lby")){
        iterator.remove();
    }
}
```

### 第二种方法：

fori顺序遍历会导致重复元素没删除。因为删除第一个元素后，后面的元素会向前进一位，就会略过下一个元素。

解决：倒序删除

```java
for(int i=list.size()-1;i>-1;i--){
    if(list.get(i).equals("lby")){
        list.remove(list.get(i));
    }
}
```

## ArrayList集合加入1万条数据，应该怎么提高效率

因为ArrayList的底层是数组实现,并且数组的默认值是10,如果插入10000条要不断的扩容,耗费时间,所以我们调用ArrayList的指定容量的构造器方法ArrayList(int size) 就可以实现不扩容,就提高了性能。



## ArrayList 和 HashMap 的默认大小是多少？

在 Java 7 中，ArrayList 的默认大小是 10 个元素，HashMap 的默认大小是16个元素（必须是2的幂）。



## 快速失败(fail-fast)和安全失败(fail-safe)的区别是什么？

**快速失败（fail-fast）：**是Java集合一种错误检测机制。在遍历一个集合对象时，如果遍历过程中对集合对象的内容进行修改（增加、删除、修改），会抛出Concurrent Modification Exception

在遍历过程中会使用一个modCount变量，如果集合在遍历期间内容发生改变，就会改变modCount的值。每当迭代器使用`next()/hashNext()`时，都会检测modCount是否改变。如果改变了就抛出异常。

java.util包下的集合类都是快速失败的

**安全失败（fail-safe）：**在遍历时不是直接在集合内容上访问，而是先复制原有集合内容，在拷贝的集合上进行遍历

java.util.concurrent包下的集合类都是安全失败的



## Hash碰撞是什么？

hash碰撞指的是，两个不同的值（比如张三、李四的学号）经过hash计算后，得到的hash值相同，后来的李四要放到原来的张三的位置，但是数组的位置已经被张三占了，导致冲突。

JDK7的HashMap数据结构是数组+链表

JDK8的HashMap数据结构是数组+链表+红黑树

数组用来存储数据元素，链表是用来解决冲突，红黑树是为了提高查询速度

如果发生冲突，从冲突的位置生成一个链表，插入冲突的元素。如果链表长度超过8。链表转化为红黑树



如果两个键值映射到同一个相同的槽位上，就会产生哈希冲突/碰撞，可以通过再增加一层链表解决问题

​	005槽位->金庸->韦小宝->李炳煜

​	006槽位->付娆



**hash碰撞的解决方式是开放寻址法和拉链法。**

开放寻址法和拉链法都是想办法找到下一个空位置来存发生冲突的值。

**开放寻址法指的是**，当前数组位置1被占用了，就放到下一个位置2上去，如果2也被占用了，就继续往下找，直到找到空位置

**拉链法采用的是链表的方式**，这个时候位置1就不单单存放的是Entry了，此时的Entry还要额外保存一个next指针，指向数组外的另一个位置，将李四安排在这里，张三那个Entry中的next指针就指向李四的这个位置，也就是保存的这个位置的内存地址。如果还有冲突，就把又冲突的那个Entry放到一个新位置上，然后李四的Entry指向它，这样就形成一个链表。



## HashMap常用方法

### **添加：**

`put(key,value)`

### **取数据：**

`get(key)`

### **删除：**

`remove(key)`

### **大小：**

`size()`

### **清空：**

`clear()`

### **判断是否为空：**

`isEmpty()`

### **values：**

`map.values()`

### **keySet：**

`map.keySet()`

### 将key元素转存在一个set集合

`map.entrySet()`

### 是否包含某个key

`containsKey()`

### 是否包含某个value

`containsValue()`



## Set常用方法

**Set 接口继承 Collection 接口，而且它不允许集合中存在重复项**

`add()`

`size()`

`isEmpty()`

`contains()`

`remove()`

`clear()`

**Set没有get方法，只能用forEach循环或者迭代器**

```java
Iterator<String> iterator = set.iterator();
while(iterator.hasNext()){
    iterator.next();
}
```



## Collections工具类的常用方法

### `sort`

```java
COllections.sort(list);
```

### `shuffle`

随机打乱排序

```java
Collections.shuffle(list);
```

### `reverse`

```java
COllections.reverse(list);
```

### *`unmodifidableMap(map)`

### *`unmodifidableList(list)`

### *`unmodifidableSet(set)`

中间层有时会初始化一些final的静态的Map供给一些字段做映射，需要不可更改的Map集合

```java
public final static Map<String,String> map = new HashMap<>();
static{
    map.put(MYSQL,"mysql");
    map.put(SQLSERVER,"sqlserver")
}
```

```java
	private static final Map<String, String> LBK_SPECIFIED_HOST;

	static {
		LBK_SPECIFIED_HOST = new HashMap<String, String>();

		LBK_SPECIFIED_HOST.put("isjpissc52p301109", "94");
		LBK_SPECIFIED_HOST.put("isjpissc52p301110", "95");
		LBK_SPECIFIED_HOST.put("isjpissc52p301111", "96");
		LBK_SPECIFIED_HOST.put("isjpissc52p301112", "97");

		Map<String, String> map = Collections.unmodifiableMap(LBK_SPECIFIED_HOST);
	}
```

### *`Collections.EMPTY_LIST()`

有时候返回Null会报异常，所以返回一个空的集合对象

```java
errInfo.put(TB_MST_GOODS.getCd(), Collections.<String> emptyList());
```



## &和&&有什么区别？

&&左侧表达式为false，则不会计算右侧表达式

`username!=null && !username.equals(" ");`

如果`username`为null，则右侧不会计算

&无论左侧是否为false，都会计算右侧



## 封装，继承和多态

封装：把一个对象的属性私有化，同时提供get和set方法

继承：子类继承父类的所有属性和方法（包括私有属性和私有方法），但父类的私有属性和方法，子类无法访问，只是拥有。

多态：一个引用变量倒底会指向哪个实例对象，该引用变量发出的方法调用到底是哪个类中实现的方法，必须在由程序运行期间才能决定。

多个子类对父类的同一个方法的重写、实现接口并重写方法





## 重载（Overload）和重写（Override）的区别？

### 重写(Override)

发生在子类和父类之间，**在子类中把父类本身有的方法**重新写一遍，**在方法名，参数列表，返回类型**都相同的情况下

### 重载(Overload)

发生在一个类中，同名的方法如果有不同的参数列表（**参数类型不同、参数个数不同甚至是参数顺序不同**）则视为重载。同时，重载对返回类型没有要求，可以相同也可以不同，但**不能通过返回类型是否相同来判断重载**



## 访问修饰符

`default:`什么也不写，同一个包内可见

`public`:可以被其他任何类访问

`protected`:作用于继承关系。定义为`protected`的字段和方法可以被子类访问，以及子类的子类

`private`:访问权限被限定在`class`的内部



## **子类调用父类的构造方法：**

任何子类的构造方法，第一行语句必须是调用父类的构造方法。如果没有明确地调用父类的构造方法，编译器会帮我们自动加一句`super();`

但是，`Person`类并没有无参数的构造方法，因此，编译失败。

```java
class Student extends Person {
    protected int score;

    public Student(String name, int age, int score) {
        super(); // 自动调用父类的构造方法
        this.score = score;
    }
}
```

解决方法是调用`Person`类存在的某个构造方法。例如：

```java
class Student extends Person {
    protected int score;

    public Student(String name, int age, int score) {
        super(name, age); // 调用父类的构造方法Person(String, int)
        this.score = score;
    }
}
```

<u>结论：如果父类没有默认的构造方法，子类就必须显式调用`super()`并给出参数以便让编译器定位到父类的一个合适的构造方法</u>



## this关键字的作用？

1.普通的直接引用，this相当于指向当前对象本身

2.形参与成员变量重名，用this区分

3.引用本类的其他构造函数



## final

- `final`修饰符有多种作用：
  - `final`修饰的class可以阻止被继承；
  - `final`修饰的方法可以阻止被覆写；
  - `final`修饰的field必须在创建对象时初始化，随后不可修改。

声明数据为常量，可以是编译时常量，也可以是在运行时被初始化后不能被改变的常量。

- 对于基本类型，final 使数值不变；

- **对于引用类型，final 使引用不变，也就不能引用其它对象，但是被引用的对象本身是可以修改的。**





## 抽象类

如果父类的方法本身不需要实现任何功能，仅仅是为了定义方法签名，目的是让子类去覆写它，那么，可以把父类的方法声明为抽象方法，父类也要声明成抽象类

```java
abstract class Person {
    public abstract void run();
}
```



## 接口

如果一个抽象类没有字段，所有方法全部都是抽象方法：就可以把该抽象类改写为接口：`interface`。

一个`interface`可以继承自另一个`interface`。`interface`继承自`interface`使用`extends`，它相当于扩展了接口的方法。接口是可以多继承的。

|            | abstract class       | interface                   |
| :--------- | :------------------- | --------------------------- |
| 继承       | 只能extends一个class | 可以implements多个interface |
| 字段       | 可以定义实例字段     | 不能定义实例字段            |
| 抽象方法   | 可以定义抽象方法     | 可以定义抽象方法            |
| 非抽象方法 | 可以定义非抽象方法   | 可以定义default方法         |

抽象是对类的抽象，是一种模板设计

接口是对行为的抽象，是一种行为的规范



类A继承类B和类C，如果B和C有重名的方法，那么类A继承的是哪个实现？

而接口里没有方法的实现，所以可以多继承

## *深拷贝和浅拷贝？

**浅拷贝：**仅拷贝对象的成员变量的值，和引用数据类型的地址值，对引用类型变量指向的对象不会拷贝



**实现：Object类的clone方法**

1.子类要实现Cloneable接口，否则调用clone()方法，会抛CloneNotSupportedException
		2.子类要重写clone()方法
		3.在clone()方法中要调用super.clone()方法，是实现复制的核心
		4.clone()方法，返回的是Object类型，自己要注意类型转换

```java
class MyObject implements Cloneable{
    protected Object clone(){
        return super.clone();
    }
}

public class test001 implements Cloneable{
    MyObject myObject = new MyObject();
    Object newObject = myobject.clone();
}
```



**深拷贝：**完全拷贝一个对象

**实现一：**重写克隆方法，引用数据类型单独克隆

```java
class Teacher implements Cloneable {
    private String name;
    private int age;

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

class Student implements Cloneable {
    private String name;
    private int age;
    private Teacher teacher;

    public Object clone() throws CloneNotSupportedException {
        // 浅复制时：
        // Object object = super.clone();
        // return object;

        // 改为深复制：
      	// 本来是浅复制，现在将Teacher对象复制一份并重新set进来
        Student student = (Student) super.clone();
        student.setTeacher((Teacher) student.getTeacher().clone());
        return student;
    }
}

public class DeepCopy {
    public static void main(String[] args) throws CloneNotSupportedException {
        Teacher teacher = new Teacher();
        teacher.setName("riemann");
        teacher.setAge(28);

        Student student1 = new Student();
        student1.setName("edgar");
        student1.setAge(18);
        student1.setTeacher(teacher);

        Student student2 = (Student) student1.clone();
        System.out.println("-------------拷贝后-------------");
        System.out.println(student2.getName());
        System.out.println(student2.getAge());
        System.out.println(student2.getTeacher().getName());
        System.out.println(student2.getTeacher().getAge());
    }
}
```



**实现二：**用Jackson，先将对象序列化成json，再反序列化成拷贝对象



## Try和Finally的执行顺序？

先执行try{}里的语句，直到return，然后将return里的数据保存，再执行finally{}，最后再取出保存的值，执行return

```java
int 1=0;
try{
    i=2;
    return i;
}catch{
    ...
}finally{
    i=3;
}

//最后结果为2；
```



## 序列化

JavaBean要实现Serializable，并且添加字段

`private static final serialVersionUID = 1L`

serialVersionUID 起到验证作用

序列化是不包含静态变量的

对于不想进行序列化的变量，用`transient`修饰



## 枚举类

引用类型的比较用`equals()`，枚举类是引用类型，但可以用`==`

**返回常量名：**

`.name()`

给每个枚举常量添加字段：使用`private`构造方法

```java
public enum LINES {
    A ("ア"),
    K ("カ"),
    S ("サ")
        
    private String head = null;

    private LINES() {}

    private LINES(String head) {
        this.head = head;
    }

    public String getHead() {
        return this.head;
    }
}
```



## BigDecimal

```java
BigDecimal bd = new BigDecimal("123.4567");
bd.scale();	//获得精度
BigDecimal d2 = bd.setScale(4, RoundingMode.HALF_UP); // 设置精度和截取办法：四舍五入，123.4568
d1.compareTo(d2)
```

**加减乘除绝对值：**

> `add()`
>
> `subtract`
>
> `multiply`
>
> `divide()`
>
> `abs()`



加减乘不会出现问题，**做除法会报错，因为除不尽**，==所以要指定**精度**，以及如何**截断**===

```java
		BigDecimal big1 = new BigDecimal(12.123456);
		BigDecimal big2 = new BigDecimal(12.123456789);
		BigDecimal big3 = big2.divide(big1, 2, RoundingMode.HALF_UP);
		System.out.println(big3);	//1.00
```

**==比较BigDecimal==**

只能用==`compareTo()`==比较，返回值为负数、正数和0，分别表示小于，大于和等于





## 反射



### **三种方法获取`class`的`class`实例：**

一、如果有一个实例变量，可以通过该实例变量提供的`getClass()`方法获取：

```java
String s ="Hello";
Class cls = s.getClass();
```

二、如果知道一个`class`的完整类名，如果通过静态方法`class.forName()`获取：

```java
Class cls = Class.forName("java.lang.String");
```

三、直接通过一个`class`的静态变量`class`获取

```java
Class cls = String.class;
```

`class`实例在JVM中是唯一

如果获取到了一个`class`实例，就可以通过该实例创建对立类型的实例

```java
Class cls = String.class;
String s = (String)cls.newInstance();	//只能调用无参数构造方法
```

通过实例获取信息：

```java
public class Test {
	public static void main(String[] args) {
		Class cls = String.class;
		System.out.println(cls.getName());		//java.lang.String
	}
}
```

### 动态加载

JVM在执行Java程序的时候，并不是一次性把所有用到的class全部加载到内存，而是第一次需要用到class时才加载。当执行`Main.java`时，由于用到了`Main`，因此，JVM首先会把`Main.class`加载到内存。然而，并不会加载`Person.class`，除非程序执行到`create()`方法，JVM发现需要加载`Person`类时，才会首次加载`Person.class`。如果没有执行`create()`方法，那么`Person.class`根本就不会被加载。



### 访问字段

对应任意一个`object`实例，只要获取了`class`，就可以获取它的一切信息

- `Field getDeclaredField(name)`：根据字段名获取当前类的某个`field`（不包括父类）

- `Field[] getDeclaredFields()`：获取当前类的所有`field`（不包括父类）

  

一个`Field`对象包含一个字段的所有信息：

`getName():`返回字段名称

`getType():`返回字段类型

```java
public class Student {	private int a	};

Class cls = Student.class;
Field filed = cls.getDeclaredField("a");
	System.out.println(field.toString());	//private int temp.Student.a
	System.out.println(field.getName());	//a
	System.out.println(field.getType());	//int
```

#### 	**获取字段值**

利用反射拿到`Field`实例只是第一步，第二步是拿到实例对象的字段值

```java
Student student = new Student("lby",23);
		Class cls = student.getClass();
		Field field = cls.getDeclaredField("name");//拿到field实例
		field.setAccessible(true);//设置可以访问
		System.out.println(field.get(student));//lby
```

#### 	**设置字段值**

```java
Student student = new Student("lby",23);
		Class cls = student.getClass();
		Field field = cls.getDeclaredField("name");
			field.setAccessible(true);
			field.set(student,"FREE");
			System.out.println(student.getName());	//FREE
```



### 调用方法

- `Method getDeclaredMethod(name, Class...)`：获取当前类的某个`Method`（不包括父类）

- `Method[] getDeclaredMethods()`：获取当前类的所有`Method`（不包括父类）

```java
Class cls = Student.class;
System.out.println(cls.getDeclaredMethod("add",int.class));//方法名，参数类型
//public int temp.Student.add(int)
```

一个Method对象包含一个方法的所有信息：

`getName()`

`getReturnType()`

`getParameterTypes()`



获取到`method`对象，就可以对它进行调用

```JAVA
String s = "Hello World";
Method m = String.class.getMethod("substring",int.class);
String result = m.invoke(s,6);	//m援引s，相当于s调用m方法，参数为6。结果为World
```

和`Field`类似，对于非public方法，需要设置`Method.setAccessible(true)`才可以调用

反射同样遵循多态原则

```java
class Person{
    private void hello(){
        sout("hello person");
    }
}

class Student{
    private void hello(){
        sout("hello student");
    }
}

public class main{
    Method m = Person.class.getMethod("hello");
     m.setAccessible(true);
    m.invoke(new Student());	//从person获得hello方法，而后援引子类student调用hello，最后结果为“hello student”，遵循多态原则
}
```

### 调用构造方法

常用方法：`Person p = new Person()`

反射方法：`Person p = Person.class.newInstance()`

	缺点：只能调用public无参数构造方法



## 正则表达式

举个例子：要判断用户输入的年份是否是`20##`年，我们先写出规则如下：

一共有4个字符，分别是：`2`，`0`，`0~9任意数字`，`0~9任意数字`。

对应的正则表达式就是：`20\d\d`，其中`\d`表示任意一个数字。

把正则表达式转换为Java字符串就变成了`20\\d\\d`，注意Java字符串用`\\`表示`\`。

```java
       String regex = "20\\d\\d";
        System.out.println("2019".matches(regex)); // true
        System.out.println("2100".matches(regex)); // false
```

### 匹配规则

#### 匹配任意字符

例如，正则表达式`a.c`中间的`.`可以匹配一个任意字符，例如，下面的字符串都可以被匹配：

- `"abc"`，因为`.`可以匹配字符`b`；
- `"a&c"`，因为`.`可以匹配字符`&`；
- `"acc"`，因为`.`可以匹配字符`c`。

#### 匹配数字

如果我们只想匹配`0`~`9`这样的数字，可以用`\d`匹配。例如，正则表达式`00\d`可以匹配：

- `"007"`，因为`\d`可以匹配字符`7`；
- `"008"`，因为`\d`可以匹配字符`8`。

#### 匹配常用字符

用`\w`可以匹配一个字母、数字或下划线，w的意思是word。例如，`java\w`可以匹配：

- `"javac"`，因为`\w`可以匹配英文字符`c`；
- `"java9"`，因为`\w`可以匹配数字字符`9`；。
- `"java_"`，因为`\w`可以匹配下划线`_`。

#### 匹配空格字符

用`\s`可以匹配一个空格字符，注意空格字符不但包括空格``，还包括tab字符（在Java中用`\t`表示）。例如，`a\sc`可以匹配：

- `"a c"`，因为`\s`可以匹配空格字符``；
- `"a c"`，因为`\s`可以匹配tab字符`\t`。

#### 重复匹配

**修饰符`*`可以匹配任意个字符，包括0个字符。我们用`A\d*`可以匹配：**

- `A`：因为`\d*`可以匹配0个数字；
- `A0`：因为`\d*`可以匹配1个数字`0`；
- `A380`：因为`\d*`可以匹配多个数字`380`。

**修饰符`+`可以匹配至少一个字符。我们用`A\d+`可以匹配：**

- `A0`：因为`\d+`可以匹配1个数字`0`；
- `A380`：因为`\d+`可以匹配多个数字`380`。

但它无法匹配`"A"`，因为修饰符`+`要求至少一个字符。

**修饰符`?`可以匹配0个或一个字符。我们用`A\d?`可以匹配：**

- `A`：因为`\d?`可以匹配0个数字；
- `A0`：因为`\d?`可以匹配1个数字`0`。

但它无法匹配`"A33"`，因为修饰符`?`超过1个字符就不能匹配了。

如果我们想精确指定n个字符怎么办？用修饰符`{n}`就可以。`A\d{3}`可以精确匹配：

- `A380`：因为`\d{3}`可以匹配3个数字`380`。

如果我们想指定匹配n~m个字符怎么办？用修饰符`{n,m}`就可以。`A\d{3,5}`可以精确匹配：

- `A380`：因为`\d{3,5}`可以匹配3个数字`380`；
- `A3800`：因为`\d{3,5}`可以匹配4个数字`3800`；
- `A38000`：因为`\d{3,5}`可以匹配5个数字`38000`。

#### 匹配开头和结尾

用正则表达式进行多行匹配时，我们用`^`表示开头，`$`表示结尾。例如，`^A\d{3}$`，可以匹配`"A001"`、`"A380"`。

#### 匹配指定范围

如果我们规定一个7~8位数字的电话号码不能以`0`开头，应该怎么写匹配规则呢？`\d{7,8}`是不行的，因为第一个`\d`可以匹配到`0`。

使用`[...]`可以匹配范围内的字符，例如，`[123456789]`可以匹配`1`~`9`，这样就可以写出上述电话号码的规则：`[123456789]\d{6,7}`。

把所有字符全列出来太麻烦，`[...]`还有一种写法，直接写`[1-9]`就可以。

#### 或规则匹配

用`|`连接的两个正则规则是*或*规则，例如，`AB|CD`表示可以匹配`AB`或`CD`。



## 内部类的优点

- 一个内部类对象可以访问创建它的外部类对象的内容，包括私有数据！
- 内部类不为同一包的其他类所见，具有很好的封装性；
- 内部类有效实现了“多重继承”，优化 java 单继承的缺陷。



## **单例模式实现**

只能有一个实例

自己创建自己的唯一实例

必须为其他对象提供唯一的实例

### 	**懒汉：**

线程不安全

```java
public class Singleton {
    private static Singleton instance;
    private Singleton (){}

    public static Singleton getInstance() {
	if (instance == null) {
	    instance = new Singleton();
	}
	return instance;
    }
}
```

	**饿汉：**

```java
public class Singleton {  
    private static Singleton instance = new Singleton();  
    private Singleton (){}  
    public static Singleton getInstance() {  
    return instance;  
    }  
}
```

更好的实现方式是静态内部类	

**静态内部类：**

```java
public class Singleton {  
    private static class SingletonHolder {  
    private static final Singleton INSTANCE = new Singleton();  
    }  
    private Singleton (){}  
    public static final Singleton getInstance() {  
    return SingletonHolder.INSTANCE;  
    }  
} 
```



## 匿名类

重写一个类的方法，有两种方式：

一、写一个类继承这个类并重写需要重写的方法

二、使用匿名内部类的方式重写其方法



### 函数式接口

```java
interface It{	//函数式接口（只有一个抽象方法）
   void hi(String s);	//此接口只有个抽象方法hi(String s)
}

It  t = s->System.out.println(s);
		 t.hi("大家好!");//输出 大家好!
```



### 普通接口

```java
public interface MyService {
    public void invoke();
}

//将接口作为参数，然后调用接口里的方法
public static void test(MyService myService) {
  //执行打印的业务逻辑
    myService.invoke();
}

public static void main(String[] args) {
     test(new MyService() {
         @Override
         public void invoke() {
             for (int i = 0; i < 10; i++) {
                 System.out.println(i);
             }
         }
     });
}
```



### 使用匿名内部类创建线程

```java
new Thread() {
    public void run() {
        System.out.println("匿名内部类创建线程并启动");
    }

}.start();
```

### 使用匿名内部类初始化Map

```java
Map map = new HashMap(){
    {
        put("name", "张三");
        put("age", "24");
        put("sex", "man");
    }
};
```



## **如何自定义异常？**

1. 自定义一个编译期异常: 自定义类并继承于*`java.lang.Exception`*

2. 自定义一个运行时期的异常类: 自定义类并继承于*`java.lang.RuntimeException`*



```mermaid
classDiagram
 Collection*--Set
 Collection*--List
 class Collection{
 }
 class List{
 }
 class Set{
 }
 
 List*--ArrayList
 List*--LinkedList
 class ArrayList{
 }
 class LinkedList{
 }
 
 Set*--SortedSet
 SortedSet*--TreeSet
 Set*--HashSet
 HashSet<--LinkedHashSet
 class SortedSet{
 }
 class HashSet{
 }
 class LinkedHashSet{
 }
 class TreeSet{
 }
 
 class Map{
 }
 class SortedMap{
 }
 class TreeMap{
 }
 class HashMap{
 }
 class LinkedHashMap{
 }
 class HashTable{
 }
 
 Map*--HashMap
 Map*--HashTable
 Map*--SortedMap
 SortedMap<--TreeMap
 HashMap<--LinkedHashMap
 
 
```

# **`Collection:`**

## **`List:`**

**特性：**

- 元素可以重复

- **内部维护元素之间的顺序，是有序集合**(遍历时按照插入顺序遍历)
- 插入元素可以为null

```java
//添加
boolean add(E e)；
boolean addAll(Collection c)；

//获取
E get(int index);
Iterator iterator();

//删除
void clear();
E remove(int index);
boolean remove(Object o);
boolean removeAll(Collection e);

//修改
E set(int index, E element);

boolean isEmpty();
boolean contains(Object o);
int size();
Object[] toArray();
```



### **`ArrayList:`**

**实现方式：**数组

**优点：**查询和遍历快

**缺点：**插入和删除慢、线程不安全

实现`Cloneable`，得到了`clone()`方法，可以实现克隆功能



### **`LinkedList`**

**实现方式：**双向链表

**优点：**插入和删除快、提供了操作表头表尾元素的API，可以当作堆栈、队列和双向链表使用

**缺点：**随机访问和遍历慢

实现`Cloneable`，得到了`clone()`方法，可以实现克隆功能



## **`Set:`**

**特性：**

- 元素不可以重复，判断依据是对象的hashCode



### **`HashSet（hash表）:`**

**实现方式 :**hash表

**特性**：哈希表边存放的是**哈希值**。 存储元素的顺序并不是按照存入时的顺序（和 List 显然不同） 而是**按照哈希值来存的**所以取数据也是按照哈希值取得。元素的哈希值是通过元素的hashcode方法来获取的, HashSet首先判断两个元素的哈希值，如果哈希值一样，接着会比较equals方法 如果 equls结果为true ，HashSet就视为同一个元素。如果equals 为false就不是同一个元素



### **`LinkedHashSet:`**

**实现方式：**hash表和链表

哈希表决定了它元素是唯一的，而链表则保证了他是有序的（存储和取出顺序一致）

```java
LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>();
linkedHashSet.add(1);
linkedHashSet.add(9);
linkedHashSet.add(5);
linkedHashSet.add(3);
linkedHashSet.add(10);
linkedHashSet.add(2);
System.out.println(linkedHashSet.toString());

[1, 9, 5, 3, 10, 2]
```



### **`TreeSet（二叉树）:`**

**实现方式：**红黑树

**特点：**按照二叉树排序，遍历时也是按照中序方式遍历。<u>当TreeSet中要存入自己实现的类为元素时，该类必须实现`Comparable`接口重写`compareTo()`或者在创建Set时传入一个该类的Comparator比较器</u>

| 返回值 | API            | 描述                                                         |
| ------ | -------------- | ------------------------------------------------------------ |
| E      | `first()`      | 返回集合中序遍历第一个                                       |
| E      | `last()`       | 返回集合中序遍历最后一个                                     |
| E      | `pollFirst()`  | 删除集合中序遍历第一个并返回                                 |
| E      | `pollLast()`   | 删除集合中序遍历最后一个并返回                               |
| E      | `floor(E e)`   | 返回比传入元素小的所有元素中最大的一个。包含相等（因为传入的元素可能不在集合中，也可能在集合中，这个方法若传入元素在集合中则返回传入元素，如不在则返回比传入元素小的所有元素中最大的一个） |
| E      | `ceiling(E e)` | 返回比传入元素大的所有元素中最小的一个，包含相等             |
| E      | `lower(E e)`   | 返回比传入元素小的所有元素中最大的一个                       |
| E      | `higher(E e)`  | 返回比传入元素大的所有元素中最小的一个                       |

**中序遍历：**先左节点、后根节点、最后右节点

```java
TreeSet<Integer> treeSet = new TreeSet<>();
treeSet.add(1);
treeSet.add(9);
treeSet.add(5);
treeSet.add(3);
treeSet.add(10);
treeSet.add(2);
System.out.println("first->"+treeSet.first());
System.out.println("last->"+treeSet.last());
System.out.println("floor->"+treeSet.floor(3));
System.out.println("ceiling->"+treeSet.ceiling(3));
System.out.println("lower->"+treeSet.lower(3));
System.out.println("higher->"+treeSet.higher(3));
System.out.println(treeSet.toString());
for (Integer integer : treeSet) {
    System.out.println(integer);
}

first ->1
last ->10
floor ->3
ceiling ->3
lower ->2
higher ->5
结果都是按照中序遍历
[1, 2, 3, 5, 9, 10]
1
2
3
5
9
10
```



# **`Map`**

## **`HashMap（hash表）:`**

**实现方式：**数组+链表+红黑树



## **`LinkedHashMap（hash表和红黑树）:`**

就如`LinkedHashSet`，`LinkedHashMap`可以按照插入的顺序，进行遍历

## **`TreeMap（红黑树）:`**

就如`TreeSet`，`TreeMap`也是按照中序遍历

**注意：**如果key为`String`类型，`String`类型的`compareTo()`方法，比较的是字典顺序，需要匿名构造比较器，转化成`Integer`类型，再进行比较

```java
TreeMap<String, Object> treeMap = new TreeMap<>(new Comparator<String>() {
    @Override
    public int compare(String o1, String o2) {
        Integer s1 = Integer.parseInt(o1);
        Integer s2= Integer.parseInt(o2);
        return s1.compareTo(s2);
    }
});
treeMap.put("1", 1);
treeMap.put("9", 9);
treeMap.put("5", 5);
treeMap.put("3", 3);
treeMap.put("10", 10);
treeMap.put("2", 2);

System.out.println("10".compareTo("2"));
System.out.println(treeMap.toString());

-1
{1=1, 2=2, 3=3, 5=5, 9=9, 10=10}
```
