### Checked Exception/ Unchecked Exception（runtime exception&Error）

- 抛出checked exception是期望调用者处理它，维持程序继续运行
- unchecked exception代表着不可恢复的错误
- 你实现的所有 unchecked 可抛出项都应该继承 RuntimeException（直接或间接）
- API 设计人员常常忘记异常是成熟对象，可以为其定义任意方法。此类方法的主要用途是提供捕获异常的代码，并提供有关引发异常的附加信息
- 为可恢复条件抛出 checked 异常，为编程错误抛出 unchecked 异常
- 避免不必要地使用checked异常

checked异常强制程序员处理问题，提高了可靠性。但过度使用，会带来负担。

可以把checked异常更改为unchecked异常

```java
// Invocation with checked exception
try {
    obj.action(args);
}
catch (TheCheckedException e) {
    ... // Handle exceptional condition
}
```

into

```
// Invocation with state-testing method and unchecked exception
if (obj.actionPermitted(args)) {
    obj.action(args);
}
else {
    ... // Handle exceptional condition
}
```

如果对象将在缺少外部同步的情况下被并发访问，或者可被外界改变状态，这种重构就是不恰当的，因为在 actionPermitted 和 action 这两个调用的间隔，对象的状态有可能会发生变化。如果单独的 actionPermitted 方法必须重复 action 方法的工作，出于性能的考虑，这种 API 重构就不值得去做。

总之，如果谨慎使用，checked 异常可以提高程序的可靠性；当过度使用时，它们会使 API 难以使用。如果调用者不应从失败中恢复，则抛出 unchecked 异常。如果恢复是可能的，并且你希望强制调用者处理异常条件，那么首先考虑返回一个 Optional 对象。只有当在失败的情况下，提供的信息不充分时，你才应该抛出一个 checked 异常。

- 抛出能用抽象解释的异常

当一个方法抛出一个与它所执行的任务没有明显关联的异常时，这是令人不安的。这种情况经常发生在由方法传播自低层抽象抛出的异常。它不仅令人不安，而且让实现细节污染了上层的 API。如果高层实现在以后的版本中发生变化，那么它抛出的异常也会发生变化，可能会破坏现有的客户端程序。

为了避免这个问题，**高层应该捕获低层异常，并确保抛出的异常可以用高层抽象解释。** 这个习惯用法称为异常转换：

```
// Exception Translation
try {
    ... // Use lower-level abstraction to do our bidding
} catch (LowerLevelException e) {
    throw new HigherLevelException(...);
}
```

- Include failure capture information in detail messages

当程序由于未捕获异常而失败时，系统可以自动打印出异常的堆栈跟踪。堆栈跟踪包含异常的字符串表示，这是调用其 toString 方法的结果。这通常包括异常的类名及其详细信息。通常，这是程序员或管理员在调查软件故障时所掌握的唯一信息。如果失败不容易重现，想获得更多的信息会非常困难。因此，异常的 toString 方法返回尽可能多的关于失败原因的信息是非常重要的。换句话说，由失败导致的异常的详细信息应该被捕获，以便后续分析。

------



