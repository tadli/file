

# 介绍

Markdown可以导出成多种文档格式

Markdown的文档格式为：`.md`或者`.markdown`

Typora官网： https://typora.io/



# 语法(Mac系统)

**标题：**`com+(1,2,3,4,5,6)`

**将标题转化成段落：**`com+0`

**粗体：**`com+B`

**斜体：**`com+I`

**分割线：**`***`或者`+++`或者`---`

**删除线：**`~~文本~~`（英文输入法）

**表格：**`option+com+T`

**引用：**`option+com+Q`

**有序列表：**`option+com+O`

**无序列表：**`option+com+U`

**任务列表：**`option+com+X`

**上引用：**`^ ^`

**写HTML：**`<font color="blue">color Test </font>`/ `<p align="center">eqeq</p>`

**插入emoji：**`:carb:`



# 画图

## **流程图：**

````flow`

```flow
start=>start: start
end=>end: 就职
end2=>end: 继续找工作
operation=>operation: 面试
condition=>condition: 通过/不通过?
start->operation->condition(yes)->end
start->operation->condition(no)->end2
end2->operation
```

## **时序图:**

````sequence`

```sequence
title: test sequence 

A->B: Hello?
B->C: Fuck me!
Note right of B: This's the right side of B
Note left of A: This's the left side of B
Note over B,C: They are lovers.
B->A: I'm OK
A-->>B: Are you sure? 

participant D
D->A:  Who are you?
```

## **类图：**

````mermaid classDiagram`

```mermaid
classDiagram

A<-- B 
A<-- C
C<-- B

class A{
	-String name
	-Integer age
	+String getName()
	+Integer getAge()
}
```

## **饼图：**

````mermaid pie`

```mermaid
pie 
	title How to write a pie?
	"A": 30
	"B": 23
	"C": 32
```



















