```shell
#!/bin/bash
```



## Variables

:warning:No space permitted on either side of `=` sign when initializing variables

```bash
PRICE_PER_APPLE=5
MyFirstLetters=ABC
greeting='Hello        world!'
```

A backslash " \ " is used to escape special character meaning

```shell
PRICE_PER_APPLE=5
echo "The price of an Apple today is: \$HK $PRICE_PER_APPLE"
#The price of an Apple today is: $HK 5
```

Encapsulating the variable name with ${} is used to avoid ambiguity

```shell
MyFirstLetters=ABC
echo "The first 10 letters in the alphabet are: ${MyFirstLetters}DEFGHIJ"
```

Encapsulating the variable name with "" will preserve any white space values

```shell
greeting='Hello        world!'
echo $greeting" now with spaces: $greeting"
#Hello world! now with spaces: Hello        world!
```

Variables can be assigned with the value of a command output. This is referred to as substitution. Substitution can be done by encapsulating the command with **``** (known as back-ticks) or with **$()**

```shell
FILELIST=`ls`
FileWithTimeStamp=/tmp/my-dir/file_$(/bin/date +%Y-%m-%d).txt
```



## Arrays

An array is initialized by assign space-delimited values enclosed in ()

```shell
my_array=(apple banana "Fruit Basket" orange)
new_array[2]=apricot
```

```shell
my_array=(apple banana "Fruit Basket" orange)
echo  ${#my_array}                   
# 5
echo  ${my_array[@]}   
# apple banana Fruit Basket orange

echo  ${#my_array[@]}                   
# 4
```



## Basic Operators

### Arithmetic Operators

Simple arithmetics on variables can be done using the arithmetic expression: $((expression))

```shell
A=3
B=$((100 * $A + 5)) # 305
```



## Basic String Operations

### **String length**

```shell
STRING="this is a string"
echo ${#STRING}            
# 16
```

### **Index**

Find the numerical position in `$STRING` of **any single character** in `$SUBSTRING` that matches. 

Note that the 'expr' command is used in this case.

```shell
STRING="this is a string"
SUBSTRING="hat"
expr index "$STRING" "$SUBSTRING"    
# 1 is the position of the first 't' in $STRING
```

### Substring Extraction

Extract substring of length `$LEN` from `$STRING` starting after position `$POS`.

Note that first position is 0.

```bash
STRING="this is a string"
POS=1
LEN=3
echo ${STRING:$POS:$LEN}   # his
```

If ``:$LEN` is omitted, extract substring from`$POS` to end of line

### Substring Replacement

```shell
STRING="to be or not to be"
```

Replace first occurrence of substring 

```shell
echo ${$STRING[@]/be/eat}
#to eat or not to be
```

Replace all occurrences of substring

```shell
$ echo ${STRING[@]//be/eat}
#to eat or not to eat
```

 Delete all occurrences of substring (replace with empty string)

```shell
echo ${STRING[@]//not/ }
```

Replace occurrence of substring if at the end of $STRING

```shell
echo ${STRING[@]/%be/eat}
```



## Decision Making

`if [ expression ]; then`

`fi`

```shell
NAME="George"
if [ "$NAME" = "John" ]; then
  echo "John Lennon"
elif [ "$NAME" = "George" ]; then
  echo "George Harrison"
else
  echo "This leaves us with Paul and Ringo"
fi
```

### Types of numeric comparisons

```shell
comparison    Evaluated to true when
$a -lt $b    $a < $b
$a -gt $b    $a > $b
$a -le $b    $a <= $b
$a -ge $b    $a >= $b
$a -eq $b    $a is equal to $b
$a -ne $b    $a is not equal to $b
```

### Types of string comparisons

```shell
comparison    Evaluated to true when
"$a" = "$b"     $a is the same as $b
"$a" == "$b"    $a is the same as $b
"$a" != "$b"    $a is different from $b
-z "$a"         $a is empty
```

### Logical combinations

```shell
if [[ $VAR_A[0] -eq 1 && ($VAR_B = "bee" || $VAR_T = "tee") ]] ; then
    command...
fi
```

### case structure

```shell
case "$variable" in
    "$condition1" )
        command...
    ;;
    "$condition2" )
        command...
    ;;
esac
```

```shell
mycase=1
case $mycase in
    1) echo "You selected bash";;
    2) echo "You selected perl";;
    3) echo "You selected phyton";;
    4) echo "You selected c++";;
    5) exit
esac
```

## Loops

```shell
# basic construct
for arg in [list]; do
 command(s)...
done
```

```shell
# basic construct
while [ condition ]; do
 command(s)...
done
```

For example:

```shell
NAMES=(Joe Jenny Sara Tony)
for N in ${NAMES[@]} ; do
  echo "My name is $N"
done
```

```shell
COUNT=4
while [ $COUNT -gt 0 ]; do
  echo "Value of count is: $COUNT"
  COUNT=$(($COUNT - 1))
done
```



## Special variables

