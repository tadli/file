# find:

```shell
find / -name 'b'		查询根目录下（包括子目录），名以b的目录和文件；
find / -name 'b*'		查询根目录下（包括子目录），名以b开头的目录和文件； 
```



# 重命名/剪切：

```shell
mv 原先目录 文件的名称   mv tomcat001 tomcat 
mv	/aaa /bbb		    将根目录下的aaa目录，移动到bbb目录下(假如没有bbb目录，则重命名为bbb)；
mv	bbbb usr/bbb		将当前目录下的bbbb目录，移动到usr目录下，并且修改名称为bbb；
mv	bbb usr/aaa			将当前目录下的bbbb目录，移动到usr目录下，并且修改名称为aaa；
```



# 复制：

```shell
cp -r /aaa /bbb			将/目录下的aaa目录复制到/bbb目录下，在/bbb目录下的名称为aaa
cp -r /aaa /bbb/aaa		将/目录下的aa目录复制到/bbb目录下，且修改名为aaa;
```



# 删除：

```shell
#强删
rm -rf /bbb			强制删除/目录下的bbb目录。如果bbb目录中还有子目录，也会被强制删除，不会提示；

rm -r /bbb			普通删除。会询问你是否删除每一个文件
```





# 文件操作

```shell
rm -r a.java		删除当前目录下的a.java文件（每次回询问是否删除y：同意）

rm -rf a.java		强制删除当前目录下的a.java文件
rm -rf ./a*			强制删除当前目录下以a开头的所有文件；
rm -rf ./*			强制删除当前目录下所有文件（慎用）；

touch testFile
```



# 文件内容操作

```shell
vim a.java   	进入一般模式
i(按键)   		进入插入模式(编辑模式)
ESC(按键)  		退出
:wq 			保存退出（shift+：调起输入框）
:q！			不保存退出（shift+：调起输入框）（内容有更改）(强制退出，不保留更改内容)
:q				不保存退出（shift+：调起输入框）（没有内容更改）
```



# 文件内容查看

```shell
cat a.java		查看a.java文件的最后一页内容；
more a.java		从第一页开始查看a.java文件内容，按回车键一行一行进行查看，
                    按空格键一页一页进行查看，q退出；
less a.java		从第一页开始查看a.java文件内容，按回车键一行一行的看，
                    按空格键一页一页的看，支持使用PageDown和PageUp翻页，q退出；
```

# 查看日志

```shell
tail -10 a.java			(实时查看)查看a.java文件的后10行内容；

head a.java				查看a.java文件的前10行内容；
tail -f a.java			查看a.java文件的后10行内容；
head -n 7 a.java		查看a.java文件的前7行内容；
tail -n 7 a.java		查看a.java文件的后7行内容；

#email.log文件中查询包含error的行
grep 'error' email.log
```

# 文件内部搜索

```shell
grep under 123.txt			在123.txt文件中搜索under字符串，大小写敏感，显示行；
grep -n under 123.txt		在123.txt文件中搜索under字符串，大小写敏感，显示行及行号；
grep -v under 123.txt		在123.txt文件中搜索under字符串，大小写敏感，显示没搜索到的行；
grep -i under 123.txt		在123.txt文件中搜索under字符串，大小写敏感，显示行；
grep -ni under 123.txt		在123.txt文件中搜索under字符串，大小写敏感，显示行及行号；
```

# 终止操作

```shell
ctrl+z
ctrl+c
```

# 重定向

```shell
#可以使用 > 或 < 将命令的输出的命令重定向到test.txt文件中（没有则创建一个）
echo 'Hello World' > /root/test.txt
```



# 解压

```shell
#压缩
tar -cvf start.tar ./*					//将当前目录下的所欲文件打包压缩成haha.tar文件
tar -zcvf start.tar.gz ./*				//将当前目录下的所欲文件打包压缩成start.tar.gz文件

#解压
tar -xvf start.tar						//解压start.tar压缩包，到当前文件夹下；
tar -zxvf start.tar.gz					//解压start.tar.gz压缩包，到当前文件夹下；

#压缩zip
zip lib.zip tomcat.jar							//将单个文件压缩(lib.zip)
unzip file1.zip  								//解压一个zip格式压缩包
```

# 文件描述

```shell
 R:Read  w:write  x: execute执行
-rw-r--r-- 1 root root  34942 Jan 19  2018 bootstrap.jar
前三位代表当前用户对文件权限：可以读/可以写/不能执行
中间三位代表当前组的其他用户对当前文件的操作权限：可以读/不能写/不能执行
后三位其他用户对当前文件权限：可以读/不能写/不能执行
```

# 更改文件权限

```shell
chmod u+x web.xml （---x------）		为文件拥有者（user）添加执行权限；
chmod g+x web.xml （------x---）		为文件拥有者所在组（group）添加执行权限；
chmod 111 web.xml  （---x--x--x）	    为所有用户分类，添加可执行权限；
chmod 222 web.xml （--w--w--w-）		为所有用户分类，添加可写入权限；	
chmod 444 web.xml （-r--r--r--）		为所有用户分类，添加可读取权限；

#设置所有人可以读写及执行
chmod 777 file -R(等价于 chmod u=rwx,g=rwx,o=rwx file 或 chmod a=rwx file)
```

# 目录结构

```shell
根目录：/

├── bin -> usr/bin # 可执行二进制文件的目录，如常用的命令ls、tar、mv、cat等
├── boot # 内核及引导系统程序所在的目录
├── etc # 配置文件默认路径、服务启动命令存放目录						
├── home # 用户家目录，root用户为/root									
├── lib -> usr/lib # 系统使用的函数库的目录
├── mnt # 临时挂载设备目录
├── opt # 自定义软件安装存放目录													
├── proc # 进程及内核信息存放目录
├── root # Root用户家目录													
├── run # 系统运行时产生临时文件，存放目录
├── sbin -> usr/sbin # 系统管理命令存放目录
├── srv # 服务启动之后需要访问的数据目录
├── sys # 系统使用目录
├── tmp # 临时文件目录
├── usr # 应用程序存放目录，/usr/bin 存放应用程序，/usr/share 存放共享数据，/usr/lib 存放不能直接运行的，却是许多程序运行所必需的一些函数库文件。/usr/local: 存放软件升级包。/usr/share/doc: 系统说明文件存放目录。/usr/share/man: 程序说明文件存放目录			
└── var # 存放内容易变的文件的目录
```





# 基础命令

```shell
clear #清理屏幕
sync	#同步数据
shutdown	#关机
reboot #重启

pwd				查看当前工作目录
clear 			清除屏幕
cd ~			当前用户目录
cd /			根目录
cd -			上一次访问的目录
cd ..			上一级目录
ll				查看当前目录下内容（LL的小写）
ls -a 			显示隐藏文件夹

mkdir aaa		在当前目录下创建aaa目录，相对路径；
mkdir ./bbb		在当前目录下创建bbb目录，相对路径；
mkdir /ccc		在根目录下创建ccc目录，绝对路径；

history N		显示最近N条命令
history -c		清除所有的历史记录
history -w xxx.txt	保存历史记录到文本xxx.txt
```



# 文件



## 文件权限

```shell
读权限（r）
写权限（w）
可执行权限（x）

-rw-------  1 vagrant vagrant     8396 Feb 13 07:31  .viminfo
drwxr-xr-x  3 vagrant vagrant     4096 Mar  6  2022  .vscode/
-rw-r--r--  1 vagrant vagrant      385 Dec 17  2021  .wget-hsts
	d 表示文件夹
	- 表示普通文件
	后9个字母分别表示三组权限【文件所有者】、【用户组】、【其他用户】

chmod修改权限
	u 表示文件所有者
	g 表示同一个group里的其他人，即用户组
	o other，其他用户
	a 表示以上三者

	+ 赋予权限
	- 删除权限
	= 设定权限
	
	同时，rwx也可以用数字替代
	
	chmod 777 【fileName] -R
	给所有用户增加读写执行权限，-R表示递归目录下的所有文件
```



## 查看文件

```shell
cat 由第一行开始显示文件内容 -b 显示行号，不显示空白行 -n 显示行号，显示空白行
tac 从最后一行开始显示
head -n 只看头几行
tail -n 只看尾巴几行
```



## 操作文件

**输出重定向**

```shell
ls > test.txt  #如果文件存在，则覆盖；不存在，则创建
```

**创建文件**

```shell
touch test.txt	#如果文件不存在，可以创建一个空白文件；如果文件已存在，可以修改文件的末次修改日期
```

**删除文件**

```shell
rm -f #强制删除文件
rm -r #递归删除目录下的内容
```

**复制文件**

```shell
cp 【源文件】 【目标文件】
cp -r 【源文件】 【目标文件】 #若给出的源文件是目录文件，则cp将递归复制该目录下的所有子目录和文件，目标文件必须为一个目录名
```

**移动文件**

```shell
mv 【源文件】 【目标文件】
```

**Tree**

```shell
tree 【目录】
```

**文本搜素**

```shell

```





