# Spring Data JPA

**什么是JPA**

```xml
JPA的全称是Java Persistence API，是SUN公司提出的一种ORM接口规范，Object Relational Mapping，ORM框架有Hibernate。SpringDataJPA默认的实现就是Hibernate

规范：
	1.ORM映射元数据，通过XML/注解方式，描述对象和表之间的关系
	2.JPA的API，用来操作实体对象，执行CRUD
	3.JPQL，通过面向对象的查询语句进行查询数据
```

# 配置

```yaml
spring:
  datasource:
    url: jdbc:mysql://localhost:3306/springbootJPA?serverTimezone=UTC&useSSL=false
    driver-class-name: com.mysql.cj.jdbc.Driver
    password: 123456
    username: root

  jpa:
    hibernate:
      #自动建表
      ddl-auto: update
    #开启日志
    show-sql: true
```



# JPA注解

## **实体类和表的映射关系**

`@Entity`:声明实体类

`@Table(name = "jpa_user")`:声明实体类和表的映射关系

## **成员变量和表字段的映射关系**

`@Id`:声明主键

`@GeneratedValue(strategy = GenerationType.IDENTITY)`:主键声明策略，自增策略

`@Column(name = "id")`:和表字段映射



**主键生成策略**

`GenerationType.IDENTITY`:自增，mysql

`GenerationType.SEQUENCE`:序列，oracle

`GenerationType.TABLE`: jpa提供的机制，通过一张数据库表的形式帮助实现主键自增

`GenerationType.AUTO`：jpa自动选择合适的主键生成策略



# JPA的API

Dao层接口需要继承两个接口**`JpaRepository`**和**`JpaSpecificationExecutor`**

```java
public interface UserRepository extends JpaRepository<T,ID>, JpaSpecificationExecutor<T> 
  
JpaRepository提供基础的增删改查
JpaSpecificationExecutor提供复杂查询和分页
```



**`save()`和`saveAndFlush()`**

```
save方法不会立刻提交事务
saveAndFlush会立刻提交事务
```



**`save()的两种使用方式`**

```
第一种方式是增加

第二种方式是更新，先获取对象，然后将字段一个个设置值，然后使用save方法更新。缺陷是：如果有的字段没有更新，会自动设置成null。所以最好使用JPQL语句
```



**`getOne()`和`findOne()`的区别**

```
findOne是立即加载，而getOne是延迟加载

getOne返回的是一个引用，即代理对象，什么时候用，什么时候查询。当getOne查询不到结果时会抛出异常。需要用到事务注解：	@Transactional(rollbackFor = Exception.class)
```



**`existsById()`的用处**

```
判断是否存在
```



# JPQL

## **面向对象**

### 查询

```java
//将JPQL语句配置到接口上的方法，通过注解@Query，面向对象写SQL
@Query(value = "from User where name = ?1")
List<User> findByName(String name);

//测试方法
List<User> lby = userRepository.findByName("lby");
for (User user:lby) {
    System.out.println(lby.toString());
}
```

### **更新**

==Jpa实现更新/删除操作：需要手动添加事务的支持，默认执行结束后回滚事务，需要手动关闭==

**`@Modifying`**表示这个更新操作

```java
@Query(value = "update User set name = ?2 where id = ?1")
@Modifying
Integer update(Integer id, String name);

@Test
@Transactional(rollbackOn = Exception.class)
@Rollback(value = false)
void test002() throws Exception {
    Integer update = userRepository.update(1, "FR333");
}
```



## **原生SQL语句实现查询**

```java
@Query(value = "select * from jpa_user where name=?1",nativeQuery = true)
List<User> queryByName(String name);
```



## **方法名称规则查询**

```java
对JPQL的封装，只需要按照规定的命名规则，自动生成SQL
User findByName(String name);  

//模糊查询
User findByNameLike(String name);
//模糊查询测试方法
User fr = userRepository.findByNameLike("%R%");


//多条件查询
User findByNameLikeAndId(String name, int id);
//多条件查询测试方法
final User fr = userRepository.findByNameLikeAndId("%R%", 1);
```

## JpaSpecificationExecutor（动态查询）

```java
//查询一个
Optional<T> findOne(@Nullable Specification<T> var1);
//查询全部
List<T> findAll(@Nullable Specification<T> var1);
//分页，返回值pageBean
Page<T> findAll(@Nullable Specification<T> var1, Pageable var2);
//查询全部，排序参数
List<T> findAll(@Nullable Specification<T> var1, Sort var2);
//统计查询
long count(@Nullable Specification<T> var1);
```

### `Specification(查询条件)`

`自定义Specification实现类：`

`root:`获得属性

`CriteriaBuilder:`查询的构造器，封装了很多的查询条件

`Sort.by(Sort.Direction.Desc,"id"):`排序方向和排序属性

`Page.of(<page>,<size>):`分页

#### **多条件查询**

```java
/**
用root获得属性
用criteriaBuilder构造查询条件
将criteriaBuilder返回
*/
@Test
void test004() {
    Specification<User> spec = new Specification<User>() {
        @Override
        public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
            Path age = root.get("age");
            Path height = root.get("height");
            Predicate equal = criteriaBuilder.equal(age, "23");
            Predicate equal1 = criteriaBuilder.equal(height, "177");
            Predicate predicate = criteriaBuilder.and(equal, equal1);
            return predicate;
        }
    };

    List<User> users = userRepository.findAll(spec);
    for (User user:users) {
        System.out.println(user.toString());
    }
}
```

#### 模糊查询

```java
void test005() {
  Specification<User> spec = new Specification<User>() {
      @Override
      public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
          Path name = root.get("name");
          Path height = root.get("height");
          Predicate equal = criteriaBuilder.like(name.as(String.class), "%"+"by");
          Predicate equal1 = criteriaBuilder.equal(height, 177);
          Predicate predicate = criteriaBuilder.and(equal, equal1);
          return predicate;
      }
  };

  List<User> users = userRepository.findAll(spec);
  for (User user:users) {
      System.out.println(user.toString());
  }
}
```

#### 排序

```java
@Test
void test006() {
  Specification<User> spec = new Specification<User>() {
      @Override
      public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
          Path name = root.get("name");
          Path height = root.get("height");
          Predicate equal = criteriaBuilder.like(name.as(String.class), "%" + "by");
          Predicate equal1 = criteriaBuilder.equal(height, 177);
          Predicate predicate = criteriaBuilder.and(equal, equal1);
          return predicate;
      }
  };

  List<User> users = userRepository.findAll(spec, Sort.by(Sort.Direction.DESC,"id"));
  for (User user : users) {
      System.out.println(user.toString());
  }
}
```

#### 分页

```java
@Test
void testPage() {
    Specification spec = new Specification() {
        @Override
        public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
            Path height = root.get("height");
            Predicate predicate = criteriaBuilder.greaterThanOrEqualTo(height, 100);
            return predicate;
        }
    };

    Page all = userRepository.findAll(spec, PageRequest.of(2, 5));
    //获得查询结果
    List<User> list = all.getContent();

    for (User user : list) {
        System.out.println(user.toString());
    }
    //get()可以获得一个流
    Object[] array = all.get().toArray();
    System.out.println(Arrays.toString(array));
}
```



# 多表操作

```
一对多：
	​	一的一方：主表	
	​	多的一方：从表
	
多对多：通过中间表，中间表最少有两个字段组成，两个字段作为外键指向两张表的主键，它们又组成了联合主键

实体类的关系：
	​包含关系：用实体类种的包含关系描述表关系
	
	Constraint fk_test FOREIGN KEY (主表_id) REFERENCES （从表.id）
```

## **一对多**

```java
/*
1.在多的一方，也就是从表维护关系
@ManyToOne(targetEntity = Company.class,fetch = FetchType.LAZY)

2.在一的一方，也就是主表设置级联
 @OneToMany(mappedBy = "company",cascade = {CascadeType.ALL})
 
3.重写toString()方法
*/


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "company")
/**
* 切记重写toString和hashCode，否则会导致无线递归*/
public class Company {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "company_id")
  private Integer company_id;
  @Column(name = "company_name")
  private String company_name;
  //主表放弃维护关系
  @OneToMany(mappedBy = "company",cascade = {CascadeType.ALL})
  private Set<Employee> employees;

  @Override
  public String toString() {
      return "Company{" +
              "company_id=" + company_id +
              ", company_name='" + company_name + '\'' +
              '}';
  }

  @Override
  public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Company company = (Company) o;
      return company_id.equals(company.company_id) &&
              company_name.equals(company.company_name);
  }

  @Override
  public int hashCode() {
      return Objects.hash(company_id, company_name);
  }
}

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "employee")
public class Employee {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "employee_Id")
  private Integer employee_Id;
  @Column(name = "employee_name")
  private String employee_name;
	
  //从表维护关系  
  @ManyToOne(targetEntity = Company.class,fetch = FetchType.LAZY)
  private Company company;

  @Override
  public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Employee employee = (Employee) o;
      return employee_Id.equals(employee.employee_Id) &&
              employee_name.equals(employee.employee_name);
  }

  @Override
  public int hashCode() {
      return Objects.hash(employee_Id, employee_name);
  }

  @Override
  public String toString() {
      return "Employee{" +
              "employee_Id=" + employee_Id +
              ", employee_name='" + employee_name + '\'' +
              '}';
  }
}
```

**双向一对多：在多的一方维护关联关系，而1的一方不维护关系**

hibernate中@ManyToOne默认是立即加载，@OneToMany默认是懒加载

`@OneToMany `：配置一对多关系，mappedBy 属性值为主表实体类在从表实体类中对应的属性名。

`@ManyToOne `：配置多对一关系，targetEntity 属性值为主表对应实体类的字节码。

`@JoinColumn`：配置外键关系，name 属性值为从表主键（外键名称），referencedColumnName 属性值为主表主键名称。

`fetch`属性是用来修改加载策略的



## 级联

**删除主表的时候，如果有外键关联它，则无法删除数据**

==级联添加/级联删除==：操作一个对象的同时操作它的关联对象

1. 需要区分操作主体
2. 需要在操作主体的实体类上，添加级联属性，添加到多表映射关系的注解上
3. cascade，配置级联

```java
@OneToMany(mappedBy = "student",cascade = CascadeType.ALL)
CascadeType.ALL :所有操作都有级联
  				 .MERGE:更新
           .PERSIST:保存
           .REMOVE:删除
```

## 多对多

```java
/*
一个用户可以有多个角色
一个角色可以有多个用户

1.在有选择权的一方维护关系，并且在有选择权的一方设置级联
@ManyToMany(targetEntity = Role.class, cascade = {CascadeType.ALL})
@JoinTable(
	name = "user_role_relation",
  joinColumns = {
  		@JoinColumn(name = "relation_user_id", referencedColumnName = "user_id")},
  inverseJoinColumns = {
      @JoinColumn(name = "relation_role_id", referencedColumnName = "role_id")}
)

2.在没有选择权的一方仅仅做映射
@ManyToMany(mappedBy = "roles")
  
3.重写toString()方法
*/
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "user")
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer user_id;
  @Column(name = "user_name")
  private String user_name;

  @ManyToMany(targetEntity = Role.class, cascade = {CascadeType.ALL})
  @JoinTable(
          name = "user_role_relation",
          joinColumns = {
                  @JoinColumn(name = "relation_user_id", referencedColumnName = "user_id")},
          inverseJoinColumns = {
                  @JoinColumn(name = "relation_role_id", referencedColumnName = "role_id")}
  )
  private Set<Role> roles = new HashSet<>();

  @Override
  public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      User user = (User) o;
      return Objects.equals(user_id, user.user_id) &&
              Objects.equals(user_name, user.user_name);
  }

  @Override
  public int hashCode() {
      return Objects.hash(user_id, user_name);
  }

  @Override
  public String toString() {
      return "User{" +
              "user_id=" + user_id +
              ", roles=" + roles +
              '}';
  }
}

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "role")
public class Role {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer role_id;
  @Column(name = "role_name")
  private String role_name;

  @ManyToMany(mappedBy = "roles")
  private Set<User> users = new HashSet<>();

  @Override
  public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Role role = (Role) o;
      return role_id.equals(role.role_id) &&
              role_name.equals(role.role_name);
  }

  @Override
  public int hashCode() {
      return Objects.hash(role_id, role_name);
  }

  @Override
  public String toString() {
      return "Role{" +
              "role_id=" + role_id +
              ", role_name='" + role_name + '\'' +
              '}';
  }
}
```



