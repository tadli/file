Git有四个工作区域：

- 工作目录（Working Directory）
- 暂存区（Stage）
- 本地仓库（Repository）
- 远程git仓库（remote Directory）

![731659022327_.pic](/Users/Typora_Pictures/:Users:xiaobing:Desktop:731659022327_.png)

git管理的文件有三种状态：已修改modified、已暂存staged、已提交committed



```shell
git add . #添加所有文件
git status [fileName]	#查看文件状态
git status #查看所有文件状态
```



> **忽略文件**

数据库文件/临时文件/设计文件，这些文件不需要纳入版本控制

在主目录建立`.gitignore`文件，规则如下：

```shell
*.txt #忽略所有txt文件
build/	#build/目录下所有文件
```

```shell
#切换版本号
git checkout 1f829623b4c0c26364c175f89b3065fc381c708

#查看提交记录
git log

#HEAD向上移动1个版本，或者n个版本

git checkout master^

git checkout master~2

#分支移动几个版本/指定位置
git branch -f master HEAD^

#回退上版本
git revert HEAD^

#回退本版本修改前的状态
git revert HEAD

#让当前分支变成任意分支的一个版本
git cherry-pick <版本号>

#删除分支
git branch -d <分支名>
```



**git提交代码起冲突了怎么办？**

我一般放弃本地代码，`git checkout`恢复文件，然后`git pull`更新代码，再添加自己的修改



git reset <paths> 是 git add <paths> 的逆操作