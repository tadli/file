### **比较数组元素是否相等:**

`Arrays.equals(arr1,arr2);`

equals比较的是两个对象的地址，但Arrays重写了equals，所以可以比较数组里的值



### **截取数组：**

`Arrays.copyOf()`

`Arrays.copyOfRange()`

```java
int[] arr = {1,2,3,4,5};
int[] newArr = Arrays.copyOf(arr,3);	//1,2,3
int[] newArr2 = Arrays.copyOfRange(arr,1,3);//2,3
```



## 数组和List之间的转换

List 转Array，必须使用集合的 `toArray(T[] array)`

```java
String[] array = list.toArray(new String[list.size()])
```

Array转List，返回值是一个内部类的ArrayList，而不是java.util.ArrayList，因此不能使用修改集合的相关方法

```java
ArrayList<String> list = new ArrayList<>(Arrays.asList(strArray));
```

