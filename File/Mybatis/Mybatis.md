

## 简介

Mybatis 是⼀个半 ORM（对象关系映射）框架，它内部封装了 JDBC，开发时只需要关注 SQL 语句本⾝，不需要花费精⼒去处理加载驱动、创建连接、创建statement 等繁杂的过 程。程序员直接编写原⽣态 sql，可以严格控制 sql 执⾏性能，灵活度⾼

MyBatis 可以使⽤ XML 或注解来配置和映射原⽣信息，将 POJO 映射成数据库中的记 录，避免了⼏乎所有的 JDBC 代码和⼿动设置参数以及获取结果集

**缺点：**

SQL语句的编写⼯作量较⼤，尤其当字段多、关联表多时，对开发⼈员编写SQL语句的功 底有⼀定要求

 SQL语句依赖于数据库，导致数据库移植性差，不能随意更换数据库



为什么说Mybatis是半⾃动ORM映射⼯具？**它与全⾃动的区别在哪⾥？**

Hibernate属于全⾃动ORM映射⼯具，使⽤Hibernate查询关联对象或者关联集合对象时， 可以根据对象关系模型直接获取，所以它是全⾃动的。 ⽽Mybatis在查询关联对象或关联集合对象时，需要⼿动编写SQL来完成，所以，被称之为半⾃动ORM映射⼯具。



## 第一个Mybatis程序

环境：

```sql
CREATE DATABASE Mybatis_test;
use Mybatis_test
CREATE TABLE user(
    -> id INT(20) NOT NULL,
    -> name VARCHAR(20) DEFAULT NULL,
    -> password VARCHAR(20) DEFAULT NULL,
    -> PRIMARY KEY (id));
show tables;
desc user;
INSERT INTO user 
VALUES(1,"lby","123456"),
			(2,"lby2","123456"),
			(3,"lby3","123456");

```

创建项目：

1.编写核心配置文件XML

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">

<configuration>
    <environments default="development">
        <environment id="development">
            <transactionManager type="JDBC"/>
            <dataSource type="POOLED">
                <property name="driver" value="com.mysql.jdbc.Driver"/>
                <property name="url" value="jdbc:mysql://localhost:3306/Mybatis_test"/>
                <property name="username" value="root"/>
                <property name="password" value="123456"/>
            </dataSource>
        </environment>
    </environments>
    <mappers>
        <mapper resource="com/dao/UserMapper.xml"/>
    </mappers>
</configuration>
```

2.工具类，获得SqlSession

```sql
package com.utils;
import org.apache.ibatis.io.Resources;

public class MybatisUtils
{
    private static SqlSessionFactory sqlSessionFactory;

    static
    {
        try
        {
            String resource = "mybatis.xml";
            InputStream inputStream = Resources.getResourceAsStream(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            e.getMessage();
        }
    }
    
    public static SqlSession getSqlSession(){
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        return sqlSession;
    }
}

```

3.UserDao接口

```java
public interface UserDao
{
    List<User> getUserList();
}
```

4.UserMapper

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.dao.UserDao">
    <select id="getUserList" resultType="com.pojo.User">
        select * from Mybatis_test.user;
    </select>
</mapper>
```

5.测试类

```java
public class MyTest
{
    @Test
    public void test_01(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        List<User> userList = mapper.getUserList();
        System.out.println(userList.toString());
    }
}
```

 SqlSession用途:根据接口，获得对应的Mapper

补充，Maven容易将配置文件过滤，需要手动关闭过滤：

```xml
<build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>false</filtering>
            </resource>
            <resource>
                <directory>src/main/java</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>false</filtering>
            </resource>
        </resources>
</build>
```

## 增删改查

UserMapper

```java
public interface UserMapper
{
    int insertUser(User user);
    int deleteUser(int id);
    int updateUser(User user);
    String getUser(int id);
    List<User> getUserList();
}
```

UserMapper.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.dao.UserMapper">
    <insert id="insertUser" parameterType="com.pojo.User">
        insert into Mybatis_test.user
        values(#{id},#{name},#{password});
    </insert>

    <delete id="deleteUser" parameterType="int">
        delete from Mybatis_test.user where id = #{id};
    </delete>

    <update id="updateUser" parameterType="com.pojo.User">
        Update Mybatis_test.user
        set name = #{name}
        where id = #{id};
    </update>

    <select id="getUser" resultType="String">
        select * from Mybatis_test.user where id = #{id};
    </select>

    <select id="getUserList" resultType="com.pojo.User">
        select * from Mybatis_test.user;
    </select>
</mapper>
```



## Map的使用+模糊查询

如果实体类中的参数过多，实例化一个对象需要配置的参数太多，就可以用Map，只需要配置特定的参数，因为Map的Key值可以随意定义

```java
List<User> getUserByLike(Map<String,Object> value);
```

```xml
<select id="getUserByLike" resultType="com.pojo.User">
    select * from Mybatis_test.user where name like "%"#{likeValue}"%";
</select>
```

```java
@Test
public void test002(){
    SqlSession sqlsession = MybatisUtils.getSqlsession();
    UserMapper mapper = sqlsession.getMapper(UserMapper.class);
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("likeValue","lby");
    List<User> userByLike = mapper.getUserByLike(map);
    for(User user : userByLike){
        System.out.println(user.toString());
    }
}
```

## 配置解析

核心配置文件：Mybatis_config.xm

properties，属性可以是外部配置且可动态替换的

```xml
<properties resource="db.properties"></properties>
```

别名：

```xml
<typeAliases>
    <typeAlias type="com.pojo.User" alias="User"></typeAlias>	//第一种方式
    <package name="com.pojo"/>		//第二种方式
</typeAliases>
```

第一种可以DIY

第二种可以扫描整个包，默认是类名的小写字母作为alias，但可以通过注解DIY`@Alias("user")`

## 生命周期

`SqlSessionFactoryBuilder`一旦创建`Factory`就不需要它了

`SqlSessionFactory`一旦创建，就一直存在，相当于⼀个数据库连接池，每次创建 SqlSessionFactory都会使⽤数据库资源，多次创建和销毁是对资源的浪费。所以 SqlSessionFactory是应⽤级的⽣命周期，⽽且应该是单例的

`SqlSession`相当于JDBC中的Connection，请求开启、请求关闭，用完之后直接关闭

![Screen Shot 2022-07-17 at 9.49.44 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-17 at 9.49.44 PM.png)



## 传参方式

### JavaBean传参

```java
public User selectUser(User user);

<select id="selectUser" parameterType="com.jourwon.pojo.User"> 
  select * from user where user_name = #{userName} and dept_id = #{deptId} 
</select>
```

### Map传参

```java
public User selectUser(Map<String, Object> params);

<select id="selectUser" parameterType="java.util.Map"> 
  select * from user where user_name = #{userName} and dept_id = #{deptId}
</select>
```

### @Param注解传参法

```java
public User selectUser(@Param("userName") String name, int @Param("deptId") deptId);

<select id="selectUser">
	select * from user where user_name = #{userName} and dept_id = #{deptId} 
</select>
```



## ResultMap—属性名和字段名不一致

**解决方法：**

方式一：通过在查询的SQL语句中定义字段名的别名，让字段名的别名和实体类的属性名

⼀致

```java
<select id="getOrder" parameterType="int" resultType="com.jourwon.pojo.Order"> 
  select order_id id, order_no orderno ,order_price price 
  from orders 
  where order_id=#{id}; 
</select>
```

方式二：通过resultMap—结果集映射，映射字段名和实体类属性名的⼀⼀对应的关系

```xml
//当数据库的字段名和对象的属性名不一致时，可以用resultMap映射
<resultMap id="UserMap" type="User">
    <result column="pwd" property="password"/>
</resultMap>

<select id="getUserByLike" resultMap="UserMap">
    ...
</select>
```

## log配置

默认log工厂：

```xml
<settings>
    <setting name="logImpl" value="STDOUT_LOGGING"/>
</settings>
```

log4j:

```xml
<dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>1.2.17</version>
</dependency>
```

properties:

```xml
### set log levels ###
log4j.rootLogger = DEBUG,console,file

### 输出到控制台 ###
log4j.appender.console = org.apache.log4j.ConsoleAppender
log4j.appender.console.Target = System.out
log4j.appender.console.Threshold = DEBUG
log4j.appender.console.layout = org.apache.log4j.PatternLayout
log4j.appender.console.layout.ConversionPattern = [%c]-%m%n

### 输出到日志文件 ###
log4j.appender.file=org.apache.log4j.RollingFileAppender
log4j.appender.file.File=./log/hou.log
log4j.appender.file.MaxFileSize=10mb 
log4j.appender.file.Threshold=DEBUG 
log4j.appender.file.layout=org.apache.log4j.PatternLayout
log4j.appender.file.layout.ConversionPattern=[%p][%d{yy-MM-dd}][%c]%m%n

# 日志输出级别
log4j.logger.org.mybatis=DEBUG
log4j.logger.java.sql=DEBUG
log4j.logger.java.sql.Statement=DEBUG
log4j.logger.java.sql.ResultSet=DEBUG
log4j.logger.java.sql.PreparedStatement=DEBUG
```

xml:

```xml
<settings>
    <setting name="logImpl" value="LOG4J"/>
</settings>
```



## 注解开发

面向接口编程：解耦

```java
@Select("select * from Mybatis_test.user")
List<User> getUserList();
```

还要在xml里配置接口



## Mybatis执行流程

Resource获取全局配置文件

实例化builder

解析配置文件xml

实例化SqlsessionFactory

事务管理

执行器

创建Sqlsession

实现CRUD

提交事务



## 多对一处理，联表查询



![Screen Shot 2022-07-17 at 10.04.18 PM](/Users/xiaobing/Library/Application Support/typora-user-images/Screen Shot 2022-07-17 at 10.04.18 PM.png)student

```java
public class Student
{
    private int id;
    private String name;
    private Teacher teacher;
}
```

teacher

```java
public class Teacher
{
    private int id;
    private String name;
}
```

思路：

	1、查询所有的学生
	
	2、根据查询出来的学生的tid，寻找对应的老师

```xml
//方法一
<mapper namespace="com.dao.StudentMapper">
    <select id="getAllStudent" resultMap="studentteacher">
        select * from Mybatis_test.student s;
    </select>

    <resultMap id="studentteacher" type="student">
        <result property="id" column="id"/>
        <result property="name" column="name"/>
        <association property="teacher" column="tid" javaType="Teacher" select="getAllTeacher"/>
    </resultMap>

    <select id="getAllTeacher" resultType="teacher">
        select * from Mybatis_test.teacher where id = #{id};
    </select>
</mapper>

//方法二
<mapper namespace="com.dao.StudentMapper">
    <select id="getAllStudent" resultMap="studentteacher">
        select s.id,s.name,t.name
        from Mybatis_test.student s, Mybatis_test.teacher t
        where s.tid = t.id;
    </select>

    <resultMap id="studentteacher" type="Student">
        <result property="id" column="id"/>
        <result property="name" column="name"/>
        <association property="teacher" javaType="Teacher">
            <result property="name" column="name"/>
        </association>
    </resultMap>
</mapper>
```

## 一对多

一个老师拥有多个学生

**按照结果处理查询**

```xml
<mapper namespace="com.dao.TeacherMapper">
    <select id="getAllTeacher" resultMap="getAllStudent">
                select s.id sid,s.name sname,t.name tname,t.id
                from Mybatis_test.student s, Mybatis_test.teacher t
                where s.tid = t.id and t.id=#{id};
    </select>
    
    <resultMap id="getAllStudent" type="Teacher">
        <result property="id" column="t.id"/>
        <result property="name" column="tname"/>
        <collection property="students" ofType="Student">
            <result property="id" column="sid"/>
            <result property="name" column="sname"/>
            <result property="tid" column="tid"/>
        </collection>
    </resultMap>
</mapper>
```

javaType：指定实体类中属性的类型

ofType：用来映射范型（generic）中的约束的类型



## 动态SQL

根据不同的条件，生出不同的sql语句

```xml
    <settings>
        <setting name="mapUnderscoreToCamelCase" value="true"/>
    </settings>
//将数据库字段名的下划线换成驼峰命名
```

```xml
<select id="findActiveBlogLike"
     resultType="Blog">
  SELECT * FROM BLOG
  <where>
    <if test="state != null">
         state = #{state}
    </if>
    <if test="title != null">
        AND title like #{title}
    </if>
    <if test="author != null and author.name != null">
        AND author_name like #{author.name}
    </if>
  </where>
</select>
```

类似于switch

```xml
<select id="findActiveBlogLike"
     resultType="Blog">
  SELECT * FROM BLOG WHERE state = ‘ACTIVE’
  <choose>
    <when test="title != null">
      AND title like #{title}
    </when>
    <when test="author != null and author.name != null">
      AND author_name like #{author.name}
    </when>
    <otherwise>
      AND featured = 1
    </otherwise>
  </choose>
</select>
```

*set* 元素可以用于动态包含需要更新的列，忽略其它不更新的列。比如：

```xml
<update id="updateAuthorIfNecessary">
  update Author
    <set>
      <if test="username != null">username=#{username},</if>
      <if test="password != null">password=#{password},</if>
      <if test="email != null">email=#{email},</if>
      <if test="bio != null">bio=#{bio}</if>
    </set>
  where id=#{id}
</update>
```

Foreach（用来批量处理，比如插入）

```xml
<select id="selectPostIn" resultType="domain.blog.Post">
  SELECT *
  FROM POST P
  WHERE ID in
  <foreach item="item" index="index" collection="list"
      open="(" separator="," close=")">
        #{item}
  </foreach>
</select>
```



## 缓存

一级缓存和二级缓存

默认开始一级缓存，存储作⽤域为SqlSession

select会加载缓存

update、delete、insert会更新缓存

```java
sqlSession.clearCache();//手动清理缓存
```



二级缓存需要手动开启和配置

机制：一个Sqlsession查询一条数据，数据放到当前会话的一级缓存中。当前会话关闭，一级缓存失效，将数据保存到二级缓存中。新的会话查询信息，从二级缓存中获取内容。

1.开启全局缓存

```xml
<setting name="cacheEnabled" value="true"></setting>
```

2.在mapper.xml里配置cache

```xml
<cache
  eviction="FIFO"
  flushInterval="60000"
  size="512"
  readOnly="true"/>
```

第一次查询，先查二级缓存，再查一级缓存



## **#{}和${}的区别是什么？**

#{} 是预编译处理，${}是字符串替换

Mybatis在处理#{}时，#{}传⼊参数是以字符串传⼊，会将sql中的#{}替换为?号，调用PreparedStatement的set方法来赋值；

Mybatis在处理¥{}时，就是把${}替换成变量的值

使用#{}可以有效的防止SQL注入，提高系统安全性。

## 模糊查询like语句该怎么写？

- "%"#{question}"%" 注意：因为#{…}解析成sql语句时候，会在变量外侧⾃动加单引号’ '，所以这⾥ % 需要使⽤双引号" "，不能使⽤单引号 ’ '，不然会查不到任何结果
- CONCAT(’%’,#{question},’%’) 使⽤CONCAT()函数，（推荐✨ ）

```mysql
select * from user where name like concat(“%”, #{name},”%”)
```

- 在Java代码中添加sql通配符。

  
