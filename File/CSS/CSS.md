# 选择器

## 标签选择器

```css
label {
    color: red;
}
```

标签选择器最大的优势是能快速为页面中同类型的标签统一样式，同时这也是它的缺点，不能设计差异化样式



## 类选择器 

```css
.类名 {
}


```

### 多类名的使用

使用场景：对CSS进行封装，方便复用

```html
<style>
  .red {
    color: red;
  }
  .font20 {
    font-size: 200px;
  }
</style>
<div class="red font20">
  lby
</div>
```

![截屏2022-08-29 下午9.31.32](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-08-29 下午9.31.32.png)



## id选择器

`id`是唯一的，id只能调用一次

id选择器和类选择器的区别是，使用次数上

```css
#id {
}
```



## 通配符选择器

*，选择页面中所有元素标签

通常用于清除所有元素标签的内外边距

```css
* {
}
```



# 字体属性

标题标签比较特殊，需要单独指定文字大小

```css
body {
  font-family: "Comic Sans MS", "Times New Roman";	//字体
  font-size: 32px;		//文字大小
  font-weight: bold;	//font-weight: 100; 粗细
  font-style: italic;		//一般用normal，让<em>的字体不倾斜
}
```



# 文本属性

`test-decoration: none`最常用，用于删除下划线

```css
div {
  color: red;
  text-align: center;
  text-decoration: line-through; 	//none无线, line-through删除线，underline下划线
  text-indent: 2em;; //缩进
  line-height: 1.5em; //行间距  如果line-height=盒子高度，文字就可以垂直居中
}
```

em是相对单位，当前元素一个文字的大小

<img src="/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-08-29 下午10.20.21.png" alt="截屏2022-08-29 下午10.20.21" style="zoom:50%;" />

# 复合选择器

## 后代选择器

`父元素 子元素`

父元素可以是任意选择器

`.nav li a`

```html
<style>
    .nav a{
        color: red;
    }
</style>

<div class="nav">
    <a href="#">我是儿子</a>
    <p>
        <a href="#">我是孙子</a>
    </p>
</div>
```

nav所有的子元素a都变成了红色

<img src="/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-08-30 上午1.55.41.png" alt="截屏2022-08-30 上午1.55.41" style="zoom:50%;" />

## 子选择器

选择某元素的最近一级子元素

`父元素>子元素`

```html
<style>
    .nav>a{
        color: red;
    }
</style>

<div class="nav">
    <a href="#">我是儿子</a>
    <p>
        <a href="#">我是孙子</a>
    </p>
</div>
```

只有第一个子元素a变成红色

<img src="/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-08-30 上午1.56.52.png" alt="截屏2022-08-30 上午1.56.52" style="zoom:50%;" />

## 并集选择器

并集的选择器用逗号分隔

.nav > a，选择了儿子和孙子

p a，选择了中间

```html
<style>
    .nav > a, p a {
        color: red;
    }
</style>

<div class="nav">
    <a href="#">我是儿子</a><br>
    <p>
        <a href="#">我是中间</a>
    </p>
    <a href="#">我是孙子</a>
</div>
```

## 伪类选择器

### 链接

`a:link`：未访问的链接

`a:visited`：访问过的链接

`a:hover`：鼠标悬停的链接

`a:active`：鼠标点击后未松开的链接

```css
a {
    text-decoration: none;
}
a:link {
    color: aqua;
}
a:visited{
    color: red;
}
a:hover{
    color: blueviolet;
}
a:active{
    color: darkgreen;
}
```

### :focus 

focus表示有光标焦点，用于input

```html
<style>
    input:focus {
        background-color: red;
    }
</style>

<input type="text">
```

![截屏2022-08-30 上午2.14.00](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-08-30 上午2.14.00.png)





# 元素显示模式

## 块元素

- 独占一行
- 高、宽、外边距和内边距都可以控制
- 宽度默认是容器的100%
- 是一个容器，里面可以放行内元素和块级元素

**`<h1>~<h6> <p> <div> <ul> <ol> <li>`**



## 行内元素

- 一行可以显示多个
- 高、宽直接设置是无效的
- 默认宽度是内容本身的宽度
- 行内元素只能容纳文本和其他行内元素

`<span>`



## 行内块元素

`<img> <input> <td>`，同时具备块元素和行元素的特点

- 一行可以显示多个，**但有空白缝隙**
- 默认宽度就是内容的宽度
- 高、宽、外边距和内边距都可以控制

![IMG_236CE07F99A8-1](/Users/Typora_Pictures/:Users:xiaobing:Downloads:IMG_236CE07F99A8-1.png)

## 显示模式转换

`display:block`

`display:inline`

`display:inline-block`

```html
<style>
    a {
        display: block;
        background-color: red;
        height: 10em;
        width: 10em;
    }
</style>

<a href="#">test</a>
```

![截屏2022-08-30 下午8.17.26](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-08-30 下午8.17.26.png)

# 背景

```css
background-color:
background-image: url("截屏2022-08-30 下午8.39.27.png");  //默认背景平铺
background-repeat: repeat-x;		//平铺
background-position: left top; //背景图片，方位名词：x y  left right center top bottom 
background-position: 30px top; //背景图片，精确位置
backgroud-attachment:fixed		//背景附着（背景不移动）
background: rgba(0,0,0,1%);	//背景颜色半透明
```



# 优先级

行内样式

id选择器

类选择器

元素选择器

继承



**复合选择器有权重叠加**



# 盒子模型

![IMG_479849F5946D-1](/Users/Typora_Pictures/:Users:xiaobing:Downloads:IMG_479849F5946D-1.png)

## 边框

```css
border-color: black;
border-width: 3px;
border-style: dashed;	//solid实线边框, dashed虚线
border-left:red 3px dashed;		//简写写法，只设置一边的边框

	表格的边框
table {
    border: 2px black solid;
    border-spacing: 0;

}
th,td{
    border: 1px black solid;
    padding: 2em;
    border-collapse: collapse;
}
```

![截屏2022-08-31 上午12.20.41](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-08-31 上午12.20.41.png)

## padding

```css
padding:1个值 //内边距	统一设置上右下左
padding:2个值 //内边距	上下、左右
padding:4个值 //内边距	分别设置上右下左

padding会撑大盒子
但如果没有指定width和height值，padding不会自动撑大盒子
```



## margin

```css
margin用法和padding类似
margin:10px auto;

清除内外边距
*{
  padding:0;
  margin:0;
}

行内元素尽量只设置左右内外边距
```



# **水平居中**

## 块级元素

盒子必须设置宽度

`margin`属性设置`auto`

## 行内元素和行内块元素

给父元素设置`text-align: center`





# 盒子模型外边距塌陷



![IMG_A91F12972AC1-1](/Users/Typora_Pictures/:Users:xiaobing:Downloads:IMG_A91F12972AC1-1.png)

![IMG_21ECDD22CC1F-1](/Users/Typora_Pictures/:Users:xiaobing:Downloads:IMG_21ECDD22CC1F-1.png)



# 圆角矩形

![IMG_B1FE0083661F-1](/Users/Typora_Pictures/:Users:xiaobing:Downloads:IMG_B1FE0083661F-1.png)

# 盒子阴影

![IMG_86DC174B9216-1](/Users/Typora_Pictures/:Users:xiaobing:Downloads:IMG_86DC174B9216-1.png)



# 浮动

多个元素横向排列

`float:` `none`,`left`,`right`

## 浮动特性：

- 浮动元素会脱离标准流
- 浮动元素会一行内显示，并且元素顶部对齐
- 浮动元素具有行内块元素的特性



## 和标准流父级搭配使用

- 用标准流的父元素排列上下
- 内部子元素采用浮动排列左右位置

```css
<ul class="container">
    <li><img src="ai.png"></li>
    <li><img src="ai.png"></li>
    <li><img src="ai.png"></li>
    <li class="lastLi"><img src="ai.png"></li>
</ul>

/*去除所有元素的padding和margin*/
* {
    margin: 0;
    padding: 0;
}

.container {
    width: 1200px;
    height: 400px;
    background-color: red;
    /*父元素居中*/
    margin: 0 auto;
}

img {
    width: 296px;
    height: 400px;
}

.container li {
    /*去除小圆点*/
    list-style: none;
    float: left;
    width: 296px;
    height: 400px;
    margin-right: 4px;
}

/*最好一个li不需要右外边距，手动清除，注意权重*/
.container li.lastLi {
    margin-right: 0;
}
```

![截屏2022-09-01 下午2.12.14](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-09-01 下午2.15.23.png)

## 浮动元素影响后面的标准流，不影响前面的标准流

![截屏2022-09-01 下午2.41.52](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-09-01 下午2.41.52.png)

粉色盒子：浮动

绿色盒子：不浮动

橙色盒子：浮动

绿色盒子被浮动元素有影响，它被粉色压住。**但橙色盒子不被影响，沿着绿色盒子浮动**

## 清除浮动

### 为什么清除浮动

父元素只给宽度，不方便给高度，希望有多少个子元素，父元素就变多大。

如果子盒子浮动，它就会脱离文档流，父元素就不拥有子元素了，它的高度就会变成0，黑色的footer就会上升

<img src="/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-09-01 下午3.07.49.png" alt="截屏2022-09-01 下午3.07.49" style="zoom:50%;" />

清除元素的本质：清除浮动元素造成的影响。清除浮动之后，**父级元素会根据浮动的子元素自动检测高度。父级有了高度之后，就不会影响下面的标准流了。**

`clear:both; left,right,both`

## 如何清除浮动

1. 额外标签法
2. 父级添加overflow属性
3. 父级添加after伪元素
4. 父级添加双伪元素

### 额外标签法

![截屏2022-09-01 下午3.44.06](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-09-01 下午3.44.06.png)

![截屏2022-09-01 下午3.44.29](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-09-01 下午3.44.29.png)

### 父级添加overflow属性

**`overflow:hidden;`**



### 父级添加after伪元素

```css
.clear:after{
  content:"";
  display:block;
  height:0;
  clear:both;
  visibility:hidden;
}
```



# 图片格式

![截屏2022-09-01 下午3.53.40](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-09-01 下午3.53.40.png)

# CSS属性书写顺序

![截屏2022-09-01 下午3.55.49](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-09-01 下午3.55.49.png)

# 案例

## 导航栏制作

<img src="/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-09-01 下午4.09.07.png" alt="截屏2022-09-01 下午4.09.07" style="zoom:200%;" />

- 所有元素都要有浮动
- 导航栏必须用`li>a`
- 每个`li`使用`display:block`
- 每个`li`里的文字居中使用`pading:0 10px;`
- 每个`li`使用`height:100%`

```css
.toolbar-container {
  width: 100%;
  /**IE浏览器默认的盒子属性值
		 border-box的width和height从border算起,包含border和padding
  */
  box-sizing: border-box;
  padding: 0 24px;
  height: 48px;
  /*行高*/
  line-height: 48px;
  /*居中显示*/  
  margin: 0 auto;
}  

猿如意{
  text-decoration: none;
	display: block;
	height: 100%;
	color: inherit;
	padding: 0 10px;
}
```

![截屏2022-09-01 下午10.54.08](/Users/xiaobing/Library/Application Support/typora-user-images/截屏2022-09-01 下午10.54.08.png)

搜索框使用`input`和`button`，两者都为行内块元素，中间有空白缝隙。可以**使用浮动清除空隙**



做出透明效果：

`background:rgba(0,0,0,0.3);`



**浮动的盒子没有外边距合并问题**



footer使用`dl dt dd`



# 定位

浮动：让多个块级盒子一行没有缝隙显示，横向排列盒子

定位：让盒子自由地在某个盒子内移动，或者固定在某个位置，并且可以压住其他盒子

**定位=定位模式+边偏移**

定位模式：指定一个元素在文档中的定位方式

边偏移：决定该元素的最终位置



## 边偏移

`top`,`bottom`,`left`,`right`

## position

`static:`静态定位

`relative:`相对定位

`absolute:`绝对定位

`fixed:`固定定位

### `static（了解）`

默认定位方式，无定位

### `relative`

相对于**原来的位置**移动

在文档流中依然**保留位置**

`position: relative;`

典型场景：给绝对定位当爹

### `absolute`

相对于**祖先元素的位置**移动

如果**没有祖先元素/祖先元素没有定位**，则以浏览器为准进行定位

绝对定位**脱离文档流**

==子元素使用绝对定位，父元素必须使用相对定位==



### `fixed`

固定在**浏览器**中的某个位置

**脱离文档流**

==固定在版心右侧位置==

`position:fixed`元素若要以窗口进行定位，最好是放在`body`根标签下



### `sticky`

粘性定位，可以认为是相对定位和固定定位的混合

以可视窗口为参照点

不脱离文档流

必须添加top、left、right、bottom其中一个才有效



## 定位布局的叠放次序 z-index

`z-index:1;`

数值越大，盒子越向上



