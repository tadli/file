#英语

***Don't***

- Don't worry
- Don't move
- Don't be lazy
- Don't stand here
- Don't intrude your own views upon others



#英语

***Let's not***

- Let's not waste time
- Let's not risk it in this way



#英语

***What a***

- What an enthusiastic welcome we received
- What a good couple they are



#英语

***How***

- How happy I am to meet you here



#英语

***How do you like***

- How do you like working in a foreign company?
- How do you like your room?



#英语

***Why do you think***

- Why do you think I am wrong?
- Why do you think I am unqualified for the job?
- Why do you think you can persuade him to leave?



#英语

***Thank you for***

- Thank you for your kindness
- Thank you for your being in my life
- Thank you for everything you've done for me



#英语

***I'm sorry***

- I'm sorry to bother you a second time
- I'm sorry to have broken your glass



#英语

***I'm afraid***

- I'm afraid I can't agree with you



#英语

***Could I possibly ask you to*** 

- Could I possibly ask you to give me a ride?<!--载我一路-->

- Could I possibly ask you to carry the luggage for me?



#英语

***I sincerely hope***

- I sincerely hope you can be together all life long
- I sincerely hope your grandma can get well soon



#英语

***how about 和what about的区别***

how about 和what about都是用于提出建议或询问对方对某个建议的看法。但是,在具体用法上有一定的区别:
1. how about 的结构为“how about + 动词ing......”。它的语气通常比较客气,委婉地提出一个建议,征求对方的看法。例如:
  How about going out for a walk?
  How about taking a walk after dinner？

  

2. what about的结构为“what about + 动名词或名词短语”。它的语气比较直截了当,提出一个选项让对方考虑和选择。例如:
  What about seeing that new action movie?
  What about Italian food for dinner? 



#英语

***It's my great honor to***

- It's my great honor to introduce our new chairman
- It's my great honor to have you in my life
- It's my great honor to be here on so special an occasion



#英语

***Would you please***

- Would you please lower your voice?
- Would you please give me a hand?
- Would you please keep the secret for me?
- Would you please explain a little more clearly?



#英语

***I'd like to***

- I'd like to talk about that matter

- I'd like to have my car repaired



#英语 

***May I***

- May I come in?
- May I help you?
- May I ask you a question?
- May I speak to Tad?
- May I have a seat?



#英语 

***Why is it***

- Why is it so difficult to find a job?
- Why is it that you never listen to me?
- Why is it so important to you? 
- Why is it always raining when I go on vacation?
- Why is it that people tend to raise their voice when arguing?



#英语 

***How come***

- How come you didn't show up last night?
- How come this package arrived opened?
- How come she looks so sad today? 
- How come I didn't get the job I wanted?
- How come it's snowing in April?



#英语 

***What's with***

- What's with the noise downstairs?	<!--楼下的噪音怎么回事?-->
- What's with all these emails? <!--这么多封邮件是怎么回事-->
- What's with the frown on your face? <!--你脸上的皱眉是怎么回事--> 
- What's with the change in plan?<!--计划突然改变是怎么回事-->
- What's with all the secrecy? <!--这么神秘是怎么回事-->



#英语 

***How could it be that***

- How could it be that you forgot? <!--你怎么可能会忘记-->
- How could it be that no one told me? <!--怎么可能没有人告诉我-->
- How could it be that I'm so unlucky? <!--我怎么可能这么不走运-->
- How could it be that you don't care? <!--你怎么可能不在乎?-->
- How could it be that I always mess up? <!--我怎么总是搞砸-->



#英语 

***Can you please*** 

- Can you please tell me where is the bus stop?



#英语 

***Would you like me to help you*** 

- Would you like me to help you carry the luggage?



#英语 

***What do you think***

- What do you think I should do?
- What do you think Mother will do to punish him?



#英语 

***When will it be convenient for you to***

- When will it be convenient for you to arrange an interview for me?
- When will it be convenient for me to come to your home?



#英语 

***When are you going to***

- When are you going to stop bothering me?
- When are you going to have your hair cut?



#英语 

***Forgive me for***

- Forgive me for my being rude that day
- Forgive me for leaving without telling you
- Forgive me for being late
- Forgive me for asking a personal question



#英语 

***Please don't hesitate to***

- Please don't hesitate to call me if you need help



#英语 

***Everybody says***

- Everybody says Tom is a lady killer



#英语 

***I suggest that***

- I suggest that you get rid of your bad habits
- I suggest that you spend more time reading



#英语 

***You'd better***

- You'd better not make such a silly mistake



#英语 

***In my opinion***

- In my opinion, you did the right thing



#英语 

***If I were you, I***

- If I were you, I would have given up



#英语 

***If I did, I would***

- If you had known the truth, I wouldn't have blamed him



#英语 

***I wish***

- I wish you had told me before



#英语 

***It's time*** 

- It's time we went
- It's time I started my work
- It's time you slept



#英语 

***What surprise me most is that***

- What surprise me most is that it was the kind old woman who had killed the 6 men



#英语 

***It's a pity that***

- It's a pity that you couldn't catch the train



#英语 

***That seems unlikely***

- That such a poor man could become a millionaire seems unlikely



#英语 

***I've been hoping that***

- I have been hoping that she would come back to me some day



#英语 

***There can be no doubt that***

- There can be no doubt that it is he who stole the money



#英语 
		A fool never learns.

#英语 
		Anything will do!

#英语 
		Are you pulling my leg?

#英语 
		Can you give me some feedback?

#英语 
		Come in and make yourself at home.

#英语 
		Cross my heart. (我发誓)

#英语 
		Do you have a room available?

#英语 
		Don't be so fussy.

#英语 
		Don't mention it. (别客气)

#英语 
		Don't take it for granted.



#英语 
		Don't you dare come back again!

#英语 
		Enjoy your trip.

#英语 
		Fill it up.

#英语 
		Free of charge.

#英语 
		Give him the works. (给他最严厉的惩罚)

#英语 
		Have you got anything larger?

#英语 
		He's all talk. (他只说不做)

#英语 
		How long will it take me to get there?

#英语 
		I can't seem to get to sleep.

#英语 
		I couldn't reach him.

#英语 
		I don't have the nerve to do it.

#英语 
		I got sick and tired of hotels.

#英语 
		I wasn't born yesterday.

#英语 
		I'll think it over.

#英语 
		I'm fed up.

#英语 
		I'm off now.

#英语 
		If I were in your shoes.

#英语 
		It really comes in handy. (这真的很方便"或"这真的派上用场了")

#英语 
It can't be helped.

#英语 
It won't work.

#英语 
It's been a long time.

#英语 
It's just what I had in mind.

#英语 
It's on me. (我请客)

#英语 
Let me get back to you. (让我回复你"或"让我与你联系")

#英语 
Let's get going.

#英语 
Make water.

#英语 
Maybe some other time. (改天)

#英语 
Same old story.

#英语 
She is sick in bed.

#英语 
That rings a bell. ("这听起来耳熟"或"这唤起了我的记忆")

#英语 
That's a touchy issue!

#英语 
That's life.

#英语 
There is nothing on your business.

#英语 
This for sale?

#英语 
What a mess!

#英语 
What brings you to Beijing?

#英语 
What for?

#英语 
What's bothering you?

#英语 
Who is to blame?

#英语 
You are just saying that.

#英语 
You look very serious about something.

#英语 
You never tell the truth.

#英语 
You'll be sorry.

#英语 
You're going too far!